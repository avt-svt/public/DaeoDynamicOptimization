#include <iostream>
#include "FC.h"

#define MPBNGC FC_GLOBAL(mpbngc,MPBNGC)

extern "C" void MPBNGC(int& N,double *X,int *IX,double *BL,
                       double *BU,int& M,int& MG,int& MC,int *IC,
                       double *CL,double *CU, double *CG, double *F,
                       void (*FASG)(int&, double*, int&, double*, double*,int&, int*, double*),
                       double & RL,int& LMAX,
                       double *GAM,double& EPS,
                       double& FEAS, int& JMAX,int& NITER,
                       int& NFASG,int& NOUT, int& IPRINT, int& IERR,
                       int *IWORK, int& LIWORK,double *WORK, int& LWORK,
                       int* IUSER,double *USER);

extern "C" void FASG(int& N, double* X, int& M, double* F, double * G, int& IERR, int* IUSER, double* USER);


int main()
{
    const int NN   = 2;
    int N = NN;
    const int JJMAX   = 10;
    int JMAX = JJMAX;
    const int MM      = 1;
    int M = MM;
    const int MMC     = 0;
    int MC = MMC;
    const int MMG     = 0;
    int MG = MMG;
    const int MP1    = MM;
    int NOUT   = 6;
    int IPRINT = 3;
    const int LLIWORK = 2*(MP1*(JJMAX+1)+MMC+NN);
    int LIWORK = LLIWORK;
    const int LLWORK  = NN*(NN+2*MP1*JJMAX+2*MP1+2*MMC+2*MMG+2*MM+31)/2+MP1*(6*JJMAX+10)+JJMAX+5*MMC+MMG+MM+18;
    int LWORK = LLWORK;

    int NITER,NFASG,IERR,I,LMAX;
    int IX[NN],IC[MMC+1],IWORK[LLIWORK],IUSER[1];

    double  RL,EPS,FEAS;
    double  X[NN],F[MM+MMG],GAM[MP1],BL[NN],BU[NN];
    double  CG[NN*(MMC+1)],CL[MMC+1],CU[MMC+1],WORK[LLWORK],USER[1];

    NITER =  1000;
    NFASG =  1000;

    IX[0] =  0;
    IX[1] =  0;

    IC[0] =  0;

    X[0]  = -1.2;
    X[1]  =  1.0;

    RL=0.01;
    LMAX=100;
    GAM[0]=0.5;

    EPS=1.0E-05;
    FEAS=1.0E-09;


    MPBNGC(N,X,IX,BL,BU,
           M,MG,MC,IC,CL,
           CU,CG,F,FASG,RL,
           LMAX,GAM,EPS,FEAS,JMAX,
           NITER,NFASG,NOUT,IPRINT,IERR,
           IWORK,LIWORK,WORK,LWORK,IUSER,USER);

    std::cout << "\n" << "IERR = " << IERR << "\n" << std::endl;
    for(int i = 0; i<N;++i) {
        std::cout << "X(" << i+1 << ") = " << X[i] << std::endl;
    }
    std::cout << "\n" << "F(X) = " << F[0] << std::endl;
    std::cout << "NITER = " << NITER << std::endl;
    std::cout << "NFASG = " << NFASG << std::endl;


    return 0;
}

void FASG(int& N, double* X, int& M, double* F, double * G, int& IERR, int* IUSER, double* USER)
{
    F[0] = 100*(X[1]-X[0]*X[0])*(X[1]-X[0]*X[0])+(1-X[0])*(1-X[0]);
    G[0]=-400*X[0]*(X[1]-X[0]*X[0])-2*(1-X[0]);
    G[1]=200*(X[1]-X[0]*X[0]);
}
