#ifndef A1_FCN_HPP
#define A1_FCN_HPP

#if defined(__APPLE__) || defined(__linux__)
  #include <sys/time.h>
#endif

#include "ad/ad.hpp"

#define AD_A1_REAL double
#define AD_T1_REAL AD_A1_REAL
#define AD_A1_CREATE if(!ad::ga1s<AD_A1_REAL>::global_tape) ad::ga1s<AD_A1_REAL>::global_tape = ad::ga1s<AD_A1_REAL>::tape_t::create();
#define AD_A1_RESET ad::ga1s<AD_A1_REAL>::global_tape->reset();
//#define AD_A1_REMOVE ad::ga1s<AD_A1_REAL>::tape_t::remove(ad::ga1s<AD_A1_REAL>::global_tape);

#define AD_A1_DEC1(name) \
  ad::ga1s<AD_A1_REAL>::type ad_##name; \
  ad::value(ad_##name) = name; \
  ad::derivative(ad_##name) = 0.0; \
  ad::ga1s<AD_A1_REAL>::global_tape->register_variable(ad_##name);


#define AD_T1_DEC1(name) \
  ad::gt1s<AD_T1_REAL>::type ad_##name; \
  ad::value(ad_##name) = name; \
  ad::derivative(ad_##name) = 0.0;

#define AD_A1_DEC2(name, size) \
  ad::ga1s<AD_A1_REAL>::type *ad_##name = new ad::ga1s<AD_A1_REAL>::type[size]; \
  for (int i = 0; i < size; ++i) { \
    ad::value(ad_##name[i]) = name[i]; \
    ad::derivative(ad_##name[i]) = 0.0; \
    ad::ga1s<AD_A1_REAL>::global_tape->register_variable(ad_##name[i]); \
  }



#define AD_A1_DEC3(name, size1, size2) \
  ad::ga1s<AD_A1_REAL>::type **ad_##name = new ad::ga1s<AD_A1_REAL>::type*[size2]; \
  for (int i = 0; i < size2; ++i) { \
    ad_##name[i] = new ad::ga1s<AD_A1_REAL>::type[size1]; \
    for (int j = 0; j < size1; ++j) { \
      ad::value(ad_##name[i][j]) = name[i][j]; \
      ad::derivative(ad_##name[i][j]) = 0.0; \
      ad::ga1s<AD_A1_REAL>::global_tape->register_variable(ad_##name[i][j]); \
    } \
  }

#define AD_A1_INT1(name) \
  ad::ga1s<AD_A1_REAL>::global_tape->register_output_variable(ad_##name); \
  ad::derivative(ad_##name) = a1_##name; \
  ad::ga1s<AD_A1_REAL>::global_tape->interpret_adjoint();

#define AD_A1_INT2(name, size) \
  for (int i = 0; i < size; ++i) { \
    ad::ga1s<AD_A1_REAL>::global_tape->register_output_variable(ad_##name[i]); \
    ad::derivative(ad_##name[i]) = a1_##name[i]; \
  } \
  ad::ga1s<AD_A1_REAL>::global_tape->interpret_adjoint();

#define AD_A1_GET1(name) \
  a1_##name = ad::derivative(ad_##name);

#define AD_A1_GET2(name, size) \
  for (int i = 0; i < size; ++i) { \
    a1_##name[i] = ad::derivative(ad_##name[i]); \
  } \
  delete [] ad_##name;

#define AD_A1_GET3(name, size1, size2) \
  for (int i = 0; i < size2; ++i) { \
    for (int j = 0; j < size1; ++j) { \
      a1_##name[i][j] = ad::derivative(ad_##name[i][j]); \
    } \
    delete [] ad_##name[i]; \
  } \
  delete [] ad_##name;


#endif // A1_FCN_HPP
