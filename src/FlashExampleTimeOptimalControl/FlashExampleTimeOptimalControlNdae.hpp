#ifndef FLASH_EXAMPLE_NDAE_HPP
#define FLASH_EXAMPLE_NDAE_HPP
#include <iostream>
#include "a1_Fcn.hpp"

using namespace std;

#include "Ndae.hpp"
#include <math.h>
//#include <cmath>
enum GEModel { NRTL, Margules };

template<typename Real>
class FlashExampleNewThermoNdae : public Ndae<Real>
{
public:
  FlashExampleNewThermoNdae() {
    p_0[0] = 8000.0;
    mode0[0] = vfrac(y_0) > 1e-6;
    mode0[1] = vfrac(y_0) < 0.99999;

    H_0 = M_L(y_0) * calc_hL(T_0, Pressure(x_0, y_0, 0), x_l_0);
    x_0[0] = H_0;
    //setObjMode(ObjectiveMode::ComputeConstraint);
  }

  virtual ~FlashExampleNewThermoNdae() {}

  unsigned Eval_n_p() const override ;
  unsigned Eval_n_x() const override;
  unsigned Eval_n_y() const override;
  unsigned Eval_n_Theta() const override;
  unsigned Eval_n_sigma() const override;
  Real Eval_t0() const override;
  void Eval_Theta(Real *Theta) const override;


  std::vector<int> EvalMode(Real time, Real *x, Real *y, Real *p, const std::vector<int>& oldMode) const override;
  std::vector<int> EvalRootDirection(const std::vector<int>& mode) const override;
  void EvalInitialGuess_y(Real *y) const override;
  std::vector<int> EvalInitialMode() const override;

  template<typename T>
  void Eval_f_impl(T *f, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const;
  virtual void Eval_f(Real *f, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const override;

  //template<typename T>
  //void Eval_g_impl(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const;
  template<typename T>
  void Eval_g_impl_v2(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const;
  template<typename T>
  void Eval_g_impl_v3(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const;
  void Eval_g(Real *g, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const override;

  template<typename T>
  void Eval_g_VLE(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode, const int iTray) const;


  template<typename T>
  void Eval_sigma_impl(T* sigma, T& time,
                       T* x, T *y, T* p, const std::vector<int>& mode) const;
  void Eval_sigma(Real* sigma, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const override;

  template<typename T>
  void Eval_psi_impl(T *psi, T& time, T* x, T *y,
                     T* p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const;
  void Eval_psi(Real *psi, Real& time, Real *x, Real *y, Real *p,
                        const std::vector<int>& newMode, const std::vector<int>& oldMode) const override;

  template<typename T>
  void Eval_psi0_impl(T *psi0, T* p) const;
  void Eval_psi0(Real *psi0, Real *p) const override;

  template<typename T>
  void Eval_phi_impl(T& phi, T *Theta, T **X, T **Y, T *p) const;
  void Eval_phi(Real& phi, Real *Theta, Real **X, Real **Y, Real *p) const override;


  void Eval_a1_f(Real *f, Real *a1_f, Real& time, Real& a1_time, Real *x, Real *a1_x,
                         Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode) const override;

  void Eval_a1_g(Real *g, Real *a1_g, Real& time, Real& a1_time, Real *x, Real *a1_x,
                         Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode) const override;

  void Eval_a1_sigma(Real* sigma, Real* a1_sigma, Real& time, Real& a1_time, Real *x, Real *a1_x,
                             Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode) const override;

  void Eval_a1_psi(Real * psi, Real *a1_psi, Real& time, Real& a1_time, Real *x, Real *a1_x,
                           Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& newMode,
                           const std::vector<int>& oldMode) const override;

  void Eval_a1_psi0(Real *psi0, Real *a1_psi0, Real *p, Real* a1_p) const override;
  void Eval_a1_phi(Real& phi, Real& a1_phi, Real *Theta, Real *a1_Theta, Real **X, Real ** a1_X,
                           Real **Y, Real **a1_Y, Real *p, Real *a1_p) const override;

  void Eval_t1_f(Real *f, Real *t1_f, Real& time, Real& t1_time, Real *x, Real *t1_x,
                         Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const override;

  void Eval_t1_g(Real *g, Real *t1_g, Real& time, Real& t1_time, Real *x, Real *t1_x,
                         Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const override;

  void Eval_t1_sigma(Real* sigma, Real* t1_sigma, Real& time, Real& t1_time, Real *x, Real *t1_x,
                             Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const override;

  void Eval_t1_psi(Real * psi, Real *t1_psi, Real& time, Real& t1_time, Real *x, Real *t1_x,
                           Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& newMode,
                           const std::vector<int>& oldMode) const  override;

  void Eval_t1_psi0(Real *psi0, Real *t1_psi0, Real *p, Real* t1_p) const override;

  void Eval_t1_phi(Real& phi, Real& t1_phi, Real *Theta, Real *t1_Theta, Real **X, Real ** t1_X,
                           Real **Y, Real **t1_Y, Real *p, Real *t1_p) const override;


public:
  enum class ObjectiveMode { ComputeObjective, ComputeConstraint };
  ObjectiveMode objMode = ObjectiveMode::ComputeObjective;
  void setObjMode(ObjectiveMode mode) { objMode = mode; }
  static ObjectiveMode& objectiveMode() {
    static ObjectiveMode mode = ObjectiveMode::ComputeObjective;
    return mode;
  }

  static const int nC = 2;
  static const int n_comp = nC;
  static const int N_X = 3; // number of differential vars
  static const int N_Y = 2*nC+9; // number od algebraic vars
  static const int N_P = 1; // number of parameters
  static const int N_THETA = N_P; // numebr of THETA which is number of time points where the objective functional is evaluated
  static constexpr int N_S = 2; // number of switching functions
  static const int NTRAYVARS = 2 * nC + 2; // y_i,x_i,vfrac,beta
  int int_p = 0;

  //static const int i_s = 6 * n_comp + 1; // index of y to get s = y[i_s]

  // model parameters
  const double pressure = 101325.0; //pressure in Pa
  const double p_M = 1.0000; // total moles in mol
  const double p_M_i[nC] = { 0.5*p_M, 0.5*p_M }; // moles of component i in mol
  const double T_set = 345.0; //temperature set point in K
  const double Q_max = 5.0e3*p_M;
  const double scaling_p = 1.0;
  const double delta_t = 100.0;
  const double scale_final_deviation = 1.e5;
  double tf = 1.0;
  double t0 = 0.0;
  double deltat = (double)(tf / N_THETA);

  double H_0;
  double T_0 = 330; //initial temperature in K
  std::vector<double> x_l_0= std::vector<double>(nC, 1.0/double(nC));
  //int mode0[N_S] = { 1,1 };
  std::vector<int> mode0=std::vector<int>(N_S,1); // why is this not working?

  // thermodynamic parameters
  const GEModel gemodel = NRTL;
  const bool considerExcessEnthalpy = true;
  const double MargulesA0 = 8.3;
  const double MargulesA1 = 7.5;
  const double MargulesA2 = 7.0;

  const double MargulesA[nC][nC] = {};// = { { 0.0, 1.0*MargulesA0, 1.0*MargulesA2 },{ MargulesA0, 0.0, 1.0*MargulesA1 },{ MargulesA2, MargulesA1, 0.0 } };

  const double NRTLA1[nC][nC] = { { 3.00000e-01, 3.00000e-01 },
                                  { 3.00000e-01, 3.00000e-01 } };
  const double NRTLA2[nC][nC] = { { 0.0, 0.0 },
                                  { 0.0, 0.0 } };
  const double NRTLG1[nC][nC] = { { 0.0, 2.06597e+02 },
                                  { 4.79050e+02, 0.0 } };
  const double NRTLG2[nC][nC] = { { 0.0, -3.47100e-01 },
                                  { -1.07870, 0.0 } };
  const double NRTLG3[nC][nC] = { { 0.0, 0.0 },
                                  { 0.0, 0.0 } };
  const double NRTLG4[nC][nC] = { { 0.0, 0.0 },
                                  { 0.0, 0.0 } };
  const double RGAS = 1.0;

  const double ANT[nC][12] = {
    { 0.0, 1.0, 2.71828, 69.006, -5599.6, 0.0, 0.0, -7.0985, 6.2237e-06,
      2.0, 178.45, 508.2 },
    { 0.0, 1.0, 2.71828, 73.304, -7122.3, 0.0, 0.0, -7.1424, 2.8853e-06,
      2.0, 159.05, 514.0 } };
  const double t_Bezug = 298.15;
  const double KRIT[nC][4] = { { 508.2, 4701000.0, 0.209, 0.233 },
                               { 514.0, 6137000.0, 0.168, 0.241 } };
  const double DHFORM[nC] = { -215700000.0, -234950000.0 };
  const double CPIGDP[nC][7] = {
    { 57040.0, 163200.0, 1607.0, 96800.0, 731.5, 200.0, 1500.0 },
    { 49200.0, 145770.0, 1662.8, 93900.0, 744.7, 273.15, 1500.0 } };
  const double DHVLDP[nC][7] = {
    { 49258000.0, 1.0809, -1.3684, 0.69723, 0.0, 178.45, 508.2 },
    { 65831000.0, 1.1905, -1.7666, 1.0012, 0.0, 159.05, 514.0 } };
  const double R_alg = 8314.3;

  // initial guesses
  double y_0[N_Y] = { 0.554299, 0.445701, 0.302657, 0.697343,
                      p_M_i[nC], 0.000000e+00,	p_M, 1.1, T_0, Q_max/scaling_p };

  double x_0[N_X] = { -2.6e11,0,0 };// { -2.3e11,0 };

  double p_0[N_P];

  //void Eval_i_Theta(int itheta) {
  //	set_iP(itheta);
  //};

  // function to change iP
  void set_iP(int new_ip) {
    int_p = new_ip;
  }

  // function to change iP
  void set_tf(double new_tf) {
    tf = new_tf;
  }

  void set_t0(double new_t0) {
    t0 = new_t0;
  }

  void set_x_0(std::vector<double> x_0_new) {
    for (int i = 0; i < N_X; ++i) {
      x_0[i] = x_0_new[i];
    }
  }

  void set_y_0(std::vector<double> y_0_new) {
    for (int i = 0; i < N_X; ++i) {
      y_0[i] = y_0_new[i];
    }
  }

  void set_mode0(std::vector<int> mode0_new) {
    for (int i = 0; i < N_X; ++i) {
      mode0[i] = mode0_new[i];
    }
  }

  // inline functions to help match variables with their entry in variable vector
  //new inline functions with tray dependency

  //template<typename T> inline const T& z_i(const T* x, const T* y, const int jComp, const int iTray) const {
  //	return M_i(x, y, jComp) / M_total(x, y);
  //}
  template <typename T> inline const T& y_i(const T* /* x */, const T* y, const int jComp, const int iTray) const {
    return y[iTray*NTRAYVARS+jComp];
  }

  template <typename T> inline const T& x_i(const T* /* x */, const T* y, const int jComp, const int iTray) const {
    return y[iTray*NTRAYVARS+nC + jComp];
  }

  template <typename T> inline const T Temperature(const T* /* x */, const T* y, const int /* iTray */) const {
    return y[2 * nC + 4];
  };
  template <typename T> inline const T Pressure(const T* /* x */, const T* /* y */, const int /* iTray */) const {
    return pressure;
  };
  template <typename T> inline const T vfrac(const T* /* x */, const T* y, const int /* iTray */) const {
    return (M_V(y) / p_M);
  };

  template <typename T> inline const T beta(const T* /* x */, const T* y, const int /* iTray */) const {
    return y[2 * nC + 3];
  };

  //	T beta_ = beta(x, y, jComp, iTray);
  //template <typename T> inline const int& getMode(const T* x, const T* y, const int i, const int iTray) const {
  //	return mode[2*iTray+i];
  //};

  template <typename T> inline const T& y_i(const T* y, const int i) const {
    return y[i];
  }

  template <typename T> inline const T& x_i(const T* y, const int i) const {
    return y[nC + i];
  }

  template <typename T> inline const T M_C(const T* y) const {
    return y[2 * nC];
  }

  template <typename T> inline const T M_V(const T* y) const {
    return y[2 * nC + 1];
  }
  template <typename T> inline const T vfrac(const T* y) const {
    return M_V(y) / p_M;
  };
  template <typename T> inline const T M_L(const T* y) const {
    return y[2 * nC + 2];
  }

  template <typename T> inline const T beta(const T* y) const {
    return y[2 * nC + 3];
  }

  template <typename T> inline const T Temperature(const T* y) const {
    return y[2 * nC + 4];
  }

  template <typename T> inline const T obj_new(const T* y) const {
    return y[2 * nC + 6];
  }

  template <typename T> inline const T T_deviation(const T* y) const {
    return y[2 * nC + 7];
  }

  template <typename T> inline const T M_i(const T* /* x */, const T* /* y */, const int i) const {
    return this->p_M_i[i];
  }

  template <typename T> inline const T h(const T* x) const {
    return x[0];
  }

  template <typename T> inline const T objective(const T* x) const {
    return x[1];
  }

  template <typename T> inline const T M_total(const T* /* x */, const T* /* y */) const {
    return p_M;
  }

  template <typename T>
  inline const T Q(const T /* t */, const T* /* p */, const std::vector<int>& /* mode */) const {
    // Piecewise constant interpolation
    //int itheta_ = get_itheta();
    //if (itheta_ > N_P - 1) {
    //	std::cout << "itheta = " << itheta_ << std::endl;
    //}
    return Q_max;// / scaling_p * p[itheta_];
    //return Q_max * p[N_P-1];
  }

  template <typename T> inline const T tfrac(const T* y) const {
    return (Temperature(y)-T_set)/(T_0-T_set);
  }

  // thermodynamic functions

  /*! \brief Calculate vapor pressure of each component by extended Antoine model.
  \param[in] Temp: Temperature in K,
  \return pi0_XANT[nC] : vapor pressure of each component */
  template <typename T> std::vector<T> calc_pi0_XANT(const T Temp) const {
    vector<T> pi0_XANT(nC);
    vector<T> expo(nC);

    for (int i = 0; i < nC; i++) {
      expo[i] = ANT[i][3] + ANT[i][4] /
          (Temp + ANT[i][5]) + ANT[i][6] * Temp;
      expo[i] += ANT[i][7] * log(Temp);
      expo[i] += ANT[i][8] * pow(Temp, ANT[i][9]);
      pi0_XANT[i] = ANT[i][1] * pow(ANT[i][2], expo[i]);
    }

    return pi0_XANT;
  }

  /*! \brief Calculate ln of activity coefficients.
  \param[in] Temp: Temperature in K,
  \param[in] pressure: pressure (Pa),
  \param[in] x_l[nC]: mole fractions (of liquid phase).
  \return lngamma_i[nC] : ln of activity coefficients */
  template <typename T>
  vector<T> calc_lngamma_i(const T Temp, const T pressure, const vector<T>& x_l) const {
    vector<T> lngamma_i(nC);
    switch (gemodel) {
    case NRTL:
      lngamma_i = calc_lngamma_i_NRTL(Temp, pressure, x_l);
      break;
    case Margules:
      lngamma_i = calc_lngamma_i_Margules(Temp, pressure, x_l);
      break;
    default:
      std::fill(lngamma_i.begin(), lngamma_i.end(), 0.0);
    }

    return lngamma_i;
  }

  /*! \brief Calculate ln of activity coefficients by NRTL model.
  \param[in] Temp: Temperature in K,
  \param[in] pressure: pressure (Pa),
  \param[in] x_l[nC]: mole fractions (of liquid phase).
  \return lngamma_i[nC] : ln of activity coefficients by NRTL model */
  template <typename T>
  vector<T> calc_lngamma_i_NRTL(const T Temp, const T /* pressure */, const vector<T>& x_l) const {
    vector<vector<T> > tau(nC, vector<T>(nC));
    vector<vector<T> > gg(nC, vector<T>(nC));
    vector<vector<T> > ggtau(nC, vector<T>(nC));
    vector<T> xg(nC);
    vector<T> xtg(nC);
    vector<T> br(nC);
    vector<T> sum(nC);
    vector<vector<T> > ggtaubr(nC, vector<T>(nC));
    vector<T> lngamma_i(nC);

    for (int i = 0; i < nC; i++) {
      for (int j = 0; j < nC; j++) {
        tau[i][j] = 1.0 / (RGAS * Temp) *
            (NRTLG1[i][j] + NRTLG2[i][j] * Temp);
        gg[i][j] = exp(-NRTLA1[i][j] * tau[i][j]);
        ggtau[i][j] = gg[i][j] * tau[i][j];
      }
    }

    for (int i = 0; i < nC; i++) {
      xg[i] = 0.0;
      xtg[i] = 0.0;
      for (int j = 0; j < nC; j++) {
        xg[i] += gg[j][i] * x_l[j];
        xtg[i] += gg[j][i] * tau[j][i] * x_l[j];
      }
      br[i] = xtg[i] / xg[i];
    }

    for (int i = 0; i < nC; i++) {
      for (int j = 0; j < nC; j++) {
        ggtaubr[i][j] += gg[i][j] * (tau[i][j] - br[j]);
      }
    }

    for (int i = 0; i < nC; i++) {
      sum[i] = 0.0;
      for (int j = 0; j < nC; j++) {
        sum[i] += ggtaubr[i][j] * x_l[j] / xg[j];
      }

      lngamma_i[i] = br[i] + sum[i];
    }

    return lngamma_i;
  }

  /*! \brief Calculate ln of activity coefficients by Margules model.
  \param[in] Temp: Temperature in K,
  \param[in] pressure: pressure (Pa),
  \param[in] x_l[nC]: mole fractions (of liquid phase).
  \return lngamma_i[nC] : ln of activity coefficients by Margules model */
  template <typename T>
  vector<T> calc_lngamma_i_Margules(const T Temp, const T /* pressure */, const vector<T>& x_l) const {
    vector<T> lngamma_i(nC);
    for (int k = 0; k<nC; k++) {
      lngamma_i[k] = 0.;
      for (int i = 0; i < nC; i++) {
        for (int j = 0; j < nC; j++) {
          lngamma_i[k] += (MargulesA[i][k]
                           + MargulesA[j][k]
                           - MargulesA[i][j])*x_l[i] * x_l[j];
        }
      }
      lngamma_i[k] /= (2 * Temp);
    }

    return lngamma_i;
  }

  /*! \brief Calculates derivative of lngamma_i w.r.t. temperature by NRTL model.
  \param[in] Temp: Temperature in K,
  \param[in] pressure: pressure (Pa),
  \param[in] x_l[nC]: mole fractions (of liquid phase).
  \return dlngamma_i_dT[nC] : derivative of lngamma_i w.r.t. temperature with NRTL model */
  template <typename T>
  vector<T> calc_dlngamma_i_dT_NRTL(const T Temp, const T /* pressure */, const vector<T>& x_l) const {
    vector<vector<T> > tau(nC, vector<T>(nC));
    vector<vector<T> > dtau_dT(nC, vector<T>(nC));
    vector<vector<T> > gg(nC, vector<T>(nC));
    vector<vector<T> > dgg_dT(nC, vector<T>(nC));
    vector<vector<T> > ggtau(nC, vector<T>(nC));
    vector<T> xg(nC);
    vector<T> dxg_dT(nC);
    vector<T> xtg(nC);
    vector<T> dxtg_dT(nC);
    vector<T> br(nC);
    vector<T> dbr_dT(nC);
    vector<T> sum(nC);
    vector<T> dsum_dT(nC);
    vector<vector<T> > ggtaubr(nC, vector<T>(nC));
    vector<vector<T> > dggtaubr_dT(nC, vector<T>(nC));
    vector<T> lngamma_i(nC);
    vector<T> dlngamma_i_dT(nC);

    for (int i = 0; i < nC; i++) {
      for (int j = 0; j < nC; j++) {
        tau[i][j] = 1.0 / (RGAS * Temp) *
            (NRTLG1[i][j] + NRTLG2[i][j] * Temp);
        dtau_dT[i][j] = -NRTLG1[i][j] / (RGAS * Temp *
                                         Temp);
        gg[i][j] = exp(-NRTLA1[i][j] * tau[i][j]);

        if (tau[i][j] > 0.0) {
          dgg_dT[i][j] = -NRTLA1[i][j] * gg[i][j] * dtau_dT[i][j];
        }
        else {
          dgg_dT[i][j] = 0;
        }

        ggtau[i][j] = gg[i][j] * tau[i][j];
      }
    }

    for (int i = 0; i < nC; i++) {
      xg[i] = 0.0;
      dxg_dT[i] = 0.0;
      xtg[i] = 0.0;
      dxtg_dT[i] = 0.0;

      for (int j = 0; j < nC; j++) {
        xg[i] += gg[j][i] * x_l[j];
        dxg_dT[i] += dgg_dT[j][i] * x_l[j];
        xtg[i] += gg[j][i] * tau[j][i] * x_l[j];
        dxtg_dT[i] += (gg[j][i] * dtau_dT[j][i] +
                       dgg_dT[j][i] * tau[j][i]) * x_l[j];
      }

      br[i] = xtg[i] / xg[i];
      dbr_dT[i] = (xg[i] * dxtg_dT[i] - xtg[i] * dxg_dT[i]) /
          (xg[i] * xg[i]);
    }

    for (int i = 0; i < nC; i++) {
      for (int j = 0; j < nC; j++) {
        ggtaubr[i][j] = gg[i][j] * (tau[i][j] - br[j]);
        dggtaubr_dT[i][j] = dgg_dT[i][j] * (tau[i][j] - br[j]) +
            gg[i][j] * (dtau_dT[i][j] - dbr_dT[j]);
      }
    }

    for (int i = 0; i < nC; i++) {
      sum[i] = 0.0;
      dsum_dT[i] = 0.0;

      for (int j = 0; j < nC; j++) {
        sum[i] += ggtaubr[i][j] * x_l[j] / xg[j];
        dsum_dT[i] += (dggtaubr_dT[i][j] * xg[j] -
                       dxg_dT[j] * ggtaubr[i][j]) /
            (xg[j] * xg[j]) * x_l[j];
      }

      lngamma_i[i] = br[i] + sum[i];
      dlngamma_i_dT[i] = dbr_dT[i] + dsum_dT[i];
    }

    return dlngamma_i_dT;
  }

  /*! \brief Calculates derivative of lngamma_i w.r.t. temperature with Margules model.
  \param[in] Temp: Temperature in K,
  \param[in] pressure: pressure (Pa),
  \param[in] x_l[nC]: mole fractions (of liquid phase).
  \return dlngamma_i_dT[nC] : derivative of lngamma_i w.r.t. temperature with Margules model */
  template <typename T>
  vector<T> calc_dlngamma_i_dT_Margules(const T Temp, const T /* pressure */, const vector<T>& x_l) const {
    vector<T> dlngamma_i_dT(nC);
    for (int k = 0; k<nC; k++) {
      dlngamma_i_dT[k] = 0.;
      for (int i = 0; i < nC; i++) {
        for (int j = 0; j < nC; j++) {
          dlngamma_i_dT[k] += (MargulesA[i][k]
                               + MargulesA[j][k]
                               - MargulesA[i][j])*x_l[i] * x_l[j];
        }
      }
      dlngamma_i_dT[k] /= (2 * Temp * Temp);
      dlngamma_i_dT[k] *= -1.0;
    }

    return dlngamma_i_dT;
  }

  /*! \brief Calculate molar enthalpy [J/mol] for vapor phase. Here: ideal gas, no dependency on pressure. May, however, be added later.
  \param[in] Temp: Temperature in K,
  \param[in] pressure: pressure (Pa),
  \param[in] y_v[nC]: mole fractions (of vapor phase).
  \return h_V : molar enthalpy of vapor phase */
  template <typename T>
  T calc_hV(const T Temp, const T /* pressure */, const vector<T>& y_v) const {
    T h_V = 0.0;
    vector<T> h0ig(nC);
    h0ig = calc_h0ig(Temp);

    for (int i = 0; i < nC; i++) {
      h_V += y_v[i] * h0ig[i];
    }

    return h_V;
  }

  /*! \brief Calculate molar enthalpy [J/mol] for liquid phase.
  \param[in] Temp: Temperature in K,
  \param[in] pressure: pressure (Pa),
  \param[in] x_l[nC]: mole fractions (of liquid phase).
  \return h_L : molar enthalpy of liquid phase */
  template <typename T>
  T calc_hL(const T Temp, const T pressure, const vector<T>& x_l) const {
    T h_L = 0.0;
    vector<T> h0ig(nC);
    h0ig = calc_h0ig(Temp);
    T hig = 0.0;

    for (int i = 0; i < nC; i++) {
      hig += x_l[i] * h0ig[i];
    }

    vector<T> deltah0vap(nC);
    deltah0vap = calc_deltah0vap_DIPPR(Temp);

    T hEL = 0.0;
    if (considerExcessEnthalpy) { hEL = calc_hEL(Temp, pressure, x_l); }

    for (int i = 0; i < nC; i++) {
      h_L -= x_l[i] * deltah0vap[i];
    }

    h_L += hEL;
    h_L += hig;

    return h_L;
  }

  /*! \brief Calculate enthalpy deviation [J/mol] with DIPPR model.
  \param[in] Temp: Temperature in K,
  \return dh_DIPPR[nC] : Enthalpy deviation with DIPPR */
  template <typename T>
  vector<T> calc_dh_DIPPR(const T Temp) const {
    vector<T> dh_DIPPR(nC);

    for (int i = 0; i < nC; i++) {
      dh_DIPPR[i] = CPIGDP[i][0] * Temp;

      if (CPIGDP[i][2] == 0) {
        dh_DIPPR[i] = dh_DIPPR[i];
      }
      else {
        dh_DIPPR[i] = dh_DIPPR[i] + CPIGDP[i][1] * CPIGDP[i][2] /
            tanh(CPIGDP[i][2] / Temp);
      }

      if (CPIGDP[i][4] == 0) {
        dh_DIPPR[i] = dh_DIPPR[i];
      }
      else {
        dh_DIPPR[i] = dh_DIPPR[i] - CPIGDP[i][3] * CPIGDP[i][4] *
            tanh(CPIGDP[i][4] / Temp);
      }
    }

    return dh_DIPPR;
  }

  /*! \brief Calculate idael gas molar enthalpies [J/mol] for pure components.
  \param[in] Temp: Temperature in K,
  \return h0ig[nC] : Ideal gas molar enthalpy for each (pure) component */
  template <typename T>
  vector<T> calc_h0ig(const T Temp) const {
    vector<T> h0ig(nC);
    vector<T> dh_T(nC);
    dh_T = calc_dh_DIPPR(Temp);
    vector<T> dh_T_bezug(nC);
    dh_T_bezug = calc_dh_DIPPR(T(t_Bezug));

    for (int i = 0; i < nC; i++) {
      h0ig[i] = DHFORM[i] + dh_T[i] - dh_T_bezug[i];
    }

    return h0ig;
  }

  /*! \brief Calculate heat of vaporization using DIPPR equation. Only valid if CPIGDP[i,5] <= T <= CPIGDP[i,6]!
  \param[in] Temp: Temperature in K,
  \return h0ig[nC] : Ideal gas molar enthalpy for each component */
  template <typename T>
  vector<T> calc_deltah0vap_DIPPR(const T Temp) const {
    T base, expo;
    vector<T> deltah0vap_DIPPR(nC);
    vector<T> tr(nC);

    for (int i = 0; i < nC; i++) {
      tr[i] = Temp / KRIT[i][0];
      base = 1 - tr[i];
      expo = DHVLDP[i][1] + DHVLDP[i][2] * tr[i] +
          DHVLDP[i][3] * tr[i] * tr[i] +
          DHVLDP[i][4] * tr[i] * tr[i] * tr[i];
      deltah0vap_DIPPR[i] = DHVLDP[i][0] * pow(base, expo);
    }

    return deltah0vap_DIPPR;
  }

  /*! \brief Calculates excess enthalpy [J/mol].
  \param[in] Temp: Temperature in K,
  \param[in] pressure: pressure (Pa),
  \param[in] x_l[nC]: mole fractions (of vapor phase).
  \return h_L : molar enthalpy of vapor phase */
  template <typename T>
  T calc_hEL(const T Temp, const T pressure, const vector<T>& x_l) const {
    T hEL = 0.0;
    vector<T> dlngamma_i_dT(nC);
    switch (gemodel) {
    case NRTL:
      dlngamma_i_dT = calc_dlngamma_i_dT_NRTL(Temp, pressure, x_l);
      break;
    case Margules:
      dlngamma_i_dT = calc_dlngamma_i_dT_Margules(Temp, pressure, x_l);
      break;
    default:
      std::fill(dlngamma_i_dT.begin(), dlngamma_i_dT.end(), 0.0);
    }

    for (int i = 0; i < nC; i++) {
      hEL += -R_alg * Temp * Temp * x_l[i] *
          dlngamma_i_dT[i];
    }

    return hEL;
  }
};

template<typename Real> inline unsigned FlashExampleNewThermoNdae<Real>::Eval_n_p() const { return N_P;}
template<typename Real> inline unsigned FlashExampleNewThermoNdae<Real>::Eval_n_sigma() const { return N_S; }
template<typename Real> inline unsigned FlashExampleNewThermoNdae<Real>::Eval_n_x() const { return N_X;}
template<typename Real> inline unsigned FlashExampleNewThermoNdae<Real>::Eval_n_y() const { return N_Y;}
template<typename Real> inline unsigned FlashExampleNewThermoNdae<Real>::Eval_n_Theta() const { return N_THETA;}
template<typename Real> inline Real FlashExampleNewThermoNdae<Real>::Eval_t0() const { return t0;}

template<typename Real> void FlashExampleNewThermoNdae<Real>::Eval_Theta(Real* Theta) const {
  for (int i = 0; i < N_THETA; ++i) {
    Theta[i] = (double)(i+1) * tf / (double)N_THETA;
    //std::cout << "i = " << i << ", Theta[i] = " << Theta[i] << std::endl;
  }

  //Theta[0] = tf;
}

template<typename Real> std::vector<int> FlashExampleNewThermoNdae<Real>::EvalInitialMode() const {
  std::vector<int> temp(N_S);
  for (int i = 0; i < temp.size(); ++i) { temp[i] = mode0[i]; }
  //temp[N_S-1] = 0;
  return temp;
}

template<typename Real> std::vector<int> FlashExampleNewThermoNdae<Real>::EvalRootDirection(const std::vector<int>& /* mode */) const {
  std::vector<int> rootDirection(N_S, 0);
  for (int i = 0; i < N_S; ++i) {
    rootDirection[i] = -1;
  }
  return rootDirection;

}

template<typename Real> void FlashExampleNewThermoNdae<Real>::EvalInitialGuess_y(Real *y) const {
  unsigned n_y = Eval_n_y();
  // initial guess for alg variables
  for (int i = 0; i < n_y; ++i) {
    y[i] = y_0[i];
  }
}

template<typename Real> std::vector<int> FlashExampleNewThermoNdae<Real>::EvalMode(Real time, Real *x, Real *y, Real *p, const std::vector<int>& oldMode) const {
  std::vector<int> newMode(N_S);
  std::vector<Real> sigma(N_S);
  Eval_sigma(sigma.data(), time, x, y, p, oldMode);
  for (int i = 0; i < N_S; i++) {
    if (sigma[i] < 0)
      if (oldMode[i]) {
        newMode[i] = 0;
      }
      else {
        newMode[i] = 1;
      }
    else
      newMode[i] = oldMode[i];
    //mode0[i] = newMode[i];
  }

  //std::cout << "newMode[0] = " << newMode[0] << ", oldMode[0] = " << oldMode[0] << ", newMode[1] = " << newMode[1] << ", oldMode[1] = " << oldMode[1] << std::endl;
  return newMode;
}

template<typename Real>
template<typename T>
void FlashExampleNewThermoNdae<Real>::Eval_f_impl(T *f, T& time, T * /* x */, T *y, T *p, const std::vector<int>& mode) const
{
  f[0] = p[0]*Q(time, p, mode); //energy balance without any inlet/outlet mass flows
  f[1] = p[0]*(Temperature(y) - T_set)*(Temperature(y) - T_set);// tfrac(y)*tfrac(y);// / (T_0 - T_set) / (T_0 - T_set); //objective function
  f[2] = p[0]; //time
  //for (int i = 0; i < N_X; ++i) {
  //	std::cout << "f[" << i << "] = " << f[i] << ", x[" << i << "] = " << x[i] << std::endl;
  //}
}

template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_f(Real *f, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const
{
  Eval_f_impl(f, time, x, y, p, mode);
}

//template<typename Real>
//template<typename T>
//void FlashExampleNewThermoNdae<Real>::Eval_g_impl(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const
//{
//	vector<T> x_l(nC);
//	vector<T> y_v(nC);
//	for (int i = 0; i < nC; ++i) {
//		x_l[i] = x_i(y, i);
//		y_v[i] = y_i(y, i);
//	}	
//	vector<T> lngamma_i(nC);
//	//T Temp = Temperature(y);
//	//T press = pressure;
//	lngamma_i = calc_lngamma_i_NRTL(Temperature(y),Pressure(x,y,0),x_l);
//	std::vector<T> pi0(nC);
//	pi0 = calc_pi0_XANT(Temperature(y));
//	//T M_ = M_total(x, y);
//	//T vfrac_ = vfrac(y);
//
//	std::vector<T> M_i_all_(nC);
//
//	for (int i = 0; i < nC; ++i) {
//		if (i < nC - 1) {
//			M_i_all_[i] = M_i(x, y, i);
//		}
//		else {
//			M_i_all_[i] = M_C(y);
//		}
//		g[i] = y_v[i] - beta(y)*pi0[i] / Pressure(x, y, 0) * exp(lngamma_i[i])*x_l[i];
//		g[nC + i] = M_i_all_[i] - vfrac(y)*M_total(x, y) * y_v[i] - (1- vfrac(y))*M_total(x, y)*x_l[i];
//	}
//	//g[2 * nC] = M_ - M_V(y) - M_L(y);
//
//	T sum_y = 0.0;
//	T sum_x = 0.0;
//	for (int i = 0; i < nC; i++) {
//		sum_y += y_i(y, i);
//		sum_x += x_i(y, i);
//	}
//
//	// summation equation
//	g[2 * nC] = sum_y - sum_x;
//
//	T sum_M = 0.0;
//	for (int i = 0; i < nC; i++) {
//		sum_M += M_i_all_[i];// M_i_all_[i];
//	}
//	g[2 * nC + 1] = M_total(x, y) - sum_M;
//	T hV = calc_hV(Temperature(y),Pressure(x,y,0),y_v);
//	T hL = calc_hL(Temperature(y), Pressure(x, y, 0), x_l);
//	g[2 * nC + 2] = 1e-11 * (h(x) - (M_total(x, y)*vfrac(y) * hV + M_total(x, y)*(1- vfrac(y)) * hL));
//	if (mode[0] == 0) {
//		g[2 * nC + 3] = vfrac(y); // liquid only
//	}
//	else if (mode[1] == 0) {
//		g[2 * nC + 3] = (1- vfrac(y)); // vapor only
//	}
//	else {
//		g[2 * nC + 3] = beta(y) - 1; // vapor-liquid equilibrium
//	}
//	g[2 * nC + 4] = y[2 * nC + 5] - Q(time, p, mode);
//	g[2 * nC + 5] = M_total(x, y) - M_V(y) - M_L(y);
//	T sum_res_sq = 0.0;
//	for (int i = 0; i < N_Y; ++i) {
//		sum_res_sq += g[i] * g[i];
//		//std::cout << "g[" << i << "] = " << g[i] << ", y[" << i << "] = " << y[i] << std::endl;
//	}
//	//std::cout << "sum_res_sq =" << sum_res_sq << std::endl;
//	if (sum_res_sq > -1e-16) {
//		for (int i = 0; i < N_Y; ++i) {
//			//sum_res_sq += g[i] * g[i];
//			std::cout << "g[" << i << "] = " << g[i] << ", y[" << i << "] = " << y[i] << std::endl;
//		}
//		std::cout << "sum_res_sq_v1 =" << sum_res_sq << std::endl;
//		std::cout << "------------------" << std::endl;
//	}
//}

template<typename Real>
template<typename T>
void FlashExampleNewThermoNdae<Real>::Eval_g_impl_v2(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const
{
  const int iTray = 0;
  // vapor-liquid equilibrium
  T g_VLE[2 * nC + 2];
  Eval_g_VLE(g_VLE, time, x, y, p, mode, iTray);
  for (int i = 0; i < (2 * nC + 2); ++i) {
    g[i] = g_VLE[i];
  }

  // temporary variables for additional equations
  vector<T> x_l(nC);
  vector<T> y_v(nC);
  for (int jComp = 0; jComp < nC; ++jComp) {
    x_l[jComp] = x_i(x, y, jComp, iTray);
    y_v[jComp] = y_i(x, y, jComp, iTray);
  }
  //T Temperature_ = Temperature(x, y, iTray);
  //T Pressure_ = Pressure(x, y, iTray);
  //T vfrac_ = vfrac(x, y, iTray);
  //T beta_ = beta(x, y, iTray);
  //T M_ = M_total(x, y);
  std::vector<T> M_i_all_(nC);
  for (int i = 0; i < nC; ++i) {
    if (i < nC - 1) {
      M_i_all_[i] = M_i(x, y, i);
    }
    else {
      M_i_all_[i] = M_C(y);
    }
  }
  T sum_M = 0.0;
  for (int i = 0; i < nC; i++) {
    sum_M += M_i_all_[i];// M_i_all_[i];
  }
  // additional algebraic equations
  T hV = calc_hV(Temperature(y), Pressure(x, y, 0), y_v);
  T hL = calc_hL(Temperature(y), Pressure(x, y, 0), x_l);
  g[2 * nC + 2] = 1e-11 * (h(x) - (M_total(x, y)*vfrac(y) * hV + M_total(x, y)*(1 - vfrac(y)) * hL));
  g[2 * nC + 3] = M_total(x, y) - sum_M;
  g[2 * nC + 4] = y[2 * nC + 5] - Q(time, p, mode);
  g[2 * nC + 5] = M_total(x, y) - M_V(y) - M_L(y);
  g[2 * nC + 6] = obj_new(y) - objective(x) - scale_final_deviation * (Temperature(y)-T_set)*(Temperature(y)-T_set);
  g[2 * nC + 7] = T_deviation(y) + (Temperature(y) - T_set);
  g[2 * nC + 8] = y[2 * nC + 8] - calc_hEL(Temperature(y), Pressure(x, y, 0), x_l)/hL;


  //T sum_res_sq = 0.0;
  //for (int i = 0; i < N_Y; ++i) {
  //	sum_res_sq += g[i] * g[i];
  //	//std::cout << "g[" << i << "] = " << g[i] << ", y[" << i << "] = " << y[i] << std::endl;
  //}
  ////std::cout << "sum_res_sq =" << sum_res_sq << std::endl;
  //if (sum_res_sq > 1e-16) {
  //	for (int i = 0; i < N_Y; ++i) {
  //		//sum_res_sq += g[i] * g[i];
  //		std::cout << "g[" << i << "] = " << g[i] << ", y[" << i << "] = " << y[i] << std::endl;
  //	}
  //	std::cout << "sum_res_sq_v2 =" << sum_res_sq << std::endl;
  //	std::cout << "------------------" << std::endl;
  //}
}

template<typename Real>
template<typename T>
void FlashExampleNewThermoNdae<Real>::Eval_g_impl_v3(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const
{
  const int iTray = 0;
  std::vector<T> M_i_all_(nC);
  for (int i = 0; i < nC; ++i) {
    if (i < nC - 1) {
      M_i_all_[i] = M_i(x, y, i);
    }
    else {
      M_i_all_[i] = M_C(y);
    }
  }
  // vapor-liquid equilibrium
  vector<T> z(nC);
  vector<T> x_l(nC);
  vector<T> y_v(nC);
  for (int jComp = 0; jComp < nC; ++jComp) {
    z[jComp] = z_i(x, y, jComp, iTray);
    x_l[jComp] = x_i(x, y, jComp, iTray);
    y_v[jComp] = y_i(x, y, jComp, iTray);
  }
  //T Temperature_ = Temperature(x, y, iTray);
  //T Pressure_ = Pressure(x, y, iTray);
  //T vfrac_ = vfrac(x, y, iTray);
  //T beta_ = beta(x, y, iTray);
  vector<int> mode_(2);
  for (int i = 0; i < 2; ++i) {
    mode_[i] = mode[iTray * 2 + i];
  }

  vector<T> lngamma_i(nC);
  lngamma_i = calc_lngamma_i_NRTL(Temperature(x, y, iTray), Pressure(x, y, iTray), x_l);

  std::vector<T> pi0(nC);
  pi0 = calc_pi0_XANT(Temperature(x, y, iTray));

  for (int i = 0; i < nC; ++i) {
    // relaxed equilibrium condition
    g[i] = y_v[i] - beta(x, y, iTray)*pi0[i] / Pressure(x, y, iTray) * exp(lngamma_i[i])*x_l[i];
    // mass balance equations
    //g[nC + i] = z[i] - vfrac(x, y, iTray) * y_v[i] - (1 - vfrac(x, y, iTray))*x_l[i];
    g[nC + i] = M_i_all_[i] - vfrac(y)*M_total(x, y) * y_v[i] - (1 - vfrac(y))*M_total(x, y)*x_l[i];
  }

  T sum_y = 0.0;
  T sum_x = 0.0;
  for (int i = 0; i < nC; i++) {
    sum_y += y_v[i];
    sum_x += x_l[i];
  }

  // summation equation
  g[2 * nC] = sum_y - sum_x;

  if (mode_[0] == 0) {
    g[2 * nC + 1] = vfrac(x, y, iTray); // liquid only
  }
  else if (mode_[1] == 0) {
    g[2 * nC + 1] = (1 - vfrac(x, y, iTray)); // vapor only
  }
  else {
    g[2 * nC + 1] = beta(x, y, iTray) - 1; // vapor-liquid equilibrium
  }


  // temporary variables for additional equations

  T sum_M = 0.0;
  for (int i = 0; i < nC; i++) {
    sum_M += M_i_all_[i];// M_i_all_[i];
  }

  // additional algebraic equations
  T hV = calc_hV(Temperature(y), Pressure(x, y, 0), y_v);
  T hL = calc_hL(Temperature(y), Pressure(x, y, 0), x_l);
  g[2 * nC + 2] = 1e-11 * (h(x) - (M_total(x, y)*vfrac(y) * hV + M_total(x, y)*(1 - vfrac(y)) * hL));
  g[2 * nC + 3] = M_total(x, y) - sum_M;
  g[2 * nC + 4] = y[2 * nC + 5] - Q(time, p, mode);
  g[2 * nC + 5] = M_total(x, y) - M_V(y) - M_L(y);

  //T sum_res_sq = 0.0;
  //for (int i = 0; i < N_Y; ++i) {
  //	sum_res_sq += g[i] * g[i];
  //	//std::cout << "g[" << i << "] = " << g[i] << ", y[" << i << "] = " << y[i] << std::endl;
  //}
  ////std::cout << "sum_res_sq =" << sum_res_sq << std::endl;
  //if (sum_res_sq > 1e-16) {
  //	for (int i = 0; i < N_Y; ++i) {
  //		//sum_res_sq += g[i] * g[i];
  //		std::cout << "g[" << i << "] = " << g[i] << ", y[" << i << "] = " << y[i] << std::endl;
  //	}
  //	std::cout << "sum_res_sq_v3 =" << sum_res_sq << std::endl;
  //	std::cout << "------------------" << std::endl;
  //}
}

template<typename Real>
template<typename T>
void FlashExampleNewThermoNdae<Real>::Eval_g_VLE(T *g, T& /* time */, T *x, T *y, T* /* p */, const std::vector<int>& mode, const int iTray) const
{
  vector<T> z(nC);
  vector<T> M_i_all_(nC);
  vector<T> x_l(nC);
  vector<T> y_v(nC);
  for (int jComp = 0; jComp < nC; ++jComp) {
    if (jComp < nC - 1) {
      M_i_all_[jComp] = M_i(x, y, jComp);
    }
    else {
      M_i_all_[jComp] = M_C(y);
    }
    z[jComp] = M_i_all_[jComp] / M_total(x, y);
    x_l[jComp] = x_i(x, y, jComp, iTray);
    y_v[jComp] = y_i(x, y, jComp, iTray);
  }
  //T Temperature_ = Temperature(x, y, iTray);
  //T Pressure_ = Pressure(x, y, iTray);
  T vfrac_ = vfrac(x, y, iTray);
  //T beta_ = beta(x, y, iTray);
  vector<int> mode_(2);
  for (int i = 0; i < 2; ++i) {
    mode_[i] = mode[iTray*2+i];
  }

  vector<T> lngamma_i(nC);
  lngamma_i = calc_lngamma_i_NRTL(Temperature(x, y, iTray), Pressure(x, y, iTray), x_l);

  std::vector<T> pi0(nC);
  pi0 = calc_pi0_XANT(Temperature(x, y, iTray));

  for (int i = 0; i < nC; ++i) {
    // relaxed equilibrium condition
    g[i] = y_v[i] - beta(x, y, iTray)*pi0[i] / Pressure(x, y, iTray) * exp(lngamma_i[i])*x_l[i];
    // mass balance equations
    g[nC + i] = z[i] - vfrac_ * y_v[i] - (1 - vfrac_)*x_l[i];
  }

  T sum_y = 0.0;
  T sum_x = 0.0;
  for (int i = 0; i < nC; i++) {
    sum_y += y_v[i];
    sum_x += x_l[i];
  }

  // summation equation
  g[2 * nC] = sum_y - sum_x;

  if (mode_[0] == 0) {
    g[2 * nC + 1] = vfrac(x, y, iTray); // liquid only
  }
  else if (mode_[1] == 0) {
    g[2 * nC + 1] = (1 - vfrac(x, y, iTray)); // vapor only
  }
  else {
    g[2 * nC + 1] = beta(x, y, iTray) - 1; // vapor-liquid equilibrium
  }
}


template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_g(Real *g, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const
{
  Eval_g_impl_v2(g, time, x, y, p, mode);
}

template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_a1_f(Real*f, Real *a1_f, Real& time, Real& a1_time,
                                                Real *x, Real *a1_x, Real *y, Real *a1_y,
                                                Real *p, Real* a1_p, const std::vector<int>& mode) const
{

  AD_A1_CREATE
      AD_A1_RESET

      AD_A1_DEC2(f, N_X)
      AD_A1_DEC1(time)
      AD_A1_DEC2(x, N_X)
      AD_A1_DEC2(y, N_Y)
      AD_A1_DEC2(p, N_P)

      Eval_f_impl(ad_f, ad_time, ad_x, ad_y, ad_p, mode);
  AD_A1_INT2(f, N_X)
      AD_A1_GET1(time)
      AD_A1_GET2(x, N_X)
      AD_A1_GET2(y, N_Y)
      AD_A1_GET2(p, N_P)

}




template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_a1_g(Real* g, Real *a1_g, Real& time, Real& a1_time,
                                                Real *x, Real *a1_x, Real *y, Real *a1_y,
                                                Real *p, Real* a1_p, const std::vector<int>& mode) const
{
  AD_A1_CREATE
      AD_A1_RESET

      AD_A1_DEC2(g, N_Y)
      AD_A1_DEC1(time)
      AD_A1_DEC2(x, N_X)
      AD_A1_DEC2(y, N_Y)
      AD_A1_DEC2(p, N_P)

      Eval_g_impl_v2(ad_g, ad_time, ad_x, ad_y, ad_p, mode);
  AD_A1_INT2(g, N_Y)

      AD_A1_GET2(g, N_Y)
      AD_A1_GET1(time)
      AD_A1_GET2(x, N_X)
      AD_A1_GET2(y, N_Y)
      AD_A1_GET2(p, N_P)
}

template<typename Real>
template<typename T>
void FlashExampleNewThermoNdae<Real>::Eval_sigma_impl(T* sigma, T&  /* time */,
                                                      T* /* x */, T *y, T* /* p */, const std::vector<int>& mode) const
{
  T vfrac_ = vfrac(y);
  T beta_ = beta(y);
  if (mode[0]){
    sigma[0] = vfrac_;
  }
  else {
    sigma[0] = beta_ -1;
  }

  if (mode[1]) {
    sigma[1] = 1- vfrac_;
  }
  else {
    sigma[1] = 1-beta(y);
  }
  //if (mode[2]) {
  //	sigma[2] = 10 - time;
  //}
  //else {
  //	sigma[2] = time;
  //}
  //std::cout << "time = " << time << ", " << "sigma[" << N_S - 1 << "] = " << sigma[N_S - 1] << ", " << "mode[" << N_S - 1 << "] = " << mode[N_S - 1] << ", itheta = " << get_itheta() << std::endl;
  //std::cout << "time = " << time << ", ";
  //for (int i = 0; i < N_S; i++) {
  //	std::cout << "sigma[" << i << "] = " << sigma[i] << ", ";
  //}
  //std::cout << std::endl;
}

template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_sigma(Real* sigma, Real& time,
                                                 Real* x, Real *y, Real* p, const std::vector<int>& mode) const
{
  Eval_sigma_impl(sigma, time, x, y, p, mode);
}

template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_a1_sigma(Real* sigma, Real* a1_sigma, Real& time, Real& a1_time,
                                                    Real *x, Real *a1_x, Real *y, Real *a1_y,
                                                    Real *p, Real* a1_p, const std::vector<int>& mode) const
{

  AD_A1_CREATE
      AD_A1_RESET

      AD_A1_DEC2(sigma, N_S)
      AD_A1_DEC1(time)
      AD_A1_DEC2(x, N_X)
      AD_A1_DEC2(y, N_Y)
      AD_A1_DEC2(p, N_P)

      Eval_sigma_impl(ad_sigma, ad_time, ad_x, ad_y, ad_p, mode);
  AD_A1_INT2(sigma, N_S)

      AD_A1_GET2(sigma, N_S)
      AD_A1_GET1(time)
      AD_A1_GET2(x, N_X)
      AD_A1_GET2(y, N_Y)
      AD_A1_GET2(p, N_P)
}

template<typename Real>
template<typename T>
void FlashExampleNewThermoNdae<Real>::Eval_psi_impl(T *psi, T& time, T* x, T *y,
                                                    T* p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const
{
  for (int i = 0; i < N_X; ++i) {
    psi[i] = x[i];
  }
}

template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_psi(Real *psi, Real& time, Real* x, Real *y,
                                               Real* p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const
{
  Eval_psi_impl(psi, time, x, y, p, newMode, oldMode);
}

template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_a1_psi(Real*psi, Real *a1_psi, Real& time, Real& a1_time,
                                                  Real *x, Real *a1_x, Real *y, Real *a1_y,
                                                  Real *p, Real* a1_p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const
{
  AD_A1_CREATE
      AD_A1_RESET

      AD_A1_DEC2(psi, N_X)
      AD_A1_DEC1(time)
      AD_A1_DEC2(x, N_X)
      AD_A1_DEC2(y, N_Y)
      AD_A1_DEC2(p, N_P)

      Eval_psi_impl(ad_psi, ad_time, ad_x, ad_y, ad_p, newMode, oldMode);
  AD_A1_INT2(psi, N_X)

      AD_A1_GET1(time)
      AD_A1_GET2(x, N_X)
      AD_A1_GET2(y, N_Y)
      AD_A1_GET2(p, N_P)
}

template<typename Real>
template<typename T>
void FlashExampleNewThermoNdae<Real>::Eval_psi0_impl(T *psi0, T* p) const
{
  for (int i = 0; i < N_X; ++i) {
    psi0[i] = x_0[i];
  }
  //psi0_fcn(psi0,p);
}

template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_psi0(Real *psi0, Real* p) const
{
  Eval_psi0_impl(psi0, p);
}

template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_a1_psi0(Real *psi0, Real *a1_psi0, Real* p, Real *a1_p) const
{
  AD_A1_CREATE
      AD_A1_RESET

      AD_A1_DEC2(psi0, N_X)
      AD_A1_DEC2(p, N_P)

      Eval_psi0_impl(ad_psi0, ad_p);
  AD_A1_INT2(psi0, N_X)

      AD_A1_GET2(p, N_P)
}


template<typename Real>
template<typename T>
void FlashExampleNewThermoNdae<Real>::Eval_phi_impl(T& phi, T *Theta, T **X, T **Y, T *p) const
{
  //for (int i = 0; i < N_THETA; i++) {
  //	for (int j = 0; j < N_Y; j++) {
  //		std::cout << "Y[" << i << "][" << j << "] = " << Y[i][j] << std::endl;
  //	}
  //}
  //std::cout << "objMode: " << getObjMode() << std::endl;
  if (objMode == ObjectiveMode::ComputeObjective) {
    phi = p[0];
  }
  else if (objMode == ObjectiveMode::ComputeConstraint) {
    phi = Y[N_THETA - 1][N_Y - 2];
  }
  else {
    phi = Y[N_THETA - 1][N_Y - 3];
  }
  //switch (objMode) {
  //case ObjectiveMode::ComputeObjective:
  //	phi = p[0];
  //case ObjectiveMode::ComputeConstraint:
  //	phi = Y[N_THETA - 1][N_Y - 1];
  //default:
  //	phi = Y[N_THETA - 1][N_Y - 2];
  //}
}

template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_phi(Real& phi, Real *Theta, Real **X, Real **Y, Real *p) const
{
  Eval_phi_impl(phi, Theta, X, Y, p);
}

template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_a1_phi(Real& phi, Real& a1_phi, Real *Theta, Real* a1_Theta,
                                                  Real **X, Real ** a1_X,
                                                  Real **Y, Real **a1_Y, Real *p, Real *a1_p) const
{
  AD_A1_CREATE
      AD_A1_RESET

      AD_A1_DEC1(phi)
      AD_A1_DEC2(Theta, N_THETA)
      AD_A1_DEC3(X, N_X, N_THETA)
      AD_A1_DEC3(Y, N_Y, N_THETA)
      AD_A1_DEC2(p, N_P)

      Eval_phi_impl(ad_phi, ad_Theta, ad_X, ad_Y, ad_p);
  AD_A1_INT1(phi)

      AD_A1_GET2(Theta, N_THETA)
      AD_A1_GET3(X, N_X, N_THETA)
      AD_A1_GET3(Y, N_Y, N_THETA)
      AD_A1_GET2(p, N_P)
}
using Gt1s = ad::gt1s<double>::type;
inline std::vector<Gt1s> CreateGt1sVector(const double* values, const double* t1_values, const unsigned n)
{
  std::vector<Gt1s> gt1sVector(n);
  for (unsigned i = 0; i < n; ++i) {
    ad::value(gt1sVector[i]) = values[i];
    ad::derivative(gt1sVector[i]) = t1_values[i];
  }
  return gt1sVector;
}

inline std::vector<Gt1s> CreateGt1sVector(const unsigned n)
{
  std::vector<Gt1s> gt1sVector(n);
  for (unsigned i = 0; i < n; ++i) {
    ad::value(gt1sVector[i]) = 0.0;
    ad::derivative(gt1sVector[i]) = 0.0;
  }
  return gt1sVector;
}

inline void ExtractGt1sVector(double *vec, double *t1_vec, const std::vector<Gt1s>& ad_vec) {
  for (std::size_t i = 0; i < ad_vec.size(); ++i) {
    vec[i] = ad::value(ad_vec[i]);
    t1_vec[i] = ad::derivative(ad_vec[i]);
  }
}

inline Gt1s CreateGt1sScalar(const double value) {
  Gt1s scalar;
  ad::value(scalar) = value;
  ad::derivative(scalar) = 0.0;
  return scalar;
}

inline void ExtractGt1sScalar(double& scalar, double& t1_scalar, const Gt1s& ad_scalar) {
  scalar = ad::value(ad_scalar);
  t1_scalar = ad::derivative(ad_scalar);
}



template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_t1_f(Real *f, Real *t1_f, Real& time, Real& t1_time, Real *x, Real *t1_x,
                                                Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const
{

  auto ad_f = CreateGt1sVector(N_X);
  auto ad_time = CreateGt1sScalar(time);
  auto ad_x = CreateGt1sVector(x, t1_x, N_X);
  auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
  auto ad_p = CreateGt1sVector(p, t1_p, N_P);

  Eval_f_impl(ad_f.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), mode);

  ExtractGt1sVector(f, t1_f, ad_f);


}

template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_t1_g(Real *g, Real *t1_g, Real& time, Real& t1_time, Real *x, Real *t1_x,
                                                Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const
{
  auto ad_g = CreateGt1sVector(N_Y);
  auto ad_time = CreateGt1sScalar(time);
  auto ad_x = CreateGt1sVector(x, t1_x, N_X);
  auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
  auto ad_p = CreateGt1sVector(p, t1_p, N_P);

  Eval_g_impl_v2(ad_g.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), mode);

  ExtractGt1sVector(g, t1_g, ad_g);
}


template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_t1_sigma(Real* sigma, Real* t1_sigma, Real& time, Real& t1_time, Real *x, Real *t1_x,
                                                    Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const
{
  auto ad_sigma = CreateGt1sVector(N_S);
  auto ad_time = CreateGt1sScalar(time);
  auto ad_x = CreateGt1sVector(x, t1_x, N_X);
  auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
  auto ad_p = CreateGt1sVector(p, t1_p, N_P);

  Eval_sigma_impl(ad_sigma.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), mode);

  ExtractGt1sVector(sigma, t1_sigma, ad_sigma);
}

template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_t1_psi(Real * psi, Real *t1_psi, Real& time, Real& t1_time, Real *x, Real *t1_x,
                                                  Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& newMode,
                                                  const std::vector<int>& oldMode) const
{
  auto ad_psi = CreateGt1sVector(N_X);
  auto ad_time = CreateGt1sScalar(time);
  auto ad_x = CreateGt1sVector(x, t1_x, N_X);
  auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
  auto ad_p = CreateGt1sVector(p, t1_p, N_P);

  Eval_psi_impl(ad_psi.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), newMode, oldMode);

  ExtractGt1sVector(psi, t1_psi, ad_psi);
}


template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_t1_psi0(Real *psi0, Real *t1_psi0, Real *p, Real* t1_p) const
{
  auto ad_psi0 = CreateGt1sVector(N_X);
  auto ad_p = CreateGt1sVector(p, t1_p, N_P);

  Eval_psi0_impl(ad_psi0.data(), ad_p.data());

  ExtractGt1sVector(psi0, t1_psi0, ad_psi0);
}

template<typename Real>
void FlashExampleNewThermoNdae<Real>::Eval_t1_phi(Real& phi, Real& t1_phi, Real *Theta, Real *t1_Theta, Real **X, Real ** t1_X,
                                                  Real **Y, Real **t1_Y, Real *p, Real *t1_p) const
{
  std::vector<Gt1s*> ad_X(N_THETA), ad_Y(N_THETA);
  auto ad_X_data = CreateGt1sVector(N_THETA * N_X);
  auto ad_Y_data = CreateGt1sVector(N_THETA * N_Y);
  for (unsigned i = 0; i < N_THETA; ++i) {
    ad_X[i] = &ad_X_data[i*N_X];
    ad_Y[i] = &ad_Y_data[i*N_Y];
    for (unsigned j = 0; j < N_X; ++j) {
      ad::value(ad_X[i][j]) = X[i][j];
      ad::derivative(ad_X[i][j]) = t1_X[i][j];
    }
    for (unsigned j = 0; j < N_Y; ++j) {
      ad::value(ad_Y[i][j]) = Y[i][j];
      ad::derivative(ad_Y[i][j]) = t1_Y[i][j];
    }

  }
  auto ad_p = CreateGt1sVector(p, t1_p, N_P);
  auto ad_Theta = CreateGt1sVector(Theta, t1_Theta, N_THETA);
  Gt1s ad_phi;
  Eval_phi_impl(ad_phi, ad_Theta.data(), ad_X.data(), ad_Y.data(), ad_p.data());

  ExtractGt1sScalar(phi, t1_phi, ad_phi);

}


#endif // FLASH_EXAMPLE_NDAE_HPP
