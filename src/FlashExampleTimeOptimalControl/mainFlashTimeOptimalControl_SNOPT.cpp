#include <iostream>
#include <cmath>

#include "FlashExampleTimeOptimalControlNdae.hpp"
#include "NdaeSolver.hpp"
#include "NdaeUtils.hpp"

#include "SNOPTWrapper.hpp"
#include "MyNlp_FlashExampleTimeOptimalControl.hpp"

using namespace NdaeUtils;

int doSimulation(int N, double* X, bool doPlot, double* F); //without derivatives
int doSimulation(int N, double* X, bool doPlot, double* F, double * G); //with derivatives


int main()
{
	double optTol = 1.e-6;
	//std::string s = to_string(d);
	std::ostringstream streamObj;
	//Add double to stream
	streamObj << optTol;
	// Get string from output string stream
	//std::string strObj = streamObj.str();
	std::cout << "Conversion of double to string: " << streamObj.str() << std::endl;

	std::map<std::string, std::string> optimizerOptions;
	//optimizerOptions.insert(make_pair<std::string, std::string>("scale option", "2")); //seemingly not working
	//optimizerOptions.insert(make_pair<std::string, std::string>("Scale print", "1"));
	optimizerOptions.insert(make_pair<std::string, std::string>("major iterations limit", "500"));
	optimizerOptions.insert(make_pair<std::string, std::string>("minor iterations limit", "1000"));
	optimizerOptions.insert(make_pair<std::string, std::string>("derivative level", "3"));
	streamObj.str(std::string());
	streamObj << optTol;
	optimizerOptions.insert(make_pair<std::string, std::string>("major optimality tolerance", streamObj.str()));
	//optimizerOptions.insert(make_pair<std::string, std::string>("minor optimality tolerance", "5e-11"));
	optimizerOptions.insert(make_pair<std::string, std::string>("major feasibility tolerance", streamObj.str()));
	optimizerOptions.insert(make_pair<std::string, std::string>("function precision", streamObj.str()));
	streamObj.str(std::string());
	streamObj << 0.1*optTol;
	optimizerOptions.insert(make_pair<std::string, std::string>("minor feasibility tolerance", streamObj.str()));
	optimizerOptions.insert(make_pair<std::string, std::string>("major print level", "111111"));
	optimizerOptions.insert(make_pair<std::string, std::string>("minor print level", "11111"));


	//optimizerOptions.insert(make_pair<std::string, std::string>("elastic weight", "1"));
	SNOPTWrapper snoptWrapper(optimizerOptions);
	MyNlp myNlp;
	snoptWrapper.solve(&myNlp);

	//const unsigned n_p = FlashExampleNewThermoNdae<double>::N_P;
	//FlashExampleNewThermoNdae<double> dae;

	//std::vector<double> p0;
	//p0.resize(n_p);
	//for (int i = 0; i < n_p; ++i) {
	//	p0[i] = dae.p_0[i];
	//}
	//double time = 0.0;

	//// vector for results of dynamic optimization
	//std::vector<double> x_vec;
	//x_vec.resize(n_p);

	//// vector for intial values for dynamic optimization
	//std::vector<double> x_0;
	//x_0.resize(n_p);
	//for (int i = 0; i < x_0.size(); ++i) {
	//	x_0[i] = 1.0;// dae.p_0[i]; // 0.0
	//	x_vec[i] = x_0[i];
	//}


	//std::vector<double> phi = { 0.0 };
	//// simulation without derivatives
	//doSimulation(n_p, x_vec.data(), true, phi.data());

	//std::vector<double> dphi_dp(n_p, 0.0);

	//// simulation with derivatives
	//doSimulation(n_p, x_vec.data(), true, phi.data(), dphi_dp.data());

	return 0;
}

int doSimulation(int N, double* X, bool doPlot, double* F) {

	// create instance of dae
	const unsigned n_p = FlashExampleNewThermoNdae<double>::N_P;

	FlashExampleNewThermoNdae<double> dae;

	//set up options for integration
	NdaeSolver::Options options;
	options.SetMaxStepDenseOutput(1.0);
	options.SetTol(1e-8);
	options.SetStateEventTol(1e-8);
	options.SetMaxDiscontinuityTol(1e-7);
	options.SetDoPlot(doPlot);
	options.SetFilenameStates("SimulationResults_Flash.txt");


	if (N != n_p) {
		std::cout << "Number of parameters is n_p = " << n_p << " but there are N = " << N << " optimization variables." << std::endl;
		return -1;
	}

	// set parameters to values of X
	realtype p_0[n_p];
  for (size_t i = 0; i < n_p; ++i) {
		p_0[i] = X[i];
	}
	// create variables for objective (Phi)
	realtype Phi = 0;

	// run simulation
	auto flag = NdaeSolver::EvaluatePhi(Phi, p_0, dae, options);
  if (flag == NdaeSolver::Flag_Failure) {
    std::cerr << "error NdaeSolver::EvaluatePhi() failed" << std::endl;
  }
  else {
    std::cout << "Successful call to NdaeSolver::EvaluatePhi()." << std::endl;
  }

	// return objective function F
	F[0] = Phi;
  return 0;
}

int doSimulation(int N, double* X, bool doPlot, double* F, double * G) {

	// create instance of dae
	const unsigned n_p = FlashExampleNewThermoNdae<double>::N_P;
	FlashExampleNewThermoNdae<double> dae;

	//set up options for integration
	NdaeSolver::Options options;
	options.SetMaxStepDenseOutput(1.0);
	options.SetTol(1e-8);
	options.SetStateEventTol(1e-8);
	options.SetMaxDiscontinuityTol(1e-7);
	options.SetDoPlot(doPlot);
	options.SetFilenameStates("SimulationResults_Flash.txt");


	if (N != n_p) {
		std::cout << "Number of parameters is n_p = " << n_p << " but there are N = " << N << " optimization variables." << std::endl;
		return -1;
	}

	// set parameters to values of X
	realtype p_0[n_p];
  for (size_t i = 0; i < n_p; ++i) {
		p_0[i] = X[i];
	}
	// create variables for objective (Phi) and its sensitivities (Phi_temp and Phi_p) w.r.t. optimization variables (parameters)
	realtype Phi = 0;
	realtype Phi_p[n_p];

	// run simulation
	auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, p_0, dae, options);
  if (flag == NdaeSolver::Flag_Failure) {
    std::cerr << "error NdaeSolver::EvaluatePhiGradient() failed" << std::endl;
  }
  else {
    std::cout << "Successful call to EvaluatePhiGradient::EvaluatePhi()." << std::endl;
  }

	// return objective function F and gradients w.r.t. parameters G
	F[0] = Phi;
	for (int i = 0; i < N; ++i) {
		G[i] = Phi_p[i];
		std::cout << X[i] << ", " << G[i] << std::endl;
	}
  return 0;
}
