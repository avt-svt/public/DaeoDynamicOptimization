#include "MyNlp_FlashExampleTimeOptimalControl.hpp"

void MyNlp::EvalDimensions(int& numVariables, int& numConstraints) {
  numVariables=dae.N_P;
  numConstraints=1; // dummy constraint for now
}


void MyNlp::EvalVariableBounds(double *lowerBounds, double* upperBounds, int numVariables){
  for(int i=0; i<numVariables; i++) {
	  lowerBounds[i] = 0.0;// -1.0*dae.scaling_p;
    upperBounds[i]=1.0*8000;
  }
}


void MyNlp::EvalConstraintBounds(double *lowerBounds, double* upperBounds, int numConstraints){
  lowerBounds[0]=-1e3;
  //lowerBounds[1]=-1e20;
  upperBounds[0]=0;
  //upperBounds[1]=10;
}


void MyNlp::EvalInitialGuess(double* x, int numVariables){
	std::ofstream ofile("initialSolution.txt");
	for (int i = 0; i < n_p; ++i) {
		x[i] = dae.p_0[i];
		ofile << x[i] << std::endl;
	}
	ofile.close();
}


void MyNlp::EvalObjectiveAndConstraints(int derivativeOrder, double* x, double& f, double* grad_f, 
  double* g, double** jac_g, int numVariables, int numConstraints){
	const int numVars = dae.N_P;
	
	// create variables for objective (Phi) and its sensitivities (Phi_temp and Phi_p) w.r.t. optimization variables (parameters)
	realtype Phi = 0;
	realtype Phi_p[numVars];
	realtype constr = 0;
	realtype constr_p[numVars];
  //  if( derivativeOrder >= 0) {
		//// run simulation
		//dae.setObjMode(ObjectiveMode::ComputeObjective);
		//auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, x, dae, options);
		//f = Phi;
		////std::cout << "Objective: " << f << std::endl;
		//dae.setObjMode(ObjectiveMode::ComputeConstraint);
		//auto flag2 = NdaeSolver::EvaluatePhiGradient(constr, constr_p, x, dae, options);
		//g[0]=constr;
		////std::cout << "Constraint: " << g[0] << std::endl;
  //  }
	if (derivativeOrder == 3) {
		// run simulation
		dae.setObjMode(ObjectiveMode::ComputeObjective);
		auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, x, dae, options);
		f = Phi;
	}
	else if (derivativeOrder == 4) {
		//std::cout << "Objective: " << f << std::endl;
		dae.setObjMode(ObjectiveMode::ComputeConstraint);
		auto flag2 = NdaeSolver::EvaluatePhiGradient(constr, constr_p, x, dae, options);
		g[0] = constr;
		//std::cout << "Constraint: " << g[0] << std::endl;
	}
	else {
		// run simulation
		dae.setObjMode(ObjectiveMode::ComputeObjective);
		auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, x, dae, options);
		f = Phi;
		//std::cout << "Objective: " << f << std::endl;
		dae.setObjMode(ObjectiveMode::ComputeConstraint);
		auto flag2 = NdaeSolver::EvaluatePhiGradient(constr, constr_p, x, dae, options);
		g[0]=constr;
		//std::cout << "Constraint: " << g[0] << std::endl;
	}

    if( derivativeOrder >= 1) {
		for (int i = 0; i < numVariables; ++i) {
			grad_f[i] = Phi_p[i];
			//std::cout << x[i] << ", " << grad_f[i] << std::endl;
			jac_g[0][i] = constr_p[i];
		}
    }

}

void MyNlp::FinalizeSolution(int n, const double * x,
	const double* z_L, const double* z_U,
	int m, const double* g, const double* lambda,
	double obj_value) {
	const int numVars = dae.N_P;

	double Phi = 0;
	double Phi_p[numVars];

	std::ostringstream filename_results;
	filename_results << "SimulationResults_Flash_TimeOptimalControl";
	//filename_results << dae.scale_final_deviation;
	filename_results << ".txt";
	options.SetDoPlot(TRUE);
	options.SetFilenameStates(filename_results.str());
	auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, x, dae, options);
	std::ofstream ofile("finalSolution.txt");
	ofile << "Objective value = " << Phi << ", c = " << dae.scale_final_deviation << std::endl;
	for (int i = 0; i < numVars; ++i) {
		//std::cout << "X(" << i + 1 << ") = " << X[i] << std::endl;
		//X_vec[i] = X[i];
		ofile << x[i] << ", " << Phi_p[i] << std::endl;
	}
	ofile.close();
}

MyNlp::MyNlp() {
	//set up options for integration
	const double tol = 1e-10;
	options.SetMaxStepDenseOutput(1.0);
	options.SetTol(tol);
	options.SetStateEventTol(tol);
	options.SetMaxDiscontinuityTol(10.0*tol);
	options.SetDoPlot(FALSE);
	options.SetFilenameStates("SimulationResults_Flash.txt");
	//options.SetAbsTolAdjoint(1e-8);
	//options.SetRelTolAdjoint(1e-8);
	std::cout << "Adjoint abs tol: " << options.GetAbsTolAdjoint() << std::endl;
	std::cout << "Adjoint rel tol: " << options.GetRelTolAdjoint() << std::endl;
}

