#pragma once
#include <vector>
//function declarations
template <typename Real> void f_stillPot_fcn(Real* f, Real& time, Real* x, Real* y, Real *p, const std::vector<int>& mode);
template <typename Real> void g_VLLE_fcn(Real* g, Real& time, Real* x, Real* y, Real *p, const std::vector<int>& mode);


template <typename Real> void sigma_VLLE_fcn(Real* sigma, Real& time, Real* x, Real* y, Real *p, const std::vector<int>& mode)
{
	constexpr unsigned n_comp = 4;
	sigma[0] = y[6 * n_comp];
	sigma[1] = 1-y[6 * n_comp];

}
template <typename Real> void sigma_fcn(Real* sigma, Real& time, Real* x, Real* y, Real *p, const std::vector<int>& mode)
{
	constexpr unsigned n_comp = 4;
	Real sigma_VLLE[2];
	sigma_VLLE_fcn(sigma_VLLE, time, x, y, p,mode);
	for (int i = 0; i < 2; i++) {// gives sigma the value of s to check sign; is lfrac_LLE the correct variable??
		sigma[i] = sigma_VLLE[i];
	}
    sigma[2] = 1-time;
}



template <typename Real> void f_fcn(Real* f, Real& time, Real* x, Real* y, Real *p, const std::vector<int>& mode)
{
	constexpr unsigned n_comp = 4;
	Real x_massBal_stillPot[5];
	Real y_massBal_stillPot[4];
	Real p_massBal_stillPot[1];
	
	Real f_stillPot[5];

	for (int i = 0; i < 5; i++) {
		x_massBal_stillPot[i] = x[i];
	}
	
	for (int i = 0; i < n_comp; i++) {
		y_massBal_stillPot[i] = y[2 * n_comp + i];
	}

	p_massBal_stillPot[0] = p[1];

	f_stillPot_fcn(f_stillPot, time, x_massBal_stillPot, y_massBal_stillPot, p_massBal_stillPot,mode);

	for (int i = 0; i < 5; i++) {
		f[i] = f_stillPot[i];
	}
  /*Real iC = 0;
  Real C = 0;
  iC = y[4];
  C = p[2];

  f[0] = iC / C; //differential equation for u2*/
}

template <typename Real> void g_fcn(Real* g, Real& time, Real* x, Real* y, Real *p, const std::vector<int>& mode)
{
	int n_comp = 4;
	Real x_VLLE_stillPot[5];
	Real y_VLLE_stillPot[27];
	Real p_VLLE_stillPot[1];

	Real g_VLLE_stillPot[27];

	for (int i = 0; i < 5; i++) {
		x_VLLE_stillPot[i] = x[i];
	}
	
	for (int i = 0; i < 27; i++) {
		y_VLLE_stillPot[i] = y[i];
	}
	
	p_VLLE_stillPot[0] = p[0]; //only pressure P

	//VLLE for still pot
	g_VLLE_fcn(g_VLLE_stillPot, time, x_VLLE_stillPot, y_VLLE_stillPot, p_VLLE_stillPot, mode);

	for (int i = 0; i < 6*n_comp+3; i++) {
		g[i] = g_VLLE_stillPot[i];
	}

}

template <typename Real> void psi_fcn(Real *psi, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& newMode, const std::vector<int>& oldMode)
{
  psi[0] = x[0]; // psi is transition condition, the diff vars do not change when switching modes (H,n1,n2,n3,n4 remain diff vars)
  psi[1] = x[1];
 // psi[2] = x[2];
 // psi[3] = x[3];
 // psi[4] = x[4]; //extend as for loop
}

template <typename Real> void psi0_fcn(Real* psi0, Real *p)
{
  psi0[0] = 20; // initial condition of H
  psi0[1] = 8; // initial condition of n1
  //psi0[2] = 4; // initial condition of n2
 // psi0[3] = 0; // initial condition of n3
  //psi0[4] = 8; // initial condition of n4
}

template <typename Real> void phi_fcn(Real& phi, Real *Theta, Real **X, Real **Y, Real *p)
{
	phi = X[0][1];//1000.0 *(Y[0][4]*Y[0][4] + Y[1][4]*Y[1][4]); // arbitrary objective funcitonal //for sensitivity analysis
}

template <typename Real>  void calcgamma(Real* gamma, Real& x1, Real& x2, Real& x3, Real& x4, Real& T)
#pragma ad indep x1 x2 x3 x4 T
#pragma ad dep gamma
{
	Real QUACU[4][4]; // = {{0, 581.1471, 527.9269, 461.4747}, {68.0083, 0, 148.2833, 82.5336}, {-343.593, -131.7686, 0, -298.4344}, {685.71, 24.6386, 712.2349, 0}};
	Real r[4]; // = {0.92, 3.4543, 2.2024, 4.8274};
	Real q[4]; // = {1.4, 3.052, 2.072, 4.196};
	Real x[4]; // = {x1,x2,x3,x4};

	Real phi[4]; // = {0, 0, 0, 0};
	Real psi[4]; // = {0, 0, 0, 0};
	Real tau[4][4]; // = {{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
	Real tautranspsi[4]; // = {0, 0, 0, 0};
	Real psidtauTpsi[4]; // = {0, 0, 0, 0};
	Real s[4]; // = {0, 0, 0, 0};
	Real l[4]; // = {0, 0, 0, 0};
	Real lngr[4]; // = {0, 0, 0, 0};
	Real lngc[4]; // = {0, 0, 0, 0};
	Real gammaforexposure[4];

	int i = 0;
	int j = 0;

	Real sumr = 0;
	Real sumq = 0;
	Real sumxl = 0;
	Real sq = 0;
	Real sq2 = 0;
	Real z = 10;
	Real Temp = 0;

	x[0] = x1;
	x[1] = x2;
	x[2] = x3;
	x[3] = x4;
	Temp = T;

	QUACU[0][0] = 0;
	QUACU[0][1] = 581.1471;
	QUACU[0][2] = 527.9269;
	QUACU[0][3] = 461.4747;
	QUACU[1][0] = 68.0083;
	QUACU[1][1] = 0;
	QUACU[1][2] = 148.2833;
	QUACU[1][3] = 82.5336;
	QUACU[2][0] = -343.593;
	QUACU[2][1] = -131.7686;
	QUACU[2][2] = 0;
	QUACU[2][3] = -298.4344;
	QUACU[3][0] = 685.71;
	QUACU[3][1] = 24.6386;
	QUACU[3][2] = 712.2349;
	QUACU[3][3] = 0;

	r[0] = 0.92;
	r[1] = 3.4543;
	r[2] = 2.2024;
	r[3] = 4.8274;

	q[0] = 1.4;
	q[1] = 3.052;
	q[2] = 2.072;
	q[3] = 4.196;

	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			tau[i][j] = exp(-1 / (1.9948 * Temp) *	QUACU[i][j]);
		}
	}

	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			phi[i] = 0;
			psi[i] = 0;
			tautranspsi[i] = 0;
			psidtauTpsi[i] = 0;
			s[i] = 0;
			l[i] = 0;
			lngr[i] = 0;
			lngc[i] = 0;
		}
	}

	for (i = 0; i <= 3; i++) {
		sumr = sumr + r[i] * x[i];
		sumq = sumq + q[i] * x[i];
		phi[i] = r[i] * x[i];
		psi[i] = q[i] * x[i];
	}

	for (i = 0; i < 4; i++) {
		phi[i] = 1 / sumr * phi[i];
		psi[i] = 1 / sumq * psi[i];
	}

	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			tautranspsi[i] = tautranspsi[i] + tau[j][i] * psi[j];
		}
	}


	for (i = 0; i < 4; i++) {
		sq = tautranspsi[i] * tautranspsi[i];
		if (sq > 1e-10)
		{
			psidtauTpsi[i] = psi[i] / tautranspsi[i];
		}
		else {
			psidtauTpsi[i] = 0;
		}
	}

	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			s[i] = s[i] + tau[i][j] * psidtauTpsi[j];
		}
	}

	for (i = 0; i < 4; i++) {
		l[i] = 0.5 * z * (r[i] - q[i]) - (r[i] - 1);
	}

	for (i = 0; i < 4; i++) {
		sumxl = sumxl + x[i] * l[i];
	}

	for (i = 0; i < 4; i++) {
		if (tautranspsi[i] > 1e-10) {
			lngr[i] = q[i] * (1 - log(tautranspsi[i]) - s[i]);
		}
		else {
			lngr[i] = 0;
		}
		sq = x[i] * x[i];
		sq2 = phi[i] * phi[i];

		lngc[i] = log(r[i] / sumr) + 0.5 * z * q[i] * log(q[i] / r[i] * sumr / sumq) + l[i] - r[i] / sumr * sumxl;

		gamma[i] = exp(lngr[i] + lngc[i]);
		//gammaforexposure[i] = gamma[i];
	}
}

template <typename Real>  void calcPi0(Real* p_i0, Real& T)
#pragma ad indep T
#pragma ad dep p_i0
{
	Real A[4]; // {23.2256,21.91783,22.1001,21.07637}
	Real B[4]; // {-3835.18,-3080.66,-3654.62,-3151.09}
	Real C[4]; // {-45.343,-96.15,-45.392,-69.15}
	Real Temp = T;

	A[0] = 23.2256;
	A[1] = 21.91783;
	A[2] = 22.1001;
	A[3] = 21.07637;
	B[0] = -3835.18;
	B[1] = -3080.66;
	B[2] = -3654.62;
	B[3] = -3151.09;
	C[0] = -45.343;
	C[1] = -96.15;
	C[2] = -45.392;
	C[3] = -69.15;

	int i;
	for (i = 0; i < 4; i++) {
		p_i0[i] = exp(A[i] + B[i] / (Temp + C[i]));
	}
}

// here go all functions that include algebraic equations:

template <typename Real> void g_VLLE_fcn(Real* g, Real& time, Real* x, Real* y, Real *p, const std::vector<int>& mode) {
	constexpr int n_comp = 4;
	Real x_I[n_comp], x_II[n_comp], y_[n_comp], x_I_LLE[n_comp], x_II_LLE[n_comp], gamma_I_LLE[n_comp], gamma_II_LLE[n_comp], gamma_z[n_comp], z[n_comp], p_i0[n_comp], n[n_comp], lfrac_LLE, lfrac, T, H, P;
	Real sum_LLE_closure = 0;
	Real sum_y_closure = 0;
	//assign diff variables to specified variables: #: n_comp + 1
	H = x[0];
	for (int i = 0; i < n_comp; i++) {
		n[i] = x[i + 1];
	}
	//assign alg variables to specified variables: #: 6*n_comp + 3
	for (int i = 0; i < n_comp; i++) {
		x_I[i] = y[i];
		x_II[i] = y[n_comp + i];
		y_[i] = y[2 * n_comp + i];
		x_I_LLE[i] = y[3 * n_comp + i];
		x_II_LLE[i] = y[4 * n_comp + i];
		z[i] = y[5 * n_comp + i];
		lfrac_LLE = y[6 * n_comp];
		lfrac = y[6 * n_comp + 1];
		T = y[6 * n_comp + 2];
	}
	//assign parameters to specified paramters: #: 1
	P = p[0];

	//algebraic equations go here:
	calcgamma(gamma_I_LLE, x_I_LLE[0], x_I_LLE[1], x_I_LLE[2], x_I_LLE[3], T);
	calcgamma(gamma_II_LLE, x_II_LLE[0], x_II_LLE[1], x_II_LLE[2], x_II_LLE[3], T);
	calcgamma(gamma_z, z[0], z[1], z[2], z[3], T);
	calcPi0(p_i0, T);
	for (int i = 0; i < n_comp; i++) {
		g[i] = gamma_I_LLE[i] * x_I_LLE[i] - gamma_II_LLE[i] * x_II_LLE[i]; // liquid liquid equilibirum
		g[n_comp + i] = x_I_LLE[i] * (1 - lfrac_LLE) + x_II_LLE[i] * lfrac_LLE - z[i]; // mass balance for VLLE at boiling point (zero vapor fraction)
		if (mode[0] == 0) {
			g[2 * n_comp + i] = z[i] - x_I[i];
			g[3 * n_comp + i] = x_II_LLE[i] - x_II[i];
			g[4 * n_comp + i] = gamma_z[i] * z[i] - y_[i] * P / p_i0[i];
		}
		else if (mode[1] == 0) {
			g[2 * n_comp + i] = x_I_LLE[i] - x_I[i];
			g[3 * n_comp + i] = z[i] - x_II[i];
			g[4 * n_comp + i] = gamma_z[i] * z[i] - y_[i] * P / p_i0[i];
		}
		else {
			g[2 * n_comp + i] = x_I_LLE[i] - x_I[i];
			g[3 * n_comp + i] = x_II_LLE[i] - x_II[i];
			g[4 * n_comp + i] = gamma_I_LLE[i] * x_I_LLE[i] - y_[i] * P / p_i0[i];
		}
		g[5 * n_comp + i] = H * z[i] - n[i]; //dependence of species moles and overall molar fraction
		sum_LLE_closure = sum_LLE_closure + x_I_LLE[i] - x_II_LLE[i]; // sum for closure condition
		sum_y_closure = sum_y_closure + y_[i]; // sum for closure condition
	}
	g[6 * n_comp] = sum_LLE_closure; // closure condition sum(x_I_LLE - x_II_LLE) = 0
	g[6 * n_comp + 1] = sum_y_closure - 1; //closure condition sum(y) = 1
	if (mode[0] == 0) {
		g[6 * n_comp + 2] = lfrac;
	}
	else if (mode[1] == 0) {
		g[6 * n_comp + 2] = lfrac - 1;
		}
	else {
		g[6 * n_comp + 2] = lfrac - lfrac_LLE;
	}
}

//here go all functions that include differential equations:

template <typename Real> void f_stillPot_fcn(Real* f, Real& time, Real* x, Real* y, Real *p, const std::vector<int>& mode) {
	constexpr unsigned n_comp = 4;
	Real H, n[n_comp], y_[n_comp], V_S;
	//assign diff variables to specified variables H_s, n1, n2, n3, n4: #: n_comp + 1
	H = x[0];
	for (int i = 0; i < n_comp; i++) {
		n[i] = x[i + 1];
	}

	//assign alg variables to specified variables: #: n_comp
	for (int i = 0; i < n_comp; i++) {
		y_[i] = y[i];
	}
	//assign parameters to specified paramters: #: 1
	V_S = p[0];

	//diff equations go here:
	/*f[0] = -V_S; // diff eq: der(H_s) = -V_S
	for (int i = 0; i < n_comp; i++) {
		f[i + 1] = -V_S * y_[i]; // diff eqs der(n) = -V_S * y
	}*/
	//diff equations go here:
	//f[0] = -V_S; // diff eq: der(H_s) = -V_S
				 //for (int i = 0; i < n_comp; i++) {
				 //f[i + 1] = -V_S * y_[i]; // diff eqs der(n) = -V_S * y
	bool organic = true;
	if (organic) {
			f[0] = 0;
			f[1] = -8;
			f[2] = 6;
			f[3] = 0;
			f[4] = 2;
	}
    else
    {
			f[0] = 0;
			f[1] = 12;
			f[2] = -4;
			f[3] = 0;
			f[4] = -8;
	}
    for(unsigned i = 0; i < 5; ++i)
        f[i] *= mode[2];
}

