#include <iostream>
#include <cmath>

#include "FlashExampleTimeOptimalControlNdae.hpp"
#include "NdaeSolver.hpp"
#include "NdaeUtils.hpp"

#include "FC.h"

using namespace NdaeUtils;
// The code performs dynamic optimization of a flash example using a proximal bundle method MPBNGC.

#define MPBNGC FC_GLOBAL(mpbngc,MPBNGC)

extern "C" void MPBNGC(int& N, double *X, int *IX, double *BL,
	double *BU, int& M, int& MG, int& MC, int *IC,
	double *CL, double *CU, double *CG, double *F,
	void(*FASG)(int&, double*, int&, double*, double*, int&, int*, double*),
	double & RL, int& LMAX,
	double *GAM, double& EPS,
	double& FEAS, int& JMAX, int& NITER,
	int& NFASG, int& NOUT, int& IPRINT, int& IERR,
	int *IWORK, int& LIWORK, double *WORK, int& LWORK,
	int* IUSER, double *USER);

extern "C" void FASG(int& N, double* X, int& M, double* F, double * G, int& IERR, int* IUSER, double* USER);

int optimize_flash(std::vector<double>& X_vec, std::vector<double>& X_0);

int doSimulation(int N, double* X, bool doPlot, double* F, double * G);

using ObjectiveMode = FlashExampleNewThermoNdae<double>::ObjectiveMode;

int optimize_flash(std::vector<double>& X_vec, std::vector<double>& X_0)
{
	// optimization loop
	const int NN = 1;// static_cast<const int>(X_vec.size()); // Number of optimization variables (here, number of intervals in time horizon)
	int N = NN;// static_cast<const int> (NN);
	const int JJMAX = 5; // The maximum number of stored subgradients       (2 <=JJMAX)
	int JMAX = JJMAX;
	const int MM = 1; // The number of objective functions.              (1 <= MM)
	int M = MM;
	const int MMC = 0; // The number of linear constraints.               (0 <= MMC).
	int MC = MMC;
	const int MMG = 1; // The number of general constraint functions.     (0 <= MMG).
	int MG = MMG;
	const int MP1 = MM + 1;
	int NOUT = 6;
	int IPRINT = 4;
	const int LLIWORK = 2 * (MP1*(JJMAX + 1) + MMC + NN);
	int LIWORK = LLIWORK;
	const int LLWORK = NN*(NN + 2 * MP1*JJMAX + 2 * MP1 + 2 * MMC + 2 * MMG + 2 * MM + 31) / 2 + MP1*(6 * JJMAX + 10) + JJMAX + 5 * MMC + MMG + MM + 18;
	int LWORK = LLWORK;

	int NITER, NFASG, IERR, I, LMAX;
	int IC[MMC + 1], IWORK[LLIWORK], IUSER[1];


	double  RL, EPS, FEAS;
	double  X[NN], F[MM + MMG], GAM[MP1];
	double BL[NN]; // Lower bounds of X
	double BU[NN]; //  Upper bounds of X.
	int IX[NN]; //Types of box constraints for individual variables
	std::ofstream initSol("initialSolution.txt");
	for (int i = 0; i < NN; ++i) {
		BL[i] = 0;// 1e-12; // lower bound of reflux is actually 0
		BU[i] = 8000.0; // upper bound of reflux is actually 1
		X[i] = X_0[i];// inst.p_0[i]; // initial guess for reflux (required to be a feasible point)
		initSol << X[i] << endl;
		IX[i] = 3; // Two-side bound - BL(I) <= X(I) <= BU(I).
	}
	initSol.close();

	double  CG[NN*(MMC + 1)], CL[MMC + 1], CU[MMC + 1], WORK[LLWORK], USER[1];


	// define maximum number of iterations and maximum number of calls of function FASG
	NITER = 500;
	NFASG = 1000;

	IC[0] = 0;
	RL = 0.25;
	LMAX = 100;
	GAM[0] = GAM[1] = 0.5; // The MP1 vector of distance measure parameters   (0 <=GAM(I)).
	GAM[0] = 0.0;
						   //					   // set optimality tolerance
	EPS = 1.0E-06; // optimality tolerance
	FEAS = 1.0E-06; // feasibility tolerance

	//double G[NN];
	

	MPBNGC(N, X, IX, BL, BU,
		M, MG, MC, IC, CL,
		CU, CG, F, FASG, RL,
		LMAX, GAM, EPS, FEAS, JMAX,
		NITER, NFASG, NOUT, IPRINT, IERR,
		IWORK, LIWORK, WORK, LWORK, IUSER, USER);

	std::cout << "\n" << "IERR = " << IERR << "\n" << std::endl;
	
	std::ofstream ofile("optimalSolution.txt");
	for (int i = 0; i<N; ++i) {
		std::cout << "X(" << i + 1 << ") = " << X[i] << std::endl;
		X_vec[i] = X[i];
		ofile << X[i] << std::endl;
	}
	ofile.close();
	std::cout << "\n" << "F(X) = " << F[0] << std::endl;
	std::cout << "NITER = " << NITER << std::endl;
	std::cout << "NFASG = " << NFASG << std::endl;
	//int a; cin >> a;

	return IERR;
}

void FASG(int& N, double* X, int& M, double* F, double * G, int& IERR, int* IUSER, double* USER)
{
	// simulation with derivatives
	doSimulation(N, X, false, F, G);
}


int main()
{
  const unsigned n_p = FlashExampleNewThermoNdae<double>::N_P;
  FlashExampleNewThermoNdae<double> dae;

  std::vector<double> p0;
  p0.resize(n_p);
  for (int i = 0; i < n_p; ++i) {
   p0[i] = dae.p_0[i];
  }
  double time = 0.0;

  // vector for results of dynamic optimization
  std::vector<double> x_vec;
  x_vec.resize(n_p);

  // vector for intial values for dynamic optimization
  std::vector<double> x_0;
  x_0.resize(n_p);
  for (int i = 0; i < x_0.size(); ++i) {
	  x_0[i] = dae.p_0[i]; // 0.0
	  x_vec[i] = x_0[i];
  }


  optimize_flash(x_vec, x_0);
  
  std::vector<double> phi = {0.0};
  std::vector<double> dphi_dp(n_p,0.0);

  doSimulation(n_p, x_vec.data(), true, phi.data(), dphi_dp.data());

  std::ofstream ofile("finalSolution_MPBNGC.txt");
  ofile << "Objective value = " << phi[0] << std::endl;
  for (int i = 0; i < n_p; ++i) {
	  //std::cout << "X(" << i + 1 << ") = " << X[i] << std::endl;
	  //X_vec[i] = X[i];
	  ofile << x_vec[i] << ", " << dphi_dp[i] << std::endl;
  }
  ofile.close();

  return 0;
}

int doSimulation(int N, double* X, bool doPlot, double* F, double * G) {

	// create instance of dae
	const unsigned n_p = FlashExampleNewThermoNdae<double>::N_P;
	const unsigned n_x = FlashExampleNewThermoNdae<double>::N_X;
	const unsigned n_s = FlashExampleNewThermoNdae<double>::N_S;
	const unsigned n_y = FlashExampleNewThermoNdae<double>::N_Y;
	FlashExampleNewThermoNdae<double> dae;

	//set up options for integration
	NdaeSolver::Options options;
	options.SetMaxStepDenseOutput(1.0);
	options.SetTol(1e-8);
	options.SetStateEventTol(1e-8);
	options.SetMaxDiscontinuityTol(1e-7);
	options.SetDoPlot(doPlot);
	options.SetFilenameStates("SimulationResults_Flash.txt");
	

	if (N != n_p) {
		std::cout << "Number of parameters is n_p = " << n_p << " but there are N = " << N << " optimization variables." << std::endl;
		return -1;
	}

	// set parameters to values of X
	realtype p_0[n_p];
	for (int i = 0; i < n_p; ++i) {
		p_0[i] = X[i];
	}
	// create variables for objective (Phi) and its sensitivities (Phi_temp and Phi_p) w.r.t. optimization variables (parameters)
	realtype Phi = 0;
	realtype Phi_p[n_p];

	// evaluate objective and gradients
	dae.setObjMode(ObjectiveMode::ComputeObjective);
	auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, p_0, dae, options);

	// return objective function F and gradients w.r.t. parameters G
	F[0] = Phi;
	for (int i = 0; i < N; ++i) {
		G[i] = Phi_p[i];
		std::cout << X[i] << ", " << G[i] << std::endl;
	}

	// evaluate constraint and gradients
	realtype constr = 0;
	realtype constr_p[n_p];

	dae.setObjMode(ObjectiveMode::ComputeConstraint);
	auto flag2 = NdaeSolver::EvaluatePhiGradient(constr, constr_p, p_0, dae, options);

	// return objective function F and gradients w.r.t. parameters G
	F[1] = constr;
	for (int i = 0; i < N; ++i) {
		G[1*N+i] = constr_p[i];
		std::cout << X[i] << ", " << G[i] << std::endl;
	}

	return 0;
}