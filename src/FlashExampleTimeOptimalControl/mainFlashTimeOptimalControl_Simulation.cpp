#include <iostream>
#include <cmath>

#include "FlashExampleTimeOptimalControlNdae.hpp"
#include "NdaeSolver.hpp"
#include "NdaeUtils.hpp"

//#include "FC.h"

using namespace NdaeUtils;

double integrationTolerance = 1e-10;
double derivativeTolerance = integrationTolerance;
const int forwarddifferences = 0;
const int backwarddifferences = 1;
const int centraldifferences = 2;
int fd_type = backwarddifferences;

int doSimulation(int N, double* X, bool doPlot, double* F); //without derivatives
int doSimulation(int N, double* X, bool doPlot, double* F, double * G); //with derivatives

int check_derivatives(std::vector<double>& p);

int main()
{
  FlashExampleNewThermoNdae<double> dae;

  const unsigned n_p = FlashExampleNewThermoNdae<double>::N_P;
  std::vector<double> p0(n_p);
  for (size_t i = 0; i < n_p; ++i) {
   p0[i] = dae.p_0[i];
  }

  // vector for results of dynamic optimization
  std::vector<double> x_vec(n_p);

  // vector for intial values for dynamic optimization
  std::vector<double> x_0(n_p);
  for (size_t i = 0; i < x_0.size(); ++i) {
	  x_0[i] = dae.p_0[i]; // 0.0
	  x_vec[i] = x_0[i];
  }

  //check_derivatives(dae, x_vec, 1);
  check_derivatives(x_vec);

  std::vector<double> phi = {0.0};
  //// simulation without derivatives
  //doSimulation(n_p, x_vec.data(), true, phi.data());

  std::vector<double> dphi_dp(n_p,0.0);

  // simulation with derivatives
  doSimulation(n_p, x_vec.data(), true, phi.data(), dphi_dp.data());

  return 0;
}

int doSimulation(int N, double* X, bool doPlot, double* F) {

	// create instance of dae
	const unsigned n_p = FlashExampleNewThermoNdae<double>::N_P;

	FlashExampleNewThermoNdae<double> dae;

	//set up options for integration
	NdaeSolver::Options options;
	const double tolerance = integrationTolerance;
	options.SetTol(tolerance);
	options.SetStateEventTol(2.0*tolerance);
	options.SetMaxDiscontinuityTol(10.0*tolerance);
	options.SetDoPlot(doPlot);
	options.SetAbsTolAdjoint(10.0 * tolerance);
	options.SetRelTolAdjoint(10.0 * tolerance);
	options.SetFilenameStates("SimulationResults_Flash.txt");


	if (N != n_p) {
		std::cout << "Number of parameters is n_p = " << n_p << " but there are N = " << N << " optimization variables." << std::endl;
		return -1;
	}

	// set parameters to values of X
	realtype p_0[n_p];
	std::vector<double> p0(n_p, 0.0);
  for (size_t i = 0; i < n_p; ++i) {
		p_0[i] = X[i];
		p0[i] = X[i];
	}
	// create variables for objective (Phi)
	realtype Phi = 0;


	// run simulation
	auto flag = NdaeSolver::EvaluatePhi(Phi, p_0, dae, options);
  if (flag == NdaeSolver::Flag_Failure) {
    std::cerr << "error NdaeSolver::EvaluatePhi() failed" << std::endl;
  }
  else {
    std::cout << "Successful call to NdaeSolver::EvaluatePhi()." << std::endl;
  }

	// return objective function F
	F[0] = Phi;
  return 0;
}

int doSimulation(int N, double* X, bool doPlot, double* F, double * G) {

	// create instance of dae
	const unsigned n_p = FlashExampleNewThermoNdae<double>::N_P;
	FlashExampleNewThermoNdae<double> dae;

	//set up options for integration
	NdaeSolver::Options options;
	//options.SetMaxStepDenseOutput(0.01);
	const double tolerance = integrationTolerance;
	options.SetTol(tolerance);
	options.SetStateEventTol(2.0*tolerance);
	options.SetMaxDiscontinuityTol(10.0*tolerance);
	options.SetDoPlot(doPlot);
	options.SetAbsTolAdjoint(10.0 * tolerance);
	options.SetRelTolAdjoint(10.0 * tolerance);
	options.SetFilenameStates("SimulationResults_Flash.txt");
	

	if (N != n_p) {
		std::cout << "Number of parameters is n_p = " << n_p << " but there are N = " << N << " optimization variables." << std::endl;
		return -1;
	}

	// set parameters to values of X
	realtype p_0[n_p];
  for (size_t i = 0; i < n_p; ++i) {
		p_0[i] = X[i];
	}
	// create variables for objective (Phi) and its sensitivities (Phi_temp and Phi_p) w.r.t. optimization variables (parameters)
	realtype Phi = 0;
	realtype Phi_p[n_p];

	// run simulation
	auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, p_0, dae, options);
  if (flag == NdaeSolver::Flag_Failure) {
    std::cerr << "error NdaeSolver::EvaluatePhiGradient() failed" << std::endl;
  }
  else {
    std::cout << "Successful call to NdaeSolver::EvaluatePhiGradient()." << std::endl;
  }

	// return objective function F and gradients w.r.t. parameters G
	F[0] = Phi;
	for (int i = 0; i < N; ++i) {
		G[i] = Phi_p[i];
		//std::cout << X[i] << ", " << G[i] << std::endl;
	}
  return 0;
}
int check_derivatives(std::vector<double>& p) {
	// sensitivity calculation via adjoints
	std::vector<double> dphi_dp_adjoint(p.size(), 0.0);
	std::vector<double> phi = { 0.0 };
	doSimulation(p.size(), p.data(), false, phi.data(), dphi_dp_adjoint.data());

	// sensitivity calculation via finite differences
	std::vector<double> dphi_dp_fd(p.size(), 0.0);
	double h = sqrt(integrationTolerance);
	std::vector<double> p_fd(p);

	if (fd_type == forwarddifferences) {
		std::vector<double> phi_0 = { 0.0 };
		doSimulation(p.size(), p.data(), false, phi_0.data());

		std::vector<double> phi_temp = { 0.0 };
    for (size_t i = 0; i < p.size(); i++) {
			p_fd = p;
			p_fd[i] = p[i] * (1.0 + h);
			doSimulation(p.size(), p_fd.data(), false, phi_temp.data());
			dphi_dp_fd[i] = (phi_temp[0] - phi_0[0]) / (h*p[i]);
			//if ((dphi_dp_fd[i] - dphi_dp_adjoint[i])*(dphi_dp_fd[i] - dphi_dp_adjoint[i]) > derivativeTolerance) {
			//	std::cout << "dphi_dp_adj[" << i << "] = " << dphi_dp_adjoint[i] << ", dphi_dp_fd[" << i << "] = " << dphi_dp_fd[i] << "." << std::endl;
			//}
		}
		std::cout << "Comparing adjoint sensitivities to forward differences:" << std::endl;
    for (size_t i = 0; i < p.size(); i++) {
			if ((dphi_dp_fd[i] - dphi_dp_adjoint[i])*(dphi_dp_fd[i] - dphi_dp_adjoint[i]) > derivativeTolerance) {
				std::cout << "dphi_dp_adj[" << i << "] = " << dphi_dp_adjoint[i] << ", dphi_dp_fd[" << i << "] = " << dphi_dp_fd[i] << "." << std::endl;
			}
			else { std::cout << "Squared deviation[" << i << "] = " << (dphi_dp_fd[i] - dphi_dp_adjoint[i])*(dphi_dp_fd[i] - dphi_dp_adjoint[i]) << std::endl; }
		}
	}
	else if (fd_type == backwarddifferences) {
		std::vector<double> phi_0 = { 0.0 };
		doSimulation(p.size(), p.data(), false, phi_0.data());

		std::vector<double> phi_temp = { 0.0 };
    for (size_t i = 0; i < p.size(); i++) {
			p_fd = p;
			p_fd[i] = p[i] * (1.0 - h);
			doSimulation(p.size(), p_fd.data(), false, phi_temp.data());
			dphi_dp_fd[i] = (phi_0[0] - phi_temp[0]) / (h*p[i]);
			//if ((dphi_dp_fd[i] - dphi_dp_adjoint[i])*(dphi_dp_fd[i] - dphi_dp_adjoint[i]) > derivativeTolerance) {
			//	std::cout << "dphi_dp_adj[" << i << "] = " << dphi_dp_adjoint[i] << ", dphi_dp_fd[" << i << "] = " << dphi_dp_fd[i] << "." << std::endl;
			//}
		}
		std::cout << "Comparing adjoint sensitivities to backward differences:" << std::endl;
    for (size_t i = 0; i < p.size(); i++) {
			if ((dphi_dp_fd[i] - dphi_dp_adjoint[i])*(dphi_dp_fd[i] - dphi_dp_adjoint[i]) > derivativeTolerance) {
				std::cout << "dphi_dp_adj[" << i << "] = " << dphi_dp_adjoint[i] << ", dphi_dp_fd[" << i << "] = " << dphi_dp_fd[i] << "." << std::endl;
			}
			else { std::cout << "Squared deviation[" << i << "] = " << (dphi_dp_fd[i] - dphi_dp_adjoint[i])*(dphi_dp_fd[i] - dphi_dp_adjoint[i]) << std::endl; }
		}
	}
	else if (fd_type == centraldifferences) {
		//std::vector<double> phi_0 = { 0.0 };
		//doSimulation(p.size(), p.data(), false, phi_0.data());

		std::vector<double> phi_plus = { 0.0 };
		std::vector<double> phi_minus = { 0.0 };
    for (size_t i = 0; i < p.size(); i++) {
			p_fd = p;
			p_fd[i] = p[i] * (1.0 + h);
			doSimulation(p.size(), p_fd.data(), false, phi_plus.data());
			p_fd = p;
			p_fd[i] = p[i] * (1.0 - h);
			doSimulation(p.size(), p_fd.data(), false, phi_minus.data());
			dphi_dp_fd[i] = (phi_plus[0] - phi_minus[0]) / (2 * h * p[i]);
			//if ((dphi_dp_fd[i] - dphi_dp_adjoint[i])*(dphi_dp_fd[i] - dphi_dp_adjoint[i]) > derivativeTolerance) {
			//	std::cout << "dphi_dp_adj[" << i << "] = " << dphi_dp_adjoint[i] << ", dphi_dp_fd[" << i << "] = " << dphi_dp_fd[i] << "." << std::endl;
			//}
		}
		std::cout << "Comparing adjoint sensitivities to central differences:" << std::endl;
    for (size_t i = 0; i < p.size(); i++) {
			if ((dphi_dp_fd[i] - dphi_dp_adjoint[i])*(dphi_dp_fd[i] - dphi_dp_adjoint[i]) > derivativeTolerance) {
				std::cout << "dphi_dp_adj[" << i << "] = " << dphi_dp_adjoint[i] << ", dphi_dp_fd[" << i << "] = " << dphi_dp_fd[i] << "." << std::endl;
			}
			else { std::cout << "Squared deviation[" << i << "] = " << (dphi_dp_fd[i] - dphi_dp_adjoint[i])*(dphi_dp_fd[i] - dphi_dp_adjoint[i]) << std::endl; }
		}
	}

	return 0;
}
