#ifndef NDAE_LINEAR_SOLVER_HPP
#define NDAE_LINEAR_SOLVER_HPP


namespace NdaeLinearSolver {

template <typename TYPE>
inline void DecompositionLU(int N, TYPE *A, int *IP, int* ier)
//-----------------------------------------------------------------------
//  MATRIX TRIANGULARIZATION A=PLU BY GAUSSIAN ELIMINATION, BASED ON
//  SUBROUTINE DEC OF Ernst Hairer, modified by Ralf Hannemann-Tamas
//  INPUT..
//     n : ORDER OF MATRIX.
//     a : MATRIX TO BE TRIANGULARIZED, THE MATRIX a IS ASSUMED
//         TO BE ARRAY OF DIMENSION n*n, WHERE a HAS COLUMN MAJOR FORMAT
//  OUTPUT..
//     a[n*(j-1]+i-1], i <= j : UPPER TRIANGULAR FACTOR, U .
//     a[n*(j-1]+i-1], i > j : = MULTIPLIERS = LOWER TRIANGULAR FACTOR, I - L.
//     ip[k], 0 <= k < (n-1) : INDEX OF K-TH PIVOT ROW
//     ip[n-1] : (-1)^(NUMBER OF INTERCHANGES) OR O .
//     ier[0] : 0 IF MATRIX A IS NONSINGULAR, OR k IF FOUND TO BE
//           SINGULAR AT STAGE k.
//
//  USE FUNCTION SolveLU  TO OBTAIN SOLUTION OF LINEAR SYSTEM.
//  DETERMINANT(A) = ip(n)*a[0]*a[n-1+1]*...*a(n*(n-1)+n-1).
//  IF ip[n-1]=O, A IS SINGULAR, SOL WILL DIVIDE BY ZERO.
//
//  REFERENCE..
//     C. B. MOLER, ALGORITHM 423, LINEAR EQUATION SOLVER,
//     C.A.C.M. 15 (1972), P. 274.
//-----------------------------------------------------------------------
{
  TYPE T=0;
  TYPE U=0;
  int K=0;
  int I=0;
  int J=0;
  int M=0;
  int KP1=0;
  int NM1=0;
  int stop=0;
  int index1=0;
  int index2=0;



  ier[0]=0;
  NM1=N-1;
  IP[NM1]=1;

  for(K = 0; (K < NM1) && (stop == 0); K++) {
    KP1=K+1;
    M=K;
    for(I=KP1; I < N; I++) {
      index1=N*K+I;
      T=A[index1]; if (T < 0) {T=-T;}
      index1=N*K+M;
      U=A[index1]; if (U < 0) {U=-U;}
      if(T > U) {
        M=I;
      }
    }
    IP[K]=M;
    if (M != K) {
      IP[NM1]=-IP[NM1];
    }
    index1=N*K+M;
    index2=N*K+K;
    T=A[index1];
    A[index1]=A[index2];
    A[index2]=T;
    if(T != 0) {
      for (I=KP1; I < N; I++) {
        index1=N*K+I;
        A[index1] = -A[index1]/T;
      }
      //swap remaining elements of row
      for(J=0; J<K;J++) {
        index1=N*J+M;
        index2=N*J+K;
        T=A[index1];
        A[index1]=A[index2];
        A[index2]=T;
      }
      for (J=KP1; J<N; J++) {
        index1=N*J+M;
        index2=N*J+K;
        T=A[index1];
        A[index1]=A[index2];
        A[index2]=T;
        for (I=KP1; I<N; I++) {
          index1=N*J+I;
          index2=N*K+I;
          A[index1]=A[index1]+A[index2]*T;
        }
      }

    }
    else {
      IP[NM1]=-2;
      ier[0]=K+1;
      stop=1;
    }
  }

  index1=N*NM1+NM1;
  if(A[index1] == 0 ) {
    IP[NM1]=-2;
    ier[0]=K+1;
  }

}



template <typename TYPE>
inline void SolveLU(int N, TYPE* A, TYPE* B, int *IP)
{
  TYPE T=0;
  int NM1=0;
  int K=0;
  int M=0;
  int I=0;
  int index=0;

  NM1=N-1;

  //swap solution vector
  for (K=0;K < NM1; K++) {
    M=IP[K];
    T=B[M];
    B[M]=B[K];
    B[K]=T;
  }


  for (K=0;K < NM1; K++) {
    T=B[K];
    for (I=K+1; I < N; I++) {
      index=N*K+I;
      B[I]=B[I] + A[index]*T;
    }
  }

  for (K=NM1; K>0; K=K-1) {
    index=N*K+K;
    B[K] = B[K]/A[index];
    T = -B[K];
    for (I=0; I < K; I++) {
      index=N*K+I;
      B[I] = B[I] + A[index]*T;
    }
  }
  B[0]=B[0]/A[0];
}

template <typename TYPE>
inline void SolveTransposedLU(int N, TYPE* A, TYPE* B, int *IP)
{
  TYPE T=0;
  int NM1=0;
  int K=0;
  int M=0;
  int I=0;
  int index=0;

  NM1=N-1;

  //solve U'*x=B, replace B by x
  for (K=0;K < N; K++) {
    index=N*K+K;
    B[K]=B[K]/A[index];
    T=-B[K];
    for (I=K+1; I < N; I++) {
      index=N*I+K;
      B[I]=B[I] + A[index]*T;
    }
  }

  //solve L'*x=B, replace B by x
  for (K=NM1; K>=0; K=K-1) {
    index=N*K+K;
    B[K]=B[K];
    T=B[K];
    for (I=0; I < K; I++) {
      index=N*I+K;
      B[I] = B[I] + A[index]*T;
    }
  }

  //swap solution vector
  for (K=NM1-1;K >= 0; K=K-1) {
    M=IP[K];
    T=B[K];
    B[K]=B[M];
    B[M]=T;
  }

}

}

#endif //NDAE_LINEAR_SOLVER_HPP

