#ifndef NDAE_WRAPPER_HPP
#define NDAE_WRAPPER_HPP

#include "Ndae.hpp"
#include "NdaeUtils.hpp"
#include "NdaeLinearSolver.hpp"
#include <vector>

using namespace NdaeUtils;

template <typename Real>
class NdaeWrapper
{
public:  
  NdaeWrapper(Ndae<Real>& ndae);
  int get_itheta() { return m_Ndae.get_itheta(); }
  void set_itheta(int value) { return m_Ndae.set_itheta(value); }
  unsigned Eval_n_p() { return m_Ndae.Eval_n_p(); }
  unsigned Eval_n_x() { return m_Ndae.Eval_n_x(); }
  unsigned Eval_n_y() { return m_Ndae.Eval_n_y(); }
  unsigned Eval_n_Theta () { return m_Ndae.Eval_n_Theta(); }
  unsigned Eval_n_sigma() { return m_Ndae.Eval_n_sigma(); }
  void Set_ActiveSet(const std::vector<int>& mode) const { return m_Ndae.Set_ActiveSet(mode); };
  Real Eval_t0() { return m_Ndae.Eval_t0(); }
  void Eval_Theta(Real* Theta) { return m_Ndae.Eval_Theta(Theta); }
  void EvalInitialGuess_y(Real *y) { return m_Ndae.EvalInitialGuess_y(y); }
  std::vector<int> EvalInitialMode() { return m_Ndae.EvalInitialMode(); }
  std::vector<int> EvalInitialActiveSet(Real time, Real *x, Real *y, Real *p) { return m_Ndae.EvalInitialActiveSet(time, x, y, p); }
  std::vector<int> EvalRootDirection(const std::vector<int>& mode) { return m_Ndae.EvalRootDirection(mode); }
  std::vector<int> EvalMode(Real time, Real *x, Real *y, Real *p, const std::vector<int>& oldMode) { return m_Ndae.EvalMode(time, x, y, p, oldMode); }
  void Eval_f(Real *f, Real time, Real *x, Real *y, Real *p, const std::vector<int>& mode) { return m_Ndae.Eval_f(f,time,x,y,p,mode); }
  void Eval_g(Real *g, Real time, Real *x, Real *y, Real *p, const std::vector<int>& mode) { return m_Ndae.Eval_g(g,time,x,y,p,mode); }
  void Eval_sigma(Real* sigma, Real time, Real *x, Real *y, Real *p, const std::vector<int>& mode) { return m_Ndae.Eval_sigma(sigma,time,x,y,p,mode); }
  void Eval_psi(Real *psi, Real time, Real *x, Real *y, Real *p, const std::vector<int>& newMode, const std::vector<int>& oldMode){
    { return m_Ndae.Eval_psi(psi,time,x,y,p,newMode,oldMode); }
  }

  void Eval_psi0(Real *psi0, Real *p) { return m_Ndae.Eval_psi0(psi0,p); }
  void Eval_phi(Real& phi, Real *Theta, Real **X, Real **Y, Real *p) { return m_Ndae.Eval_phi(phi,Theta,X,Y,p); }

  void Eval_a1_f(Real *a1_f, Real time, Real& a1_time, Real *x, Real *a1_x,
                         Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode);

  void Eval_a1_g(Real *a1_g,Real time, Real& a1_time, Real *x, Real *a1_x,
                         Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode);

  void Eval_a1_sigma(Real* a1_sigma, Real time, Real& a1_time, Real *x, Real *a1_x,
                             Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode);

  void Eval_a1_psi(Real *a1_psi, Real time, Real& a1_time, Real *x, Real *a1_x,
                           Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& newMode, const std::vector<int>& oldMode);


  void Eval_a1_psi0(Real *a1_psi0, Real *p, Real *a1_p);


  void Eval_a1_phi(Real a1_phi, Real *Theta, Real **X, Real ** a1_X,
                           Real **Y, Real **a1_Y, Real *p, Real *a1_p);

  void Eval_f_x(Real **f_x, Real time, Real *x, Real *y, Real *p, const std::vector<int>& mode);
  void Eval_f_y(Real **f_y, Real time, Real *x, Real *y, Real *p, const std::vector<int>& mode);
  void Eval_g_x(Real **g_x, Real time, Real *x, Real *y, Real *p, const std::vector<int>& mode);
  void Eval_g_y(Real **g_y, Real time, Real *x, Real *y, Real *p, const std::vector<int>& mode);

  void Eval_Dt_sigma(Real& Dt_sigma, Real time, Real *x, Real *yhat, Real *p, const std::vector<int>& mode, unsigned indexActiveSigma);

  void Eval_a1_sigma_hat(Real* a1_sigma, Real* LU, int* IP, Real* yhat, Real time, Real& a1_time, Real *x, Real *a1_x,
                        Real *p, Real *a1_p, const std::vector<int>& mode);

  void Eval_a1_psi_hat(Real *a1_psi, Real* LU, int* IP, Real *yhat, Real time, Real& a1_time, Real *x, Real *a1_x,
                      Real *p, Real *a1_p, const std::vector<int>& newMode, const std::vector<int>& oldMode);

  void Eval_a1_phi_hat(Real a1_phihat,  Real **Yhat, Real *Theta, Real **X, Real ** a1_X,
                              Real *p, Real *a1_p, const std::vector<std::vector<int> >& modes);

  void Eval_a1_y_hat(Real* a1_yhat, Real* LU, int* IP, Real* yhat, Real time, Real& a1_time, Real *x, Real *a1_x,
                    Real *p, Real *a1_p, const std::vector<int>& mode);

  int DecompositionLU_g_y(Real *LU, int *IP, Real time, Real *x, Real *y, Real *p, const std::vector<int>& mode);

private:
  Ndae<Real>& m_Ndae;
  const unsigned m_n_p, m_n_x, m_n_y, m_n_Theta, m_n_sigma;
  std::vector<Real> m_f, m_a1_f, m_g, m_a1_g;
  std::vector<Real> m_a1_x, m_a1_y, m_a1_p;
  std::vector<Real> m_Theta, m_a1_Theta;
  std::vector<Real*> m_g_y;
};




template <typename Real> NdaeWrapper<Real>::NdaeWrapper(Ndae<Real>& ndae) :
m_Ndae(ndae),
m_n_p(ndae.Eval_n_p()),
m_n_x(ndae.Eval_n_x()),
m_n_y(ndae.Eval_n_y()),
m_n_sigma(ndae.Eval_n_sigma()),
m_n_Theta(ndae.Eval_n_Theta()),
m_f(ndae.Eval_n_x()),
m_a1_f(ndae.Eval_n_x()),
m_a1_x(ndae.Eval_n_x()),
m_g(ndae.Eval_n_y()),
m_a1_g(ndae.Eval_n_y()),
m_a1_y(ndae.Eval_n_y()),
m_a1_p(ndae.Eval_n_p()),
m_Theta(ndae.Eval_n_Theta()),
m_a1_Theta(ndae.Eval_n_Theta()),
m_g_y(ndae.Eval_n_y()){ }

template <typename Real> void NdaeWrapper<Real>::Eval_a1_f(Real *a1_f, Real time, Real& a1_time, Real *x, Real *a1_x,
                                                      Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode)
{
  a1_time = 0;
  ArrayCopy(a1_x,0.0,m_n_x);
  ArrayCopy(a1_y,0.0,m_n_y);
  ArrayCopy(a1_p,0.0,m_n_p);
  ArrayCopy(m_f.data(),0.0,m_n_x);
  ArrayCopy(m_a1_f.data(),a1_f,m_n_x);
  m_Ndae.Eval_a1_f(m_f.data(),m_a1_f.data(),time,a1_time,x,a1_x,y,a1_y,p,a1_p,mode);
}

template <typename Real> void NdaeWrapper<Real>::Eval_a1_g(Real *a1_g,Real time, Real& a1_time, Real *x, Real *a1_x,
                                                           Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode)
{
  a1_time = 0;
  ArrayCopy(a1_x,0.0,m_n_x);
  ArrayCopy(a1_y,0.0,m_n_y);
  ArrayCopy(a1_p,0.0,m_n_p);
  ArrayCopy(m_g.data(),0.0,m_n_y);
  ArrayCopy(m_a1_g.data(),a1_g,m_n_y);
  m_Ndae.Eval_a1_g(m_g.data(),m_a1_g.data(),time,a1_time,x,a1_x,y,a1_y,p,a1_p,mode);

}


template <typename Real> void NdaeWrapper<Real>::Eval_a1_sigma(Real* a1_sigma, Real time, Real& a1_time, Real *x, Real *a1_x,
                                                               Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode)
{

  std::vector<Real> sigma(m_n_sigma,0);
  a1_time = 0;
  ArrayCopy(a1_x,0.0,m_n_x);
  ArrayCopy(a1_y,0.0,m_n_y);
  ArrayCopy(a1_p,0.0,m_n_p);
  m_Ndae.Eval_a1_sigma(sigma.data(),a1_sigma,time,a1_time,x,a1_x,y,a1_y,p,a1_p,mode);
}


template <typename Real> void NdaeWrapper<Real>::Eval_a1_psi(Real *a1_psi, Real time, Real& a1_time, Real *x, Real *a1_x,
                                                             Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& newMode, const std::vector<int>& oldMode)
{

  a1_time = 0;
  ArrayCopy(a1_x,0.0,m_n_x);
  ArrayCopy(a1_y,0.0,m_n_y);
  ArrayCopy(a1_p,0.0,m_n_p);
  Real *psi_tmp = m_f.data();
  Real *a1_psi_tmp = m_a1_f.data();
  ArrayCopy(psi_tmp,0.0,m_n_x);
  ArrayCopy(a1_psi_tmp,a1_psi,m_n_x);
  m_Ndae.Eval_a1_psi(psi_tmp,a1_psi_tmp,time,a1_time,x,a1_x,y,a1_y,p,a1_p,newMode,oldMode);
}


template <typename Real> void NdaeWrapper<Real>::Eval_a1_psi0(Real *a1_psi0, Real *p, Real *a1_p)
{
  Real *psi0_tmp = m_f.data();
  Real *a1_psi0_tmp = m_a1_f.data();
  ArrayCopy(psi0_tmp,0.0,m_n_x);
  ArrayCopy(a1_psi0_tmp,a1_psi0,m_n_x);
  ArrayCopy(a1_p,0.0,m_n_p);
  m_Ndae.Eval_a1_psi0(psi0_tmp,a1_psi0_tmp,p,a1_p);
}


template <typename Real> void NdaeWrapper<Real>::Eval_a1_phi(Real a1_phi, Real *Theta, Real **X, Real ** a1_X,
                         Real **Y, Real **a1_Y, Real *p, Real *a1_p)
{

  double phi = 0;
  ArrayCopy(m_a1_Theta.data(),0.0,m_n_Theta);
  ArrayCopy(m_Theta.data(),Theta,m_n_Theta);

  for(unsigned i = 0; i < m_n_Theta; i++){
    ArrayCopy(a1_X[i],0.0,m_n_x);
    ArrayCopy(a1_Y[i],0.0,m_n_y);
  }
  ArrayCopy(a1_p,0.0,m_n_p);
  m_Ndae.Eval_a1_phi(phi,a1_phi,m_Theta.data(),m_a1_Theta.data(),X,a1_X,Y,a1_Y,p,a1_p);
}


template <typename Real> void NdaeWrapper<Real>::Eval_f_x(Real **f_x, Real time, Real *x, Real *y, Real *p, const std::vector<int>& mode)
{
//  for(unsigned i=0; i < m_n_x; i++){
//    double a1_time = 0;
//    ArrayCopy(m_f.data(),0.0,m_n_x);
//    ArrayCopy(m_a1_f.data(),0.0,m_n_x);
//    ArrayCopy(m_a1_x.data(),0.0,m_n_x);
//    ArrayCopy(m_a1_y.data(),0.0,m_n_y);
//    ArrayCopy(m_a1_p.data(),0.0,m_n_p);
//    m_a1_f[i] = 1.0;
//    m_Ndae.Eval_a1_f(m_f.data(),m_a1_f.data(),time,a1_time,x,m_a1_x.data(),y,m_a1_y.data(),p,m_a1_p.data(),mode);
//    for(unsigned j = 0; j < m_n_x; j++) {
//      f_x[j][i] = m_a1_x[j];
//    }

//  }
  double a1_time = 0;
  ArrayCopy(m_f.data(),0.0,m_n_x);
  ArrayCopy(m_a1_f.data(),0.0,m_n_x);
  ArrayCopy(m_a1_x.data(),0.0,m_n_x);
  ArrayCopy(m_a1_y.data(),0.0,m_n_y);
  ArrayCopy(m_a1_p.data(),0.0,m_n_p);
  for(unsigned i = 0; i < m_n_x;++i) {
      m_a1_x[i] = 1.0;
      m_Ndae.Eval_t1_f(m_f.data(),m_a1_f.data(),time,a1_time,x,m_a1_x.data(),y,m_a1_y.data(),p,m_a1_p.data(),mode);
      m_a1_x[i] = 0.0;
      for(unsigned j = 0; j < m_n_x; ++j) {
          f_x[i][j] = m_a1_f[j];
      }
  }
}


template <typename Real> void NdaeWrapper<Real>::Eval_f_y(Real **f_y, Real time, Real *x, Real *y, Real *p, const std::vector<int>& mode)
{
//  for(unsigned i=0; i < m_n_x; i++){
//    double a1_time = 0;
//    ArrayCopy(m_f.data(),0.0,m_n_x);
//    ArrayCopy(m_a1_f.data(),0.0,m_n_x);
//    ArrayCopy(m_a1_x.data(),0.0,m_n_x);
//    ArrayCopy(m_a1_y.data(),0.0,m_n_y);
//    ArrayCopy(m_a1_p.data(),0.0,m_n_p);
//    m_a1_f[i] = 1.0;
//    m_Ndae.Eval_a1_f(m_f.data(),m_a1_f.data(),time,a1_time,x,m_a1_x.data(),y,m_a1_y.data(),p,m_a1_p.data(),mode);
//    for(unsigned j = 0; j < m_n_y; j++) {
//      f_y[j][i] = m_a1_y[j];
//    }

//  }

  double a1_time = 0;
  ArrayCopy(m_f.data(),0.0,m_n_x);
  ArrayCopy(m_a1_f.data(),0.0,m_n_x);
  ArrayCopy(m_a1_x.data(),0.0,m_n_x);
  ArrayCopy(m_a1_y.data(),0.0,m_n_y);
  ArrayCopy(m_a1_p.data(),0.0,m_n_p);
  for(unsigned i = 0; i < m_n_y;++i) {
      m_a1_y[i] = 1.0;
      m_Ndae.Eval_t1_f(m_f.data(),m_a1_f.data(),time,a1_time,x,m_a1_x.data(),y,m_a1_y.data(),p,m_a1_p.data(),mode);
      m_a1_y[i] = 0.0;
      for(unsigned j = 0; j < m_n_x; ++j) {
          f_y[i][j] = m_a1_f[j];
      }
  }
}


template <typename Real> void NdaeWrapper<Real>::Eval_g_x(Real **g_x, Real time, Real *x, Real *y, Real *p, const std::vector<int>& mode)
{
//  for(unsigned i=0; i < m_n_y; i++){
//    double a1_time = 0;
//    ArrayCopy(m_g.data(),0.0,m_n_y);
//    ArrayCopy(m_a1_g.data(),0.0,m_n_y);
//    ArrayCopy(m_a1_x.data(),0.0,m_n_x);
//    ArrayCopy(m_a1_y.data(),0.0,m_n_y);
//    ArrayCopy(m_a1_p.data(),0.0,m_n_p);
//    m_a1_g[i] = 1.0;
//    m_Ndae.Eval_a1_g(m_g.data(),m_a1_g.data(),time,a1_time,x,m_a1_x.data(),y,m_a1_y.data(),p,m_a1_p.data(),mode);
//    for(unsigned j = 0; j < m_n_x; j++) {
//      g_x[j][i] = m_a1_x[j];
//    }

//  }
    double a1_time = 0;
    ArrayCopy(m_g.data(),0.0,m_n_y);
    ArrayCopy(m_a1_g.data(),0.0,m_n_y);
    ArrayCopy(m_a1_x.data(),0.0,m_n_x);
    ArrayCopy(m_a1_y.data(),0.0,m_n_y);
    ArrayCopy(m_a1_p.data(),0.0,m_n_p);
    for(unsigned i = 0; i < m_n_x;++i) {
        m_a1_x[i] = 1.0;
        m_Ndae.Eval_t1_g(m_g.data(),m_a1_g.data(),time,a1_time,x,m_a1_x.data(),y,m_a1_y.data(),p,m_a1_p.data(),mode);
        m_a1_x[i] = 0.0;
        for(unsigned j = 0; j < m_n_y; ++j) {
            g_x[i][j] = m_a1_g[j];
        }
    }
}

template <typename Real> void NdaeWrapper<Real>::Eval_g_y(Real **g_y, Real time, Real *x, Real *y, Real *p, const std::vector<int>& mode)
{
//  for(unsigned i=0; i < m_n_y; i++){
//    double a1_time = 0;
//    ArrayCopy(m_g.data(),0.0,m_n_y);
//    ArrayCopy(m_a1_g.data(),0.0,m_n_y);
//    ArrayCopy(m_a1_x.data(),0.0,m_n_x);
//    ArrayCopy(m_a1_y.data(),0.0,m_n_y);
//    ArrayCopy(m_a1_p.data(),0.0,m_n_p);
//    m_a1_g[i] = 1.0;
//    m_Ndae.Eval_a1_g(m_g.data(),m_a1_g.data(),time,a1_time,x,m_a1_x.data(),y,m_a1_y.data(),p,m_a1_p.data(),mode);
//    for(unsigned j = 0; j < m_n_y; j++) {
//      g_y[j][i] = m_a1_y[j];
//    }

//  }
    double a1_time = 0;
    ArrayCopy(m_g.data(),0.0,m_n_y);
    ArrayCopy(m_a1_g.data(),0.0,m_n_y);
    ArrayCopy(m_a1_x.data(),0.0,m_n_x);
    ArrayCopy(m_a1_y.data(),0.0,m_n_y);
    ArrayCopy(m_a1_p.data(),0.0,m_n_p);
    for(unsigned i = 0; i < m_n_y;++i) {
        m_a1_y[i] = 1.0;
        m_Ndae.Eval_t1_g(m_g.data(),m_a1_g.data(),time,a1_time,x,m_a1_x.data(),y,m_a1_y.data(),p,m_a1_p.data(),mode);
        m_a1_y[i] = 0.0;
        for(unsigned j = 0; j < m_n_y; ++j) {
            g_y[i][j] = m_a1_g[j];
        }
    }
}

template<typename Real> int NdaeWrapper<Real>::DecompositionLU_g_y(Real *LU, int *IP, Real time, Real *x, Real *y, Real *p, const std::vector<int>& mode)
{
  int ier = 0;

  for(unsigned i = 0; i < m_n_y; i++) {
    m_g_y[i] = &LU[i * m_n_y];
  }
  Eval_g_y(m_g_y.data(),time,x,y,p,mode);
  NdaeLinearSolver::DecompositionLU(m_n_y,LU,IP,&ier);
  return ier;
}


template<typename Real>  void NdaeWrapper<Real>::Eval_a1_y_hat(Real* a1_yhat, Real* LU, int* IP, Real* yhat, Real time,
                                                               Real& a1_time, Real *x, Real *a1_x,
                                                               Real *p, Real *a1_p, const std::vector<int>& mode)
{

  for(unsigned i = 0; i < m_n_y; i++) {
    m_a1_g[i] = -a1_yhat[i];
  }

  NdaeLinearSolver::SolveTransposedLU(m_n_y,LU,m_a1_g.data(),IP);

  ArrayCopy(m_g.data(),0.0,m_n_y);
  a1_time = 0;
  ArrayCopy(m_a1_y.data(),0.0,m_n_y);
  ArrayCopy(a1_x,0.0,m_n_x);
  ArrayCopy(a1_p,0.0,m_n_p);

  m_Ndae.Eval_a1_g(m_g.data(),m_a1_g.data(),time,a1_time,x,a1_x,yhat,m_a1_y.data(),p,a1_p,mode);
}

template<typename Real> void NdaeWrapper<Real>::Eval_a1_sigma_hat(Real* a1_sigma, Real* LU, int* IP, Real* yhat, Real time, Real& a1_time, Real *x, Real *a1_x,
                                                                  Real *p, Real *a1_p, const std::vector<int>& mode)
{
  const unsigned n_y = Eval_n_y();
  const unsigned n_x = Eval_n_x();
  const unsigned n_p = Eval_n_p();
  std::vector<Real> tmp_a1_y(n_y,0.0);
  std::vector<Real> tmp_a1_x(n_x,0.0);
  std::vector<Real> tmp_a1_p(n_p,0.0);
  Real tmp_a1_time = 0;

  Eval_a1_sigma(a1_sigma,time,tmp_a1_time,x,&tmp_a1_x[0],yhat,&tmp_a1_y[0],p,&tmp_a1_p[0],mode);
  Eval_a1_y_hat(&tmp_a1_y[0],LU,IP,yhat,time,a1_time,x,a1_x,p,a1_p,mode);

  // build up sum

  for(unsigned i = 0; i < n_x; i++) a1_x[i] += tmp_a1_x[i];
  for(unsigned i = 0; i < n_p; i++) a1_p[i] += tmp_a1_p[i];
  a1_time += tmp_a1_time;


}

template<typename Real> void NdaeWrapper<Real>::Eval_a1_psi_hat(Real *a1_psi, Real* LU, int* IP, Real* yhat, Real time, Real& a1_time, Real *x, Real *a1_x,
                          Real *p, Real *a1_p, const std::vector<int>& newMode, const std::vector<int>& oldMode)
{

  std::vector<Real> tmp_a1_y(m_n_y,0.0);
  std::vector<Real> tmp_a1_x(m_n_x,0.0);
  std::vector<Real> tmp_a1_p(m_n_p,0.0);
  Real tmp_a1_time = 0;

  Eval_a1_psi(a1_psi,time,tmp_a1_time,x,&tmp_a1_x[0],yhat,&tmp_a1_y[0],p,&tmp_a1_p[0],newMode,oldMode);
  Eval_a1_y_hat(&tmp_a1_y[0],LU,IP,yhat,time,a1_time,x,a1_x,p,a1_p,oldMode);

  // build up sum

  for(unsigned i = 0; i < m_n_x; i++) a1_x[i] += tmp_a1_x[i];
  for(unsigned i = 0; i < m_n_p; i++) a1_p[i] += tmp_a1_p[i];
  a1_time += tmp_a1_time;

}


template<typename Real> void NdaeWrapper<Real>::Eval_a1_phi_hat(Real a1_phihat, Real** Yhat, Real *Theta, Real **X, Real ** a1_X,
                                Real *p, Real *a1_p, const std::vector<std::vector<int> >& modes)
{

  std::vector<Real*> tmp_a1_Y(m_n_Theta);
  std::vector<Real> data_tmp_a1_Y(m_n_y*m_n_Theta,0.0);
  std::vector<Real> tmp_a1_x(m_n_x);
  for(unsigned i = 0; i < m_n_Theta; i++) {
    tmp_a1_Y[i] = &data_tmp_a1_Y[i*m_n_y];
  }
  std::vector<Real> tmp_a1_p(m_n_p,0.0);
  std::vector<Real> LU(m_n_y*m_n_y);
  std::vector<int> IP(m_n_y);
  Real a1_time = 0;


  Eval_a1_phi(a1_phihat,Theta,X,a1_X,Yhat,&tmp_a1_Y[0],p,a1_p);

  for(unsigned i = 0; i < m_n_Theta; i++) {
    ArrayCopy(&LU[0],0.0,m_n_y*m_n_y);
    ArrayCopy(&IP[0],0,m_n_y);
	//std::vector<int> tempMode = Mode;
    DecompositionLU_g_y(&LU[0],&IP[0],Theta[i],X[i],Yhat[i],p,modes[i]);
    Eval_a1_y_hat(tmp_a1_Y[i],&LU[0],&IP[0],Yhat[i],Theta[i],a1_time,X[i],&tmp_a1_x[0],p,&tmp_a1_p[0],modes[i]);
    for(unsigned j = 0; j < m_n_x; j++) a1_X[i][j] += tmp_a1_x[j];
    for(unsigned j = 0; j < m_n_p; j++) a1_p[j] += tmp_a1_p[j];
  }

}


template<typename Real>
void NdaeWrapper<Real>::Eval_Dt_sigma(Real& Dt_sigma, Real time, Real *x, Real *yhat, Real *p, const std::vector<int>& mode, unsigned indexActiveSigma)
{
  std::vector<Real> f(m_n_x);

  Eval_f(f.data(),time,x,yhat,p,mode);

  std::vector<Real> LU(m_n_y*m_n_y,0.0);
  std::vector<int>  IP(m_n_y,0);

  DecompositionLU_g_y(LU.data(),IP.data(),time,x,yhat,p,mode);

  std::vector<Real> a1_sigma(m_n_sigma, 0);
  a1_sigma[indexActiveSigma] = 1.0;
 
  Real a1_time = 0;
  std::vector<Real> a1_x(m_n_x,0.0);
  std::vector<Real> a1_p(m_n_p);

  Eval_a1_sigma_hat(a1_sigma.data(),LU.data(),IP.data(),yhat,time,a1_time,x,a1_x.data(),p,a1_p.data(),mode);

  Dt_sigma = a1_time;
  for(unsigned i = 0; i < m_n_x; i++){
    Dt_sigma += a1_x[i]*f[i];
  }

}

#endif // NDAE_WRAPPER_HPP
