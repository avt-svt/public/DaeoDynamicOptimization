#ifndef FLASH_EXAMPLE_NDAE_HPP
#define FLASH_EXAMPLE_NDAE_HPP
#include <iostream>
#include "a1_Fcn.hpp"

using namespace std;

#include "Ndae.hpp"
#include <math.h>
//#include <cmath>
enum GEModel { NRTL, Margules };

template<typename Real>
class DistillationColumnNdae : public Ndae<Real>
{
public:
	DistillationColumnNdae() {
		for (int i = 0; i < N_P; i++) {
			p_0[i] = 0.0;
		}

		for (int i = N_P / 2; i < N_P; i++) {
			p_0[i] = 0.0;
		}

		//p_0[0] = 1; // -2.31373e+08
		//p_0[1] = 1; // -2.94015e+07
		//p_0[2] = 1; // -4.56269e+06
		//p_0[3] = 0.943196; // 0.676747
		//p_0[4] = 0.81043; // 0.665279
		//p_0[5] = 0.66861; // -0.0658395
		//p_0[6] = 0.577923; // 0.528406
		//p_0[7] = 0.570362; // 0.465787
		//p_0[8] = 0.57816; // 0.454677
		//p_0[9] = 0.584939; // 0.251666
		//p_0[10] = 1; // -1.0077e+07
		//p_0[11] = 1; // -430043
		//p_0[12] = 1; // -138161
		//p_0[13] = 1; // -92105
		//p_0[14] = 0.884006; // 0.154535
		//p_0[15] = 0.217452; // 0.207654
		//p_0[16] = 0; // 8643.67
		//p_0[17] = 0; // 11612.4
		//p_0[18] = 0; // 6179.35
		//p_0[19] = 0; // 2133.39

		p_0[0] = 1; // -232527
		p_0[1] = 1; // -29648
		p_0[2] = 1; // -4610.29
		p_0[3] = 0.944075; // 0.000115774
		p_0[4] = 0.8111; // 9.99764e-05
		p_0[5] = 0.669097; // -0.000114061
		p_0[6] = 0.577979; // 2.25124e-05
		p_0[7] = 0.570299; // 2.30156e-05
		p_0[8] = 0.57811; // -6.68403e-05
		p_0[9] = 0.584912; // 0.000154535
		p_0[10] = 1; // -10484.9
		p_0[11] = 1; // -433.846
		p_0[12] = 1; // -138.401
		p_0[13] = 1; // -92.6344
		p_0[14] = 0.888015; // -8.32862e-06
		p_0[15] = 0.218987; // 1.40592e-05
		p_0[16] = 0; // 8.63904
		p_0[17] = 0; // 11.6622
		p_0[18] = 0; // 6.21409
		p_0[19] = 0; // 2.15013

		for (int iTray = 0; iTray < nTrays; ++iTray) {
			mode0[iTray * 4] = vfrac(x_0, y_0, iTray)>1e-6;
			mode0[iTray * 4 + 1] = vfrac(x_0, y_0, iTray) < 0.99999;
			mode0[iTray * 4 + 2] = ((1 - vfrac(x_0, y_0, iTray))*M(x_0, y_0, iTray)) > M_min[iTray];
			mode0[iTray * 4 + 3] = H_0V(x_0, y_0, iTray)>0;
		}
		p_F[4] = 0;
		p_h_F[4] = -6333.49;
		const std::vector<double> z_f = {0.79,0.2,0.01};
		for (int jComp = 0; jComp < nC; ++jComp) {
			p_z_F[4][jComp] = z_f[jComp];
		}
	}
	virtual ~DistillationColumnNdae() {}

  virtual unsigned Eval_n_p() const override;
  virtual unsigned Eval_n_x() const override;
  virtual unsigned Eval_n_y() const override;
  virtual unsigned Eval_n_Theta() const override;
  virtual unsigned Eval_n_sigma() const override;
  virtual Real Eval_t0() const override;
  virtual void Eval_Theta(Real *Theta) const override;
	//virtual void Eval_i_Theta(int itheta) { set_iP(itheta); };

  virtual std::vector<int> EvalMode(Real time, Real *x, Real *y, Real *p, const std::vector<int>& oldMode) const override;
  virtual std::vector<int> EvalRootDirection(const std::vector<int>& mode) const override;
  virtual void EvalInitialGuess_y(Real *y) const override;
  virtual std::vector<int> EvalInitialMode() const override;

	template<typename T>
  void Eval_f_impl(T *f, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const;
	virtual void Eval_f(Real *f, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const override;

	template<typename T>
  void Eval_f_tray(T *f, T& time, T *x, T *y, T *p, const std::vector<int>& mode, const int iTray) const;

	template<typename T>
  void Eval_g_impl(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const;
  virtual void Eval_g(Real *g, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const override;

	template<typename T>
  void Eval_g_VLE(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode, const int iTray) const;

	template<typename T>
  void Eval_g_tray(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode, const int iTray) const;

	template<typename T>
  void Eval_g_condenser(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const;

	template<typename T>
	void Eval_sigma_impl(T* sigma, T& time,
    T* x, T *y, T* p, const std::vector<int>& mode) const;
  virtual void Eval_sigma(Real* sigma, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const override;

	template<typename T>
	void Eval_psi_impl(T *psi, T& time, T* x, T *y,
    T* p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const;
  virtual void Eval_psi(Real *psi, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const override;

	template<typename T>
  void Eval_psi0_impl(T *psi0, T* p) const;
  virtual void Eval_psi0(Real *psi0, Real *p) const override;

	template<typename T>
  void Eval_phi_impl(T& phi, T *Theta, T **X, T **Y, T *p) const;
  virtual void Eval_phi(Real& phi, Real *Theta, Real **X, Real **Y, Real *p) const override;


	virtual void Eval_a1_f(Real *f, Real *a1_f, Real& time, Real& a1_time, Real *x, Real *a1_x,
    Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode) const override;

	virtual void Eval_a1_g(Real *g, Real *a1_g, Real& time, Real& a1_time, Real *x, Real *a1_x,
    Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode) const override;

	virtual void Eval_a1_sigma(Real* sigma, Real* a1_sigma, Real& time, Real& a1_time, Real *x, Real *a1_x,
    Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode) const override;

	virtual void Eval_a1_psi(Real * psi, Real *a1_psi, Real& time, Real& a1_time, Real *x, Real *a1_x,
    Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const override;

  virtual void Eval_a1_psi0(Real *psi0, Real *a1_psi0, Real *p, Real* a1_p) const override;
	virtual void Eval_a1_phi(Real& phi, Real& a1_phi, Real *Theta, Real *a1_Theta, Real **X, Real ** a1_X,
    Real **Y, Real **a1_Y, Real *p, Real *a1_p) const override;

	virtual void Eval_t1_f(Real *f, Real *t1_f, Real& time, Real& t1_time, Real *x, Real *t1_x,
    Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const override;

	virtual void Eval_t1_g(Real *g, Real *t1_g, Real& time, Real& t1_time, Real *x, Real *t1_x,
    Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const override;

	virtual void Eval_t1_sigma(Real* sigma, Real* t1_sigma, Real& time, Real& t1_time, Real *x, Real *t1_x,
    Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const override;

	virtual void Eval_t1_psi(Real * psi, Real *t1_psi, Real& time, Real& t1_time, Real *x, Real *t1_x,
		Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& newMode,
    const std::vector<int>& oldMode) const override;

  virtual void Eval_t1_psi0(Real *psi0, Real *t1_psi0, Real *p, Real* t1_p) const override;

	virtual void Eval_t1_phi(Real& phi, Real& t1_phi, Real *Theta, Real *t1_Theta, Real **X, Real ** t1_X,
    Real **Y, Real **t1_Y, Real *p, Real *t1_p) const override;


public:
	static const int nC = 3; // number of components
	static const int nTrays = 20; // number of trays
	static const int NTRAYVARS = 5 * nC + 21; // number of algebraic variables on each tray
	static const int NCONDENSERVARS = nC + 4; // number of unknowns for condenser: y_i,h_out,F_out,Q_in,T
	//static const int n_comp = nC;
	static const int N_X = nTrays*(nC + 1)+3; // number of differential vars (tray variables + objective function)
	static const int N_Y = nTrays*NTRAYVARS+NCONDENSERVARS+4; // number algebraic vars (4 additional variables of interest)
	static const int NINTERVALS = 10;// 40; // number of intervals for parametrization
	static const int N_P = 2*NINTERVALS; // number of total parameters
	static const int N_THETA = NINTERVALS; // numebr of THETA which is number of time points where the objective functional is evaluated
	static constexpr int N_S = nTrays * 4; // number of switching functions

	int int_p = 0;

	//static const int i_s = 6 * n_comp + 1; // index of y to get s = y[i_s]

	// model parameters
	const double pi = 3.14159265359;
	//const double p_temperature = 100; //temperature in K
	//const double p_pressure = 553321.0; //pressure in Pa
	const double p_out = 5.5e5; //pressure in Pa
	const double p_z_i[nC] = { 0.729922, 0.258229, 0.0118489 };
	std::vector<double> p_z_F_i = std::vector<double>(nC, 1/((double)nC));
	std::vector<double> p_F = std::vector<double>(nTrays, 0.0); //feed rate for each tray
	std::vector<double> p_h_F = std::vector<double>(nTrays, 0.0); //enthalpy of feed for each tray
	std::vector<std::vector<double>> p_z_F = std::vector<std::vector<double>>(nTrays, p_z_F_i); //composition of feed for each tray

	//vapor inlet
	double p_V_in = 60.0; //mol/s << control of dynamic optimization problem
	double p_V_in_start = 60.0;
	// vapor with constant enthalpy and composition
	double p_h_V_in = -5172.63; //J/mol 
	std::vector<double> p_y_V_in = { 0.79,0.2,0.01 };

	// simulation horizon
	double deltat = 360.0;
	double tf = double(NINTERVALS) * deltat;
	double t0 = 0.0;
	

	// parameters related to dynamic optimization
	const double n_set = 50.0; // set point for product flow rate [mol/s]
	const double x_set = 0.995; // set point for product purity

	const double weight_n = 0.001;// / (n_set*n_set); //weighting parameter for product flow deviation in objective function
	const double weight_x = 1.0;// / (x_set*x_set); //weighting parameter for product purity deviation in objective function
	const double weight_obj = 1.0;// / 1000.0;// / 10000.0;
	const double splitfactor_ub = 0.8;
	const double splitfactor_lb = 0.0;

	const double n_flow_ub = 120.0;
	const double n_flow_lb = 80.0;

	// column related parameters
	const std::vector<double> kd_tray = std::vector<double>(nTrays, 0.0001); //hydraulic parameter
	const std::vector<double> alpha_v = std::vector<double>(nTrays, 3);
	const std::vector<double> alpha_l = std::vector<double>(nTrays, 0.01);
	const std::vector<double> alpha_w = std::vector<double>(nTrays, 1.0);
	const std::vector<double> X = std::vector<double>(nTrays, 0.0);
	const std::vector<double> Y = std::vector<double>(nTrays, 0.0);
	const std::vector<double> diameter = std::vector<double>(nTrays, 1.0); // column diameter
	const std::vector<double> height = std::vector<double>(nTrays, 1.0); // tray height
	const std::vector<double> M_min = std::vector<double>(nTrays, 100); // minimum amount of substance on each tray

	// thermodynamic parameters
	const GEModel gemodel = Margules;
	const bool considerExcessEnthalpy = false;
	const double NRTLA1[nC][nC] = { { 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 } };
	const double NRTLA2[nC][nC] = { { 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 } };
	const double NRTLG1[nC][nC] = { { 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 } };
	const double NRTLG2[nC][nC] = { { 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 } };
	const double NRTLG3[nC][nC] = { { 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 } };
	const double NRTLG4[nC][nC] = { { 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 },{ 0.0, 0.0, 0.0 } };
	const double RGAS = 1.0;
	const double R_ = 8.3144598;

	const double MargulesA0 = 8.3;
	const double MargulesA1 = 7.5;
	const double MargulesA2 = 7.0;

	const double MargulesA[nC][nC] = { { 0.0, 1.0*MargulesA0, 1.0*MargulesA2 },{ MargulesA0, 0.0, 1.0*MargulesA1 },{ MargulesA2, MargulesA1, 0.0 } };

	const double ANT[nC][12] = {
		{ 0.0, 1.0, 2.718281828, 58.282, -1084.1, 0.0, 0.0, -8.3144, 0.044127,
		1.0, 0.0, 600.0 },
		{ 0.0, 1.0, 2.718281828, 51.245, -1200.2, 0.0, 0.0, -6.4361, 0.028405,
		1.0, 0.0, 600.0 },
		{ 0.0, 1.0, 2.718281828, 42.127, -1093.1, 0.0, 0.0, -4.1425, 5.7254e-5,
		2.0, 0.0, 600.0 } };
	const double t_Bezug = 298.15;
	const double KRIT[nC][4] = { { 126.2, 3353000, 0.28971, 0.0 },
	{ 154.58, 4980000, 0.28962, 0.0 },
	{ 150.86, 4810000, 0.29216, 0.0 } };
	const double DHFORM[nC] = { 0.0, 0.0, 0.0 };
	const double CPIGDP[nC][7] = {
		{ 29.105, 8.6149, 1701.6, 0.10347, 909.79, 0.0, 1500.0 },
		{ 29.103, 10.040, 2526.5, 9.356, 1153.8, 0.0, 1500.0 },
		{ 20.786, 0.0, 0.0, 0.0, 0.0, 0.0, 1500.0 } };
	const double DHVLDP[nC][7] = {
		{ 7490.5, 0.40406, -0.317, 0.27343, 0.0, 0.0, 1500.0 },
		{ 9008, 0.4542, -0.4096, 0.3183, 0.0, 0.0, 1500.0 },
		{ 8421.5, 0.28333, 0.033281, 0.030551, 0.0, 0.0, 1500.0 } };
	const double R_alg = 8314.3;
	const double k_deltah0vap = 20;

	// initial guesses
	double y_0[N_Y] = {
		0.790000, //0
		0.200000, //1
		0.010000, //2
		0.610835, //3
		0.373664, //4
		0.015502, //5
		1.000000, //6
		0.280902, //7
		120.000000, //8
		552133.300000, //9
		-5172.630000, //10
		-8630.214000, //11
		-5172.630000, //12
		553.386600, //13
		52583.370000, //14
		553.386600, //15
		0.010000, //16
		80.000000, //17
		-5172.630000, //18
		0.790000, //19
		0.200000, //20
		0.010000, //21
		80.000000, //22
		0.000000, //23
		-10888.700000, //24
		0.790000, //25
		0.200000, //26
		0.010000, //27
		0.000000, //28
		0.000000, //29
		0.000000, //30
		0.333333, //31
		0.333333, //32
		0.333333, //33
		6400.000000, //34
		0.000000, //35
		0.790000, //36
		0.200000, //37
		0.010000, //38
		0.610835, //39
		0.373664, //40
		0.015502, //41
		1.000000, //42
		0.281987, //43
		120.000000, //44
		554266.700000, //45
		-5172.630000, //46
		-8630.214000, //47
		-5172.630000, //48
		555.524800, //49
		52583.370000, //50
		555.524800, //51
		0.010000, //52
		80.000000, //53
		-5172.630000, //54
		0.790000, //55
		0.200000, //56
		0.010000, //57
		80.000000, //58
		0.000000, //59
		-8630.214000, //60
		0.610835, //61
		0.373664, //62
		0.015502, //63
		0.000000, //64
		0.000000, //65
		0.000000, //66
		0.333333, //67
		0.333333, //68
		0.333333, //69
		6400.000000, //70
		-0.000000, //71
		0.790000, //72
		0.200000, //73
		0.010000, //74
		0.610835, //75
		0.373664, //76
		0.015502, //77
		1.000000, //78
		0.283072, //79
		120.000000, //80
		556400.000000, //81
		-5172.630000, //82
		-8630.214000, //83
		-5172.630000, //84
		557.662900, //85
		52583.370000, //86
		557.662900, //87
		0.010000, //88
		80.000000, //89
		-5172.630000, //90
		0.790000, //91
		0.200000, //92
		0.010000, //93
		80.000000, //94
		0.000000, //95
		-8630.214000, //96
		0.610835, //97
		0.373664, //98
		0.015502, //99
		0.000000, //100
		0.000000, //101
		0.000000, //102
		0.333333, //103
		0.333333, //104
		0.333333, //105
		6400.000000, //106
		-0.000000, //107
		0.790000, //108
		0.200000, //109
		0.010000, //110
		0.610835, //111
		0.373664, //112
		0.015502, //113
		1.000000, //114
		0.284158, //115
		120.000000, //116
		558533.300000, //117
		-5172.630000, //118
		-8630.214000, //119
		-5172.630000, //120
		559.801100, //121
		52583.370000, //122
		559.801100, //123
		0.010000, //124
		80.000000, //125
		-5172.630000, //126
		0.790000, //127
		0.200000, //128
		0.010000, //129
		80.000000, //130
		0.000000, //131
		-8630.214000, //132
		0.610835, //133
		0.373664, //134
		0.015502, //135
		0.000000, //136
		0.000000, //137
		0.000000, //138
		0.333333, //139
		0.333333, //140
		0.333333, //141
		6400.000000, //142
		0.000000, //143
		0.790000, //144
		0.200000, //145
		0.010000, //146
		0.610835, //147
		0.373664, //148
		0.015502, //149
		1.000000, //150
		0.285243, //151
		120.000000, //152
		560666.700000, //153
		-5172.630000, //154
		-8630.214000, //155
		-5172.630000, //156
		561.939300, //157
		52583.370000, //158
		561.939300, //159
		0.010000, //160
		80.000000, //161
		-5172.630000, //162
		0.790000, //163
		0.200000, //164
		0.010000, //165
		80.000000, //166
		0.000000, //167
		-8630.214000, //168
		0.610835, //169
		0.373664, //170
		0.015502, //171
		0.000000, //172
		0.000000, //173
		-6333.490000, //174
		0.790000, //175
		0.200000, //176
		0.010000, //177
		6400.000000, //178
		0.000000, //179
		0.790000, //180
		0.200000, //181
		0.010000, //182
		0.610835, //183
		0.373664, //184
		0.015502, //185
		1.000000, //186
		0.286328, //187
		120.000000, //188
		562800.000000, //189
		-5172.630000, //190
		-8630.214000, //191
		-5172.630000, //192
		564.077500, //193
		52583.370000, //194
		564.077500, //195
		0.010000, //196
		80.000000, //197
		-5172.630000, //198
		0.790000, //199
		0.200000, //200
		0.010000, //201
		80.000000, //202
		0.000000, //203
		-8630.214000, //204
		0.610835, //205
		0.373664, //206
		0.015502, //207
		0.000000, //208
		0.000000, //209
		0.000000, //210
		0.333333, //211
		0.333333, //212
		0.333333, //213
		6400.000000, //214
		0.000000, //215
		0.790000, //216
		0.200000, //217
		0.010000, //218
		0.610835, //219
		0.373664, //220
		0.015502, //221
		1.000000, //222
		0.287414, //223
		120.000000, //224
		564933.300000, //225
		-5172.630000, //226
		-8630.214000, //227
		-5172.630000, //228
		566.215600, //229
		52583.370000, //230
		566.215600, //231
		0.010000, //232
		80.000000, //233
		-5172.630000, //234
		0.790000, //235
		0.200000, //236
		0.010000, //237
		80.000000, //238
		0.000000, //239
		-8630.214000, //240
		0.610835, //241
		0.373664, //242
		0.015502, //243
		0.000000, //244
		0.000000, //245
		0.000000, //246
		0.333333, //247
		0.333333, //248
		0.333333, //249
		6400.000000, //250
		0.000000, //251
		0.790000, //252
		0.200000, //253
		0.010000, //254
		0.610835, //255
		0.373664, //256
		0.015502, //257
		1.000000, //258
		0.288499, //259
		120.000000, //260
		567066.700000, //261
		-5172.630000, //262
		-8630.214000, //263
		-5172.630000, //264
		568.353800, //265
		52583.370000, //266
		568.353800, //267
		0.010000, //268
		80.000000, //269
		-5172.630000, //270
		0.790000, //271
		0.200000, //272
		0.010000, //273
		80.000000, //274
		0.000000, //275
		-8630.214000, //276
		0.610835, //277
		0.373664, //278
		0.015502, //279
		0.000000, //280
		0.000000, //281
		0.000000, //282
		0.333333, //283
		0.333333, //284
		0.333333, //285
		6400.000000, //286
		0.000000, //287
		0.790000, //288
		0.200000, //289
		0.010000, //290
		0.610835, //291
		0.373664, //292
		0.015502, //293
		1.000000, //294
		0.289585, //295
		120.000000, //296
		569200.000000, //297
		-5172.630000, //298
		-8630.214000, //299
		-5172.630000, //300
		570.492000, //301
		52583.370000, //302
		570.492000, //303
		0.010000, //304
		80.000000, //305
		-5172.630000, //306
		0.790000, //307
		0.200000, //308
		0.010000, //309
		80.000000, //310
		0.000000, //311
		-8630.214000, //312
		0.610835, //313
		0.373664, //314
		0.015502, //315
		0.000000, //316
		0.000000, //317
		0.000000, //318
		0.333333, //319
		0.333333, //320
		0.333333, //321
		6400.000000, //322
		-0.000000, //323
		0.790000, //324
		0.200000, //325
		0.010000, //326
		0.610835, //327
		0.373664, //328
		0.015502, //329
		1.000000, //330
		0.290670, //331
		120.000000, //332
		571333.300000, //333
		-5172.630000, //334
		-8630.214000, //335
		-5172.630000, //336
		572.630200, //337
		52583.370000, //338
		572.630200, //339
		0.010000, //340
		80.000000, //341
		-5172.630000, //342
		0.790000, //343
		0.200000, //344
		0.010000, //345
		80.000000, //346
		0.000000, //347
		-8630.214000, //348
		0.610835, //349
		0.373664, //350
		0.015502, //351
		0.000000, //352
		0.000000, //353
		0.000000, //354
		0.333333, //355
		0.333333, //356
		0.333333, //357
		6400.000000, //358
		0.000000, //359
		0.790000, //360
		0.200000, //361
		0.010000, //362
		0.610835, //363
		0.373664, //364
		0.015502, //365
		1.000000, //366
		0.291755, //367
		120.000000, //368
		573466.700000, //369
		-5172.630000, //370
		-8630.214000, //371
		-5172.630000, //372
		574.768400, //373
		52583.370000, //374
		574.768400, //375
		0.010000, //376
		80.000000, //377
		-5172.630000, //378
		0.790000, //379
		0.200000, //380
		0.010000, //381
		80.000000, //382
		0.000000, //383
		-8630.214000, //384
		0.610835, //385
		0.373664, //386
		0.015502, //387
		0.000000, //388
		0.000000, //389
		0.000000, //390
		0.333333, //391
		0.333333, //392
		0.333333, //393
		6400.000000, //394
		0.000000, //395
		0.790000, //396
		0.200000, //397
		0.010000, //398
		0.610835, //399
		0.373664, //400
		0.015502, //401
		1.000000, //402
		0.292841, //403
		120.000000, //404
		575600.000000, //405
		-5172.630000, //406
		-8630.214000, //407
		-5172.630000, //408
		576.906500, //409
		52583.370000, //410
		576.906500, //411
		0.010000, //412
		80.000000, //413
		-5172.630000, //414
		0.790000, //415
		0.200000, //416
		0.010000, //417
		80.000000, //418
		0.000000, //419
		-8630.214000, //420
		0.610835, //421
		0.373664, //422
		0.015502, //423
		0.000000, //424
		0.000000, //425
		0.000000, //426
		0.333333, //427
		0.333333, //428
		0.333333, //429
		6400.000000, //430
		0.000000, //431
		0.790000, //432
		0.200000, //433
		0.010000, //434
		0.610835, //435
		0.373664, //436
		0.015502, //437
		1.000000, //438
		0.293926, //439
		120.000000, //440
		577733.300000, //441
		-5172.630000, //442
		-8630.214000, //443
		-5172.630000, //444
		579.044700, //445
		52583.370000, //446
		579.044700, //447
		0.010000, //448
		80.000000, //449
		-5172.630000, //450
		0.790000, //451
		0.200000, //452
		0.010000, //453
		80.000000, //454
		0.000000, //455
		-8630.214000, //456
		0.610835, //457
		0.373664, //458
		0.015502, //459
		0.000000, //460
		0.000000, //461
		0.000000, //462
		0.333333, //463
		0.333333, //464
		0.333333, //465
		6400.000000, //466
		-0.000000, //467
		0.790000, //468
		0.200000, //469
		0.010000, //470
		0.610835, //471
		0.373664, //472
		0.015502, //473
		1.000000, //474
		0.295011, //475
		120.000000, //476
		579866.700000, //477
		-5172.630000, //478
		-8630.214000, //479
		-5172.630000, //480
		581.182900, //481
		52583.370000, //482
		581.182900, //483
		0.010000, //484
		80.000000, //485
		-5172.630000, //486
		0.790000, //487
		0.200000, //488
		0.010000, //489
		80.000000, //490
		0.000000, //491
		-8630.214000, //492
		0.610835, //493
		0.373664, //494
		0.015502, //495
		0.000000, //496
		0.000000, //497
		0.000000, //498
		0.333333, //499
		0.333333, //500
		0.333333, //501
		6400.000000, //502
		0.000000, //503
		0.790000, //504
		0.200000, //505
		0.010000, //506
		0.610835, //507
		0.373664, //508
		0.015502, //509
		1.000000, //510
		0.296097, //511
		120.000000, //512
		582000.000000, //513
		-5172.630000, //514
		-8630.214000, //515
		-5172.630000, //516
		583.321100, //517
		52583.370000, //518
		583.321100, //519
		0.010000, //520
		80.000000, //521
		-5172.630000, //522
		0.790000, //523
		0.200000, //524
		0.010000, //525
		80.000000, //526
		0.000000, //527
		-8630.214000, //528
		0.610835, //529
		0.373664, //530
		0.015502, //531
		0.000000, //532
		0.000000, //533
		0.000000, //534
		0.333333, //535
		0.333333, //536
		0.333333, //537
		6400.000000, //538
		-0.000000, //539
		0.790000, //540
		0.200000, //541
		0.010000, //542
		0.610835, //543
		0.373664, //544
		0.015502, //545
		1.000000, //546
		0.297182, //547
		120.000000, //548
		584133.300000, //549
		-5172.630000, //550
		-8630.214000, //551
		-5172.630000, //552
		585.459200, //553
		52583.370000, //554
		585.459200, //555
		0.010000, //556
		80.000000, //557
		-5172.630000, //558
		0.790000, //559
		0.200000, //560
		0.010000, //561
		80.000000, //562
		0.000000, //563
		-8630.214000, //564
		0.610835, //565
		0.373664, //566
		0.015502, //567
		0.000000, //568
		0.000000, //569
		0.000000, //570
		0.333333, //571
		0.333333, //572
		0.333333, //573
		6400.000000, //574
		-0.000000, //575
		0.790000, //576
		0.200000, //577
		0.010000, //578
		0.610835, //579
		0.373664, //580
		0.015502, //581
		1.000000, //582
		0.298267, //583
		120.000000, //584
		586266.700000, //585
		-5172.630000, //586
		-8630.214000, //587
		-5172.630000, //588
		587.597400, //589
		52583.370000, //590
		587.597400, //591
		0.010000, //592
		80.000000, //593
		-5172.630000, //594
		0.790000, //595
		0.200000, //596
		0.010000, //597
		80.000000, //598
		0.000000, //599
		-8630.214000, //600
		0.610835, //601
		0.373664, //602
		0.015502, //603
		0.000000, //604
		0.000000, //605
		0.000000, //606
		0.333333, //607
		0.333333, //608
		0.333333, //609
		6400.000000, //610
		0.000000, //611
		0.790000, //612
		0.200000, //613
		0.010000, //614
		0.610835, //615
		0.373664, //616
		0.015502, //617
		1.000000, //618
		0.299353, //619
		120.000000, //620
		588400.000000, //621
		-5172.630000, //622
		-8630.214000, //623
		-5172.630000, //624
		589.735600, //625
		52583.370000, //626
		589.735600, //627
		0.010000, //628
		80.000000, //629
		-5172.630000, //630
		0.790000, //631
		0.200000, //632
		0.010000, //633
		80.000000, //634
		0.000000, //635
		-8630.214000, //636
		0.610835, //637
		0.373664, //638
		0.015502, //639
		0.000000, //640
		0.000000, //641
		0.000000, //642
		0.333333, //643
		0.333333, //644
		0.333333, //645
		6400.000000, //646
		-0.000000, //647
		0.790000, //648
		0.200000, //649
		0.010000, //650
		0.610835, //651
		0.373664, //652
		0.015502, //653
		1.000000, //654
		0.300438, //655
		120.000000, //656
		590533.300000, //657
		-5172.630000, //658
		-8630.214000, //659
		-5172.630000, //660
		591.873800, //661
		52583.370000, //662
		591.873800, //663
		0.010000, //664
		80.000000, //665
		-5172.630000, //666
		0.790000, //667
		0.200000, //668
		0.010000, //669
		80.000000, //670
		0.000000, //671
		-8630.214000, //672
		0.610835, //673
		0.373664, //674
		0.015502, //675
		0.000000, //676
		0.000000, //677
		0.000000, //678
		0.333333, //679
		0.333333, //680
		0.333333, //681
		6400.000000, //682
		-0.000000, //683
		0.790000, //684
		0.200000, //685
		0.010000, //686
		0.610835, //687
		0.373664, //688
		0.015502, //689
		1.000000, //690
		0.301523, //691
		120.000000, //692
		592666.700000, //693
		-5172.630000, //694
		-8630.214000, //695
		-5172.630000, //696
		594.011900, //697
		52583.370000, //698
		594.011900, //699
		0.010000, //700
		80.000000, //701
		-5172.630000, //702
		0.790000, //703
		0.200000, //704
		0.010000, //705
		80.000000, //706
		0.000000, //707
		-8630.214000, //708
		0.610835, //709
		0.373664, //710
		0.015502, //711
		0.000000, //712
		0.000000, //713
		0.000000, //714
		0.333333, //715
		0.333333, //716
		0.333333, //717
		6400.000000, //718
		0.000000, //719
		0.918578, //720
		0.076440, //721
		0.004983, //722
		97.191820, //723
		-10888.700000, //724
		0.000000, //725
		0.000000, //726
		-41.000000, //727
		80.000000, //728
		0.000000, //729
		80.000000, //730
	};


	double x_0[N_X] = {
		434.628800, //0
		0.790000, //1
		0.200000, //2
		-2681819.000000, //3
		436.308100, //4
		0.790000, //5
		0.200000, //6
		-2692181.000000, //7
		437.987500, //8
		0.790000, //9
		0.200000, //10
		-2702543.000000, //11
		439.666800, //12
		0.790000, //13
		0.200000, //14
		-2712905.000000, //15
		441.346100, //16
		0.790000, //17
		0.200000, //18
		-2723267.000000, //19
		443.025400, //20
		0.790000, //21
		0.200000, //22
		-2733629.000000, //23
		444.704700, //24
		0.790000, //25
		0.200000, //26
		-2743991.000000, //27
		446.384100, //28
		0.790000, //29
		0.200000, //30
		-2754353.000000, //31
		448.063400, //32
		0.790000, //33
		0.200000, //34
		-2764715.000000, //35
		449.742700, //36
		0.790000, //37
		0.200000, //38
		-2775077.000000, //39
		451.422000, //40
		0.790000, //41
		0.200000, //42
		-2785439.000000, //43
		453.101300, //44
		0.790000, //45
		0.200000, //46
		-2795801.000000, //47
		454.780600, //48
		0.790000, //49
		0.200000, //50
		-2806163.000000, //51
		456.460000, //52
		0.790000, //53
		0.200000, //54
		-2816525.000000, //55
		458.139300, //56
		0.790000, //57
		0.200000, //58
		-2826887.000000, //59
		459.818600, //60
		0.790000, //61
		0.200000, //62
		-2837249.000000, //63
		461.497900, //64
		0.790000, //65
		0.200000, //66
		-2847611.000000, //67
		463.177200, //68
		0.790000, //69
		0.200000, //70
		-2857973.000000, //71
		464.856600, //72
		0.790000, //73
		0.200000, //74
		-2868335.000000, //75
		466.535900, //76
		0.790000, //77
		0.200000, //78
		-2878697.000000, //79
		0.0, //80
		0.0, //81
		0.0, //82
	}; 

	double p_0[N_P];

	std::vector<int> mode0 = std::vector<int>(N_S, 1);

	// function to change iP
	void set_iP(int new_ip) {
		int_p = new_ip;
	}

	// function to change final simulation time
	void set_tf(double new_tf) {
		tf = new_tf;
	}

	void set_t0(double new_t0) {
		t0 = new_t0;
	}

	void set_x_0(std::vector<double> x_0_new) {
		for (int i = 0; i < N_X; ++i) {
			x_0[i] = x_0_new[i];
		}
	}

	void set_y_0(std::vector<double> y_0_new) {
		for (int i = 0; i < N_X; ++i) {
			y_0[i] = y_0_new[i];
		}
	}

	void set_mode0(std::vector<int> mode0_new) {
		for (int i = 0; i < N_X; ++i) {
			mode0[i] = mode0_new[i];
		}
	}

	// inline functions to help match variables with their entry in variable vector
	template <typename T> inline const T P_out_(const T* x, const T* y) const {
		return p_out;
	};
	// inline functions for differential states
	template <typename T> inline const T& z_i(const T* x, const T* y, const int jComp, const int iTray) const {
		return x[iTray*(nC + 1) + jComp + 1];
	};
	template <typename T> inline const T M(const T* x, const T* y, const int iTray) const {
		return x[iTray*(nC + 1)];
	};
	template <typename T> inline const T U_total(const T* x, const T* y, const int iTray) const {
		return x[iTray*(nC + 1) + nC];
	};


	template <typename T> inline const T& y_i(const T* x, const T* y, const int jComp, const int iTray) const {
		return y[iTray*NTRAYVARS + jComp];
	};
	template <typename T> inline const T& x_i(const T* x, const T* y, const int jComp, const int iTray) const {
		return y[iTray*NTRAYVARS + nC + jComp];
	};
	template <typename T> inline const T vfrac(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC];
	};
	template <typename T> inline const T beta(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC + 1];
	};

	template <typename T> inline const T Temperature(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC + 2];;
	};
	template <typename T> inline const T Pressure(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC + 3];
	};

	template <typename T> inline const T h_vapor(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC + 4];
	};
	template <typename T> inline const T h_liquid(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC + 5];
	};
	template <typename T> inline const T h_(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC + 6];
	};

	template <typename T> inline const T rho_vapor(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC + 7];
	};
	template <typename T> inline const T rho_liquid(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC + 8];
	};
	template <typename T> inline const T rho_(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC + 9];
	};

	template <typename T> inline const T z_nC(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC + 10];
	};

	// vapor inlet stream on tray
	template <typename T> inline const T V_in(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC + 11];
	};
	template <typename T> inline const T h_V_in(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC + 12];
	};
	template <typename T> inline const T& y_V_in(const T* x, const T* y, const int jComp, const int iTray) const {
		return y[iTray*NTRAYVARS + 2 * nC + 13 + jComp];
	};

	// vapor outlet stream on tray
	template <typename T> inline const T V_out(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 3 * nC + 13];
	};

	// liquid inlet stream on tray
	template <typename T> inline const T L_in(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 3 * nC + 14];
	};
	template <typename T> inline const T h_L_in(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 3 * nC + 15];
	};
	template <typename T> inline const T& x_L_in(const T* x, const T* y, const int jComp, const int iTray) const {
		return y[iTray*NTRAYVARS + 3 * nC + 16 + jComp];
	};

	// liquid outlet stream on tray
	template <typename T> inline const T L_out(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 4 * nC + 16];
	};

	// feed on tray inlet stream on tray
	template <typename T> inline const T F(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 4 * nC + 17];
	};
	template <typename T> inline const T h_F_in(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 4 * nC + 18];
	};
	template <typename T> inline const T& z_F(const T* x, const T* y, const int jComp, const int iTray) const {
		return y[iTray*NTRAYVARS + 4 * nC + 19 + jComp];
	};

	//variables for outlet calculation
	template <typename T> inline const T H_0V(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 5 * nC + 19];
	};

	template <typename T> inline const T H_0L(const T* x, const T* y, const int iTray) const {
		return y[iTray*NTRAYVARS + 5 * nC + 20];
	};


	// condenser variables
	template <typename T> inline const T& x_i_condenser(const T* x, const T* y, const int jComp) const {
		return y_i(x,y,jComp,0);
	};

	template <typename T> inline const T& y_i_condenser(const T* x, const T* y, const int jComp) const {
		return y[nTrays*NTRAYVARS+jComp];
	};

	template <typename T> inline const T Temperature_condenser(const T* x, const T* y) const {
		return y[nTrays*NTRAYVARS + nC];
	};

	template <typename T> inline const T Pressure_condenser(const T* x, const T* y) const {
		return p_out;
	};

	template <typename T> inline const T F_in_condenser(const T* x, const T* y, const T* p) const {
		return splitfactor(x, y, p)*V_out(x, y, 0);
	};

	template <typename T> inline const T F_out_condenser(const T* x, const T* y) const {
		return y[nTrays*NTRAYVARS + nC + 2];
	};

	template <typename T> inline const T h_out_condenser(const T* x, const T* y) const {
		return y[nTrays*NTRAYVARS + nC + 1];
	};

	template <typename T> inline const T Q_in_condenser(const T* x, const T* y) const {
		return y[nTrays*NTRAYVARS + nC + 3];
	};

	template <typename T> inline const T h_in_condenser(const T* x, const T* y) const {
		return h_vapor(x,y,0);
	};

	// product variables
	template <typename T> inline const T n_product(const T* x, const T* y, const T* p) const {
		return y[nTrays*NTRAYVARS + NCONDENSERVARS+1];
	};

	template <typename T> inline const T x_product(const T* x, const T* y, const T* p) const {
		return y_i(x, y, 0, 0);
	};

	template <typename T> inline const T x_product_purity(const T* x, const T* y, const T* p) const {
		return y[nTrays*NTRAYVARS + NCONDENSERVARS];
	};

	template <typename T>
	inline const T splitfactor(const T* x, const T* y, const T* p) const {
		// Piecewise constant interpolation
		//int itheta_ = get_itheta();
		//if (itheta_ > N_P - 1) {
		//	std::cout << "itheta = " << itheta_ << std::endl;
		//}
		return y[nTrays*NTRAYVARS + NCONDENSERVARS + 2];
	}

	template <typename T>
	inline const T n_flow(const T* x, const T* y, const T* p) const {
		//std::cout << "n_flow = " << y[nTrays*NTRAYVARS + NCONDENSERVARS + 3] << std::endl;
		return  y[nTrays*NTRAYVARS + NCONDENSERVARS + 3];
	}

	// thermodynamic functions

	/*! \brief Calculate vapor pressure of each component by extended Antoine model.
	\param[in] Temp: Temperature in K,
	\return pi0_XANT[nC] : vapor pressure of each component */
	template <typename T> std::vector<T> calc_pi0_XANT(const T Temp) const {
		vector<T> pi0_XANT(nC);
		vector<T> expo(nC);
		T T_smooth = 0.5*(1 + tanh((Temp - 1) * 50))*Temp + 0.01;

		for (int i = 0; i < nC; i++) {
			expo[i] = ANT[i][3] + ANT[i][4] /
				(T_smooth + ANT[i][5]) + ANT[i][6] * T_smooth;
			expo[i] += ANT[i][7] * log(T_smooth);
			expo[i] += ANT[i][8] * pow(T_smooth, ANT[i][9]);
			pi0_XANT[i] = ANT[i][1] * pow(ANT[i][2], expo[i]);
		}

		return pi0_XANT;
	}

	/*! \brief Calculate ln of activity coefficients.
	\param[in] Temp: Temperature in K,
	\param[in] pressure: pressure (Pa),
	\param[in] x_l[nC]: mole fractions (of liquid phase).
	\return lngamma_i[nC] : ln of activity coefficients */
	template <typename T>
	vector<T> calc_lngamma_i(const T Temp, const T pressure, const vector<T>& x_l) const {
		vector<T> lngamma_i(nC);
		switch (gemodel) {
		case NRTL:
			lngamma_i = calc_lngamma_i_NRTL(Temp, pressure, x_l);
			break;
		case Margules:
			lngamma_i = calc_lngamma_i_Margules(Temp, pressure, x_l);
			break;
		default:
			std::fill(lngamma_i.begin(), lngamma_i.end(), 0.0);
		}

		return lngamma_i;
	}

	/*! \brief Calculate ln of activity coefficients by NRTL model.
	\param[in] Temp: Temperature in K,
	\param[in] pressure: pressure (Pa),
	\param[in] x_l[nC]: mole fractions (of liquid phase).
	\return lngamma_i[nC] : ln of activity coefficients by NRTL model */
	template <typename T>
	vector<T> calc_lngamma_i_NRTL(const T Temp, const T pressure, const vector<T>& x_l) const {
		vector<vector<T> > tau(nC, vector<T>(nC));
		vector<vector<T> > gg(nC, vector<T>(nC));
		vector<vector<T> > ggtau(nC, vector<T>(nC));
		vector<T> xg(nC);
		vector<T> xtg(nC);
		vector<T> br(nC);
		vector<T> sum(nC);
		vector<vector<T> > ggtaubr(nC, vector<T>(nC));
		vector<T> lngamma_i(nC);

		for (int i = 0; i < nC; i++) {
			for (int j = 0; j < nC; j++) {
				tau[i][j] = 1.0 / (RGAS * Temp) *
					(NRTLG1[i][j] + NRTLG2[i][j] * Temp);
				gg[i][j] = exp(-NRTLA1[i][j] * tau[i][j]);
				ggtau[i][j] = gg[i][j] * tau[i][j];
			}
		}

		for (int i = 0; i < nC; i++) {
			xg[i] = 0.0;
			xtg[i] = 0.0;
			for (int j = 0; j < nC; j++) {
				xg[i] += gg[j][i] * x_l[j];
				xtg[i] += gg[j][i] * tau[j][i] * x_l[j];
			}
			br[i] = xtg[i] / xg[i];
		}

		for (int i = 0; i < nC; i++) {
			for (int j = 0; j < nC; j++) {
				ggtaubr[i][j] += gg[i][j] * (tau[i][j] - br[j]);
			}
		}

		for (int i = 0; i < nC; i++) {
			sum[i] = 0.0;
			for (int j = 0; j < nC; j++) {
				sum[i] += ggtaubr[i][j] * x_l[j] / xg[j];
			}

			lngamma_i[i] = br[i] + sum[i];
		}

		return lngamma_i;
	}

	/*! \brief Calculate ln of activity coefficients by Margules model.
	\param[in] Temp: Temperature in K,
	\param[in] pressure: pressure (Pa),
	\param[in] x_l[nC]: mole fractions (of liquid phase).
	\return lngamma_i[nC] : ln of activity coefficients by Margules model */
	template <typename T>
	vector<T> calc_lngamma_i_Margules(const T Temp, const T pressure, const vector<T>& x_l) const {
		vector<T> lngamma_i(nC);
		for (int k = 0; k < nC; k++) {
			lngamma_i[k] = 0.;
			for (int i = 0; i < nC; i++) {
				for (int j = 0; j < nC; j++) {
					lngamma_i[k] += (MargulesA[i][k]
						+ MargulesA[j][k]
						- MargulesA[i][j])*x_l[i] * x_l[j];
				}
			}
			lngamma_i[k] /= (2 * Temp);
		}

		return lngamma_i;
	}

	/*! \brief Calculates derivative of lngamma_i w.r.t. temperature by NRTL model.
	\param[in] Temp: Temperature in K,
	\param[in] pressure: pressure (Pa),
	\param[in] x_l[nC]: mole fractions (of liquid phase).
	\return dlngamma_i_dT[nC] : derivative of lngamma_i w.r.t. temperature with NRTL model */
	template <typename T>
	vector<T> calc_dlngamma_i_dT_NRTL(const T Temp, const T pressure, const vector<T>& x_l) const {
		vector<vector<T> > tau(nC, vector<T>(nC));
		vector<vector<T> > dtau_dT(nC, vector<T>(nC));
		vector<vector<T> > gg(nC, vector<T>(nC));
		vector<vector<T> > dgg_dT(nC, vector<T>(nC));
		vector<vector<T> > ggtau(nC, vector<T>(nC));
		vector<T> xg(nC);
		vector<T> dxg_dT(nC);
		vector<T> xtg(nC);
		vector<T> dxtg_dT(nC);
		vector<T> br(nC);
		vector<T> dbr_dT(nC);
		vector<T> sum(nC);
		vector<T> dsum_dT(nC);
		vector<vector<T> > ggtaubr(nC, vector<T>(nC));
		vector<vector<T> > dggtaubr_dT(nC, vector<T>(nC));
		vector<T> lngamma_i(nC);
		vector<T> dlngamma_i_dT(nC);

		for (int i = 0; i < nC; i++) {
			for (int j = 0; j < nC; j++) {
				tau[i][j] = 1.0 / (RGAS * Temp) *
					(NRTLG1[i][j] + NRTLG2[i][j] * Temp);
				dtau_dT[i][j] = -NRTLG1[i][j] / (RGAS * Temp *
					Temp);
				gg[i][j] = exp(-NRTLA1[i][j] * tau[i][j]);

				if (tau[i][j] > 0.0) {
					dgg_dT[i][j] = -NRTLA1[i][j] * gg[i][j] * dtau_dT[i][j];
				}
				else {
					dgg_dT[i][j] = 0;
				}

				ggtau[i][j] = gg[i][j] * tau[i][j];
			}
		}

		for (int i = 0; i < nC; i++) {
			xg[i] = 0.0;
			dxg_dT[i] = 0.0;
			xtg[i] = 0.0;
			dxtg_dT[i] = 0.0;

			for (int j = 0; j < nC; j++) {
				xg[i] += gg[j][i] * x_l[j];
				dxg_dT[i] += dgg_dT[j][i] * x_l[j];
				xtg[i] += gg[j][i] * tau[j][i] * x_l[j];
				dxtg_dT[i] += (gg[j][i] * dtau_dT[j][i] +
					dgg_dT[j][i] * tau[j][i]) * x_l[j];
			}

			br[i] = xtg[i] / xg[i];
			dbr_dT[i] = (xg[i] * dxtg_dT[i] - xtg[i] * dxg_dT[i]) /
				(xg[i] * xg[i]);
		}

		for (int i = 0; i < nC; i++) {
			for (int j = 0; j < nC; j++) {
				ggtaubr[i][j] = gg[i][j] * (tau[i][j] - br[j]);
				dggtaubr_dT[i][j] = dgg_dT[i][j] * (tau[i][j] - br[j]) +
					gg[i][j] * (dtau_dT[i][j] - dbr_dT[j]);
			}
		}

		for (int i = 0; i < nC; i++) {
			sum[i] = 0.0;
			dsum_dT[i] = 0.0;

			for (int j = 0; j < nC; j++) {
				sum[i] += ggtaubr[i][j] * x_l[j] / xg[j];
				dsum_dT[i] += (dggtaubr_dT[i][j] * xg[j] -
					dxg_dT[j] * ggtaubr[i][j]) /
					(xg[j] * xg[j]) * x_l[j];
			}

			lngamma_i[i] = br[i] + sum[i];
			dlngamma_i_dT[i] = dbr_dT[i] + dsum_dT[i];
		}

		return dlngamma_i_dT;
	}

	/*! \brief Calculates derivative of lngamma_i w.r.t. temperature with Margules model.
	\param[in] Temp: Temperature in K,
	\param[in] pressure: pressure (Pa),
	\param[in] x_l[nC]: mole fractions (of liquid phase).
	\return dlngamma_i_dT[nC] : derivative of lngamma_i w.r.t. temperature with Margules model */
	template <typename T>
	vector<T> calc_dlngamma_i_dT_Margules(const T Temp, const T pressure, const vector<T>& x_l) const {
		vector<T> dlngamma_i_dT(nC);
		for (int k = 0; k < nC; k++) {
			dlngamma_i_dT[k] = 0.;
			for (int i = 0; i < nC; i++) {
				for (int j = 0; j < nC; j++) {
					dlngamma_i_dT[k] += (MargulesA[i][k]
						+ MargulesA[j][k]
						- MargulesA[i][j])*x_l[i] * x_l[j];
				}
			}
			dlngamma_i_dT[k] /= (2 * Temp * Temp);
			dlngamma_i_dT[k] *= -1.0;
		}

		return dlngamma_i_dT;
	}

	/*! \brief Calculate molar enthalpy [J/mol] for vapor phase. Here: ideal gas, no dependency on pressure. May, however, be added later.
	\param[in] Temp: Temperature in K,
	\param[in] pressure: pressure (Pa),
	\param[in] y_v[nC]: mole fractions (of vapor phase).
	\return h_V : molar enthalpy of vapor phase */
	template <typename T>
	T calc_hV(const T Temp, const T pressure, const vector<T>& y_v) const {
		T h_V = 0.0;
		vector<T> h0ig(nC);
		h0ig = calc_h0ig(Temp);

		for (int i = 0; i < nC; i++) {
			h_V += y_v[i] * h0ig[i];
		}

		return h_V;
	}

	/*! \brief Calculate molar enthalpy [J/mol] for liquid phase.
	\param[in] Temp: Temperature in K,
	\param[in] pressure: pressure (Pa),
	\param[in] x_l[nC]: mole fractions (of liquid phase).
	\return h_L : molar enthalpy of liquid phase */
	template <typename T>
	T calc_hL(const T Temp, const T pressure, const vector<T>& x_l) const {
		T h_L = 0.0;
		vector<T> h0ig(nC);
		h0ig = calc_h0ig(Temp);
		T hig = 0.0;

		for (int i = 0; i < nC; i++) {
			hig += x_l[i] * h0ig[i];
		}

		vector<T> deltah0vap(nC);
		deltah0vap = calc_deltah0vap_DIPPR(Temp);

		T hEL = 0.0;
		if (considerExcessEnthalpy) { hEL = calc_hEL(Temp, pressure, x_l); }

		for (int i = 0; i < nC; i++) {
			h_L -= x_l[i] * deltah0vap[i];
		}

		h_L += hEL;
		h_L += hig;

		return h_L;
	}

	/*! \brief Calculate enthalpy deviation [J/mol] with DIPPR model.
	\param[in] Temp: Temperature in K,
	\return dh_DIPPR[nC] : Enthalpy deviation with DIPPR */
	template <typename T>
	vector<T> calc_dh_DIPPR(const T Temp) const {
		vector<T> dh_DIPPR(nC);

		//for (int i = 0; i < nC; i++) {
		//	dh_DIPPR[i] = CPIGDP[i][0] * Temp;

		//	if (CPIGDP[i][2] == 0) {
		//		dh_DIPPR[i] = dh_DIPPR[i];
		//	}
		//	else {
		//		dh_DIPPR[i] = dh_DIPPR[i] + CPIGDP[i][1] * CPIGDP[i][2] /
		//			tanh(CPIGDP[i][2] / Temp);
		//	}

		//	if (CPIGDP[i][4] == 0) {
		//		dh_DIPPR[i] = dh_DIPPR[i];
		//	}
		//	else {
		//		dh_DIPPR[i] = dh_DIPPR[i] - CPIGDP[i][3] * CPIGDP[i][4] *
		//			tanh(CPIGDP[i][4] / Temp);
		//	}
		//}
	

		for (int i = 0; i < nC - 1; i++) {
			dh_DIPPR[i] = CPIGDP[i][0] * Temp + CPIGDP[i][1] * CPIGDP[i][2] /
				tanh(CPIGDP[i][2] / Temp) - CPIGDP[i][3] * CPIGDP[i][4] *
				tanh(CPIGDP[i][4] / Temp);
		}
		dh_DIPPR[nC - 1] = CPIGDP[nC - 1][0] * Temp;

		return dh_DIPPR;
	}

	/*! \brief Calculate idael gas molar enthalpies [J/mol] for pure components.
	\param[in] Temp: Temperature in K,
	\return h0ig[nC] : Ideal gas molar enthalpy for each (pure) component */
	template <typename T>
	vector<T> calc_h0ig(const T Temp) const {
		vector<T> h0ig(nC);
		vector<T> dh_T(nC);
		dh_T = calc_dh_DIPPR(Temp);
		vector<T> dh_T_bezug(nC);
		dh_T_bezug = calc_dh_DIPPR(T(t_Bezug));

		for (int i = 0; i < nC; i++) {
			h0ig[i] = DHFORM[i] + dh_T[i] - dh_T_bezug[i];
		}

		return h0ig;
	}

	/*! \brief Calculate heat of vaporization using DIPPR equation. Only valid if CPIGDP[i,5] <= T <= CPIGDP[i,6]!
	\param[in] Temp: Temperature in K,
	\return h0ig[nC] : Ideal gas molar enthalpy for each component */
	template <typename T>
	vector<T> calc_deltah0vap_DIPPR(const T Temp) const {
		T base, expo;
		vector<T> deltah0vap_DIPPR(nC);
		vector<T> tr(nC);
		vector<T> tr_smooth(nC);
		vector<T> delta(nC);

		for (int i = 0; i < nC; i++) {
			tr[i] = Temp / KRIT[i][0];
			tr_smooth[i] = tr[i] + 0.5*(1 + tanh(20 * (tr[i] - 0.99)))*(0.99 - tr[i]);
			delta[i] = 0.5*(1 + tanh(-k_deltah0vap*(tr[i] - 1)));
			base = 1 - tr_smooth[i];
			expo = DHVLDP[i][1] + DHVLDP[i][2] * tr[i] +
				DHVLDP[i][3] * tr[i] * tr[i] +
				DHVLDP[i][4] * tr[i] * tr[i] * tr[i];
			deltah0vap_DIPPR[i] = delta[i] * DHVLDP[i][0] * pow(base, expo);
		}

		return deltah0vap_DIPPR;
	}

	/*! \brief Calculates excess enthalpy [J/mol].
	\param[in] Temp: Temperature in K,
	\param[in] pressure: pressure (Pa),
	\param[in] x_l[nC]: mole fractions (of liquid phase).
	\return h_L : molar enthalpy of liquid phase */
	template <typename T>
	T calc_hEL(const T Temp, const T pressure, const vector<T>& x_l) const {
		T hEL = 0.0;
		vector<T> dlngamma_i_dT(nC);
		switch (gemodel) {
		case NRTL:
			dlngamma_i_dT = calc_dlngamma_i_dT_NRTL(Temp, pressure, x_l);
			break;
		case Margules:
			dlngamma_i_dT = calc_dlngamma_i_dT_Margules(Temp, pressure, x_l);
			break;
		default:
			std::fill(dlngamma_i_dT.begin(), dlngamma_i_dT.end(), 0.0);
		}

		for (int i = 0; i < nC; i++) {
			hEL += -R_alg * Temp * Temp * x_l[i] *
				dlngamma_i_dT[i];
		}

		return hEL;
	}

	/*! \brief Calculates molar density using Rackett equation [mol/m3].
	\param[in] Temp: Temperature in K,
	\param[in] x_l[nC]: mole fractions (of vapor phase).
	\return rho_rackett : molar density of (liquid) phase */
	template <typename T>
	T calc_rho_rackett(const T Temp, const vector<T>& x_l) const {
		// set parameters for Rackett equation
		//T RKTTRMAX = 0.99;
		//T small_eps = 1e-8;
		vector<T> tr(nC);
		vector<T> rhoComp(nC);

		T rho_rackett = 0.0;


		T a, log_a, b, logZRA, expo = 0.0;
		//T expob = (2.0 / 7.0);
		for (int i = 0; i < nC; ++i) {
			a = R_*KRIT[i][0] / KRIT[i][1];
			log_a = log(a) / log(10);
			tr[i] = Temp / KRIT[i][0];
			b = 1 + pow((1 + tr[i]), (2.0 / 7.0));
			logZRA = log10(KRIT[i][2]);
			expo = log_a + b*logZRA;
			rhoComp[i] = 1 / (pow(10, expo));
			rho_rackett += x_l[i] * rhoComp[i];
		}

		return rho_rackett;
	}
};



template<typename Real> inline unsigned DistillationColumnNdae<Real>::Eval_n_p() const { return N_P; }
template<typename Real> inline unsigned DistillationColumnNdae<Real>::Eval_n_sigma() const { return N_S; }
template<typename Real> inline unsigned DistillationColumnNdae<Real>::Eval_n_x() const { return N_X; }
template<typename Real> inline unsigned DistillationColumnNdae<Real>::Eval_n_y() const { return N_Y; }
template<typename Real> inline unsigned DistillationColumnNdae<Real>::Eval_n_Theta() const { return N_THETA; }
template<typename Real> inline Real DistillationColumnNdae<Real>::Eval_t0() const { return t0; }

template<typename Real> void DistillationColumnNdae<Real>::Eval_Theta(Real* Theta) const {
	for (int i = 0; i < N_THETA; ++i) {
		Theta[i] = (double)(i + 1) * tf / (double)N_THETA;
		//std::cout << "i = " << i << ", Theta[i] = " << Theta[i] << std::endl;
	}

	//Theta[0] = tf;
}

template<typename Real> std::vector<int> DistillationColumnNdae<Real>::EvalInitialMode() const {
	std::vector<int> temp(N_S);
	for (int i = 0; i < temp.size(); ++i) { temp[i] = mode0[i]; }
	//temp[N_S-1] = 0;
	return temp;
}

template<typename Real> std::vector<int> DistillationColumnNdae<Real>::EvalRootDirection(const std::vector<int>& mode) const {
	std::vector<int> rootDirection(N_S, 0);
	for (int i = 0; i < N_S; ++i) {
		rootDirection[i] = -1;
	}
	return rootDirection;

}

template<typename Real> void DistillationColumnNdae<Real>::EvalInitialGuess_y(Real *y) const {
	unsigned n_y = Eval_n_y();
	// initial guess for alg variables
	for (int i = 0; i < n_y; ++i) {
		y[i] = y_0[i];
	}
}

template<typename Real> std::vector<int> DistillationColumnNdae<Real>::EvalMode(Real time, Real *x, Real *y, Real *p, const std::vector<int>& oldMode) const {
	std::vector<int> newMode(N_S);
	std::vector<Real> sigma(N_S);
	Eval_sigma(sigma.data(), time, x, y, p, oldMode);
	for (int i = 0; i < N_S; i++) {
		if (sigma[i] < 0)
			if (oldMode[i]) {
				newMode[i] = 0;
			}
			else {
				newMode[i] = 1;
			}
		else
			newMode[i] = oldMode[i];
		//mode0[i] = newMode[i];
	}

	//std::cout << "newMode[0] = " << newMode[0] << ", oldMode[0] = " << oldMode[0] << ", newMode[1] = " << newMode[1] << ", oldMode[1] = " << oldMode[1] << std::endl;
	return newMode;
}

template<typename Real>
template<typename T>
void DistillationColumnNdae<Real>::Eval_f_impl(T *f, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const
{
	for (int iTray = 0; iTray < nTrays; ++iTray) {
		T f_tray[nC + 1];
		Eval_f_tray(f_tray, time, x, y, p, mode, iTray);
		for (int j = 0; j < (nC + 1); ++j) {
			f[iTray*(nC + 1) + j] = f_tray[j];
		}
	}
	f[N_X - 3] =weight_obj*(weight_n*(n_product(x,y,p)-n_set)*(n_product(x,y,p)-n_set) + weight_x*(x_product_purity(x,y,p))*(x_product_purity(x, y, p)));// weight_n*(n)
	f[N_X-2] = weight_obj*(weight_n*(n_product(x, y, p) - n_set)*(n_product(x, y, p) - n_set));
	f[N_X-1] = weight_obj*(weight_x*(x_product_purity(x, y, p))*(x_product_purity(x, y, p)));
	//std::cout << "t =" << time << ", obj = " << x[N_X - 1] << std::endl;
	//f[0] = 0;//Q(time, p, mode); //energy balance without any inlet/outlet mass flows
	//f[1] = 0;//(Temperature(y) - T_set)*(Temperature(y) - T_set); //objective function
}

template<typename Real>
template<typename T>
void DistillationColumnNdae<Real>::Eval_f_tray(T *f, T& time, T *x, T *y, T *p, const std::vector<int>& mode, const int iTray) const
{
	vector<T> z_i_(nC);
	for (int jComp = 0; jComp < nC - 1; ++jComp) {
		z_i_[jComp] = z_i(x, y, jComp, iTray);
	}
	z_i_[nC - 1] = z_nC(x, y, iTray);
	f[0] = L_in(x, y, iTray) + V_in(x, y, iTray) - L_out(x, y, iTray) - V_out(x, y, iTray) + F(x, y, iTray);//dM/dt: total mole balance equation on tray
	for (int jComp = 0; jComp < nC - 1; ++jComp) {
		//std::cout << "jComp = " << jComp << std::endl;
		//std::cout << "x_L_in = " << x_L_in(x, y, jComp, iTray) << std::endl;
		//std::cout << "y_V_in = " << y_V_in(x, y, jComp, iTray) << std::endl;
		//std::cout << "x_i = " << x_i(x, y, jComp, iTray) << std::endl;
		//std::cout << "y_i = " << y_i(x, y, jComp, iTray) << std::endl;
		f[jComp + 1] = 1 / M(x, y, iTray)*(L_in(x, y, iTray)*(x_L_in(x, y, jComp, iTray) - z_i_[jComp])
			+ V_in(x, y, iTray)*(y_V_in(x, y, jComp, iTray) - z_i_[jComp])
			- L_out(x, y, iTray)*(x_i(x, y, jComp, iTray) - z_i_[jComp])
			- V_out(x, y, iTray)*(y_i(x, y, jComp, iTray) - z_i_[jComp])
			+ F(x, y, iTray)*(z_F(x, y, jComp, iTray) - z_i_[jComp]));//dz_i/dt: species mole balance equation on tray (nC-1 equations)
	}
	//std::cout << "L_in = " << L_in(x, y, iTray) << std::endl;
	//std::cout << "h_L_in = " << h_L_in(x, y, iTray) << std::endl;
	//std::cout << "V_in = " << V_in(x, y, iTray) << std::endl;
	//std::cout << "h_V_in = " << h_V_in(x, y, iTray) << std::endl;
	//std::cout << "L_out = " << L_out(x, y, iTray) << std::endl;
	//std::cout << "h_L_out = " << h_liquid(x, y, iTray) << std::endl;
	//std::cout << "V_out = " << V_out(x, y, iTray) << std::endl;
	//std::cout << "h_V_out = " << h_vapor(x, y, iTray) << std::endl;
	f[nC] = L_in(x, y, iTray)*h_L_in(x, y, iTray)
		+ V_in(x, y, iTray)*h_V_in(x, y, iTray)
		- L_out(x, y, iTray)*h_liquid(x, y, iTray)
		- V_out(x, y, iTray)*h_vapor(x, y, iTray)
		+ F(x, y, iTray)*h_F_in(x, y, iTray); //dU/dt: energy balance
}

template<typename Real>
void DistillationColumnNdae<Real>::Eval_f(Real *f, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const
{
	Eval_f_impl(f, time, x, y, p, mode);
}


template<typename Real>
template<typename T>
void DistillationColumnNdae<Real>::Eval_g_impl(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const
{
	for (int i = 0; i < N_Y; ++i) {
		g[i] = -2.0;
	}
	T g_TRAY[NTRAYVARS];
	for (int iTray = 0; iTray < nTrays; ++iTray) {
		Eval_g_tray(g_TRAY, time, x, y, p, mode, iTray);
		for (int i = 0; i < NTRAYVARS; ++i) {
			g[iTray*NTRAYVARS + i] = g_TRAY[i];
		}
	}
	for (int iTray = 1; iTray < nTrays; ++iTray) {
		//tray connections for vapor
		if (iTray < nTrays - 1) {
			g[iTray*NTRAYVARS + 2 * nC + 14] = V_in(x, y, iTray) - V_out(x, y, iTray + 1); //define V_in
			g[iTray*NTRAYVARS + 2 * nC + 15] = h_V_in(x, y, iTray) - h_vapor(x, y, iTray + 1); //define h_V_in
			for (int jComp = 0; jComp < nC; ++jComp) {
				g[iTray*NTRAYVARS + 2 * nC + 16 + jComp] = y_V_in(x, y, jComp, iTray) - y_i(x, y, jComp, iTray + 1); //y_V_in
			}
		}
		else
		{ //bottom tray
			g[iTray*NTRAYVARS + 2 * nC + 14] = V_in(x, y, iTray) - n_flow(x,y,p); //define V_in
			g[iTray*NTRAYVARS + 2 * nC + 15] = h_V_in(x, y, iTray) - p_h_V_in; //define h_V_in
			for (int jComp = 0; jComp < nC; ++jComp) {
				g[iTray*NTRAYVARS + 2 * nC + 16 + jComp] = y_V_in(x, y, jComp, iTray) - p_y_V_in[jComp]; //y_V_in
			}
		}
		//tray connections for liquid
		g[iTray*NTRAYVARS + 3 * nC + 16] = L_in(x, y, iTray) - L_out(x, y, iTray - 1); //define L_in
		g[iTray*NTRAYVARS + 3 * nC + 17] = h_L_in(x, y, iTray) - h_liquid(x, y, iTray - 1); //define h_L_in
		for (int jComp = 0; jComp < nC; ++jComp) {
			g[iTray*NTRAYVARS + 3 * nC + 18 + jComp] = x_L_in(x, y, jComp, iTray) - x_i(x, y, jComp, iTray - 1); //x_L_in
		}
		//tray connections for potential feed
		g[iTray*NTRAYVARS + 4 * nC + 18] = F(x, y, iTray)-p_F[iTray]; // no feed
		//std::cout << h_F_in(x, y, iTray) << std::endl;
		//std::cout << p_h_F[iTray] << std::endl;
		g[iTray*NTRAYVARS + 4 * nC + 19] = h_F_in(x, y, iTray) - p_h_F[iTray]; //define h_L_in
		for (int jComp = 0; jComp < nC; ++jComp) {
			g[iTray*NTRAYVARS + 4 * nC + 20 + jComp] = z_F(x, y, jComp, iTray) - p_z_F[iTray][jComp]; //x_L_in
		}
		//vapor height
		g[iTray*NTRAYVARS + 5 * nC + 20] = H_0V(x,y,iTray)-(alpha_v[iTray]*(Pressure(x,y,iTray)-Pressure(x,y,iTray-1))-H_0L(x,y,iTray-1));
	}

	//top tray connections

	//int iTray = 0;
	//tray connections for vapor
	g[0*NTRAYVARS + 2 * nC + 14] = V_in(x, y, 0) - V_out(x, y, 0 + 1); //define V_in
	g[0*NTRAYVARS + 2 * nC + 15] = h_V_in(x, y, 0) - h_vapor(x, y, 0 + 1); //define h_V_in
	for (int jComp = 0; jComp < nC; ++jComp) {
		g[0*NTRAYVARS + 2 * nC + 16 + jComp] = y_V_in(x, y, jComp, 0) - y_i(x, y, jComp, 0 + 1); //y_V_in
	}

	//tray connections for liquid
	g[0*NTRAYVARS + 3 * nC + 16] = L_in(x, y, 0) - F_out_condenser(x, y); //define L_in of top tray
	g[0*NTRAYVARS + 3 * nC + 17] = h_L_in(x, y, 0) - h_out_condenser(x, y); //define h_L_in of top tray
	for (int jComp = 0; jComp < nC; ++jComp) {
		g[0*NTRAYVARS + 3 * nC + 18 + jComp] = x_L_in(x, y, jComp, 0) - x_i_condenser(x, y, jComp); //x_L_in of top tray
	}
	//tray connections for potential feed
	g[0*NTRAYVARS + 4 * nC + 18] = F(x, y, 0) - p_F[0]; // no feed
	g[0*NTRAYVARS + 4 * nC + 19] = h_F_in(x, y, 0) - p_h_F[0]; //define h_L_in
	for (int jComp = 0; jComp < nC; ++jComp) {
		g[0*NTRAYVARS + 4 * nC + 20 + jComp] = z_F(x, y, jComp, 0) - p_z_F[0][jComp]; //x_L_in
	}
	g[0 * NTRAYVARS + 5 * nC + 20] = H_0V(x, y, 0) -(alpha_v[0] * (Pressure(x, y, 0) - P_out_(x, y)));

	// condenser
	T g_CONDENSER[NCONDENSERVARS];
	Eval_g_condenser(g_CONDENSER, time, x, y, p, mode);
	for (int i = 0; i < NCONDENSERVARS; ++i) {
		g[nTrays*NTRAYVARS + i] = g_CONDENSER[i];
	}
	g[nTrays*NTRAYVARS + NCONDENSERVARS] = x_product_purity(x, y, p) - (y_i(x, y, 0, 0) - x_set) / (1 - x_set);
	g[nTrays*NTRAYVARS + NCONDENSERVARS + 1] = n_product(x, y, p) - (1 - splitfactor(x, y, p))*V_out(x, y, 0);
  int itheta_ = this->get_itheta();
	if (itheta_ > N_P - 1) {
		std::cout << "itheta = " << itheta_ << std::endl;
	}
	g[nTrays*NTRAYVARS + NCONDENSERVARS + 2]=splitfactor(x, y, p) - ((splitfactor_ub-splitfactor_lb)*p[itheta_]+splitfactor_lb);
	g[nTrays*NTRAYVARS + NCONDENSERVARS + 3] = n_flow(x, y, p) - ((n_flow_ub-n_flow_lb) * p[NINTERVALS + itheta_]+n_flow_lb);
	//std::cout << "SplitFactor = " << splitfactor(x, y, p) << ", " << p[itheta_] << std::endl;
	//std::cout << "n_flow = " << n_flow(x, y, p) << ", " << p[NINTERVALS + itheta_] << std::endl;

	//T temp = 0.0;
	//for (int i = 0; i < N_Y; ++i) {
	//	temp = g[i] * g[i];
	//	if (temp > 1e-6) {
	//		std::cout << "i = " << i << ", g[i]*g[i] = " << temp << std::endl;
	//	}
	//}
}

template<typename Real>
template<typename T>
void DistillationColumnNdae<Real>::Eval_g_tray(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode, const int iTray) const
{
	for (int i = 0; i < NTRAYVARS; ++i) {
		g[i] = -1.0;
	}
	T g_VLE[2 * nC + 2];
	Eval_g_VLE(g_VLE, time, x, y, p, mode, iTray);
	for (int i = 0; i < (2 * nC + 2); ++i) {
		g[i] = g_VLE[i];
	}
	vector<T> x_l(nC);
	vector<T> y_v(nC);
	for (int jComp = 0; jComp < nC; ++jComp) {
		x_l[jComp] = x_i(x, y, jComp, iTray);
		y_v[jComp] = y_i(x, y, jComp, iTray);
	}
	// enthalpies
	g[2 * nC + 2] = h_vapor(x, y, iTray) / calc_hV(Temperature(x, y, iTray), Pressure(x, y, iTray), y_v) - 1;
	g[2 * nC + 3] = h_liquid(x, y, iTray) / calc_hL(Temperature(x, y, iTray), Pressure(x, y, iTray), x_l) - 1;
	g[2 * nC + 4] = h_(x, y, iTray) / (vfrac(x, y, iTray)*h_vapor(x, y, iTray) + (1 - vfrac(x, y, iTray))*h_liquid(x, y, iTray)) - 1;

	//densitites
	g[2 * nC + 5] = rho_vapor(x, y, iTray) / Pressure(x, y, iTray) * (Temperature(x, y, iTray)*R_) - 1;
	g[2 * nC + 6] = rho_liquid(x, y, iTray) / calc_rho_rackett(Temperature(x, y, iTray), x_l) - 1;
	g[2 * nC + 7] = rho_(x, y, iTray) / (vfrac(x, y, iTray)*rho_vapor(x, y, iTray) + (1 - vfrac(x, y, iTray)) * rho_liquid(x, y, iTray)) - 1;

	// summation for z
	T sum_z = 0.0;
	vector<T> z_i_(nC);
	for (int jComp = 0; jComp < nC - 1; ++jComp) {
		z_i_[jComp] = z_i(x, y, jComp, iTray);
		sum_z += z_i_[jComp];
	}
	z_i_[nC - 1] = z_nC(x, y, iTray);
	sum_z += z_i_[nC - 1];
	g[2 * nC + 8] = sum_z - 1.0;

	//relation for internal energy and enthalpy
	//T U_t = U_total(x, y, iTray);
	//std::cout << "U_total = " << U_total(x, y, iTray) << std::endl;
	//std::cout << "M = " << M(x, y, iTray) << std::endl;
	//std::cout << "vfrac = " << vfrac(x, y, iTray)  << std::endl;
	//std::cout << "h_vapor = " << h_vapor(x, y, iTray) << std::endl;
	//std::cout << "R*T = " << R_*Temperature(x, y, iTray) << std::endl;
	//std::cout << "h_liquid = " << h_liquid(x, y, iTray) << std::endl;
	g[2 * nC + 9] = U_total(x, y, iTray) / (M(x, y, iTray)*(vfrac(x, y, iTray)*(h_vapor(x, y, iTray) - R_*Temperature(x, y, iTray)) + (1 - vfrac(x, y, iTray))*h_liquid(x, y, iTray))) - 1;

	//liquid outflow
	if (mode[4 * iTray + 2]) {
		g[2 * nC + 10] = L_out(x, y, iTray) - kd_tray[iTray] * pow((1 - vfrac(x, y, iTray))*M(x, y, iTray), 1.5);
	}
	else
	{
		g[2 * nC + 10] = L_out(x, y, iTray); // no liquid outflow
	}

	//vapor outflow
	g[2 * nC + 11] = H_0L(x, y, iTray) - alpha_l[iTray] * (1 - vfrac(x, y, iTray))*M(x, y, iTray) / (pi*pow(diameter[iTray], 2) / 4.0 * rho_liquid(x, y, iTray));
	if (mode[4 * iTray + 3]) {
		g[2 * nC + 12] = V_out(x, y, iTray) - alpha_w[iTray] * pow(H_0V(x, y, iTray), 0.5);
	}
	else
	{
		g[2 * nC + 12] = V_out(x, y, iTray); // no vapor outflow
	}

	// restriction on volume
	g[2 * nC + 13] = pi*pow(diameter[iTray], 2) / 4.0 * height[iTray] - (vfrac(x, y, iTray)*M(x, y, iTray) / rho_vapor(x, y, iTray) + (1 - vfrac(x, y, iTray))*M(x, y, iTray) / rho_liquid(x, y, iTray));
}

template<typename Real>
template<typename T>
void DistillationColumnNdae<Real>::Eval_g_condenser(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const
{
	//total condenser model
	for (int i = 0; i < NCONDENSERVARS; ++i) {
		g[i] = -1.0;
	}
	vector<T> lngamma_i(nC);
	//vector<T> z_i_(nC);
	vector<T> x_l(nC);
	vector<T> y_v(nC);
	for (int jComp = 0; jComp < nC; ++jComp) {
		x_l[jComp] = x_i_condenser(x, y, jComp);
		y_v[jComp] = y_i_condenser(x, y, jComp);
		//std::cout << "x_l[" << jComp << "] = " << x_l[jComp] << std::endl;
		//z_i_[jComp] = z_i_condenser(x, y, jComp);
	}

	lngamma_i = calc_lngamma_i(Temperature_condenser(x, y), Pressure_condenser(x, y), x_l);

	std::vector<T> pi0(nC);
	pi0 = calc_pi0_XANT(Temperature_condenser(x, y));
	//T beta_ = beta(x, y, iTray);

	for (int jComp = 0; jComp < nC; ++jComp) {
		// relaxed equilibrium condition
		g[jComp] = y_v[jComp] - pi0[jComp] / Pressure_condenser(x, y) * exp(lngamma_i[jComp])*x_l[jComp];
		// mass balance equation
		//g[jComp + nC] = z_i_[jComp] - vfrac(x, y, iTray)*y_v[jComp] - (1 - vfrac(x, y, iTray))*x_l[jComp];
	}

	// summation equation
	T sum_y = 0.0;
	T sum_x = 0.0;
	for (int jComp = 0; jComp < nC; jComp++) {
		sum_y += y_v[jComp];
		sum_x += x_l[jComp];
	}
	g[nC] = sum_y - sum_x;
	//std::cout << F_in_condenser(x, y) << std::endl;
	//std::cout << F_out_condenser(x, y) << std::endl;
	g[nC + 1] = F_in_condenser(x, y, p) - F_out_condenser(x, y);
	g[nC + 2] = Q_in_condenser(x, y) + F_in_condenser(x, y, p)*h_in_condenser(x, y) - F_out_condenser(x, y)*h_out_condenser(x,y);
	//std::cout << h_out_condenser(x, y) << std::endl;
	//std::cout << calc_hL(Temperature_condenser(x, y), Pressure_condenser(x, y), x_l) << std::endl;
	//std::cout << Pressure_condenser(x, y) << std::endl;
	//std::cout << Temperature_condenser(x, y) << std::endl;
	g[nC + 3] = h_out_condenser(x, y) / calc_hL(Temperature_condenser(x, y), Pressure_condenser(x, y), x_l) - 1;
}

template<typename Real>
template<typename T>
void DistillationColumnNdae<Real>::Eval_g_VLE(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode, const int iTray) const
{
	vector<T> lngamma_i(nC);
	vector<T> z_i_(nC);
	vector<T> x_l(nC);
	vector<T> y_v(nC);
	for (int jComp = 0; jComp < nC; ++jComp) {
		x_l[jComp] = x_i(x, y, jComp, iTray);
		y_v[jComp] = y_i(x, y, jComp, iTray);
	}
	for (int jComp = 0; jComp < nC - 1; ++jComp) {
		z_i_[jComp] = z_i(x, y, jComp, iTray);
		//y_v[jComp] = y_i(x, y, jComp, iTray);
	}
	z_i_[nC - 1] = z_nC(x, y, iTray);

	//T Temp = Temperature(x, y, iTray);
	//T press = Pressure(x, y, iTray);
	lngamma_i = calc_lngamma_i(Temperature(x, y, iTray), Pressure(x, y, iTray), x_l);
	//for (int jComp = 0; jComp < nC; ++jComp) {
	//	gamma_i[jComp] = exp(lngamma_i[jComp]);
	//}
	std::vector<T> pi0(nC);
	pi0 = calc_pi0_XANT(Temperature(x, y, iTray));
	//T beta_ = beta(x, y, iTray);

	for (int jComp = 0; jComp < nC; ++jComp) {
		// relaxed equilibrium condition
		g[jComp] = y_v[jComp] - beta(x, y, iTray)*pi0[jComp] / Pressure(x, y, iTray) * exp(lngamma_i[jComp])*x_l[jComp];
		// mass balance equation
		g[jComp + nC] = z_i_[jComp] - vfrac(x, y, iTray)*y_v[jComp] - (1 - vfrac(x, y, iTray))*x_l[jComp];
	}

	// summation equation
	T sum_y = 0.0;
	T sum_x = 0.0;
	for (int jComp = 0; jComp < nC; jComp++) {
		sum_y += y_v[jComp];
		sum_x += x_l[jComp];
	}
	g[2 * nC] = sum_y - sum_x;

	if (mode[iTray * 4] == 0) {
		g[2 * nC + 1] = vfrac(x, y, iTray); // liquid only
	}
	else if (mode[iTray * 4 + 1] == 0) {
		g[2 * nC + 1] = 1 - vfrac(x, y, iTray); // vapor only
	}
	else {
		g[2 * nC + 1] = beta(x, y, iTray) - 1; // vapor-liquid equilibrium
	}
}


template<typename Real>
void DistillationColumnNdae<Real>::Eval_g(Real *g, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const
{
	Eval_g_impl(g, time, x, y, p, mode);
}

template<typename Real>
void DistillationColumnNdae<Real>::Eval_a1_f(Real*f, Real *a1_f, Real& time, Real& a1_time,
	Real *x, Real *a1_x, Real *y, Real *a1_y,
	Real *p, Real* a1_p, const std::vector<int>& mode) const
{

	AD_A1_CREATE
		AD_A1_RESET

		AD_A1_DEC2(f, N_X)
		AD_A1_DEC1(time)
		AD_A1_DEC2(x, N_X)
		AD_A1_DEC2(y, N_Y)
		AD_A1_DEC2(p, N_P)

		Eval_f_impl(ad_f, ad_time, ad_x, ad_y, ad_p, mode);
	AD_A1_INT2(f, N_X)
		AD_A1_GET1(time)
		AD_A1_GET2(x, N_X)
		AD_A1_GET2(y, N_Y)
		AD_A1_GET2(p, N_P)

}




template<typename Real>
void DistillationColumnNdae<Real>::Eval_a1_g(Real* g, Real *a1_g, Real& time, Real& a1_time,
	Real *x, Real *a1_x, Real *y, Real *a1_y,
	Real *p, Real* a1_p, const std::vector<int>& mode) const
{
	AD_A1_CREATE
		AD_A1_RESET

		AD_A1_DEC2(g, N_Y)
		AD_A1_DEC1(time)
		AD_A1_DEC2(x, N_X)
		AD_A1_DEC2(y, N_Y)
		AD_A1_DEC2(p, N_P)

		Eval_g_impl(ad_g, ad_time, ad_x, ad_y, ad_p, mode);
	AD_A1_INT2(g, N_Y)

		AD_A1_GET2(g, N_Y)
		AD_A1_GET1(time)
		AD_A1_GET2(x, N_X)
		AD_A1_GET2(y, N_Y)
		AD_A1_GET2(p, N_P)
}

template<typename Real>
template<typename T>
void DistillationColumnNdae<Real>::Eval_sigma_impl(T* sigma, T& time,
	T* x, T *y, T* p, const std::vector<int>& mode) const
{
	//sigma_fcn(sigma,time,x,y,p,mode);
	for (int iTray = 0; iTray < nTrays; ++iTray) {
		if (mode[4 * iTray]) { //vapor phase exists
			sigma[4 * iTray] = vfrac(x, y, iTray);
		}
		else { //currently no vapor phase
			sigma[4 * iTray] = beta(x, y, iTray) - 1;
		}

		if (mode[4 * iTray + 1]) { // liquid phase exists
			sigma[4 * iTray + 1] = 1 - vfrac(x, y, iTray);
		}
		else { // currently no liquid phase
			sigma[4 * iTray + 1] = 1 - beta(x, y, iTray);
		}

		if (mode[4 * iTray + 2]) { //exiting liquid flow exists
			sigma[4 * iTray + 2] = (1 - vfrac(x, y, iTray))*M(x, y, iTray) - M_min[iTray];
		}
		else { // no existing liquid flow
			sigma[4 * iTray + 2] = -1.0* ((1 - vfrac(x, y, iTray))*M(x, y, iTray) - M_min[iTray]);
		}

		if (mode[4 * iTray + 3]) { // existing vapor flow >0
			sigma[4 * iTray + 3] = H_0V(x, y, iTray);
		}
		else { // no vapor flow
			sigma[4 * iTray + 3] = -H_0V(x, y, iTray);
		}
	}
	//for (int i = 0; i < N_S; ++i) {
	//	if (sigma[i] < 0) {
	//		std::cout << "At t = " << time << " sigma[" << i << "] = " << sigma[i] << std::endl;
	//	}
	//}
	//if (mode[2]) {
	//	sigma[2] = 10 - time;
	//}
	//else {
	//	sigma[2] = time;
	//}
	//std::cout << "time = " << time << ", " << "sigma[" << N_S - 1 << "] = " << sigma[N_S - 1] << ", " << "mode[" << N_S - 1 << "] = " << mode[N_S - 1] << ", itheta = " << get_itheta() << std::endl;
	//std::cout << "time = " << time << ", ";
	//for (int i = 0; i < N_S; i++) {
	//	std::cout << "sigma[" << i << "] = " << sigma[i] << ", ";
	//}
	//std::cout << std::endl;
}

template<typename Real>
void DistillationColumnNdae<Real>::Eval_sigma(Real* sigma, Real& time,
	Real* x, Real *y, Real* p, const std::vector<int>& mode) const
{
	Eval_sigma_impl(sigma, time, x, y, p, mode);
}

template<typename Real>
void DistillationColumnNdae<Real>::Eval_a1_sigma(Real* sigma, Real* a1_sigma, Real& time, Real& a1_time,
	Real *x, Real *a1_x, Real *y, Real *a1_y,
	Real *p, Real* a1_p, const std::vector<int>& mode) const
{

	AD_A1_CREATE
		AD_A1_RESET

		AD_A1_DEC2(sigma, N_S)
		AD_A1_DEC1(time)
		AD_A1_DEC2(x, N_X)
		AD_A1_DEC2(y, N_Y)
		AD_A1_DEC2(p, N_P)

		Eval_sigma_impl(ad_sigma, ad_time, ad_x, ad_y, ad_p, mode);
	AD_A1_INT2(sigma, N_S)

		AD_A1_GET2(sigma, N_S)
		AD_A1_GET1(time)
		AD_A1_GET2(x, N_X)
		AD_A1_GET2(y, N_Y)
		AD_A1_GET2(p, N_P)
}

template<typename Real>
template<typename T>
void DistillationColumnNdae<Real>::Eval_psi_impl(T *psi, T& time, T* x, T *y,
	T* p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const
{
	for (int i = 0; i < N_X; ++i) {
		psi[i] = x[i];
	}
}

template<typename Real>
void DistillationColumnNdae<Real>::Eval_psi(Real *psi, Real& time, Real* x, Real *y,
	Real* p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const
{
	Eval_psi_impl(psi, time, x, y, p, newMode, oldMode);
}

template<typename Real>
void DistillationColumnNdae<Real>::Eval_a1_psi(Real*psi, Real *a1_psi, Real& time, Real& a1_time,
	Real *x, Real *a1_x, Real *y, Real *a1_y,
	Real *p, Real* a1_p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const
{
	AD_A1_CREATE
		AD_A1_RESET

		AD_A1_DEC2(psi, N_X)
		AD_A1_DEC1(time)
		AD_A1_DEC2(x, N_X)
		AD_A1_DEC2(y, N_Y)
		AD_A1_DEC2(p, N_P)

		Eval_psi_impl(ad_psi, ad_time, ad_x, ad_y, ad_p, newMode, oldMode);
	AD_A1_INT2(psi, N_X)

		AD_A1_GET1(time)
		AD_A1_GET2(x, N_X)
		AD_A1_GET2(y, N_Y)
		AD_A1_GET2(p, N_P)
}

template<typename Real>
template<typename T>
void DistillationColumnNdae<Real>::Eval_psi0_impl(T *psi0, T* p) const
{
	for (int i = 0; i < N_X; ++i) {
		psi0[i] = x_0[i];
	}
	//psi0_fcn(psi0,p);
}

template<typename Real>
void DistillationColumnNdae<Real>::Eval_psi0(Real *psi0, Real* p) const
{
	Eval_psi0_impl(psi0, p);
}

template<typename Real>
void DistillationColumnNdae<Real>::Eval_a1_psi0(Real *psi0, Real *a1_psi0, Real* p, Real *a1_p) const
{
	AD_A1_CREATE
		AD_A1_RESET

		AD_A1_DEC2(psi0, N_X)
		AD_A1_DEC2(p, N_P)

		Eval_psi0_impl(ad_psi0, ad_p);
	AD_A1_INT2(psi0, N_X)

		AD_A1_GET2(p, N_P)
}


template<typename Real>
template<typename T>
void DistillationColumnNdae<Real>::Eval_phi_impl(T& phi, T *Theta, T **X, T **Y, T *p) const
{
	//for (int i = 0; i < N_THETA; ++i) {
	//	std::cout << i << ", " << Theta[i] << ", " << X[i][1] << std::endl;
	//}
	//for (int i = 0; i < N_X; ++i) {
	//	std::cout << Theta[0] << ", " << X[0][i] << std::endl;
	//}
	phi = X[N_THETA - 1][N_X-3];

}

template<typename Real>
void DistillationColumnNdae<Real>::Eval_phi(Real& phi, Real *Theta, Real **X, Real **Y, Real *p) const
{
	Eval_phi_impl(phi, Theta, X, Y, p);
}

template<typename Real>
void DistillationColumnNdae<Real>::Eval_a1_phi(Real& phi, Real& a1_phi, Real *Theta, Real* a1_Theta,
	Real **X, Real ** a1_X,
	Real **Y, Real **a1_Y, Real *p, Real *a1_p) const
{
	AD_A1_CREATE
		AD_A1_RESET

		AD_A1_DEC1(phi)
		AD_A1_DEC2(Theta, N_THETA)
		AD_A1_DEC3(X, N_X, N_THETA)
		AD_A1_DEC3(Y, N_Y, N_THETA)
		AD_A1_DEC2(p, N_P)

		Eval_phi_impl(ad_phi, ad_Theta, ad_X, ad_Y, ad_p);
	AD_A1_INT1(phi)

		AD_A1_GET2(Theta, N_THETA)
		AD_A1_GET3(X, N_X, N_THETA)
		AD_A1_GET3(Y, N_Y, N_THETA)
		AD_A1_GET2(p, N_P)
}
using Gt1s = ad::gt1s<double>::type;
inline std::vector<Gt1s> CreateGt1sVector(const double* values, const double* t1_values, const unsigned n)
{
	std::vector<Gt1s> gt1sVector(n);
	for (unsigned i = 0; i < n; ++i) {
		ad::value(gt1sVector[i]) = values[i];
		ad::derivative(gt1sVector[i]) = t1_values[i];
	}
	return gt1sVector;
}

inline std::vector<Gt1s> CreateGt1sVector(const unsigned n)
{
	std::vector<Gt1s> gt1sVector(n);
	for (unsigned i = 0; i < n; ++i) {
		ad::value(gt1sVector[i]) = 0.0;
		ad::derivative(gt1sVector[i]) = 0.0;
	}
	return gt1sVector;
}

inline void ExtractGt1sVector(double *vec, double *t1_vec, const std::vector<Gt1s>& ad_vec) {
	for (std::size_t i = 0; i < ad_vec.size(); ++i) {
		vec[i] = ad::value(ad_vec[i]);
		t1_vec[i] = ad::derivative(ad_vec[i]);
	}
}

inline Gt1s CreateGt1sScalar(const double value) {
	Gt1s scalar;
	ad::value(scalar) = value;
	ad::derivative(scalar) = 0.0;
	return scalar;
}

inline void ExtractGt1sScalar(double& scalar, double& t1_scalar, const Gt1s& ad_scalar) {
	scalar = ad::value(ad_scalar);
	t1_scalar = ad::derivative(ad_scalar);
}



template<typename Real>
void DistillationColumnNdae<Real>::Eval_t1_f(Real *f, Real *t1_f, Real& time, Real& t1_time, Real *x, Real *t1_x,
	Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const
{

	auto ad_f = CreateGt1sVector(N_X);
	auto ad_time = CreateGt1sScalar(time);
	auto ad_x = CreateGt1sVector(x, t1_x, N_X);
	auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
	auto ad_p = CreateGt1sVector(p, t1_p, N_P);

	Eval_f_impl(ad_f.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), mode);

	ExtractGt1sVector(f, t1_f, ad_f);


}

template<typename Real>
void DistillationColumnNdae<Real>::Eval_t1_g(Real *g, Real *t1_g, Real& time, Real& t1_time, Real *x, Real *t1_x,
	Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const
{
	auto ad_g = CreateGt1sVector(N_Y);
	auto ad_time = CreateGt1sScalar(time);
	auto ad_x = CreateGt1sVector(x, t1_x, N_X);
	auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
	auto ad_p = CreateGt1sVector(p, t1_p, N_P);

	Eval_g_impl(ad_g.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), mode);

	ExtractGt1sVector(g, t1_g, ad_g);
}


template<typename Real>
void DistillationColumnNdae<Real>::Eval_t1_sigma(Real* sigma, Real* t1_sigma, Real& time, Real& t1_time, Real *x, Real *t1_x,
	Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const
{
	auto ad_sigma = CreateGt1sVector(N_S);
	auto ad_time = CreateGt1sScalar(time);
	auto ad_x = CreateGt1sVector(x, t1_x, N_X);
	auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
	auto ad_p = CreateGt1sVector(p, t1_p, N_P);

	Eval_sigma_impl(ad_sigma.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), mode);

	ExtractGt1sVector(sigma, t1_sigma, ad_sigma);
}

template<typename Real>
void DistillationColumnNdae<Real>::Eval_t1_psi(Real * psi, Real *t1_psi, Real& time, Real& t1_time, Real *x, Real *t1_x,
	Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& newMode,
	const std::vector<int>& oldMode) const
{
	auto ad_psi = CreateGt1sVector(N_X);
	auto ad_time = CreateGt1sScalar(time);
	auto ad_x = CreateGt1sVector(x, t1_x, N_X);
	auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
	auto ad_p = CreateGt1sVector(p, t1_p, N_P);

	Eval_psi_impl(ad_psi.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), newMode, oldMode);

	ExtractGt1sVector(psi, t1_psi, ad_psi);
}


template<typename Real>
void DistillationColumnNdae<Real>::Eval_t1_psi0(Real *psi0, Real *t1_psi0, Real *p, Real* t1_p) const
{
	auto ad_psi0 = CreateGt1sVector(N_X);
	auto ad_p = CreateGt1sVector(p, t1_p, N_P);

	Eval_psi0_impl(ad_psi0.data(), ad_p.data());

	ExtractGt1sVector(psi0, t1_psi0, ad_psi0);
}

template<typename Real>
void DistillationColumnNdae<Real>::Eval_t1_phi(Real& phi, Real& t1_phi, Real *Theta, Real *t1_Theta, Real **X, Real ** t1_X,
	Real **Y, Real **t1_Y, Real *p, Real *t1_p) const
{
	std::vector<Gt1s*> ad_X(N_THETA), ad_Y(N_THETA);
	auto ad_X_data = CreateGt1sVector(N_THETA * N_X);
	auto ad_Y_data = CreateGt1sVector(N_THETA * N_Y);
	for (unsigned i = 0; i < N_THETA; ++i) {
		ad_X[i] = &ad_X_data[i*N_X];
		ad_Y[i] = &ad_Y_data[i*N_Y];
		for (unsigned j = 0; j < N_X; ++j) {
			ad::value(ad_X[i][j]) = X[i][j];
			ad::derivative(ad_X[i][j]) = t1_X[i][j];
		}
		for (unsigned j = 0; j < N_Y; ++j) {
			ad::value(ad_Y[i][j]) = Y[i][j];
			ad::derivative(ad_Y[i][j]) = t1_Y[i][j];
		}

	}
	auto ad_p = CreateGt1sVector(p, t1_p, N_P);
	auto ad_Theta = CreateGt1sVector(Theta, t1_Theta, N_THETA);
	Gt1s ad_phi;
	Eval_phi_impl(ad_phi, ad_Theta.data(), ad_X.data(), ad_Y.data(), ad_p.data());

	ExtractGt1sScalar(phi, t1_phi, ad_phi);

}


#endif // FLASH_EXAMPLE_NDAE_HPP
