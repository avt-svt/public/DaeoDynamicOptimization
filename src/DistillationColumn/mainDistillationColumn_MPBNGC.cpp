#include <iostream>
#include <cmath>

#include "DistillationColumnNdae.hpp"
#include "NdaeSolver.hpp"
#include "NdaeUtils.hpp"

#include "FC.h"

using namespace NdaeUtils;
// The code performs dynamic optimization of a flash example using a proximal bundle method MPBNGC.

#define MPBNGC FC_GLOBAL(mpbngc,MPBNGC)

extern "C" void MPBNGC(int& N, double *X, int *IX, double *BL,
	double *BU, int& M, int& MG, int& MC, int *IC,
	double *CL, double *CU, double *CG, double *F,
	void(*FASG)(int&, double*, int&, double*, double*, int&, int*, double*),
	double & RL, int& LMAX,
	double *GAM, double& EPS,
	double& FEAS, int& JMAX, int& NITER,
	int& NFASG, int& NOUT, int& IPRINT, int& IERR,
	int *IWORK, int& LIWORK, double *WORK, int& LWORK,
	int* IUSER, double *USER);

extern "C" void FASG(int& N, double* X, int& M, double* F, double * G, int& IERR, int* IUSER, double* USER);

int optimize_flash(std::vector<double>& X_vec, std::vector<double>& X_0);

int doSimulation(int N, double* X, bool doPlot, double* F, double * G);
int doSimulation(int N, double* X, bool doPlot, double* F);

int testInitialConditions();
int readParameterValuesFromFile(char* filename, std::vector<double>& p_out);

int optimize_flash(std::vector<double>& X_vec, std::vector<double>& X_0)
{
	// optimization loop
	const int NN = 20; // Number of optimization variables (here, number of intervals in time horizon)
	int N = NN;
	const int JJMAX = 5; // The maximum number of stored subgradients       (2 <=JJMAX)
	int JMAX = JJMAX;
	const int MM = 1; // The number of objective functions.              (1 <= MM)
	int M = MM;
	const int MMC = 0; // The number of linear constraints.               (0 <= MMC).
	int MC = MMC;
	const int MMG = 0; // The number of general constraint functions.     (0 <= MMG).
	int MG = MMG;
	const int MP1 = MM + 1;
	int NOUT = 6;
	int IPRINT = 3;
	const int LLIWORK = 2 * (MP1*(JJMAX + 1) + MMC + NN);
	int LIWORK = LLIWORK;
	const int LLWORK = NN*(NN + 2 * MP1*JJMAX + 2 * MP1 + 2 * MMC + 2 * MMG + 2 * MM + 31) / 2 + MP1*(6 * JJMAX + 10) + JJMAX + 5 * MMC + MMG + MM + 18;
	int LWORK = LLWORK;

	int NITER, NFASG, IERR, I, LMAX;
	int IC[MMC + 1], IWORK[LLIWORK], IUSER[1];

	double  RL, EPS, FEAS;
	double  X[NN], F[MM + MMG], GAM[MP1];
	double BL[NN]; // Lower bounds of X
	double BU[NN]; //  Upper bounds of X.
	int IX[NN]; //Types of box constraints for individual variables
	std::ofstream initSol("initialSolution.txt");
	for (int i = 0; i < NN; ++i) { //parameters are scaled
		BL[i] = 0;// 1e-12; // lower bound of reflux is actually 0
		BU[i] = 1; // upper bound of reflux is actually 1
		X[i] = X_0[i];// inst.p_0[i]; // initial guess for reflux (required to be a feasible point)
		initSol << X[i] << endl;
	}
	initSol.close();

	double  CG[NN*(MMC + 1)], CL[MMC + 1], CU[MMC + 1], WORK[LLWORK], USER[1];


	// define maximum number of iterations and maximum number of calls of function FASG
	NITER = 100;
	NFASG = 1000;

	IC[0] = 0;
	RL = 0.01;
	LMAX = 100;
	GAM[0] = GAM[1] = 0.5; // The MP1 vector of distance measure parameters   (0 <=GAM(I)).

						   //					   // set optimality tolerance
	EPS = 1.0E-4; // optimality tolerance
	FEAS = 1.0E-12; // feasibility tolerance

	double G[NN];
	

	MPBNGC(N, X, IX, BL, BU,
		M, MG, MC, IC, CL,
		CU, CG, F, FASG, RL,
		LMAX, GAM, EPS, FEAS, JMAX,
		NITER, NFASG, NOUT, IPRINT, IERR,
		IWORK, LIWORK, WORK, LWORK, IUSER, USER);

	std::cout << "\n" << "IERR = " << IERR << "\n" << std::endl;
	std::ofstream ofile("optimalSolution.txt");
	for (int i = 0; i<N; ++i) {
		std::cout << "X(" << i + 1 << ") = " << X[i] << std::endl;
		X_vec[i] = X[i];
		ofile << X[i] << std::endl;
	}
	ofile.close();
	std::cout << "\n" << "F(X) = " << F[0] << std::endl;
	std::cout << "NITER = " << NITER << std::endl;
	std::cout << "NFASG = " << NFASG << std::endl;
	//int a; cin >> a;

	return IERR;
}

void FASG(int& N, double* X, int& M, double* F, double * G, int& IERR, int* IUSER, double* USER)
{
	// simulation with derivatives
	doSimulation(N, X, false, F, G);
}


int main()
{
	const unsigned n_p = DistillationColumnNdae<double>::N_P;
	const unsigned nC = DistillationColumnNdae<double>::nC;
	const unsigned nTrayVars = DistillationColumnNdae<double>::NTRAYVARS;
	DistillationColumnNdae<double> dae;

	int n_y = dae.Eval_n_y();
	int n_x = dae.Eval_n_x();
	int n_sigma = dae.Eval_n_sigma();

	std::vector<double> p0;
	p0.resize(n_p);
	for (int i = 0; i < n_p; ++i) {
		p0[i] = dae.p_0[i];
	}
	double time = 0.0;

	// vector for results of dynamic optimization
	std::vector<double> x_vec;
	x_vec.resize(n_p);

	// vector for intial values for dynamic optimization
	//char* filename = "intermediateSolution_goodDirection.txt";
	//std::vector<double> p;
	//readParameterValuesFromFile(filename, p);

	//std::vector<double> x_0;
	//x_0.resize(n_p);
	//for (int i = 0; i < x_0.size(); ++i) {
	//	x_0[i] = p0[i];// dae.p_0[i]; // 0.0
	//}

	//optimize_flash(x_vec, x_0);


	std::vector<double> phi = { 0.0 };
	//std::vector<double> dphi_dp(n_p, 0.0);

	auto res = doSimulation(n_p, p0.data(), true, phi.data());

	return 0;
}

int doSimulation(int N, double* X, bool doPlot, double* F, double * G) {

	// create instance of dae
	const unsigned n_p = DistillationColumnNdae<double>::N_P;
	const unsigned n_x = DistillationColumnNdae<double>::N_X;
	const unsigned n_s = DistillationColumnNdae<double>::N_S;
	const unsigned n_y = DistillationColumnNdae<double>::N_Y;
	DistillationColumnNdae<double> dae;

	double tolerance = 1e-8;
	//set up options for integration
	NdaeSolver::Options options;
	options.SetMaxStepDenseOutput(1);
	options.SetTol(tolerance);
	options.SetStateEventTol(tolerance);
	options.SetMaxDiscontinuityTol(10.0*tolerance);
	options.SetDoPlot(doPlot);
	options.SetFilenameStates("SimulationResults.txt");
	

	if (N != n_p) {
		std::cout << "Number of parameters is n_p = " << n_p << " but there are N = " << N << " optimization variables." << std::endl;
		return -1;
	}

	// set parameters to values of X
	realtype p_0[n_p];
	for (int i = 0; i < n_p; ++i) {
		p_0[i] = X[i];
	}
	// create variables for objective (Phi) and its sensitivities (Phi_temp and Phi_p) w.r.t. optimization variables (parameters)
	realtype Phi = 0;
	realtype Phi_p[n_p];

	// run simulation
	//auto flag = NdaeSolver::EvaluatePhi(Phi, p_0, dae, options);
	auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, p_0, dae, options);

	std::ofstream ofile("intermediateSolution_.txt");
	ofile << "Objective = " << Phi << std::endl;
	for (int i = 0; i<n_p; ++i) {
		ofile << p_0[i] << " " << Phi_p[i] << std::endl;
	}
	ofile.close();
	// return objective function F and gradients w.r.t. parameters G
	F[0] = Phi;
	for (int i = 0; i < N; ++i) {
		G[i] = Phi_p[i];
		//std::cout << X[i] << ", " << G[i] << std::endl;
	}
	//std::cout << "\n" << "IERR = " << IERR << "\n" << std::endl;
	return 0;
}

int doSimulation(int N, double* X, bool doPlot, double* F) {

	// create instance of dae
	const unsigned n_p = DistillationColumnNdae<double>::N_P;
	const unsigned n_x = DistillationColumnNdae<double>::N_X;
	const unsigned n_s = DistillationColumnNdae<double>::N_S;
	const unsigned n_y = DistillationColumnNdae<double>::N_Y;
	DistillationColumnNdae<double> dae;

	double tolerance = 1e-8;
	//set up options for integration
	NdaeSolver::Options options;
	options.SetMaxStepDenseOutput(1);
	options.SetTol(tolerance);
	options.SetStateEventTol(tolerance);
	options.SetMaxDiscontinuityTol(10.0*tolerance);
	options.SetDoPlot(doPlot);
	options.SetFilenameStates("SimulationResults.txt");


	if (N != n_p) {
		std::cout << "Number of parameters is n_p = " << n_p << " but there are N = " << N << " optimization variables." << std::endl;
		return -1;
	}

	// set parameters to values of X
	realtype p_0[n_p];
	for (int i = 0; i < n_p; ++i) {
		p_0[i] = X[i];
	}
	// create variables for objective (Phi) and its sensitivities (Phi_temp and Phi_p) w.r.t. optimization variables (parameters)
	realtype Phi = 0;
	//realtype Phi_p[n_p];

	// run simulation
	//auto flag = NdaeSolver::EvaluatePhi(Phi, p_0, dae, options);
	auto flag = NdaeSolver::EvaluatePhi(Phi,p_0, dae, options);

	std::ofstream ofile("intermediateSolution_.txt");
	ofile << "Objective = " << Phi << std::endl;
	for (int i = 0; i<n_p; ++i) {
		ofile << p_0[i] << std::endl;
	}
	ofile.close();
	// return objective function F and gradients w.r.t. parameters G
	F[0] = Phi;

	return 0;
}


int readParameterValuesFromFile(char* filename, std::vector<double>& p_out) {
	//std::vector<double> p_out;
	std::ifstream inFile(filename);
	//inFile.open(filename);
	double x, dx;
	std::string line;
	std::size_t lines_count = 0;
	while (std::getline(inFile, line))
	{
		++lines_count;
	}
	const int n_ = lines_count - 1;
	std::ifstream input(filename);
	p_out.resize(n_);
	for (int i = 0; i < lines_count; ++i) {
		std::getline(input, line);
		if (i > 0) {// not for header line
			std::istringstream iss(line);
			iss >> x >> dx; 
			p_out[i - 1] = x;
			printf("%s\n", line.c_str());
			printf("i = %i: x[%i] = %e, dx[%i] = %e\n", i, i, x, i, dx);
		}
	}
	inFile.close();
	input.close();
	return 0;
}

int testInitialConditions() {
	const unsigned n_p = DistillationColumnNdae<double>::N_P;
	const unsigned nC = DistillationColumnNdae<double>::nC;
	const unsigned nTrayVars = DistillationColumnNdae<double>::NTRAYVARS;
	DistillationColumnNdae<double> dae;

	int n_y = dae.Eval_n_y();
	int n_x = dae.Eval_n_x();
	int n_sigma = dae.Eval_n_sigma();

	std::vector<double> x0;
	x0.resize(n_x);
	for (int i = 0; i < n_x; ++i) {
		x0[i] = dae.x_0[i];
	}

	std::vector<double> y0;
	y0.resize(n_y);
	for (int i = 0; i < n_y; ++i) {
		y0[i] = dae.y_0[i];
	}
	//set temperature
	//y0[2 * nC + 4] = 97.0;

	// mole fractions of vapor phase
	std::vector<double> y_v;
	y_v.resize(nC);
	for (int i = 0; i < nC; ++i) {
		y_v[i] = dae.y_i(x0.data(),y0.data(),i,0);
	}

	std::vector<double> p0;
	p0.resize(n_p);
	for (int i = 0; i < n_p; ++i) {
		p0[i] = dae.p_0[i];
	}
	double time = 0.0;

	std::vector<int> mode;
	mode.resize(n_sigma);
	for (int i = 0; i < n_sigma; ++i) {
		mode[i] = dae.mode0[i];
	}

	std::vector<double> pi0_XANT;
	pi0_XANT.resize(nC);
	pi0_XANT = dae.calc_pi0_XANT(dae.Temperature(x0.data(), y0.data(), 0));
	double T = dae.Temperature(x0.data(), y0.data(), 0);
	std::vector<double> deltah0vap;
	deltah0vap.resize(nC);// = 0.0;
	deltah0vap = dae.calc_deltah0vap_DIPPR(T);

	//set temperature
	//y0[2 * nC + 4] = 12.8 + 273.15;
	double h_V = 0.0;
	h_V = dae.calc_hV(dae.Temperature(x0.data(), y0.data(), 0), dae.Pressure(x0.data(), y0.data(), 0), y_v);

	//set temperature
	//y0[2 * nC + 4] = 97.0;
	std::vector<double> x_l(nC);// = { 0.34,0.33,0.33 };
	for (int i = 0; i < nC; ++i) {
		x_l[i] = dae.x_i(x0.data(), y0.data(), i, 0);
	}
	std::vector<double> ln_gamma_i;
	ln_gamma_i.resize(nC);
	ln_gamma_i = dae.calc_lngamma_i(dae.Temperature(x0.data(), y0.data(), 0), dae.Pressure(x0.data(), y0.data(), 0), x_l);

	double h_L = 0.0;
	h_L = dae.calc_hL(dae.Temperature(x0.data(), y0.data(), 0), dae.Pressure(x0.data(), y0.data(), 0), x_l);

	double rho_rackett = 0.0;
	rho_rackett = dae.calc_rho_rackett(dae.Temperature(x0.data(),y0.data(),0), x_l);

	std::vector<double> g_tray(nTrayVars);
	dae.Eval_g_tray(g_tray.data(), time, x0.data(), y0.data(), p0.data(), mode, 0);

	std::vector<double> g_all(n_y);
	dae.Eval_g_impl(g_all.data(), time, x0.data(), y0.data(), p0.data(), mode);

	double sum_res_squared=0.0;
	for (int i = 0; i < g_all.size(); ++i) {
		sum_res_squared += g_all[i] * g_all[i];
		if ((g_all[i] * g_all[i]) > 1e-6) {
			std::cout << "Algebraic equation " << i << " has squared residual of " << g_all[i] * g_all[i] << std::endl;
		}
		std::cout << "i = " << i << ", sum of squared res = " << sum_res_squared << std::endl;
	}

	std::vector<double> f_tray(n_x);
	dae.Eval_f_tray(f_tray.data(), time, x0.data(), y0.data(), p0.data(), mode, 0);

	return 0;
}