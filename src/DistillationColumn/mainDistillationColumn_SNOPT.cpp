#include <iostream>
#include <cmath>

#include "DistillationColumnNdae.hpp"
#include "NdaeSolver.hpp"
#include "NdaeUtils.hpp"

using namespace NdaeUtils;
// The code performs dynamic optimization of a flash example using a proximal bundle method MPBNGC.
#include "SNOPTWrapper.hpp"
#include "MyNlp_DistillationColumn.hpp"



int main()
{
	//double optTol = 1.e-6;
	////std::string s = to_string(d);
	//std::ostringstream streamObj;
	////Add double to stream
	//streamObj << optTol;
	//// Get string from output string stream
	////std::string strObj = streamObj.str();
	//std::cout << "Conversion of double to string: " << streamObj.str() << std::endl;

	//std::map<std::string, std::string> optimizerOptions;
	////optimizerOptions.insert(make_pair<std::string, std::string>("scale option", "2")); //seemingly not working
	////optimizerOptions.insert(make_pair<std::string, std::string>("Scale print", "1"));
	//optimizerOptions.insert(make_pair<std::string, std::string>("major iterations limit", "100"));
	//optimizerOptions.insert(make_pair<std::string, std::string>("minor iterations limit", "1000"));
	//optimizerOptions.insert(make_pair<std::string, std::string>("derivative level", "3"));
	//streamObj.str(std::string());
	//streamObj << optTol;
	//optimizerOptions.insert(make_pair<std::string, std::string>("major optimality tolerance", streamObj.str()));
	////optimizerOptions.insert(make_pair<std::string, std::string>("minor optimality tolerance", "5e-11"));
	//optimizerOptions.insert(make_pair<std::string, std::string>("major feasibility tolerance", streamObj.str()));
	//optimizerOptions.insert(make_pair<std::string, std::string>("function precision", streamObj.str()));
	//streamObj.str(std::string());
	//streamObj << 0.1*optTol;
	//optimizerOptions.insert(make_pair<std::string, std::string>("minor feasibility tolerance", streamObj.str()));


	double optTol = 1.e-6;
	double feasTol = optTol;
	double funcPrec = 1.e-08;
	double major_step_limit = 1.0;
	double linesearchTol = 0.9999;
	//std::string s = to_string(d);
	std::ostringstream streamObj;
	//Add double to stream
	streamObj << optTol;
	// Get string from output string stream
	//std::string strObj = streamObj.str();
	std::cout << "Conversion of double to string: " << streamObj.str() << std::endl;

	std::map<std::string, std::string> optimizerOptions;
	//optimizerOptions.insert(make_pair<std::string, std::string>("scale option", "2")); //seemingly not working
	//optimizerOptions.insert(make_pair<std::string, std::string>("Scale print", "1"));
	optimizerOptions.insert(make_pair<std::string, std::string>("major iterations limit", "500"));
	optimizerOptions.insert(make_pair<std::string, std::string>("minor iterations limit", "1000"));

	optimizerOptions.insert(make_pair<std::string, std::string>("derivative level", "3"));

	streamObj.str(std::string());
	streamObj << optTol;
	optimizerOptions.insert(make_pair<std::string, std::string>("major optimality tolerance", streamObj.str()));
	optimizerOptions.insert(make_pair<std::string, std::string>("major feasibility tolerance", streamObj.str()));

	streamObj.str(std::string());
	streamObj << funcPrec;
	optimizerOptions.insert(make_pair<std::string, std::string>("function precision", streamObj.str()));

	streamObj.str(std::string());
	streamObj << feasTol;
	optimizerOptions.insert(make_pair<std::string, std::string>("minor feasibility tolerance", streamObj.str()));

	streamObj.str(std::string());
	streamObj << major_step_limit;
	optimizerOptions.insert(make_pair<std::string, std::string>("major step limit", streamObj.str()));

	streamObj.str(std::string());
	streamObj << linesearchTol;
	optimizerOptions.insert(make_pair<std::string, std::string>("linesearch tolerance", streamObj.str()));
	optimizerOptions.insert(make_pair<std::string, std::string>("major print level", "111111"));
	optimizerOptions.insert(make_pair<std::string, std::string>("minor print level", "11111"));

	//optimizerOptions.insert(make_pair<std::string, std::string>("elastic weight", "1"));
	SNOPTWrapper snoptWrapper(optimizerOptions);
	MyNlp myNlp;
	snoptWrapper.solve(&myNlp);

	return 0;
}