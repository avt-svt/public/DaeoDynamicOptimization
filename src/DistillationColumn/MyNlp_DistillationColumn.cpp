#include "MyNlp_DistillationColumn.hpp"

void MyNlp::EvalDimensions(int& numVariables, int& numConstraints) {
  numVariables=dae.N_P;
  numConstraints=1; // dummy constraint for now
}


void MyNlp::EvalVariableBounds(double *lowerBounds, double* upperBounds, int numVariables){
  for(int i=0; i<numVariables; i++) {
	  lowerBounds[i] = 0.0;// -1.0*dae.scaling_p;
    upperBounds[i]=1.0;
  }
}


void MyNlp::EvalConstraintBounds(double *lowerBounds, double* upperBounds, int numConstraints){
  lowerBounds[0]=-1e20;
  //lowerBounds[1]=-1e20;
  upperBounds[0]= 1e20;
  //upperBounds[1]=10;
}


void MyNlp::EvalInitialGuess(double* x, int numVariables){
	std::ofstream ofile("initialSolution.txt");
	for (int i = 0; i < n_p; ++i) {
		x[i] = dae.p_0[i];
		ofile << x[i] << std::endl;
	}
	ofile.close();
}


void MyNlp::EvalObjectiveAndConstraints(int derivativeOrder, double* x, double& f, double* grad_f, 
  double* g, double** jac_g, int numVariables, int numConstraints){
	const int numVars = dae.N_P;

	// create variables for objective (Phi) and its sensitivities (Phi_temp and Phi_p) w.r.t. optimization variables (parameters)
	realtype Phi = 0;
	realtype Phi_p[numVars];
	for (int i = 0; i < numVariables; ++i) {
		Phi_p[i] = 0.0;// grad_f[i];
	}
  //  if( derivativeOrder >= 0) {
		//// run simulation
		//auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, x, dae, options);
		//f = Phi;
		//g[0]=Phi;
  //  }

  //  if( derivativeOrder >= 1) {
		//for (int i = 0; i < numVariables; ++i) {
		//	grad_f[i] = Phi_p[i];
		//	std::cout << x[i] << ", " << grad_f[i] << std::endl;
		//	jac_g[0][i] = grad_f[i];
		//}
  //  }
	if (derivativeOrder == 3) {
		// run simulation
		//dae.setObjMode(ObjectiveMode::ComputeObjective);
		auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, x, dae, options);
		f = Phi;
	}
	else if (derivativeOrder == 4) {
		//std::cout << "Objective: " << f << std::endl;
		g[0] = 0.0;
		//std::cout << "Constraint: " << g[0] << std::endl;
	}
	else {
		// run simulation
		auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, x, dae, options);
		f = Phi;
		g[0]=0.0;
	}

	if (derivativeOrder >= 1) {
		for (int i = 0; i < numVariables; ++i) {
			grad_f[i] = Phi_p[i];
			std::cout << x[i] << ", " << grad_f[i] << std::endl;
			jac_g[0][i] = 0.0;// grad_f[i];
		}
	}

}

void MyNlp::FinalizeSolution(int n, const double * x,
	const double* z_L, const double* z_U,
	int m, const double* g, const double* lambda,
	double obj_value) {
	const int numVars = dae.N_P;

	double Phi = 0;
	double Phi_p[numVars];

	options.SetDoPlot(TRUE);
	options.SetFilenameStates("SimulationResults_DistillationColumn_final.txt");
	auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, x, dae, options);
	std::ofstream ofile("finalSolution.txt");
	ofile << "Objective value = " << Phi << std::endl;
	for (int i = 0; i < numVars; ++i) {
		//std::cout << "X(" << i + 1 << ") = " << X[i] << std::endl;
		//X_vec[i] = X[i];
		ofile << "p_final[" << i << "] = " << x[i] << "; // " << Phi_p[i] << std::endl;
	}
	ofile.close();
}

MyNlp::MyNlp() {
	//set up options for integration
	const double tol = 1e-08;
	options.SetMaxStepDenseOutput(10.0);
	options.SetTol(tol);
	options.SetStateEventTol(2*tol);
	options.SetMaxDiscontinuityTol(10.0*tol);
	options.SetDoPlot(FALSE);
	options.SetFilenameStates("SimulationResults_Flash.txt");
	//options.SetAbsTolAdjoint(1e-8);
	//options.SetRelTolAdjoint(1e-8);
	std::cout << "Adjoint abs tol: " << options.GetAbsTolAdjoint() << std::endl;
	std::cout << "Adjoint rel tol: " << options.GetRelTolAdjoint() << std::endl;
}

