#ifndef MY_NLP_FLASHEXAMPLE_HPP__
#define MY_NLP_FLASHEXAMPLE_HPP__

#include "NdaeSolver.hpp"
#include "NdaeUtils.hpp"

#include "JadeNlp.hpp"
#include "DistillationColumnNdae.hpp"

class MyNlp : public JadeNlp {
public:
	MyNlp();
  virtual void EvalDimensions(int& numVariables, int& numConstraints);
  virtual void EvalVariableBounds(double *lowerBounds, double* upperBounds, int numVariables);
  virtual void EvalConstraintBounds(double *lowerBounds, double* upperBounds, int numConstraints);
  virtual void EvalInitialGuess(double* x, int numVariables);
  virtual void EvalObjectiveAndConstraints(int derivativeOrder, double* x, double& f, double* grad_f, 
    double* g, double** jac_g, int numVariables, int numConstraints);
  virtual void FinalizeSolution(int n, const double * x,
	  const double* z_L, const double* z_U,
	  int m, const double* g, const double* lambda,
	  double obj_value);
private:
	DistillationColumnNdae<double> dae;
	const unsigned n_p = dae.N_P;
	NdaeSolver::Options options;
};

#endif
