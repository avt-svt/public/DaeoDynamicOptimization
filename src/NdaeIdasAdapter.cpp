#include "NdaeIdasAdapter.hpp"
#include "NdaeUtils.hpp"
#include <idas/idas.h>
#include <idas/idas_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_math.h>
#include <vector>
#include <fstream>
#include <stack>
#include <iostream>
#include <cstdlib>

#include <sstream>


NdaeIdasAdapter::NdaeIdasAdapter(const NdaeIdasAdapter &)
{
  std::cout << "Error: copy constructor of IdasAdapterNDAE should never be called" << std::endl;
  exit(1);
}

NdaeIdasAdapter::NdaeIdasAdapter(Ndae<realtype>& dae, const realtype *p, const Options& options, bool doAdjoint)
{
  Init(dae,p,options,doAdjoint);
}


NdaeIdasAdapter::~NdaeIdasAdapter()
{
  FreeResources();
}

int NdaeIdasAdapter::EvaluateObjective(realtype& objective, const realtype *p, Ndae<realtype>& dae, const Options& options)
{
  bool doAdjoint = false;
  NdaeIdasAdapter idas(dae,p,options,doAdjoint);
  return idas.Run(objective,0);
}

int NdaeIdasAdapter::EvaluateObjAndGrad(realtype &objective, realtype *gradient,
                                        const realtype *p, Ndae<realtype>& dae, const Options& options)
{
  bool doAdjoint = true;
  NdaeIdasAdapter idas(dae,p,options,doAdjoint);
  return idas.Run(objective,gradient);
}

//int NdaeIdasAdapter::EvaluateObjAndGradNew(realtype &objective, realtype *gradient, realtype *x_f, realtype *y_f, std::vector<int>& modeF,
//	const realtype *p, Ndae<realtype>& dae, const Options& options)
//{
//	bool doAdjoint = true;
//	NdaeIdasAdapter idas(dae, p, options, doAdjoint);
//	return idas.Run(objective, gradient, x_f, y_f, modeF);
//}


int NdaeIdasAdapter::res(realtype tres, N_Vector yy, N_Vector yp, N_Vector rr, void *user_data)
{
  UserData *user = static_cast<UserData*> (user_data);
  const unsigned n_x = user->dae->Eval_n_x();
  realtype *yval = NV_DATA_S(yy);
  realtype *ypval = NV_DATA_S(yp);
  realtype *rval = NV_DATA_S(rr);

  realtype *x = &yval[0];
  realtype *y = &yval[n_x];
  realtype *f = &rval[0];
  realtype *g = &rval[n_x];
  realtype time = tres;

  realtype *p = user->p;
  std::vector<int> mode = user->mode;

  user->dae->Eval_f(f,time,x,y,p,mode);
  user->dae->Eval_g(g,time,x,y,p,mode);

  for (unsigned i = 0; i < n_x; i++) {
    rval[i] -= ypval[i];
  }

  return 0;
}



int NdaeIdasAdapter::resB(realtype tt, N_Vector yy, N_Vector yp,
                          N_Vector yyB, N_Vector ypB,
                          N_Vector rrB, void *user_data)
{
  UserData *user = static_cast<UserData*> (user_data);
  const unsigned n_x = user->dae->Eval_n_x();
  const unsigned n_y = user->dae->Eval_n_y();
  const unsigned n_z = n_x + n_y;
  const unsigned n_p = user->dae->Eval_n_p();
  realtype *x = NV_DATA_S(yy);
  realtype *y = &x[n_x];
  realtype *zpB = NV_DATA_S(ypB);
  realtype *a1_f = NV_DATA_S(yyB);
  realtype *a1_g = &a1_f[n_x];
  realtype *rBval NV_DATA_S(rrB);
  
  std::vector<realtype> data_a1_z(n_z,0.0);
  realtype *a1_z = &data_a1_z[0];
  realtype *a1_x = a1_z;
  realtype *a1_y = &a1_x[n_x];
  
  std::vector<realtype> data_a1_p(n_p,0.0);
  realtype *a1_p = &data_a1_p[0];

  realtype time = tt;
  realtype a1_time = 0;

  ArrayCopy(rBval,0.0,n_z);
  user->dae->Eval_a1_f(a1_f,time,a1_time,x,a1_x,y,a1_y,user->p,a1_p,user->mode);
  for(unsigned i = 0; i < n_z; i++) {
    rBval[i] += a1_z[i];
  }
  user->dae->Eval_a1_g(a1_g,time,a1_time,x,a1_x,y,a1_y,user->p,a1_p,user->mode);
  for(unsigned i = 0; i < n_z; i++) {
    rBval[i] += a1_z[i];
  }

  for(unsigned i = 0; i < n_x; i++) {
    rBval[i] += zpB[i];
  }

  return 0;
}


int NdaeIdasAdapter::sigma(realtype t, N_Vector yy, N_Vector yp, realtype *gout, void *user_data)
{
    UserData *user = static_cast<UserData*> (user_data);
    const unsigned n_x = user->dae->Eval_n_x();
    const unsigned n_sigma = user->dae->Eval_n_sigma();
    if(!user->isAdjointMode) {
        realtype *yval = NV_DATA_S(yy);

        realtype *x = &yval[0];
        realtype *y = &yval[n_x];
        realtype time = t;

        realtype *p = user->p;
        std::vector<int> mode = user->mode;
        std::vector<realtype> mysigma(n_sigma);
        user->dae->Eval_sigma(mysigma.data(),time,x,y,p,mode);

        for(unsigned i = 0; i < n_sigma; ++i)
            gout[i] = mysigma[i];
        gout[user->indexCurrentRoot] -= user->incrementRootPolish;

    }
    else {
        for(unsigned i = 0; i < n_sigma; ++i)
            gout[i] = 1.0;
    }
    return 0;
}


int NdaeIdasAdapter::jacobian(long int Neq, realtype tt,  realtype cj,
                              N_Vector yy, N_Vector yp, N_Vector resvec,
                              DlsMat JJ, void *user_data,
                              N_Vector tempv1, N_Vector tempv2, N_Vector tempv3)
{
  UserData *user = static_cast<UserData*> (user_data);
  const unsigned n_x = user->dae->Eval_n_x();
  const unsigned n_y = user->dae->Eval_n_y();
  realtype *yval = NV_DATA_S(yy);

  realtype *x = &yval[0];
  realtype *y = &yval[n_x];

  realtype time = tt;

  realtype *p = user->p;
  std::vector<int> mode = user->mode;

  std::vector<realtype*> f_x(n_x);
  std::vector<realtype*> f_y(n_y);
  std::vector<realtype*> g_x(n_x);
  std::vector<realtype*> g_y(n_y);

  for(unsigned i = 0; i < n_x; i++){
    f_x[i] = &((JJ->cols)[i][0]);;
    g_x[i] = &((JJ->cols)[i][n_x]);
  }
  for(unsigned i = 0; i < n_y; i++) {
    f_y[i] = &((JJ->cols)[i+n_x][0]);
    g_y[i] = &((JJ->cols)[i+n_x][n_x]);
  }



  user->dae->Eval_f_x(&f_x[0],time,x,y,p,mode);
  user->dae->Eval_g_x(&g_x[0],time,x,y,p,mode);
  user->dae->Eval_f_y(&f_y[0],time,x,y,p,mode);
  user->dae->Eval_g_y(&g_y[0],time,x,y,p,mode);

  for(unsigned i = 0; i < n_x; i++)
    DENSE_ELEM(JJ,i,i) -= cj;

  return 0;
}


int NdaeIdasAdapter::jacobianB(long NeqB, realtype tt, realtype cj,
                               N_Vector yy, N_Vector yp,
                               N_Vector yyB, N_Vector ypB, N_Vector rrB,
                               DlsMat JB, void *user_data,
                               N_Vector tmp1B, N_Vector tmp2B, N_Vector tmp3B)
{
  UserData *user = static_cast<UserData*> (user_data);
  const unsigned n_x = user->dae->Eval_n_x();
  const unsigned n_y = user->dae->Eval_n_y();
  const unsigned n_z = n_x + n_y;
  std::vector<int> mode =  user->mode;

  realtype *p = user->p;

  std::vector<realtype*> f_x(n_x);
  std::vector<realtype*> f_y(n_y);
  std::vector<realtype*> g_x(n_x);
  std::vector<realtype*> g_y(n_y);

  realtype *x = NV_DATA_S(yy);
  realtype *y = &x[n_x];

  realtype time = tt;

  for(unsigned i = 0; i < n_x; i++){
    f_x[i] = &((JB->cols)[i][0]);;
    g_x[i] = &((JB->cols)[i][n_x]);
  }
  for(unsigned i = 0; i < n_y; i++) {
    f_y[i] = &((JB->cols)[i+n_x][0]);
    g_y[i] = &((JB->cols)[i+n_x][n_x]);
  }



  user->dae->Eval_f_x(&f_x[0],time,x,y,p,mode);
  user->dae->Eval_g_x(&g_x[0],time,x,y,p,mode);
  user->dae->Eval_f_y(&f_y[0],time,x,y,p,mode);
  user->dae->Eval_g_y(&g_y[0],time,x,y,p,mode);

  // transpose Jacobian
  for(unsigned i=0; i < n_z; i++) {
    for(unsigned j = 0; j < i; j++) {
      realtype tmp = DENSE_ELEM(JB,i,j);
      DENSE_ELEM(JB,i,j) = DENSE_ELEM(JB,j,i);
      DENSE_ELEM(JB,j,i) = tmp;
    }
  }

  for(unsigned i = 0; i < n_x; i++)
    DENSE_ELEM(JB,i,i) += cj;

  return 0;
}


int NdaeIdasAdapter::rhsQB(realtype tt,
                           N_Vector yy, N_Vector yp,
                           N_Vector yyB, N_Vector ypB,
                           N_Vector rrQB, void *user_dataB)
{

  UserData *user = static_cast<UserData*> (user_dataB);
  const unsigned n_x = user->dae->Eval_n_x();
  const unsigned n_y = user->dae->Eval_n_y();
  const unsigned n_z = n_x + n_y;
  const unsigned n_p = user->dae->Eval_n_p();
  realtype *x = NV_DATA_S(yy);
  realtype *y = &x[n_x];
  realtype *zp = NV_DATA_S(yp);
  realtype *a1_f = NV_DATA_S(yyB);
  realtype *a1_g = &a1_f[n_x];
  
  std::vector<realtype> data_a1_z(n_z,0.0);
  realtype *a1_z = &data_a1_z[0];
  realtype *a1_x = a1_z;
  realtype *a1_y = &a1_x[n_x];
  
  std::vector<realtype> data_a1_p(n_p,0.0);
  realtype *a1_p = &data_a1_p[0];

  realtype time = tt;
  realtype a1_time = 0;

  ArrayCopy(NV_DATA_S(rrQB),0.0,n_p);
  user->dae->Eval_a1_f(a1_f,time,a1_time,x,a1_x,y,a1_y,user->p,a1_p,user->mode);
  for(unsigned i = 0; i < n_p; i++) {
    (NV_DATA_S(rrQB))[i] -= a1_p[i];
  }
  user->dae->Eval_a1_g(a1_g,time,a1_time,x,a1_x,y,a1_y,user->p,a1_p,user->mode);
  for(unsigned i = 0; i < n_p; i++) {
    (NV_DATA_S(rrQB))[i] -= a1_p[i];
  }

  return 0;
}



int NdaeIdasAdapter::Run(realtype& Phi, realtype *Phi_p)
{
	std::cout.precision(12);
  int retval;
  std::vector<int> rootsfound(m_n_sigma, 0);
  static unsigned numRoots = 0;
  numRoots = 0;

  for(unsigned itheta = 0; itheta < m_n_Theta; itheta++) {
    realtype finalTime = m_Theta[itheta];
    m_DataStage.itheta = itheta;
	//m_UserData.dae->set_itheta(itheta);
	//std::cout << "Simulation of intervall " << m_UserData.dae->get_itheta() << " of " << m_n_Theta << std::endl;
	//m_UserData.dae->
	//m_UserData.dae->set_itheta(itheta);
    do
    {
	  //std::cout << "Initialize stage" << std::endl;
      InitializeStage();
	  //std::cout << "Integrate forward" << std::endl;
      retval = IntegrateForward(finalTime);
	  if (retval == IDA_TSTOP_RETURN) { retval = IDA_SUCCESS; }
      bool rootoutside = false;
	  if (retval == IDA_ROOT_RETURN && finalTime < m_Time) {
		  std::cout << "Found root outside of simulation horizon: t_root = " << m_Time << ", finalTime = " << finalTime << std::endl;
		  rootoutside = true;
		  retval = IDA_SUCCESS;
	  }
      switch (retval) {
      int flag;
      case IDA_ROOT_RETURN: {
		  //if(finalTime<m_Time){ std::cout << "Found root number outside of simulation horizon: t_event = " << m_Time << ", finalTime = " << finalTime << std::endl; }
		  realtype finalTimePolishing = 0.0;
		  if (itheta+1==m_n_Theta) { // last element
			  if (m_n_Theta == 1) { finalTimePolishing = 1.5*m_Theta[itheta]; }
			  else { finalTimePolishing = 2 * m_Theta[itheta] - m_Theta[itheta - 1]; }
		  } 
		  else { finalTimePolishing = m_Theta[itheta + 1]; }

        
		//std::cout.setf(std::ios_base::fixed, std::ios_base::floatfield);
		//std::cout.precision(12);
		//std::cout << f;
		realtype initialtime = m_Time;
        //std::cout << "Found root number " << ++numRoots << " at time " << m_Time << std::endl;
        flag = IDAGetRootInfo(m_IdasMem, rootsfound.data());
        CheckFlag(&flag, "IDAGetRootInfo", 1);

		unsigned numCurrentRoots = 0;
		for (unsigned i = 0; i < m_n_sigma; ++i) {
			if (rootsfound[i] != 0) {
				m_DataRoot.indexActiveSwitchingFunction = i;
                m_UserData.indexCurrentRoot = i;
				//std::cout << "Integer of root " << i << std::endl;
				++numCurrentRoots;
			}
		}
		if (numCurrentRoots != 1) throw std::runtime_error("Error in NdaeIdasAdapter::Run() : Wrong number of roots");
		std::cout << "Found root number " << ++numRoots << " at time " << m_Time << " for i = " << m_UserData.indexCurrentRoot << std::endl;

		// polish root
		//std::cout << "Starting root polishing" << std::endl;
		realtype Dt_sigma = 0;
		realtype *x = &m_z[0]; realtype *yhat = &m_z[m_n_x];
		realtype *p = m_p.data(); std::vector<int> mode = m_UserData.mode;
		
		m_UserData.dae->Eval_Dt_sigma(Dt_sigma, m_Time, x, yhat, p, mode, m_DataRoot.indexActiveSwitchingFunction);
		//std::cout << "Dt_sigma = " << Dt_sigma << std::endl;
		realtype increment_min = 2 * m_AbsTol;
		realtype increment_max = m_MaxDiscontinuityTol;

		if (increment_max < increment_min) {
			std::cout << "Error in root polishing" << std::endl;
			std::cout << "Value of maximal discontinuityTol too low" << std::endl;
			return 1;
		}


		realtype increment = 0.5 * m_StateEventTol * Dt_sigma;
		if (increment < 0) increment = -increment;
		if (increment < increment_min) increment = increment_min;
		if (increment > increment_max) increment = increment_max;

		//increment = 2*m_AbsTol;
		m_UserData.incrementRootPolish = rootsfound[m_DataRoot.indexActiveSwitchingFunction] * increment;
		//std::cout << "incrementRootPolish = " << m_UserData.incrementRootPolish << std::endl;

		//std::cout << "Integrate forward for root polishing" << std::endl;
		flag = IntegrateForward(finalTimePolishing);
		if ((flag != IDA_ROOT_RETURN)) { // && (flag != IDA_SUCCESS)
			std::cout << "Root polishing error in IdasAdapterNDAE::run() at time " << m_Time << std::endl;
			return -17;
		}

		
		if (m_Time < finalTime) {
			m_RootFound = true;
			m_DataRoot.oldMode = m_UserData.mode;
			m_DataRoot.time = m_Time;
			ArrayCopy(&(m_DataRoot.old_z[0]), m_z.Data(), Neq());
			//std::cout << "Polishing successful with t = " << m_Time << std::endl;
		}
		else {
			std::cout << "Polishig found outside of simulation interval: t_root " << m_Time << ", finalTime " << finalTime << std::endl;
			return -17;
		}
		m_UserData.incrementRootPolish = 0;
      }
        break;
      case IDA_SUCCESS:
        flag = IDAGetDky(m_IdasMem,finalTime,0,m_z.Nvector());
        if(CheckFlag(&flag, "IDAGetDky", 1)) return(1);
        flag = IDAGetDky(m_IdasMem,finalTime,1,m_zp.Nvector());
        if(CheckFlag(&flag, "IDASolve", 1)) return(1);
        m_Time = finalTime;
        m_RootFound = false;
        break;
      default:
        std::cout << "Unknown error in IdasAdapterNDAE::run()" << std::endl;
        return 1;
      }
      if(m_DoAdjoint) {
		//std::cout << "Starting integration for adjoint" << std::endl;
        m_DataStage.isRoot = m_RootFound;
        m_StackStageData.push(m_DataStage);
      }
      else {
          IDAFree(&m_IdasMem);
          m_StackIdasMem.pop();
      }
    }
    while (retval != IDA_SUCCESS);

    m_Modes[itheta] = m_UserData.mode;
    ArrayCopy(m_X[itheta],m_z.Data(),m_n_x);
    ArrayCopy(m_Y[itheta],&m_z[m_n_x],m_n_y);

  } // end for(unsignend itheta = 0; ...

  // Assign objective function value
  m_UserData.dae->Eval_phi(Phi,Theta(),X(),Y(),&m_p[0]);
 

  if(m_DoAdjoint) {

    //m_UserData.incrementRootPolish = 1e10; //1e10??
    m_UserData.dae->Eval_a1_phi_hat(1.0,Y(),Theta(),X(),a1_X(),&m_p[0],a1_p(),Modes());
    m_Time = m_Theta[m_n_Theta-1];

    while(!m_StackIdasMem.empty()) {
      InitializeStageB();

      realtype initialTime = m_DataStage.initialTime;
	  //std::cout << "Integrate backward from t_end = " << m_Time << " to t_initial = " << initialTime << std::endl;
      retval = IntegrateBackward(initialTime);
      if (CheckFlag(&retval, "integrateBackward", 0)) return(1);


      for(unsigned i = 0; i < m_n_p; i++) {
		  //if (i == m_n_Theta-1) {
			 // std::cout << "Phi_p[" << i << "] = " << Phi_p[i] << ", m_a1_p[i] = " << m_a1_p[i] << ", m_qB[" << i << "] = " << m_qB[i] << std::endl;
		  //}
			  m_a1_p[i] += m_qB[i];
		//std::cout << "m_a1_p[" << i << "] = " << m_a1_p[i] << std::endl;
      }

      IDAFree(&m_IdasMem);
      m_StackIdasMem.pop();

    }

    m_UserData.dae->Eval_a1_psi0(m_zB.Data(),m_UserData.p,Phi_p);
    for(unsigned i = 0; i < m_n_p; i++) {
		//if (i == m_n_Theta-1) {
		//	std::cout << "Phi_p[" << i << "] = " << Phi_p[i] << std::endl;
		//}
		//std::cout << "Phi_p[" << i << "] = " << Phi_p[i] << std::endl;
      Phi_p[i] += m_a1_p[i];
	  //if (i == m_n_Theta-1) {
		 // std::cout << "Phi_p[" << i << "] = " << Phi_p[i] << std::endl;
	  //}
    }

  }
  return 0;
}


int NdaeIdasAdapter::InitializeStage()
{


  NdaeWrapper<realtype> *dae = m_UserData.dae;
  realtype *x = m_z.Data();
  realtype *y = &m_z[m_n_x];
  realtype *p = m_UserData.p;


  bool isFirstStage = (m_Time == dae->Eval_t0());


  m_DataStage.initialTime = m_Time;
  m_DataStage.isRoot = m_RootFound;



  if (isFirstStage) {
    m_UserData.mode = dae->EvalInitialMode();
    dae->Eval_psi0(x,p);
    dae->EvalInitialGuess_y(y);
	m_UserData.mode = dae->EvalInitialActiveSet(m_Time, x, y, p);
  }
  else if(m_RootFound) {
    std::vector<realtype> old_x(m_n_x);
    ArrayCopy(&old_x[0],x,m_n_x);
	std::vector<int> oldMode =  m_UserData.mode;
	std::vector<int> newMode = dae->EvalMode(m_Time,&old_x[0],y,p,oldMode);
    dae->Eval_psi(x,m_Time,&old_x[0],y,p,newMode,oldMode);
    m_UserData.mode = newMode;
  }
  m_DataStage.mode = m_UserData.mode;

  /* Call IDACreate and IDAMalloc to initialize IDA memory */
  //PrepareIdasMemory();
  m_IdasMem = NULL;
  m_IdasMem = IDACreate();
  if(CheckFlag( m_IdasMem, "IDACreate", 0)) return(1);
  m_StackIdasMem.push(m_IdasMem);
  m_DataStage.mem = m_IdasMem;


  int retval;

  retval = IDAInit(m_IdasMem, res, m_Time, m_z.Nvector(), m_zp.Nvector());
  if(CheckFlag(&retval, "IDAInit", 1)) return(1);

  retval = IDASStolerances(m_IdasMem,m_RelTol,m_AbsTol);
  if(CheckFlag(&retval, "IDASStolerances", 1)) return(1);

  int maxnef = 20;
  retval = IDASetMaxErrTestFails(m_IdasMem, maxnef);
  if(CheckFlag(&retval, "IDASetMaxErrTestFails", 1)) return(1);

  int suppressalg = true;
  retval = IDASetSuppressAlg(m_IdasMem, suppressalg);
  if (CheckFlag(&retval, "IDASetSuppressAlg", 1)) return(1);



  /* set user_data */
  IDASetUserData(m_IdasMem, &m_UserData);

  /* Call IDARootInit to specify the root function grob with 1 component */
  const int n_root = m_n_sigma;
  retval = IDARootInit(m_IdasMem, n_root, sigma);
  if (CheckFlag(&retval, "IDARootInit", 1)) return(1);

  std::vector<int> rootdir;
  rootdir = dae->EvalRootDirection(m_UserData.mode);
 
  retval = IDASetRootDirection(m_IdasMem, rootdir.data());


  /* Call IDADense and set up the linear solver. */
  retval = IDADense(m_IdasMem, Neq());
  if(CheckFlag(&retval, "IDADense", 1)) return(1);
  retval = IDADlsSetDenseJacFn(m_IdasMem, jacobian);
  if(CheckFlag(&retval, "IDADlsSetDenseJacFn", 1)) return(1);


  retval = IDASetId(m_IdasMem,m_id.Nvector());
  if (CheckFlag(&retval, "IDASetId", 1)) return(1);


  retval = IDACalcIC(m_IdasMem, IDA_YA_YDP_INIT,m_Time + 1e-6);
  if (CheckFlag(&retval, "IDACalcIC", 1)) return(1);

  retval = IDAGetConsistentIC(m_IdasMem,m_z.Nvector(),m_zp.Nvector());
  if (CheckFlag(&retval, "IDAGetConsistentIC", 1)) return(1);

  if(m_RootFound) {
    m_DataRoot.newMode = m_UserData.mode;
    ArrayCopy(&(m_DataRoot.new_z[0]),m_z.Data(),Neq());
    m_StackRootData.push(m_DataRoot);
  }

  // If applicable, prepare for subsequent adjoint sweep
  if(m_DoAdjoint) {
		  retval = IDAAdjInit(m_IdasMem, 1l, IDA_HERMITE);
		  /*flag = IDAAdjInit(ida_mem, steps, IDA_POLYNOMIAL);*/
		  if (CheckFlag(&retval, "IDAAdjInit", 1)) return(1);
		  //if (m_RootFound) {
			 // retval = IDAAdjReInit(m_IdasMem);
			 // /*flag = IDAAdjInit(ida_mem, steps, IDA_POLYNOMIAL);*/
			 // if (CheckFlag(&retval, "IDAAdjReInit", 1)) return(1);
		  //}
  }
 


  return 0;
}

int NdaeIdasAdapter::InitializeStageB()
{
  m_DataStage = m_StackStageData.top();
  m_StackStageData.pop();

  NdaeWrapper<realtype> *dae = m_UserData.dae;


  m_UserData.mode = m_DataStage.mode;
  m_UserData.isAdjointMode = true;

  // set quadrature variables to zero
  ArrayCopy(m_qB.Data(),0.0,m_n_p);
  //std::cout << "is Root: " << m_DataStage.isRoot << " at t = " << m_Time << std::endl;
  if (!m_DataStage.isRoot) {
    const int itheta =  m_DataStage.itheta;
	//std::cout << "itheta = " << itheta << std::endl;
    for(unsigned i = 0; i < m_n_x; i++){
      m_zB[i] += m_a1_X[itheta][i];
    }
    ArrayCopy(&m_z[0], m_X[itheta],m_n_x);
    ArrayCopy(&m_z[m_n_x],m_Y[itheta], m_n_y);
    dae->Eval_f(m_zp.Data(),m_Theta[itheta],m_X[itheta],
                m_Y[itheta],m_UserData.p,m_Modes[itheta]);
  }
  else {
    m_DataRoot = m_StackRootData.top();
    m_StackRootData.pop();
    realtype *old_z = &(m_DataRoot.old_z[0]);
    realtype *old_x = old_z;
    realtype *old_y = &old_x[m_n_x];
    std::vector<realtype> data_LU(m_n_y*m_n_y,0.0);
    realtype *LU = &data_LU[0];
    std::vector<int> data_IP(m_n_y,0);
    int *IP = &data_IP[0];
    realtype time = m_Time;
    if ( time != m_DataRoot.time){
      std::cout << "Error in initialzeStageB" << std::endl;
      exit(1);
    }

    realtype * p = m_UserData.p;
	std::vector<int> oldMode = m_DataRoot.oldMode;
	std::vector<int> newMode = m_DataRoot.newMode;
	//std::vector<int> diffMode(oldMode.size(), 0);
	//for (int i = 0; i < diffMode.size(); i++) {
	//	diffMode[i] = oldMode[i] - newMode[i];
	//	if (diffMode[i] != 0) {
	//		std::cout << "for i = " << i << ", oldMode = " << oldMode[i] << ", newMode = " << newMode[i] << std::endl;
	//	}
	//}

    realtype *new_z = &(m_DataRoot.new_z[0]);
    realtype *new_x = new_z;
    realtype *new_y = &new_x[m_n_x];

	std::vector<realtype> new_f(m_n_x, 0.0);
	dae->Eval_f(&new_f[0], time, new_x, new_y, p, newMode);

	dae->Set_ActiveSet(oldMode);

	dae->DecompositionLU_g_y(LU,IP,time,old_x,old_y,p,oldMode);
	   
    std::vector<realtype> old_f(m_n_x,0.0);
    dae->Eval_f(&old_f[0],time,old_x,old_y,p,oldMode);

	//for (int i = 0; i < old_f.size(); i++) {
	//	std::cout << old_f[i] << ", " << old_x[i] << ", " << new_f[i] << ", " << new_x[i] << ", " << old_f[i] - new_f[i] << ", " << old_x[i] - new_x[i] << std::endl;
	//}
	//for (int i = 0; i < m_n_y; i++) {
	//	std::cout << i << ", " << old_y[i] << ", " << new_y[i] << std::endl;
	//}

    realtype *a1_psi = m_zB.Data();
    realtype a1_time = 0.0;
    std::vector<realtype> a1_old_x(m_n_x);
    std::vector<realtype> a1_old_x2(m_n_x);
    std::vector<realtype> a1_p(m_n_p);
    std::vector<realtype> a1_p2(m_n_p,0.0);
	std::vector<realtype>  a1_sigma(m_n_sigma,0.0);
	unsigned indexActiveSwitchingFunction = m_DataRoot.indexActiveSwitchingFunction;
	unsigned index2 = -1;

	//std::vector<int> diffMode(oldMode.size(), 0);
	//for (int i = 0; i < diffMode.size(); i++) {
	//	diffMode[i] = oldMode[i] - newMode[i];
	//	if (diffMode[i] != 0) {
	//		std::cout << "for i = " << i << ", oldMode = " << oldMode[i] << ", newMode = " << newMode[i] << std::endl;
	//		if (i != indexActiveSwitchingFunction) { index2 = i; }
	//	}
	//}
	a1_sigma[indexActiveSwitchingFunction] = 1.0;
	//if (index2 >= 0) { a1_sigma[index2] = 1.0; }

	//std::cout << "indexActiveSwitchingFunction: " << indexActiveSwitchingFunction << std::endl;
    //throw std::runtime_error("InitializeStageB() : a1_sigma has to be set appropriately. Not yet implemented!");
	//std::cout << "Calling Eval_a1_sigma_hat with mode[" << indexActiveSwitchingFunction << "] = " << oldMode[indexActiveSwitchingFunction] << std::endl;
    dae->Eval_a1_sigma_hat(a1_sigma.data(),&LU[0],&IP[0],old_y,time,a1_time,old_x,&a1_old_x2[0],
        p, &a1_p2[0],oldMode);
    realtype Dt_sigma = a1_time;
    for(unsigned i = 0; i < m_n_x; i++) {
      Dt_sigma += a1_old_x2[i]*old_f[i];
    }

    dae->Eval_a1_psi_hat(a1_psi,&LU[0],&IP[0],old_y,time,a1_time,old_x,&a1_old_x[0],
        p,&a1_p[0],newMode,oldMode);
    realtype eta = 0.0;
    eta -= a1_time;
    for (unsigned i = 0; i < m_n_x; i++) {
      eta += (new_f[i]* m_zB[i] - a1_old_x[i] * old_f[i]);
    }


    eta /= Dt_sigma;

    for(unsigned i= 0; i < m_n_x; i++) {
      m_zB[i] = a1_old_x[i] + eta * a1_old_x2[i];
    }
    for(unsigned i = 0; i < m_n_p; i++) {
      m_a1_p[i] += eta * a1_p2[i] + a1_p[i];
    }

    // init m_z and m_zp
    ArrayCopy(&m_z[0], old_z,m_n_x+m_n_y);
    for(unsigned i =0; i < m_n_x + m_n_y; ++i)
        m_zp[i] = 0.0;
    dae->Eval_f(m_zp.Data(),time,&m_z[0],
                &m_z[m_n_x],m_UserData.p,oldMode);

  }


  m_IdasMem = m_StackIdasMem.top();

  int retval = -17;
  m_Which = 1;

  N_Vector zB = m_zB.Nvector();
  N_Vector zpB = m_zpB.Nvector();
  N_Vector qB = m_qB.Nvector();


  retval = IDACreateB(m_IdasMem, &m_Which);
  if (CheckFlag(&retval, "IDACreateB", 1)) return(1);


  //IDAInitB provides problem specification, allocates internal memory, and initializes the backward problem
  retval = IDAInitB(m_IdasMem, m_Which, resB, m_Time, zB, zpB);
  if (CheckFlag(&retval, "IDAInitB", 1)) return(1);

  retval = IDASStolerancesB(m_IdasMem, m_Which,m_RelTolAdjoint,m_AbsTolAdjoint);
  if (CheckFlag(&retval, "IDASStolerancesB", 1)) return(1);

  retval = IDASetUserDataB(m_IdasMem, m_Which, &m_UserData);
  if (CheckFlag(&retval, "IDASetUserDataB", 1)) return(1);

  retval = IDASetMaxNumStepsB(m_IdasMem, m_Which, 5000);


  ///* "IDASolveB function does not support rootfinding" (IDAS Guide) --> disabled in backward mode for efficiency reasons  */
  const int n_root = 0;
  retval = IDARootInit(m_IdasMem, n_root, sigma);
  if (CheckFlag(&retval, "IDARootInit", 1)) return(1);

  retval = IDADenseB(m_IdasMem, m_Which, Neq());
  if (CheckFlag(&retval, "IDADenseB", 1)) return(1);

  retval = IDADlsSetDenseJacFnB(m_IdasMem, m_Which, jacobianB);
  if (CheckFlag(&retval, "IDASetDenseJacB", 1)) return(1);

  /* Quadrature for backward problem. */
  retval = IDAQuadInitB(m_IdasMem, m_Which, rhsQB, qB);
  if (CheckFlag(&retval, "IDAQuadInitB", 1)) return(1);

  //retval = IDAQuadReInitB(m_IdasMem, m_Which, qB);
  //if (CheckFlag(&retval, "IDAQuadReInitB", 1)) return(1);

  retval = IDAQuadSStolerancesB(m_IdasMem, m_Which,m_RelTolAdjoint,m_AbsTolAdjoint);
  if (CheckFlag(&retval, "IDAQuadSStolerancesB", 1)) return(1);

  /* Include quadratures in error control. */
  retval = IDASetQuadErrConB(m_IdasMem, m_Which, TRUE);
  if (CheckFlag(&retval, "IDASetQuadErrConB", 1)) return(1);

  /* Compute consistent final conditions */
  //IDASetIdB specifies the differential and algebraic components (required before calling IDACalcICB).
  retval = IDASetIdB(m_IdasMem, m_Which, m_id.Nvector());
  if (CheckFlag(&retval, "IDASetIdB", 1)) return(1);

  // suppress algebraic variables in error control due to discontinuities
  retval = IDASetSuppressAlgB(m_IdasMem, m_Which, TRUE);
  if (CheckFlag(&retval, "IDASetSuppressAlgB", 1)) return(1);

  //IDACalcICB corrects the values of zB(tB0) and zpB(tB0) which were specified in the previous call to IDAInitB or IDAReInitB.
  //Overwrites values calculated by IDAInitB or IDAReInitB.
  //It computes the algebraic components of zB and differential components of zpB, given the differential components of zB.
  //tbout1 (3rd argument) is needed here only to determine the direction of integration and rough scale in the independent variable t.
  retval = IDACalcICB(m_IdasMem, m_Which, m_Time-1e-6, m_z.Nvector(), m_zp.Nvector());
  if (CheckFlag(&retval, "IDACalcICB", 1)) return(1);

  //IDAGetconsistentICB returns the corrected values of zB(tB0) and zpB(tB0) that were calculated by IDACalcICB.
  retval = IDAGetConsistentICB(m_IdasMem, m_Which, zB, zpB);
  if (CheckFlag(&retval, "IDAGetConsistentICB", 1)) return(1);

  return 0;
}

int NdaeIdasAdapter::IntegrateForward(realtype finalTime){


  int retval = 0;
  m_UserData.dae->set_itheta(m_DataStage.itheta);

  realtype tstop = finalTime;
  int tmpval = IDASetStopTime(m_IdasMem, tstop);
  if (CheckFlag(&tmpval, "IDASetStopTime", 1)) return(1);

  do {
    const realtype t0=m_Time;
    if(m_DoAdjoint) {
      int ncheck;
      retval = IDASolveF(m_IdasMem, finalTime, &m_Time, m_z.Nvector(), m_zp.Nvector(), IDA_ONE_STEP,&ncheck);
	  //std::cout << m_Time << std::endl;
    }
    else {
      retval = IDASolve(m_IdasMem,finalTime,&m_Time, m_z.Nvector(), m_zp.Nvector(),IDA_ONE_STEP);
    }

    if(CheckFlag(&retval, "IDASolve", 1)) return(1);
    if (m_DoPlot){
      const double t1 = (m_Time < finalTime)? m_Time : finalTime;
      InterpolateSolution(t0,t1,true);
    }
  }
  while ( (m_Time < finalTime) && (retval == IDA_SUCCESS) );
  
  //std::cout << "finalTime = " << finalTime << ", time = " << m_Time << std::endl;

  return retval;
}

int NdaeIdasAdapter::IntegrateBackward(realtype initialTime)
{
  int retval = 0;
  m_UserData.dae->set_itheta(m_DataStage.itheta);
  //m_UserData.dae->Set_ActiveSet(m_DataStage.mode);

  do {
    const realtype t1 = m_Time;
    retval = IDASolveB(m_IdasMem,initialTime,IDA_ONE_STEP);
    if(CheckFlag(&retval,"IDASolveB",1)) return 1;

    retval = IDAGetB(m_IdasMem,m_Which,&m_Time,m_zB.Nvector(),m_zpB.Nvector());
    if(CheckFlag(&retval,"IDAGetB",1)) return 1;

	//std::cout << "Minor step from " << t1 << " to " << m_Time << std::endl;
    if (m_DoPlot){
      const realtype t0 = (m_Time > initialTime)? m_Time : initialTime;
      InterpolateSolution(t0,t1,false);
    }

  }
  while ( (m_Time > initialTime) && (retval == IDA_SUCCESS) );

  realtype tret2 = 0;
  int flag = IDAGetQuadB(m_IdasMem, m_Which, &tret2, m_qB.Nvector());
  if (CheckFlag(&flag, "IDAGetQuadB", 1)) return(1);

  if(m_Time != tret2) {
    std::cout << "Error in integrateBackward" << std::endl;
    return 1;
  }

  return retval;
}

int NdaeIdasAdapter::Init(Ndae<realtype>& dae, const realtype *p, const Options& options, bool doAdjoint)
{

  m_n_x = dae.Eval_n_x();
  m_n_y = dae.Eval_n_y();
  m_n_p = dae.Eval_n_p();
  m_n_Theta = dae.Eval_n_Theta();
  m_n_sigma = dae.Eval_n_sigma();
  const unsigned NEQ = m_n_x + m_n_y;

  m_DoAdjoint = doAdjoint;
  m_DoPlot = options.GetDoPlot();
  m_RootFound = false;

  m_PtNdaeWrapper.reset(new NdaeWrapper<realtype>(dae));

  /* Allocate N-vectors. */

  m_z.Reset(NEQ);
  m_zp.Reset(NEQ);
  m_id.Reset(NEQ);
  ArrayCopy(m_id.Data(),1.0,m_n_x); // only first n_x comnponents are differential, the remaining n_y comnponents are algebraic

  //default tolerance
  m_AbsTol = options.GetAbsTol();
  m_RelTol = options.GetRelTol();
  m_AbsTolAdjoint = options.GetAbsTolAdjoint();
  m_RelTolAdjoint = options.GetRelTolAdjoint();
  m_StateEventTol = options.GetStateEventTol();
  m_MaxDiscontinuityTol = options.GetMaxDiscontinuityTol();

  m_X.resize(m_n_Theta,0);
  mData_X.resize(m_n_Theta*m_n_x,0.0);
  for(unsigned i = 0; i < m_n_Theta; i++) m_X[i] = &mData_X[m_n_x*i];

  m_Y.resize(m_n_Theta,0);
  mData_Y.resize(m_n_Theta*m_n_y,0.0);
  for(unsigned i = 0; i < m_n_Theta; i++) m_Y[i] = &mData_Y[m_n_y*i];


  m_Modes.resize(m_n_Theta);
  m_Theta.resize(m_n_Theta,0.0);

  m_p.resize(m_n_p);
  ArrayCopy(&m_p[0],p,m_n_p);


  m_UserData.dae = m_PtNdaeWrapper.get();
  m_UserData.p = &m_p[0];
  m_UserData.mode = dae.EvalInitialMode(); // initial mode
  m_UserData.incrementRootPolish = 0.0;

  /* Integration limits */

  m_Time = dae.Eval_t0();
  dae.Eval_Theta(Theta());


  m_MaxStepDenseOutput = options.GetMaxStepDenseOutput();

  if (m_DoPlot)
  {
    m_OutFile.open(options.GetFilenameStates());
  }

  m_DataRoot.old_z.resize(NEQ,0.0);
  m_DataRoot.new_z.resize(NEQ,0.0);

  if (m_DoAdjoint)
  {

    //m_Steps = 1;

    m_a1_X.resize(m_n_Theta,0);
    mData_a1_X.resize(m_n_Theta*m_n_x,0.0);
    for(unsigned i = 0; i < m_n_Theta; i++) m_a1_X[i] = &mData_a1_X[m_n_x*i];

    m_a1_p.resize(m_n_p,0.0);

    m_zB.Reset(NEQ);
    m_zpB.Reset(NEQ);
    m_qB.Reset(m_n_p);

    if(m_DoPlot) {
      m_OutFileB.open("lambda.txt");
    }

  }

  return 0;
}


int NdaeIdasAdapter::InterpolateSolution(realtype t0, realtype t1, bool isForward)
{

  N_VectorAdapter dky(Neq());

  const unsigned N = (t1-t0)/m_MaxStepDenseOutput  + 1;

  void *mem = isForward?m_IdasMem:IDAGetAdjIDABmem(m_IdasMem,m_Which);

  std::ofstream& outFile=isForward?m_OutFile:m_OutFileB;

  for(unsigned i=0; i <= N; i++) {
    const realtype alpha = (N>0)?(static_cast<realtype>(i)/static_cast<realtype>(N)):0.0;

    realtype t = 0;
    if(isForward) {
      t = alpha * t1  + (1-alpha)*t0;
    }
    else {
      t = alpha * t0  + (1-alpha)*t1;
    }

    int flag = IDAGetDky(mem,t,0,dky.Nvector());
    if (flag < 0) {
      std::cout << "Error, calling IDAgetDky" << std::endl;
      return 1;
    }

    outFile << std::scientific << t << "\t";
    for(unsigned i=0; i < Neq(); i++)
      outFile << std::scientific << dky[i] << "\t";
   for(unsigned i= 0; i < m_UserData.mode.size(); ++i) outFile << m_UserData.mode[i];
    outFile << std::endl;
  }

  return 0;
}

void NdaeIdasAdapter::FreeResources(){

  while (!m_StackIdasMem.empty()) {
    void *mem = m_StackIdasMem.top();
    IDAFree(&mem);
    m_StackIdasMem.pop();
  }

  if(m_DoPlot){
    m_OutFile.close();
    if(m_DoAdjoint) m_OutFile.close();
  }

}
//int NdaeIdasAdapter::PrepareIdasMemory() {
//	/* Call IDACreate and IDAMalloc to initialize IDA memory */
//	m_IdasMem = NULL;
//	m_IdasMem = IDACreate();
//	if (CheckFlag(m_IdasMem, "IDACreate", 0)) return(1);
//	//m_StackIdasMem.push(m_IdasMem);
//	//m_DataStage.mem = m_IdasMem;
//
//	/* Set Idas parameters*/
//	int retval;
//
//	retval = IDAInit(m_IdasMem, res, m_Time, m_z.Nvector(), m_zp.Nvector());
//	if (CheckFlag(&retval, "IDAInit", 1)) return(1);
//
//	retval = IDASStolerances(m_IdasMem, m_RelTol, m_AbsTol);
//	if (CheckFlag(&retval, "IDASStolerances", 1)) return(1);
//
//	int maxnef = 20;
//	retval = IDASetMaxErrTestFails(m_IdasMem, maxnef);
//	if (CheckFlag(&retval, "IDASetMaxErrTestFails", 1)) return(1);
//
//	int suppressalg = true;
//	retval = IDASetSuppressAlg(m_IdasMem, suppressalg);
//	if (CheckFlag(&retval, "IDASetSuppressAlg", 1)) return(1);
//
//
//
//	/* set user_data */
//	IDASetUserData(m_IdasMem, &m_UserData);
//
//	/* Call IDARootInit to specify the root function grob with 1 component */
//	const int n_root = m_n_sigma;
//	retval = IDARootInit(m_IdasMem, n_root, sigma);
//	if (CheckFlag(&retval, "IDARootInit", 1)) return(1);
//
//	//std::vector<int> rootdir;
//	//rootdir = dae->EvalRootDirection(m_UserData.mode);
//
//	//retval = IDASetRootDirection(m_IdasMem, rootdir.data());
//
//
//	/* Call IDADense and set up the linear solver. */
//	retval = IDADense(m_IdasMem, Neq());
//	if (CheckFlag(&retval, "IDADense", 1)) return(1);
//	retval = IDADlsSetDenseJacFn(m_IdasMem, jacobian);
//	if (CheckFlag(&retval, "IDADlsSetDenseJacFn", 1)) return(1);
//
//
//	retval = IDASetId(m_IdasMem, m_id.Nvector());
//	if (CheckFlag(&retval, "IDASetId", 1)) return(1);
//
//
//	retval = IDACalcIC(m_IdasMem, IDA_YA_YDP_INIT, m_Time + 1e-6);
//	if (CheckFlag(&retval, "IDACalcIC", 1)) return(1);
//
//	retval = IDAGetConsistentIC(m_IdasMem, m_z.Nvector(), m_zp.Nvector());
//	if (CheckFlag(&retval, "IDAGetConsistentIC", 1)) return(1);
//
//	// If applicable, prepare for subsequent adjoint sweep
//	if (m_DoAdjoint) {
//		retval = IDAAdjInit(m_IdasMem, 1l, IDA_HERMITE);
//		/*flag = IDAAdjInit(ida_mem, steps, IDA_POLYNOMIAL);*/
//		if (CheckFlag(&retval, "IDAAdjInit", 1)) return(1);
//	}
//}