#ifndef NDAE_IDAS_ADAPTER_HPP
#define NDAE_IDAS_ADAPTER_HPP

#include "NdaeWrapper.hpp"
#include "NdaeSolver.hpp"

#include "idas/idas.h"
#include "idas/idas_dense.h"

#include <fstream>
#include <stack>
#include <memory>
#include <iostream>
#include <exception>

typedef NdaeSolver::Options Options;


extern "C" typedef int FUNC_res (realtype tres, N_Vector yy, N_Vector yp, N_Vector rr, void *user_data);
extern "C" typedef int FUNC_sigma (realtype t, N_Vector yy, N_Vector yp, realtype *gout, void *user_data);
extern "C" typedef int FUNC_jacobian(long int Neq, realtype tt,  realtype cj,
                             N_Vector yy, N_Vector yp, N_Vector resvec,
                             DlsMat JJ, void *user_data,
                             N_Vector tempv1, N_Vector tempv2, N_Vector tempv3);

extern "C" typedef int FUNC_resB(realtype tt, N_Vector yy, N_Vector yp,
                         N_Vector yyB, N_Vector ypB,
                         N_Vector rrB, void *user_data);

extern "C" typedef int FUNC_jacobianB(long NeqB, realtype tt, realtype cj,
                                  N_Vector yy, N_Vector yp,
                                  N_Vector yyB, N_Vector ypB, N_Vector rrB,
                                  DlsMat JB, void *user_data,
                                  N_Vector tmp1B, N_Vector tmp2B, N_Vector tmp3B);

extern "C" typedef int FUNC_rhsQB(realtype tt,
                 N_Vector yy, N_Vector yp,
                 N_Vector yyB, N_Vector ypB,
                 N_Vector rrQB, void *user_dataB);


class N_VectorAdapter{
public:
  N_VectorAdapter() : m_N_Vector(0,N_VDestroy_Serial) {}
  N_VectorAdapter(unsigned len) : m_N_Vector(N_VNew_Serial(len),N_VDestroy_Serial) { InitData(len); }
  N_Vector Nvector() { return m_N_Vector.get();}
  realtype *Data() { return NV_DATA_S(m_N_Vector.get()); }
  void Reset(unsigned len) { m_N_Vector.reset(N_VNew_Serial(len)); InitData(len); }
  realtype& operator[] (unsigned i) {return (NV_DATA_S(m_N_Vector.get()))[i];}
private:
  void InitData(unsigned len) throw (std::bad_alloc) {
    if (m_N_Vector.get() == NULL)  {
      throw std::bad_alloc();
    }
    ArrayCopy(Data(),0.0,len);
  }
  N_VectorAdapter& operator=(const N_VectorAdapter&);
  N_VectorAdapter(const N_VectorAdapter&);
  std::unique_ptr<_generic_N_Vector, void(*) (N_Vector)> m_N_Vector;
};


class NdaeIdasAdapter
{
public:
  static int EvaluateObjective(realtype& objective, const realtype *p, Ndae<realtype>& dae, const Options& options);
  static int EvaluateObjAndGrad(realtype& objective,realtype *gradient, const realtype *p, Ndae<realtype>& dae,  const Options& options);
private:
  NdaeIdasAdapter() {}
  NdaeIdasAdapter(Ndae<realtype>& dae, const realtype *p, const Options& options, bool doAdjoint);
  ~NdaeIdasAdapter();
  NdaeIdasAdapter(const NdaeIdasAdapter&);
  NdaeIdasAdapter& operator=(const NdaeIdasAdapter&);

  struct UserData {
    UserData() : dae(nullptr), p(nullptr), isAdjointMode(false), indexCurrentRoot(0), incrementRootPolish(0.0) {}
    NdaeWrapper<realtype> * dae;
    realtype *p;
	std::vector<int> mode;
    realtype incrementRootPolish;
    int indexCurrentRoot;
    bool isAdjointMode;
  };
  struct StageData {
    realtype initialTime;
    void* mem;
    int numCheckPoints;
	std::vector<int> mode;
    int itheta;
    bool isRoot;
  };
  struct RootData {
    realtype time;
	std::vector<int> oldMode;
	std::vector<int> newMode;
	unsigned indexActiveSwitchingFunction;
    std::vector<realtype> old_z;
    std::vector<realtype> new_z;
  };

  std::unique_ptr<NdaeWrapper<realtype> > m_PtNdaeWrapper;

  N_VectorAdapter m_z;
  N_VectorAdapter m_zp;
  N_VectorAdapter m_id;
  N_VectorAdapter m_zB;
  N_VectorAdapter m_zpB;
  N_VectorAdapter m_qB;

  realtype m_AbsTol;
  realtype m_RelTol;
  realtype m_AbsTolAdjoint;
  realtype m_RelTolAdjoint;

  realtype m_StateEventTol;
  realtype m_MaxDiscontinuityTol;
  realtype m_Time;
  realtype m_MaxStepDenseOutput;


  bool m_DoAdjoint;
  bool m_DoPlot;
  bool m_RootFound;


  void* m_IdasMem;
  std::stack<void*> m_StackIdasMem;

  int m_Which;
  //long m_Steps;

  unsigned m_n_x;
  unsigned m_n_y;
  unsigned m_n_p;
  unsigned m_n_Theta;
  unsigned m_n_sigma;
  unsigned Neq() { return (m_n_x + m_n_y);}

  std::vector<realtype*> m_X;
  std::vector<realtype> mData_X;
  realtype **X() {return &m_X[0];}

  std::vector<realtype*> m_a1_X;
  std::vector<realtype> mData_a1_X;
  realtype **a1_X() {return &m_a1_X[0];}

  std::vector<realtype*> m_Y;
  std::vector<realtype> mData_Y;
  realtype **Y() {return &m_Y[0];}

  std::vector<realtype> m_Theta;
  realtype *Theta() { return &m_Theta[0];}

  std::vector<realtype> m_p;

  std::vector<realtype> m_a1_p;
  realtype* a1_p() { return &m_a1_p[0]; }

  std::vector<std::vector<int> > m_Modes;
  std::vector<std::vector<int> > Modes() { return m_Modes;}

  std::ofstream m_OutFile;
  std::ofstream m_OutFileB;


  UserData m_UserData;

  RootData m_DataRoot;
  std::stack<RootData> m_StackRootData;

  StageData m_DataStage;
  std::stack<StageData> m_StackStageData;


  static FUNC_res res;
  static FUNC_sigma sigma;
  static FUNC_jacobian jacobian;
  static FUNC_resB resB;
  static FUNC_jacobianB jacobianB;
  static FUNC_rhsQB rhsQB;


  int Init(Ndae<realtype>& dae, const realtype *p, const Options& options, bool doAdjoint);
  void FreeResources();
  int Run(realtype& Phi, realtype *Phi_p);
  //int Run(realtype & Phi, realtype * Phi_p, realtype * x_f, realtype * y_f, std::vector<int>& mode_f);
  int InitializeStage();
  int InitializeStageB();
  int IntegrateForward(realtype finalTime);
  int IntegrateBackward(realtype initialTime);
  int InterpolateSolution(realtype t0, realtype t1, bool isForward = true);
  //int PrepareIdasMemory();

};

#endif // NDAE_IDAS_ADAPTER_HPP
