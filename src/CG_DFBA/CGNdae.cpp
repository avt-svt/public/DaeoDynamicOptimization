#include <cmath>
#include "FlashExampleNdae.hpp"
#include "FlashExampleFcn.hpp"
#include "a1_Fcn.hpp"






//template<>
//void FlashExampleNdae<double>::Eval_a1_f(double*f, double *a1_f, double& time, double& a1_time,
//	double *x, double *a1_x, double *y, double *a1_y,
//	double *p, double* a1_p, const std::vector<int>& mode) const
//{
//
//	AD_A1_CREATE
//		AD_A1_RESET
//
//		AD_A1_DEC2(f, N_X)
//		AD_A1_DEC1(time)
//		AD_A1_DEC2(x, N_X)
//		AD_A1_DEC2(y, N_Y)
//		AD_A1_DEC2(p, N_P)
//
//		Eval_f(ad_f, ad_time, ad_x, ad_y, ad_p, mode);
//	AD_A1_INT2(f, N_X)
//		AD_A1_GET1(time)
//		AD_A1_GET2(x, N_X)
//		AD_A1_GET2(y, N_Y)
//		AD_A1_GET2(p, N_P)
//
//}
//
//
//
//
//template<>
//void FlashExampleNdae<double>::Eval_a1_g(double* g, double *a1_g, double& time, double& a1_time,
//	double *x, double *a1_x, double *y, double *a1_y,
//	double *p, double* a1_p, const std::vector<int>& mode) const
//{
//	AD_A1_CREATE
//		AD_A1_RESET
//
//		AD_A1_DEC2(g, N_Y)
//		AD_A1_DEC1(time)
//		AD_A1_DEC2(x, N_X)
//		AD_A1_DEC2(y, N_Y)
//		AD_A1_DEC2(p, N_P)
//
//		Eval_g(ad_g, ad_time, ad_x, ad_y, ad_p, mode);
//	AD_A1_INT2(g, N_Y)
//
//		AD_A1_GET2(g, N_Y)
//		AD_A1_GET1(time)
//		AD_A1_GET2(x, N_X)
//		AD_A1_GET2(y, N_Y)
//		AD_A1_GET2(p, N_P)
//}
//
//
//template<>
//void FlashExampleNdae<double>::Eval_sigma(double* sigma, double& time,
//	double* x, double *y, double* p, const std::vector<int>& mode) const
//{
//	//sigma_fcn(sigma,time,x,y,p,mode);
//	sigma[0] = M_V(y);
//	sigma[1] = M_L(y);
//}
//
//template<>
//void FlashExampleNdae<double>::Eval_a1_sigma(double* sigma, double* a1_sigma, double& time, double& a1_time,
//	double *x, double *a1_x, double *y, double *a1_y,
//	double *p, double* a1_p, const std::vector<int>& mode) const
//{
//
//	AD_A1_CREATE
//		AD_A1_RESET
//
//		AD_A1_DEC2(sigma, N_S)
//		AD_A1_DEC1(time)
//		AD_A1_DEC2(x, N_X)
//		AD_A1_DEC2(y, N_Y)
//		AD_A1_DEC2(p, N_P)
//
//		Eval_sigma(ad_sigma, ad_time, ad_x, ad_y, ad_p, mode);
//	AD_A1_INT2(sigma, N_S)
//
//		AD_A1_GET2(sigma, N_S)
//		AD_A1_GET1(time)
//		AD_A1_GET2(x, N_X)
//		AD_A1_GET2(y, N_Y)
//		AD_A1_GET2(p, N_P)
//}
//
//
//
//template<>
//void FlashExampleNdae<double>::Eval_psi(double *psi, double& time, double* x, double *y,
//	double* p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const
//{
//	for (int i = 0; i < N_X; ++i) {
//		psi[i] = x[i];
//	}
//}
//
//template<>
//void FlashExampleNdae<double>::Eval_a1_psi(double*psi, double *a1_psi, double& time, double& a1_time,
//	double *x, double *a1_x, double *y, double *a1_y,
//	double *p, double* a1_p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const
//{
//	AD_A1_CREATE
//		AD_A1_RESET
//
//		AD_A1_DEC2(psi, N_X)
//		AD_A1_DEC1(time)
//		AD_A1_DEC2(x, N_X)
//		AD_A1_DEC2(y, N_Y)
//		AD_A1_DEC2(p, N_P)
//
//		Eval_psi(ad_psi, ad_time, ad_x, ad_y, ad_p, newMode, oldMode);
//	AD_A1_INT2(psi, N_X)
//
//		AD_A1_GET1(time)
//		AD_A1_GET2(x, N_X)
//		AD_A1_GET2(y, N_Y)
//		AD_A1_GET2(p, N_P)
//}
//
//
//template<>
//void FlashExampleNdae<double>::Eval_psi0(double *psi0, double* p) const
//{
//	for (int i = 0; i < N_X; ++i) {
//		psi0[i] = x_0[i];
//	}
//	//psi0_fcn(psi0,p);
//}
//
//template<>
//void FlashExampleNdae<double>::Eval_a1_psi0(double *psi0, double *a1_psi0, double* p, double *a1_p) const
//{
//	AD_A1_CREATE
//		AD_A1_RESET
//
//		AD_A1_DEC2(psi0, N_X)
//		AD_A1_DEC2(p, N_P)
//
//		Eval_psi0(ad_psi0, ad_p);
//	AD_A1_INT2(psi0, N_X)
//
//		AD_A1_GET2(p, N_P)
//}
//
//
//template<>
//void FlashExampleNdae<double>::Eval_phi(double& phi, double *Theta, double **X, double **Y, double *p) const
//{
//	phi = X[0][1];
//}
//
//template<>
//void FlashExampleNdae<double>::Eval_a1_phi(double& phi, double& a1_phi, double *Theta, double* a1_Theta,
//	double **X, double ** a1_X,
//	double **Y, double **a1_Y, double *p, double *a1_p) const
//{
//	AD_A1_CREATE
//		AD_A1_RESET
//
//		AD_A1_DEC1(phi)
//		AD_A1_DEC2(Theta, N_THETA)
//		AD_A1_DEC3(X, N_X, N_THETA)
//		AD_A1_DEC3(Y, N_Y, N_THETA)
//		AD_A1_DEC2(p, N_P)
//
//		Eval_phi(ad_phi, ad_Theta, ad_X, ad_Y, ad_p);
//	AD_A1_INT1(phi)
//
//		AD_A1_GET2(Theta, N_THETA)
//		AD_A1_GET3(X, N_X, N_THETA)
//		AD_A1_GET3(Y, N_Y, N_THETA)
//		AD_A1_GET2(p, N_P)
//}
//using Gt1s = ad::gt1s<double>::type;
//inline std::vector<Gt1s> CreateGt1sVector(const double* values, const double* t1_values, const unsigned n)
//{
//	std::vector<Gt1s> gt1sVector(n);
//	for (unsigned i = 0; i < n; ++i) {
//		ad::value(gt1sVector[i]) = values[i];
//		ad::derivative(gt1sVector[i]) = t1_values[i];
//	}
//	return gt1sVector;
//}
//
//inline std::vector<Gt1s> CreateGt1sVector(const unsigned n)
//{
//	std::vector<Gt1s> gt1sVector(n);
//	for (unsigned i = 0; i < n; ++i) {
//		ad::value(gt1sVector[i]) = 0.0;
//		ad::derivative(gt1sVector[i]) = 0.0;
//	}
//	return gt1sVector;
//}
//
//inline void ExtractGt1sVector(double *vec, double *t1_vec, const std::vector<Gt1s>& ad_vec) {
//	for (std::size_t i = 0; i < ad_vec.size(); ++i) {
//		vec[i] = ad::value(ad_vec[i]);
//		t1_vec[i] = ad::derivative(ad_vec[i]);
//	}
//}
//
//inline Gt1s CreateGt1sScalar(const double value) {
//	Gt1s scalar;
//	ad::value(scalar) = value;
//	ad::derivative(scalar) = 0.0;
//	return scalar;
//}
//
//inline void ExtractGt1sScalar(double& scalar, double& t1_scalar, const Gt1s& ad_scalar) {
//	scalar = ad::value(ad_scalar);
//	t1_scalar = ad::derivative(ad_scalar);
//}
//
//
//
//template<>
//void FlashExampleNdae<double>::Eval_t1_f(double *f, double *t1_f, double& time, double& t1_time, double *x, double *t1_x,
//	double *y, double *t1_y, double *p, double *t1_p, const std::vector<int>& mode) const
//{
//
//	auto ad_f = CreateGt1sVector(N_X);
//	auto ad_time = CreateGt1sScalar(time);
//	auto ad_x = CreateGt1sVector(x, t1_x, N_X);
//	auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
//	auto ad_p = CreateGt1sVector(p, t1_p, N_P);
//
//	Eval_f(ad_f.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), mode);
//
//	ExtractGt1sVector(f, t1_f, ad_f);
//
//
//}
//
//template<>
//void FlashExampleNdae<double>::Eval_t1_g(double *g, double *t1_g, double& time, double& t1_time, double *x, double *t1_x,
//	double *y, double *t1_y, double *p, double *t1_p, const std::vector<int>& mode) const
//{
//	auto ad_g = CreateGt1sVector(N_Y);
//	auto ad_time = CreateGt1sScalar(time);
//	auto ad_x = CreateGt1sVector(x, t1_x, N_X);
//	auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
//	auto ad_p = CreateGt1sVector(p, t1_p, N_P);
//
//	Eval_g(ad_g.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), mode);
//
//	ExtractGt1sVector(g, t1_g, ad_g);
//}
//
//
//template<>
//void FlashExampleNdae<double>::Eval_t1_sigma(double* sigma, double* t1_sigma, double& time, double& t1_time, double *x, double *t1_x,
//	double *y, double *t1_y, double *p, double *t1_p, const std::vector<int>& mode) const
//{
//	auto ad_sigma = CreateGt1sVector(N_S);
//	auto ad_time = CreateGt1sScalar(time);
//	auto ad_x = CreateGt1sVector(x, t1_x, N_X);
//	auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
//	auto ad_p = CreateGt1sVector(p, t1_p, N_P);
//
//	Eval_sigma(ad_sigma.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), mode);
//
//	ExtractGt1sVector(sigma, t1_sigma, ad_sigma);
//}
//
//template<>
//void FlashExampleNdae<double>::Eval_t1_psi(double * psi, double *t1_psi, double& time, double& t1_time, double *x, double *t1_x,
//	double *y, double *t1_y, double *p, double *t1_p, const std::vector<int>& newMode,
//	const std::vector<int>& oldMode) const
//{
//	auto ad_psi = CreateGt1sVector(N_X);
//	auto ad_time = CreateGt1sScalar(time);
//	auto ad_x = CreateGt1sVector(x, t1_x, N_X);
//	auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
//	auto ad_p = CreateGt1sVector(p, t1_p, N_P);
//
//	Eval_psi(ad_psi.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), newMode, oldMode);
//
//	ExtractGt1sVector(psi, t1_psi, ad_psi);
//}
//
//
//template<>
//void FlashExampleNdae<double>::Eval_t1_psi0(double *psi0, double *t1_psi0, double *p, double* t1_p) const
//{
//	auto ad_psi0 = CreateGt1sVector(N_X);
//	auto ad_p = CreateGt1sVector(p, t1_p, N_P);
//
//	Eval_psi0(ad_psi0.data(), ad_p.data());
//
//	ExtractGt1sVector(psi0, t1_psi0, ad_psi0);
//}
//
//template<>
//void FlashExampleNdae<double>::Eval_t1_phi(double& phi, double& t1_phi, double *Theta, double *t1_Theta, double **X, double ** t1_X,
//	double **Y, double **t1_Y, double *p, double *t1_p) const
//{
//	std::vector<Gt1s*> ad_X(N_THETA), ad_Y(N_THETA);
//	auto ad_X_data = CreateGt1sVector(N_THETA * N_X);
//	auto ad_Y_data = CreateGt1sVector(N_THETA * N_Y);
//	for (unsigned i = 0; i < N_THETA; ++i) {
//		ad_X[i] = &ad_X_data[i*N_X];
//		ad_Y[i] = &ad_Y_data[i*N_Y];
//		for (unsigned j = 0; j < N_X; ++j) {
//			ad::value(ad_X[i][j]) = X[i][j];
//			ad::derivative(ad_X[i][j]) = t1_X[i][j];
//		}
//		for (unsigned j = 0; j < N_Y; ++j) {
//			ad::value(ad_Y[i][j]) = Y[i][j];
//			ad::derivative(ad_Y[i][j]) = t1_Y[i][j];
//		}
//
//	}
//	auto ad_p = CreateGt1sVector(p, t1_p, N_P);
//	auto ad_Theta = CreateGt1sVector(Theta, t1_Theta, N_THETA);
//	Gt1s ad_phi;
//	Eval_phi(ad_phi, ad_Theta.data(), ad_X.data(), ad_Y.data(), ad_p.data());
//
//	ExtractGt1sScalar(phi, t1_phi, ad_phi);
//
//}
