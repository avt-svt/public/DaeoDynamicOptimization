#include "MyNlp_CG.hpp"

double integrationTolerance = 1e-10;
double derivativeTolerance = integrationTolerance;
const int forwarddifferences = 0;
const int backwarddifferences = 1;
const int centraldifferences = 2;
int fd_type = -1;// centraldifferences;// centraldifferences;

void MyNlp::EvalDimensions(int& numVariables, int& numConstraints) {
  numVariables=2;
  numConstraints=1; // dummy constraint for now
}


void MyNlp::EvalVariableBounds(double *lowerBounds, double* upperBounds, int numVariables){

	lowerBounds[0] = 0.0;
	upperBounds[0] = 1.0;
	lowerBounds[1] = 9.5;
	upperBounds[1] = 13.0;

}


void MyNlp::EvalConstraintBounds(double *lowerBounds, double* upperBounds, int numConstraints){
  lowerBounds[0]= -1;
  //lowerBounds[1]=-1e20;
  //lowerBounds[1] = 8.0; 
  upperBounds[0]= 1;
  //upperBounds[1] = 20.0;
  //upperBounds[1]=10;
}


void MyNlp::EvalInitialGuess(double* x, int numVariables){
	std::ofstream ofile("initialSolution.txt");
	int n_p;
	int n_constr;
	EvalDimensions(n_p, n_constr);
	//double x_0_indi[3] = { (1000.0 - 52.0730872602553632*5.0) / 6.0, 52.0730872602553632, 12.2369 };
	for (int i = 0; i < n_p; ++i) {
		x[i] = p_0[i]; // x_0_indi[i];
		ofile << x[i] << std::endl;
	}
	//x[0] = 131.70635;
	//x[1] = 41.95237;
	//x[2] = 10.55435;
	ofile.close();
}


void MyNlp::EvalObjectiveAndConstraints(int derivativeOrder, double* x, double& f, double* grad_f, 
  double* g, double** jac_g, int numVariables, int numConstraints){
	CorynebacteriumGlutamicumNdae<double> dae;
	const int numVars = dae.N_P;
	
	//int numVariables;
	//int numConstraints;
	//EvalDimensions(numVariables, numConstraints);
	// create variables for objective (Phi) and its sensitivities (Phi_temp and Phi_p) w.r.t. optimization variables (parameters)
	realtype Phi = 0;
	realtype Phi_p[numVars];

	realtype constr = 0;
	realtype constr_p[numVars];
	realtype time_p[numVars];

	// sensitivity calculation via finite differences
	std::vector<realtype> dphi_dp_fd(numVars, 0.0);
	double h = sqrt(integrationTolerance);
	std::vector<double> p_fd(numVars,0.0);
	std::vector<double> p(numVars, 0.0);
	realtype p_fd_sim[numVars];


	for (int i = 0; i < numVariables; ++i) {
		Phi_p[i] = 0.0;// grad_f[i];
		constr_p[i] = 0.0;
		time_p[i] = 0.0;
		p[i] = x[i];
		p_fd[i] = x[i];
	}
  //  if( derivativeOrder >= 0) {
		//// run simulation
		//dae.setObjMode(ObjectiveMode::ComputeObjective);
		//auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, x, dae, options);
		//f = Phi;
		////std::cout << "Objective: " << f << std::endl;
		//constr = Phi;
		//for (int i = 0; i < numVars; i++) { constr_p[i] = Phi_p[i]; }
		////dae.setObjMode(ObjectiveMode::ComputeConstraint);
		////auto flag2 = NdaeSolver::EvaluatePhiGradient(constr, constr_p, x, dae, options);
		//g[0]=0.001*(6.0*x[1]+5.0*x[2]);
		//constr_p[0] = 0.0;
		//constr_p[1] = 6.0e-3;
		//constr_p[2] = 5.0e-3;
		////std::cout << "Constraint: " << g[0] << std::endl;
  //  }

  //  if( derivativeOrder >= 1) {
		//for (int i = 0; i < numVariables; ++i) {
		//	grad_f[i] = Phi_p[i];
		//	//std::cout << x[i] << ", " << grad_f[i] << std::endl;
		//	jac_g[0][i] = constr_p[i];
		//}
  //  }
	if (derivativeOrder == 3) {
		if (fd_type == forwarddifferences) {
			realtype phi_0 =  0.0 ;
			auto flag = NdaeSolver::EvaluatePhi(phi_0, x, dae, options);

			realtype phi_temp = 0.0;
			for (int i = 0; i < numVars; i++) {
				p_fd = p;
				p_fd[i] = p[i] * (1.0 + h);
				for (int i = 0; i < numVars; ++i) {
					p_fd_sim[i] = p_fd[i];
				}
				flag = NdaeSolver::EvaluatePhi(phi_temp, p_fd_sim, dae, options);
				dphi_dp_fd[i] = (phi_temp - phi_0) / h;
				Phi_p[i] = dphi_dp_fd[i];
			}
			f = phi_0;

		}
		else if (fd_type == backwarddifferences) {
			realtype phi_0 = 0.0;
			auto flag = NdaeSolver::EvaluatePhi(phi_0, x, dae, options);

			realtype phi_temp = 0.0;
			for (int i = 0; i < numVars; i++) {
				p_fd = p;
				p_fd[i] = p[i] * (1.0 - h);
				for (int i = 0; i < numVars; ++i) {
					p_fd_sim[i] = p_fd[i];
				}
				flag = NdaeSolver::EvaluatePhi(phi_temp, p_fd_sim, dae, options);
				dphi_dp_fd[i] = (phi_0 - phi_temp) / h;
				Phi_p[i] = dphi_dp_fd[i];
			}
			f = phi_0;
		}
		else if (fd_type == centraldifferences) {
			realtype phi_0 = 0.0;
			auto flag = NdaeSolver::EvaluatePhi(phi_0, x, dae, options);

			realtype  phi_plus = 0.0;
			realtype  phi_minus = 0.0;
			for (int i = 0; i < numVars; i++) {
				p_fd = p;
				p_fd[i] = p[i] * (1.0 + h);
				for (int i = 0; i < numVars; ++i) {
					p_fd_sim[i] = p_fd[i];
				}
				auto flag = NdaeSolver::EvaluatePhi(phi_plus, p_fd_sim, dae, options);
				//doSimulation(p.size(), p_fd.data(), false, phi_plus.data());
				p_fd = p;
				p_fd[i] = p[i] * (1.0 - h);
				for (int i = 0; i < numVars; ++i) {
					p_fd_sim[i] = p_fd[i];
				}
				flag = NdaeSolver::EvaluatePhi(phi_minus, p_fd_sim, dae, options);
				dphi_dp_fd[i] = (phi_plus - phi_minus) / (2 * h);
				Phi_p[i] = dphi_dp_fd[i];
			}
			f = phi_0;
		}
		else {
			// run simulation
			//std::cout << "Running simulation to evaluate objective and gradients." << std::endl;
			dae.setObjMode(ObjectiveMode::ComputeObjective);
			auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, x, dae, options);
			f = Phi;
			for (int i = 0; i < numVars; ++i) {
				//grad_f[i] = Phi_p[i];
				std::cout << x[i] << ", " << Phi_p[i] << std::endl;
				// 	jac_g[0][i] = constr_p[i];
				   //jac_g[1][i] = time_p[i];
			}
		}
	}
	else if (derivativeOrder == 4) {
		//dae.setObjMode(ObjectiveMode::ComputeConstraint);
		//auto flag2 = NdaeSolver::EvaluatePhiGradient(constr, constr_p, x, dae, options);
		//std::cout << "Evaluating constraints and gradients." << std::endl;
		g[0]=0.0;
		//g[1] = x[2];
		constr_p[0] = 0.0;
		constr_p[1] = 0.0;	
	}
	else {
		//std::cout << "Running simulation to evaluate objective and gradients. Also evaluating constraints." << std::endl;
		// run simulation
		dae.setObjMode(ObjectiveMode::ComputeObjective);
		auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, x, dae, options);
		f = Phi;

		//std::cout << "Objective: " << f << std::endl;
		//constr = Phi;
		for (int i = 0; i < numVars; i++) { constr_p[i] = Phi_p[i]; }
		//dae.setObjMode(ObjectiveMode::ComputeConstraint);
		//auto flag2 = NdaeSolver::EvaluatePhiGradient(constr, constr_p, x, dae, options);
		g[0]=0.0;
		//g[1] = x[2];
		constr_p[0] = 0.0;
		constr_p[1] = 0.0;
		//std::cout << "Constraint: " << g[0] << std::endl;
	}

	  if( derivativeOrder >= 1) {
		 for (int i = 0; i < numVariables; ++i) {
		  	grad_f[i] = Phi_p[i];
		  	//std::cout << x[i] << ", " << grad_f[i] << std::endl;
		 // 	jac_g[0][i] = constr_p[i];
			//jac_g[1][i] = time_p[i];
		  }
		  jac_g[0][0] = constr_p[0];
		  jac_g[0][1] = constr_p[1];
		  //jac_g[1][2] = time_p[2];
	  }
	
	//for (int i = 0; i < numConstraints; i++) {
	//	std::cout << "Constraint " << i << " = " << g[i] << ", jac = ";
	//	for (int j = 0; j < numVariables-1; j++) {
	//		std::cout << jac_g[i][j] << ", ";
	//	}
	//	std::cout << jac_g[i][numVariables-1] << std::endl;
	//}
	//std::cout << "Objective = " << f << ", jac = ";
	//for (int j = 0; j < numVariables - 1; j++) {
	//	std::cout << grad_f[j] << ", ";
	//}
	//std::cout << grad_f[numVariables - 1] << std::endl;

}

void MyNlp::FinalizeSolution(int n, const double * x,
	const double* z_L, const double* z_U,
	int m, const double* g, const double* lambda,
	double obj_value) {
	CorynebacteriumGlutamicumNdae<double> dae;
	const int numVars = dae.N_P;

	double Phi = 0;
	double Phi_p[numVars];

	std::ostringstream filename_results;
	filename_results << "SimulationResults_CG_Optimal";
	//filename_results << dae.scale_final_deviation;
	filename_results << ".txt";
	options.SetDoPlot(TRUE);
	options.SetFilenameStates(filename_results.str());
	auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, x, dae, options);
	std::ofstream ofile("finalSolution.txt");
	ofile << "Objective value = " << Phi << std::endl;
	for (int i = 0; i < numVars; ++i) {
		//std::cout << "X(" << i + 1 << ") = " << X[i] << std::endl;
		//X_vec[i] = X[i];
		ofile << x[i] << ", " << Phi_p[i] << std::endl;
	}
	ofile.close();
}

MyNlp::MyNlp() {
	int n_p;
	int n_constr;
	EvalDimensions(n_p, n_constr);

	CorynebacteriumGlutamicumNdae<double> dae;

	p_0.resize(n_p);
	for (int i = 0; i < n_p; i++) {
		p_0[i] = dae.p_0[i];
	}
	//set up options for integration
	const double tol = integrationTolerance;
	options.SetMaxStepDenseOutput(10.0);
	options.SetTol(tol);
	options.SetStateEventTol(2 * tol);
	options.SetMaxDiscontinuityTol(10.0*tol);
	options.SetDoPlot(FALSE);
	options.SetFilenameStates("SimulationResults_Flash.txt");
	options.SetAbsTolAdjoint(10*tol);
	options.SetRelTolAdjoint(10*tol);
	std::cout << "Adjoint abs tol: " << options.GetAbsTolAdjoint() << std::endl;
	std::cout << "Adjoint rel tol: " << options.GetRelTolAdjoint() << std::endl;
	std::cout << "Integration abs tol: " << options.GetAbsTol() << std::endl;
	std::cout << "Integration rel tol: " << options.GetRelTol() << std::endl;
}

void MyNlp::set_InitialParameter(std::vector<double>& p_neu) {
	int n_p;
	int n_constr;
	EvalDimensions(n_p, n_constr);
	if (p_neu.size() != n_p) {
		std::cout << "Wrong dimensionality of new parameter vector (" << p_neu.size() << ") vs. old size: " << n_p << std::endl;
	}
	else {
		for (int i = 0; i < n_p; i++) {
			p_0[i] = p_neu[i];
		}
	}
};