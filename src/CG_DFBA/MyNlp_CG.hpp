#ifndef MY_NLP_CG_HPP__
#define MY_NLP_CG_HPP__

#include "NdaeSolver.hpp"
#include "NdaeUtils.hpp"

#include "DaeoToolbox.hpp"


#include "JadeNlp.hpp"
#include "CGNdae.hpp"
#include <filesystem>


class MyNlp : public JadeNlp {
public:
	MyNlp();
	void set_InitialParameter(std::vector<double>& p_neu);
  virtual void EvalDimensions(int& numVariables, int& numConstraints);
  virtual void EvalVariableBounds(double *lowerBounds, double* upperBounds, int numVariables);
  virtual void EvalConstraintBounds(double *lowerBounds, double* upperBounds, int numConstraints);
  virtual void EvalInitialGuess(double* x, int numVariables);
  virtual void EvalObjectiveAndConstraints(int derivativeOrder, double* x, double& f, double* grad_f, 
    double* g, double** jac_g, int numVariables, int numConstraints);
  virtual void FinalizeSolution(int n, const double * x,
	  const double* z_L, const double* z_U,
	  int m, const double* g, const double* lambda,
	  double obj_value);
private:
	//CorynebacteriumGlutamicumNdae<double> dae;
	//const int n_p = dae.N_P;
	std::vector<double> p_0;
	NdaeSolver::Options options;
	using ObjectiveMode = CorynebacteriumGlutamicumNdae<double>::ObjectiveMode;
};

#endif
