#include <iostream>
#include <sstream>
#include <cmath>

#include "CGNdae.hpp"
#include "NdaeSolver.hpp"
#include "NdaeUtils.hpp"

#include "DaeoToolbox.hpp"

#include "SNOPTWrapper.hpp"
#include "MyNlp_CG.hpp"

using namespace NdaeUtils;
int read_from_file(std::string filename, std::vector<double>& phi_xyl_0, std::vector<double>& finaltime_0);
void runOptimization(std::vector<double> p_neu, std::string filename, std::map<std::string, std::string> optimizerOptions);

int main()
{
	double optTol = 1.e-06;
	double feasTol = optTol;
	double funcPrec = 1.e-10;
	double major_step_limit = 1.0;
	double linesearchTol = 0.9999;
	//std::string s = to_string(d);
	std::ostringstream streamObj;
	//Add double to stream
	streamObj << optTol;
	// Get string from output string stream
	//std::string strObj = streamObj.str();
	std::cout << "Conversion of double to string: " << streamObj.str() << std::endl;

	std::map<std::string, std::string> optimizerOptions;
	//optimizerOptions.insert(make_pair<std::string, std::string>("scale option", "2")); //seemingly not working
	//optimizerOptions.insert(make_pair<std::string, std::string>("Scale print", "1"));
	
	optimizerOptions.insert(make_pair<std::string, std::string>("major iterations limit", "50"));
	optimizerOptions.insert(make_pair<std::string, std::string>("minor iterations limit", "1000"));

	optimizerOptions.insert(make_pair<std::string, std::string>("derivative level", "3"));

	streamObj.str(std::string());
	streamObj << optTol;
	optimizerOptions.insert(make_pair<std::string, std::string>("major optimality tolerance", streamObj.str()));
	optimizerOptions.insert(make_pair<std::string, std::string>("minor optimality tolerance", streamObj.str()));
	optimizerOptions.insert(make_pair<std::string, std::string>("major feasibility tolerance", streamObj.str()));

	streamObj.str(std::string());
	streamObj << funcPrec;
	optimizerOptions.insert(make_pair<std::string, std::string>("function precision", streamObj.str()));

	streamObj.str(std::string());
	streamObj << feasTol;
	optimizerOptions.insert(make_pair<std::string, std::string>("minor feasibility tolerance", streamObj.str()));
	
	streamObj.str(std::string());
	streamObj << major_step_limit;
	optimizerOptions.insert(make_pair<std::string, std::string>("major step limit", streamObj.str()));

	streamObj.str(std::string());
	streamObj << linesearchTol;
	optimizerOptions.insert(make_pair<std::string, std::string>("linesearch tolerance", streamObj.str()));
	optimizerOptions.insert(make_pair<std::string, std::string>("major print level", "111111"));
	optimizerOptions.insert(make_pair<std::string, std::string>("minor print level", "11111"));
	//optimizerOptions.insert(make_pair<std::string, std::string>("verify level", "3"));
	//optimizerOptions.insert(make_pair<std::string, std::string>("Print file", "1"));

	//SNOPTWrapper snoptWrapper(optimizerOptions);
	//MyNlp myNlp;
	//snoptWrapper.solve(&myNlp);
	std::vector<double> phi_xyl_0;
	std::vector<double> finaltime_0;
	
	auto flag = read_from_file("Multistart_values.txt",phi_xyl_0,finaltime_0);

	int ind = 0;
	std::vector<int> indices;
	for (int i = 0; i < 50; i++) { indices.push_back(i); } 



  for (size_t i = 0; i < indices.size(); i++) {
		ind = indices[i];
		// change initial guesses
		std::vector<double> p_neu = { phi_xyl_0[ind], finaltime_0[ind] };
		
		// change output filename
    std::ostringstream filename;
		filename << "snopt_NDAE_";
		filename << ind+1;
		filename << ".out";
		
		// solve NLP
		runOptimization(p_neu, filename.str(), optimizerOptions);
	}

	//delete(myNlp);

	return 0;
}

int read_from_file(std::string filename, std::vector<double>& phi_xyl_0, std::vector<double>& finaltime_0) {
	std::ifstream aFile(filename);
	std::size_t lines_count = 0;
	std::string line1;
	while (std::getline(aFile, line1))
		++lines_count;
	const int n_ = lines_count;
	phi_xyl_0.resize(n_);
	finaltime_0.resize(n_);

	std::ifstream input(filename);
	std::string line;

	for (int i = 0; i < n_; ++i) {
		std::getline(input, line, ',');
		std::istringstream iss(line);
		iss >> phi_xyl_0[i]; 
		std::getline(input, line);
		std::istringstream (line) >> finaltime_0[i];
	}

	return 0;
}

void runOptimization(std::vector<double> p_neu, std::string filename, std::map<std::string, std::string> optimizerOptions) {
	SNOPTWrapper snoptWrapper(optimizerOptions);
	MyNlp myNlp;
	myNlp.set_InitialParameter(p_neu);
	snoptWrapper.setOutputFilename(filename);
	snoptWrapper.solve(&myNlp);
}
