#include <iostream>
#include <cmath>

#include "CGNdae.hpp"
#include "NdaeSolver.hpp"
#include "NdaeUtils.hpp"

#include "DaeoToolbox.hpp"
//#include "DaeoToolboxAdditional.hpp"

using namespace NdaeUtils;
// The code performs dynamic optimization of a flash example using a proximal bundle method MPBNGC.


int doSimulation(int N, double* X, bool doPlot, double* F, double * G);
int doSimulation(int N, double* X, bool doPlot, double* F);

int testInitialConditions();
int testDaeoToolbox();
int readParameterValuesFromFile(char* filename, std::vector<double>& p_out);


int main()
{
	//testInitialConditions();

	const unsigned n_p = CorynebacteriumGlutamicumNdae<double>::N_P;
	const unsigned n_x = CorynebacteriumGlutamicumNdae<double>::N_X;
	const unsigned n_s = CorynebacteriumGlutamicumNdae<double>::N_S;
	const unsigned n_y = CorynebacteriumGlutamicumNdae<double>::N_Y;
	//CorynebacteriumGlutamicumNdae<double> dae;

	std::vector<double> p0;
	p0.resize(n_p);
	for (int i = 0; i < n_p; ++i) {
		p0[i] = 0.0;
	}
	p0[0] = 1-131.70635*6.0/1000.0;
	p0[1] = 10.55435;
	double time = 0.0;

	// vector for results of dynamic optimization
	std::vector<double> x_vec;
	x_vec.resize(n_p);

	// vector for intial values for dynamic optimization
	//std::vector<double> x_0;
	//x_0.resize(n_p);
	//for (int i = 0; i < x_0.size(); ++i) {
	//	x_0[i] = p[i];// dae.p_0[i]; // 0.0
	//}


	std::vector<double> phi = { 0.0 };
	std::vector<double> dphi_dp(n_p, 0.0);

	//auto res = doSimulation(n_p, p0.data(), true, phi.data() , dphi_dp.data());

	double phi_xyl_start = 0.0;
	double phi_xyl_end = 1.0;
	double phi_xyl_stepsize = 0.01;
	double phi_xyl_val = phi_xyl_start;

	double final_time_start = 9.5;
	double final_time_end = 13.0;
	double final_time_stepsize = 0.01;
	double final_time_val = final_time_start;

	std::vector<double> obj;
	std::vector<double> phi_xyl;
	std::vector<double> final_time;

	while (phi_xyl_val <= phi_xyl_end) {
		while (final_time_val<=final_time_end) {
			p0[0] = phi_xyl_val;
			p0[1] = final_time_val;

			auto res = doSimulation(n_p, p0.data(), false, phi.data());

			phi_xyl.push_back(phi_xyl_val);
			final_time.push_back(final_time_val);
			obj.push_back(phi[0]);

			final_time_val = final_time_val + final_time_stepsize;
		}
		final_time_val = final_time_start;
		phi_xyl_val = phi_xyl_val + phi_xyl_stepsize;
	}

	std::ofstream ofile("SimulationStudy_.txt");
	for (int i = 0; i < obj.size(); i++) {
		ofile << phi_xyl[i] << ", " << final_time[i] << ", " << obj[i] << std::endl;
	}
	ofile.close();

	return 0;
}

int doSimulation(int N, double* X, bool doPlot, double* F, double * G) {

	// create instance of dae
	const unsigned n_p = CorynebacteriumGlutamicumNdae<double>::N_P;
	const unsigned n_x = CorynebacteriumGlutamicumNdae<double>::N_X;
	const unsigned n_s = CorynebacteriumGlutamicumNdae<double>::N_S;
	const unsigned n_y = CorynebacteriumGlutamicumNdae<double>::N_Y;
	CorynebacteriumGlutamicumNdae<double> dae;

	double tolerance = 1e-09;
	//set up options for integration
	NdaeSolver::Options options;
	//options.SetMaxStepDenseOutput(0.01);
	options.SetTol(tolerance);
	options.SetStateEventTol(2.0*tolerance);
	options.SetMaxDiscontinuityTol(10.0*tolerance);
	options.SetDoPlot(doPlot);
	options.SetFilenameStates("SimulationResults_CG.txt");
	

	if (N != n_p) {
		std::cout << "Number of parameters is n_p = " << n_p << " but there are N = " << N << " optimization variables." << std::endl;
		return -1;
	}

	// set parameters to values of X
	realtype p_0[n_p];
	for (int i = 0; i < n_p; ++i) {
		p_0[i] = X[i];
	}
	// create variables for objective (Phi) and its sensitivities (Phi_temp and Phi_p) w.r.t. optimization variables (parameters)
	realtype Phi = 0;
	realtype Phi_p[n_p];

	// run simulation
	//auto flag = NdaeSolver::EvaluatePhi(Phi, p_0, dae, options);
	auto flag = NdaeSolver::EvaluatePhiGradient(Phi, Phi_p, p_0, dae, options);

	std::ofstream ofile("intermediateSolution_.txt");
	ofile << "Objective = " << Phi << std::endl;
	for (int i = 0; i<n_p; ++i) {
		ofile << p_0[i] << " " << Phi_p[i] << std::endl;
	}
	ofile.close();
	// return objective function F and gradients w.r.t. parameters G
	F[0] = Phi;
	for (int i = 0; i < N; ++i) {
		G[i] = Phi_p[i];
		//std::cout << X[i] << ", " << G[i] << std::endl;
	}

	//std::cout << "\n" << "IERR = " << IERR << "\n" << std::endl;
	return 0;
}

int doSimulation(int N, double* X, bool doPlot, double* F) {

	// create instance of dae
	const unsigned n_p = CorynebacteriumGlutamicumNdae<double>::N_P;
	const unsigned n_x = CorynebacteriumGlutamicumNdae<double>::N_X;
	const unsigned n_s = CorynebacteriumGlutamicumNdae<double>::N_S;
	const unsigned n_y = CorynebacteriumGlutamicumNdae<double>::N_Y;
	CorynebacteriumGlutamicumNdae<double> dae;

	const double tol = 1e-10;
	//set up options for integration
	NdaeSolver::Options options;
	options.SetMaxStepDenseOutput(10.0);
	options.SetTol(tol);
	options.SetStateEventTol(2 * tol);
	options.SetMaxDiscontinuityTol(10.0*tol);
	options.SetAbsTolAdjoint(10 * tol);
	options.SetRelTolAdjoint(10 * tol);
	//std::cout << "Adjoint abs tol: " << options.GetAbsTolAdjoint() << std::endl;
	//std::cout << "Adjoint rel tol: " << options.GetRelTolAdjoint() << std::endl;
	//std::cout << "Integration abs tol: " << options.GetAbsTol() << std::endl;
	//std::cout << "Integration rel tol: " << options.GetRelTol() << std::endl;
	options.SetDoPlot(doPlot);
	options.SetFilenameStates("SimulationResults_CG.txt");


	if (N != n_p) {
		std::cout << "Number of parameters is n_p = " << n_p << " but there are N = " << N << " optimization variables." << std::endl;
		return -1;
	}

	// set parameters to values of X
	realtype p_0[n_p];
	for (int i = 0; i < n_p; ++i) {
		p_0[i] = X[i];
	}
	// create variables for objective (Phi) and its sensitivities (Phi_temp and Phi_p) w.r.t. optimization variables (parameters)
	realtype Phi = 0;
	//realtype Phi_p[n_p];

	// run simulation
	//auto flag = NdaeSolver::EvaluatePhi(Phi, p_0, dae, options);
	auto flag = NdaeSolver::EvaluatePhi(Phi,p_0, dae, options);

	std::ofstream ofile("intermediateSolution_.txt");
	ofile << "Objective = " << Phi << std::endl;
	for (int i = 0; i<n_p; ++i) {
		ofile << p_0[i] << std::endl;
	}
	ofile.close();
	// return objective function F and gradients w.r.t. parameters G
	F[0] = Phi;

	return 0;
}


int readParameterValuesFromFile(char* filename, std::vector<double>& p_out) {
	//std::vector<double> p_out;
	std::ifstream inFile(filename);
	//inFile.open(filename);
	double x, dx;
	std::string line;
	std::size_t lines_count = 0;
	while (std::getline(inFile, line))
	{
		++lines_count;
	}
	const size_t n_ = lines_count - 1;
	std::ifstream input(filename);
	p_out.resize(n_);
	for (int i = 0; i < lines_count; ++i) {
		std::getline(input, line);
		if (i > 0) {// not for header line
			std::istringstream iss(line);
			iss >> x >> dx; 
			p_out[i - 1] = x;
			printf("%s\n", line.c_str());
			printf("i = %i: x[%i] = %e, dx[%i] = %e\n", i, i, x, i, dx);
		}
	}
	inFile.close();
	input.close();
	return 0;
}

int testInitialConditions() {
	const unsigned n_p = CorynebacteriumGlutamicumNdae<double>::N_P;
	CorynebacteriumGlutamicumNdae<double> dae;

	int n_y = dae.Eval_n_y();
	int n_x = dae.Eval_n_x();
	int n_sigma = dae.Eval_n_sigma();

	std::vector<double> x0;
	x0.resize(n_x);
	for (int i = 0; i < n_x; ++i) {
		x0[i] = dae.x_0[i];
	}

	std::vector<double> y0;
	y0.resize(n_y);
	for (int i = 0; i < n_y; ++i) {
		y0[i] = dae.y_0[i];
	}
	//set temperature
	//y0[2 * nC + 4] = 97.0;



	std::vector<double> p0;
	p0.resize(n_p);
	for (int i = 0; i < n_p; ++i) {
		p0[i] = dae.p_0[i];
	}
	double time = 0.0;

	std::vector<int> mode;
	mode.resize(n_sigma);
	for (int i = 0; i < n_sigma; ++i) {
		mode[i] = dae.mode0[i];
	}

	//test differential equation
	std::vector<double> f_all(n_x);
	dae.Eval_f_impl(f_all.data(), time, x0.data(), y0.data(), p0.data(), mode);

	std::vector<double> g_all(n_y);
	dae.Eval_g_impl(g_all.data(), time, x0.data(), y0.data(), p0.data(), mode);

	dae.Eval_g_impl(g_all.data(), time, x0.data(), y0.data(), p0.data(), mode);

	double sum_res_squared=0.0;
	for (int i = 0; i < g_all.size(); ++i) {
		sum_res_squared += g_all[i] * g_all[i];
		if ((g_all[i] * g_all[i]) > 1e-6) {
			std::cout << "Algebraic equation " << i << " has squared residual of " << g_all[i] * g_all[i] << std::endl;
		}
		//std::cout << "i = " << i << ", sum of squared res = " << sum_res_squared << std::endl;
	}


	return 0;
}

int testDaeoToolbox() {
	std::cout << "------------ Solving model SpirallusParse ------------" << std::endl;
	const int n = 8; // number of DAEO variables
	const int p = 1; // number of DAEO parameters
	const int m = 7; // number of DAEO equalities
	const int q = 5; // number of DAEO inequalities
	double y[n] = {};
	//	double duals[q];
	//	double slacks[q];
	//double basicValue[q];

	std::vector<double> dxdb;
	dxdb.resize(n*p);

	//std::vector<double> dsigmadx;
	//dsigmadx.resize(n*q);

	//double dsigmadx[n*q];

	const char* params[p] = { "b_1" };
	double x[p] = { 1 };
	const char* variables[n] = { "vupt", "v1", "v2", "v3", "v4", "v5", "v6", "v7" };
	//const double variables_l[n] = { -1e20,0,0,0,-1e20,0,0 };
	//const double variables_u[n] = { 1e20,1e20,1e20,1e20,1e20,1e20,1e20 };

	const char* objective = "-v6";

	const char* equalities[m] = {
		"vupt - 1.0*v1", "v1 - 1.0*v2 - 1.0*v5", "v4 - 1.0*v2 + v5 - 1.0*v6", "v2 - 1.0*v3", "v3 - 1.0*v4", "v3 + v4 - 1.0*v7", "vupt - 1.0*b_1" };
	//const double equalities_rhs[m] = { 0, 0, 0, 0, 0, 0, 0 };
	const char* inequalities[q] = { "v3", "v4", "v6", "v2", "v7" };


	std::vector<int> info;
	info.resize(2);

	int status[2];

	const char* fname = "Spirallus.lp";

	//IloEnv   env;
	//IloModel model(env);
	//IloNumVarArray var(env);
	//IloRangeArray con(env);

	std::vector<double> QdiagLP;
	std::vector<double> Qdiag;
	Qdiag.resize(n + p);
	for (int i = 0; i < n + p; ++i) {
		if (i < p) {
			Qdiag[i] = 1e-12;
		}
		else {
			Qdiag[i] = 1e-6;
		}
	}
	void * spirallus = parseDFBAmodel(params,
		//x,
		variables,
		objective,
		equalities,
		inequalities,
		p,
		m,
		n,
		q,
		Qdiag.data(),
		(int)Qdiag.size(),
		status);

	void * spirallus_Direct = parseDFBAmodel(params,
		//x,
		variables,
		objective,
		equalities,
		inequalities,
		p,
		m,
		n,
		q,
		Qdiag.data(),
		(int)Qdiag.size(),
		status);

	std::vector<double> sol_Direct;
	sol_Direct.resize(n);

	std::vector<double> sol_AS;
	sol_AS.resize(n);

	std::vector<double> sigma_Direct;
	sigma_Direct.resize(q);

	std::vector<double> sigma_AS;
	sigma_AS.resize(q);
	for (int i = 0; i < 10; i++) {
		//getDer_dsigmadx(spirallus, x, dsigmadx, status);
		//getDer_dxdb(spirallus, x, dxdb.data(), status);
		solveActiveSet(spirallus, x, sol_AS.data(), sigma_AS.data(), status);
		solveDirectSigma(spirallus_Direct, x, sol_Direct.data(), sigma_Direct.data(), status);
		//getDer_dxdb_new(spirallus, db.data(), dx.data(), dsigma.data(), status);


#ifdef	printExternal
		for (int j = 0; j < n; j++) {
			std::cout << "Variable " << variables[j] << " has value of " << y[j] << std::endl;
		}

		//std::cout.precision(16);
		for (int j = 0; j < q; j++) {
			std::cout << "Inequality " << j << " has sigma " << basicValue[j] << std::endl;
		}
#endif
		for (int j = 0; j < p; j++) {
			if (j == 0) { x[j] -= 0.05; };
		}
	}
	//for (int i = 0; i < 10; i++) {
	//	updatebandsolvedirect(spirallus, x, y, info);
	//	for (int j = 0; j < n; j++) {
	//		std::cout << "Variable " << variables[j] << " has value of " << y[j] << std::endl;
	//	}
	//	for (int j = 0; j < p; j++) {
	//		if (j == 0) { x[j] -= 0.05; };
	//	}
	//}
	closeDFBAmodel(spirallus);
	closeDFBAmodel(spirallus_Direct);
	return 0;
}
