#ifndef CorynebacteriumGlutamicum_NDAE_HPP
#define CorynebacteriumGlutamicum_NDAE_HPP

#include <iostream>
#include <vector>
#include "a1_Fcn.hpp"
#include "DaeoToolbox.hpp"

using namespace std;
using Gt1s = ad::gt1s<double>::type;
using Ga1s = ad::ga1s<double>::type;

#include "Ndae.hpp"

#include <math.h>

template<typename Real>
class CorynebacteriumGlutamicumNdae : public Ndae<Real>
{
public:
	CorynebacteriumGlutamicumNdae() {
		DFBA = create_external_object();
		//double g[N_Y];
		//double time0 = 0.0;
		//std::vector<double> y_DFBA_0(p_ + n_ + q_,0.0);
		//for (int i = 0; i < (p_ + n_ + q_); i++) {
		//	y_DFBA_0[i] = y_0[i];
		//}
		//Eval_g_toolbox<double>(g, time0, x_0, y_DFBA_0.data(), p_0, mode0);
		//double b0[p_];
		//for (int i = 0; i < p_; i++) {
		//	b0[i] = y_0[i];
		//}
		//int info[2];
		//returnActiveSet(DFBA, mode0.data(), info);
		//int sum_active_ineq = 0;
		//for (int i = 0; i < mode0.size(); i++) {
		//	sum_active_ineq += mode0[i];
		//}
		//std::cout << "Degrees of freedom: " << n_ - m_ - sum_active_ineq << std::endl;

	}
	virtual ~CorynebacteriumGlutamicumNdae() {
		closeDFBAmodel(DFBA);
	}

  virtual unsigned Eval_n_p() const override;
  virtual unsigned Eval_n_x() const override;
  virtual unsigned Eval_n_y() const override;
  virtual unsigned Eval_n_Theta() const override;
  virtual unsigned Eval_n_sigma() const override;
  virtual Real Eval_t0() const override;
  virtual void Eval_Theta(Real *Theta) const override;
	//virtual void Eval_i_Theta(int itheta) { set_iP(itheta); };

	
  virtual std::vector<int> EvalMode(Real time, Real *x, Real *y, Real *p, const std::vector<int>& oldMode) const override;
  virtual void Set_ActiveSet(const std::vector<int>& oldMode) const override;


	template<typename T>
  std::vector<int> EvalMode_impl(T time, T *x, T *y, T *p, const std::vector<int>& oldMode) const;

  virtual std::vector<int> EvalRootDirection(const std::vector<int>& mode) const override;
  virtual void EvalInitialGuess_y(Real *y) const override;
  virtual std::vector<int> EvalInitialMode() const override;
  virtual std::vector<int> EvalInitialActiveSet(Real time, Real *x, Real *y, Real *p) const override;

	template<typename T>
  void Eval_f_impl(T *f, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const;
	virtual void Eval_f(Real *f, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const override;

	template<typename T>
  void Eval_g_impl(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const;
	template<typename T>
  void Eval_g_params(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const;
	template<typename T>
  void Eval_g_toolbox(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const;

	
	//template<typename AD_TYPE>
  //void g_DFBA(std::vector<AD_TYPE>& x, std::vector<AD_TYPE>& y, std::vector<AD_TYPE>& sigma, void* externalObject) const override;

  void g_DFBA(std::vector<double>& b, std::vector<double>& v, std::vector<double>& sigma,
              void* externalObject, const std::vector<int>& mode) const;

  void g_DFBA(std::vector<ad::gt1s<double>::type>& b, std::vector<ad::gt1s<double>::type>& v,
              std::vector<ad::gt1s<double>::type>& sigma, void* externalObject, const std::vector<int>& mode) const;

  void g_DFBA(std::vector<ad::ga1s<double>::type>& b, std::vector<ad::ga1s<double>::type>& v,
              std::vector<ad::ga1s<double>::type>& sigma, void* externalObject, const std::vector<int>& mode) const;
	
  void g_make_gap_DFBA(std::vector<Ga1s>& b, std::vector<Ga1s>& v,
                       std::vector<Ga1s>& sigma, void* externalObject, const std::vector<int>& mode) const;

  void g_fill_gap_DFBA(ad::mode<Ga1s>::external_adjoint_object_t *D) const;

	//template<typename AD_TYPE>
  //void updateActiveSet_DFBA(std::vector<AD_TYPE>& b, void* externalObject) const override;

	//template<typename AD_TYPE>
  //std::vector<int> returnActiveSet_DFBA(std::vector<AD_TYPE>& b, void* externalObject) const override;

	template<typename AD_TYPE>
  std::vector<int> updateAndReturnActiveSet_DFBA(std::vector<AD_TYPE>& b, void* externalObject) const;

  virtual void Eval_g(Real *g, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const override;

	template<typename T>
	void Eval_sigma_impl(T* sigma, T& time,
    T* x, T *y, T* p, const std::vector<int>& mode) const;
  virtual void Eval_sigma(Real* sigma, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const override;

	template<typename T>
	void Eval_psi_impl(T *psi, T& time, T* x, T *y,
    T* p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const;
  virtual void Eval_psi(Real *psi, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& newMode,
                        const std::vector<int>& oldMode) const override;

	template<typename T>
  void Eval_psi0_impl(T *psi0, T* p) const;
  virtual void Eval_psi0(Real *psi0, Real *p) const override;

	template<typename T>
  void Eval_phi_impl(T& phi, T *Theta, T **X, T **Y, T *p) const;
  virtual void Eval_phi(Real& phi, Real *Theta, Real **X, Real **Y, Real *p) const override;


	virtual void Eval_a1_f(Real *f, Real *a1_f, Real& time, Real& a1_time, Real *x, Real *a1_x,
    Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode) const override;

	virtual void Eval_a1_g(Real *g, Real *a1_g, Real& time, Real& a1_time, Real *x, Real *a1_x,
    Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode) const override;

	virtual void Eval_a1_sigma(Real* sigma, Real* a1_sigma, Real& time, Real& a1_time, Real *x, Real *a1_x,
    Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode) const override;

	virtual void Eval_a1_psi(Real * psi, Real *a1_psi, Real& time, Real& a1_time, Real *x, Real *a1_x,
    Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const override;

  virtual void Eval_a1_psi0(Real *psi0, Real *a1_psi0, Real *p, Real* a1_p) const override;
	virtual void Eval_a1_phi(Real& phi, Real& a1_phi, Real *Theta, Real *a1_Theta, Real **X, Real ** a1_X,
    Real **Y, Real **a1_Y, Real *p, Real *a1_p) const override;

	virtual void Eval_t1_f(Real *f, Real *t1_f, Real& time, Real& t1_time, Real *x, Real *t1_x,
    Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const override;

	virtual void Eval_t1_g(Real *g, Real *t1_g, Real& time, Real& t1_time, Real *x, Real *t1_x,
    Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const override;

	virtual void Eval_t1_sigma(Real* sigma, Real* t1_sigma, Real& time, Real& t1_time, Real *x, Real *t1_x,
    Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const override;

	virtual void Eval_t1_psi(Real * psi, Real *t1_psi, Real& time, Real& t1_time, Real *x, Real *t1_x,
		Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& newMode,
    const std::vector<int>& oldMode) const override;

  virtual void Eval_t1_psi0(Real *psi0, Real *t1_psi0, Real *p, Real* t1_p) const override;

	virtual void Eval_t1_phi(Real& phi, Real& t1_phi, Real *Theta, Real *t1_Theta, Real **X, Real ** t1_X,
    Real **Y, Real **t1_Y, Real *p, Real *t1_p) const override;


public:
	enum class ObjectiveMode { ComputeObjective, ComputeConstraint };
	ObjectiveMode objMode = ObjectiveMode::ComputeObjective;
	void setObjMode(ObjectiveMode mode) { objMode = mode; }
	static ObjectiveMode& objectiveMode() {
		static ObjectiveMode mode = ObjectiveMode::ComputeObjective;
		return mode;
	}

	int eval_a1_g_counter = 0;
	//////// Metabolic network model
	static const int n_ = 68;// "number of DAEO variables";
	static const int p_ = 9;// "number of DAEO inputs";
	static const int m_ = 57;// "number of DAEO equalities";
	static const int q_ = 50;// "number of DAEO inequalities";

	void * DFBA;// = create_external_object();

	//static const int n_comp = nC;
	static const int N_X = 9; // number of differential vars (tray variables + objective function)
	static const int N_Y = p_ + n_ + q_ + 4; // number algebraic vars 
	static const int NINTERVALS = 1; // number of intervals for parametrization
	static const int N_P = 2;// NINTERVALS; // number of total parameters
	static const int N_THETA = 1; // numebr of THETA which is number of time points where the objective functional is evaluated
	static const int N_S = q_+2; // number of switching functions

	// model parameters
	//integers for entries in differential variable vector
	static const int i_X = 0;
	static const int i_C_Glc = 129; // now algebraic variables
	static const int i_C_Xyl = 130; // now algebraic variables
	static const int i_dC_Glc = 1;
	static const int i_dC_Xyl = 2;
	static const int i_C_Suc = 3;
	static const int i_C_Lac = 4;
	static const int i_C_Ace = 5;
	static const int i_V_R = 6;
	static const int i_n_C_added = 7;
	static const int i_time = 8;

	static const int i_Glc_depleted = 50;
	static const int i_Xyl_depleted = 51;

	static const int i_STY = p_+n_+q_;
	static const int i_C_atoms = p_ + n_ + q_ + 1;

	// model parameters from BEProMod paper
	const double vmax_g = 4.5;
	const double K_g = 15.0;
	const double vmax_x = 4.0;
	const double K_x = 15.0;
	const double vmax_o2 = 7.54;
	const double K_o2 = 0.001;
	const double klaO2 = 20.0;
	const double C_O2_sat = 1.03;
	//parameter needed for xylonate production
	const double M = 1e6;
	const double ndh_ub = M;
	const double xdy_ub = M;
	const double upt_max = 4.5;
	const double xi_ub = 1e6;
	const double xdh_ub = 1e6;
	const double xr_ub = 1e6;
	const double yagE_ub = 1e6;
	const double kdy_ub = 1e6;


	const double MM_GLC = 0.18016;//g/mmol
	const double MM_XYL = 0.15013;//g/mmol
	const double MM_XLT = 0.16613;//g/mmol

	const double tolEvent = 1e-9;
	std::vector<double> tolEvent_vector;

	const double C_threshold = -10.0; //"when reached, simulation is stopped"
	const double eps = 1e-5; //"when extracellular concentration of differential equation is below, der(x) = 0"

	// parameter and variables needed to handle O2 balance
	const double C_O2_const = 0.3*C_O2_sat;

	// parameters for calculation of initial values of differential states
	const double X_start = 0.1;
	const double C_C_total = 1000.0; //	"Total moles of carbon per liter at beginning of batch [mol C/L]";
	const double phi_Xyl = 0.1; //"Ratio of carbon atoms in xylose vs (xylose+glucose)";
	const double V_0 = 1.0; // "Volume in reactor at start of fed batch";
	const double n_C_added_start = 0.0;

	//additional parameters for fed-batch operation
	const double F_in = 0.0; // "Feed rate";
	const double phi_xyl_in = 1.0; //	"Ratio of carbon atoms in xylose vs (xylose+glucose) in feed";
	const double C_C_in = 40000.0; //	"Total moles of carbon per liter during feeding [mol C/L]";
	const double C_Xyl_in = phi_xyl_in*C_C_in / 5; // "Inlet D-xylose concentration";
	const double C_Glc_in = (1 - phi_xyl_in)*C_C_in / 6; //	"Inlet D-glucose concentration";
	const double C_C_B = 1000.0; //	"Total moles of carbon per liter supplied during batch phase [mol C/L]";

	// simulation horizon
	double tf = 1.0;
	double tf_ = 15.0;
	double t0 = 0.0;
	double deltat = (double)(tf / N_THETA);

	double sigma_0[N_S];

	// initial guesses
	double y_0[N_Y] = {
		4.09090900, // 0 daeoIn[1]
		2.28571439, // 1 daeoIn[2]
		7.53999996, // 2 daeoIn[3]
		4.50000000, // 3 daeoIn[4]
		1000000.00000000, // 4 daeoIn[5]
		1000000.00000000, // 5 daeoIn[6]
		1000000.00000000, // 6 daeoIn[7]
		1000000.00000000, // 7 daeoIn[8]
		1000000.00000000, // 8 daeoIn[9]
		0.22039206, // 9 daeoOut[1]
		0.00000000, // 10 daeoOut[2]
		0.00000000, // 11 daeoOut[3]
		-0.00000000, // 12 daeoOut[4]
		-0.00000000, // 13 daeoOut[5]
		-0.00000000, // 14 daeoOut[6]
		0.22039206, // 15 daeoOut[7]
		-0.12890457, // 16 daeoOut[8]
		0.18694401, // 17 daeoOut[9]
		1.85427070, // 18 daeoOut[10]
		-0.00000000, // 19 daeoOut[11]
		0.00000000, // 20 daeoOut[12]
		0.00000000, // 21 daeoOut[13]
		1.08438826, // 22 daeoOut[14]
		0.00000000, // 23 daeoOut[15]
		0.00000000, // 24 daeoOut[16]
		0.00000000, // 25 daeoOut[17]
		9.55610752, // 26 daeoOut[18]
		2.10830712, // 27 daeoOut[19]
		5.27916288, // 28 daeoOut[20]
		4.61316586, // 29 daeoOut[21]
		4.11438560, // 30 daeoOut[22]
		4.11438560, // 31 daeoOut[23]
		4.11438560, // 32 daeoOut[24]
		2.39561081, // 33 daeoOut[25]
		1.71877468, // 34 daeoOut[26]
		1.26671946, // 35 daeoOut[27]
		1.26671946, // 36 daeoOut[28]
		1.12889147, // 37 daeoOut[29]
		2.10830712, // 38 daeoOut[30]
		7.44780016, // 39 daeoOut[31]
		5.61894321, // 40 daeoOut[32]
		0.51428348, // 41 daeoOut[33]
		0.11982805, // 42 daeoOut[34]
		3.20450020, // 43 daeoOut[35]
		4.77577591, // 44 daeoOut[36]
		4.77577591, // 45 daeoOut[37]
		0.40909091, // 46 daeoOut[38]
		0.40909091, // 47 daeoOut[39]
		4.09090900, // 48 daeoOut[40]
		4.61316586, // 49 daeoOut[41]
		5.27916288, // 50 daeoOut[42]
		0.22039206, // 51 daeoOut[43]
		0.22039206, // 52 daeoOut[44]
		0.00000000, // 53 daeoOut[45]
		9.55155182, // 54 daeoOut[46]
		0.00000000, // 55 daeoOut[47]
		14.32732677, // 56 daeoOut[48]
		0.40909091, // 57 daeoOut[49]
		0.40909091, // 58 daeoOut[50]
		0.40909091, // 59 daeoOut[51]
		0.40909091, // 60 daeoOut[52]
		0.00000000, // 61 daeoOut[53]
		1.87662339, // 62 daeoOut[54]
		2.76422429, // 63 daeoOut[55]
		0.00000000, // 64 daeoOut[56]
		-0.00000000, // 65 daeoOut[57]
		0.00000000, // 66 daeoOut[58]
		0.00000000, // 67 daeoOut[59]
		-0.00000000, // 68 daeoOut[60]
		0.00000000, // 69 daeoOut[61]
		0.00000000, // 70 daeoOut[62]
		0.00000000, // 71 daeoOut[63]
		0.00000000, // 72 daeoOut[64]
		0.00000000, // 73 daeoOut[65]
		0.00000000, // 74 daeoOut[66]
		0.00000000, // 75 daeoOut[67]
		-0.00000000, // 76 daeoOut[68]
		0.22039206, // 77 sigma[1]
		0.00000000, // 78 sigma[2]
		0.04798234, // 79 sigma[3]
		0.22039206, // 80 sigma[4]
		0.18694401, // 81 sigma[5]
		1.85427070, // 82 sigma[6]
		0.00000000, // 83 sigma[7]
		0.05757881, // 84 sigma[8]
		1.08438826, // 85 sigma[9]
		-0.00000000, // 86 sigma[10]
		9.55610752, // 87 sigma[11]
		4.11438560, // 88 sigma[12]
		4.11438560, // 89 sigma[13]
		4.11438560, // 90 sigma[14]
		7.44780016, // 91 sigma[15]
		0.51428348, // 92 sigma[16]
		0.11982805, // 93 sigma[17]
		3.20450020, // 94 sigma[18]
		4.77577591, // 95 sigma[19]
		4.77577591, // 96 sigma[20]
		0.40909091, // 97 sigma[21]
		0.40909091, // 98 sigma[22]
		4.09090900, // 99 sigma[23]
		0.05757881, // 100 sigma[24]
		9.55155182, // 101 sigma[25]
		-0.00000000, // 102 sigma[26]
		14.32732677, // 103 sigma[27]
		0.40909091, // 104 sigma[28]
		0.40909091, // 105 sigma[29]
		0.40909091, // 106 sigma[30]
		0.40909091, // 107 sigma[31]
		0.00959647, // 108 sigma[32]
		1.87662339, // 109 sigma[33]
		2.76422429, // 110 sigma[34]
		0.00959647, // 111 sigma[35]
		0.00000000, // 112 sigma[36]
		0.00000000, // 113 sigma[37]
		0.03838588, // 114 sigma[38]
		0.00000000, // 115 sigma[39]
		0.00000000, // 116 sigma[40]
		0.01919294, // 117 sigma[41]
		0.05757881, // 118 sigma[42]
		0.05757881, // 119 sigma[43]
		0.00000000, // 120 sigma[44]
		2.10556126, // 121 sigma[45]
		999999.56250000, // 122 sigma[46]
		999999.56250000, // 123 sigma[47]
		1000000.00000000, // 124 sigma[48]
		1000000.00000000, // 125 sigma[49]
		1000000.00000000, // 126 sigma[50]
	};

	double x_0[N_X] = {
		0.10000000, // 0 X
		//150.00000000, // 1 C_Glc
		//20.00000000, // 2 C_Xyl
		0.00000000, // 1 dC_Glc
		0.00000000, // 2 dC_Xyl
		0.00000000, // 3 C_Suc
		0.00000000, // 4 C_Lac
		0.00000000, // 5 C_Ace
		1.00000000, // 6 V_R
		0.00000000, // 7 n_C_added
		0.0, //time
	};

	double p_0[N_P] = {
		0.5,
		tf_,
	};

	std::vector<int> mode0 = std::vector<int>(N_S, 1);

	void * create_external_object() {
		//std::cout << "------------ Creating external object for CG example ------------" << std::endl;
		//////// Metabolic network model
		static const int n_ = 68;// "number of DAEO variables";
		static const int p_ = 9;// "number of DAEO inputs";
		static const int m_ = 57;// "number of DAEO equalities";
		static const int q_ = 50;// "number of DAEO inequalities";

		const char* params[p_] = { "b_1","b_2","b_3","upt_max","xdh_ub",
			"kdy_ub","xi_ub","yagE_ub","xr_ub" };// "DAEO inputs";
		const char* variables[n_] = { "icd","odhA","sucD","sdhCAB_m","fumC",
			"mqo_mdh","gltA","pgi","pyk","pdh","aceB","aceA","odx","pyc","mez",
			"pckG","ppc","pfkA","fda","gapA","eno","zwf","opcA","gnd","rpe",
			"rpi","tkt_1","tal","tkt_2","tpiA","fbp","CO2_t","bmsynth","SO3_t",
			"NH3_t","bc1_aa3","O2_t","ksh","Xyl_t","GLC_t_PEP","pgm","pgk",
			"acnB","acnA","SUCC_t","ndh","cyto_bd","ATPase","xdh","xls","xdy",
			"kdy","dGLC_t","dXyl_t","dO2_t","xi","xylB","yagE","aldA","glcD",
			"xr","xldh","pta","ackA","pqo","AC_t","ldh","LAC_t" };//	"DAEO variables";
		const char* objective = "2*dGLC_t - bmsynth + 2*dXyl_t";//"DAEO objective function"
																// DAEO constraints with bounds
		const char* equalities[m_] = { "acnB - 1.0*aceA - 1.0*icd",
			"icd - 1.224*bmsynth + ksh - 1.0*odhA","odhA - 1.0*sucD",
			"sdhCAB_m - 1.0*fumC","aceB + fumC - 1.0*mez - 1.0*mqo_mdh",
			"mqo_mdh - 1.0*gltA - 1.68*bmsynth - 1.0*odx - 1.0*pckG + ppc + pyc",
			"aceA - 1.0*SUCC_t - 1.0*sdhCAB_m + sucD",
			"pdh - 3.177*bmsynth - 1.0*gltA - 1.0*aceB - 1.0*pta",
			"GLC_t_PEP - 2.604*bmsynth - 1.0*ldh + mez + odx - 1.0*pdh - 1.0*pqo - 1.0*pyc + pyk + yagE",
			"eno - 0.652*bmsynth - 1.0*GLC_t_PEP + pckG - 1.0*ppc - 1.0*pyk",
			"GLC_t_PEP - 0.205*bmsynth - 1.0*pgi - 1.0*zwf",
			"aceA - 1.0*aceB + glcD",
			"fbp - 0.308*bmsynth - 1.0*pfkA + pgi + tal + tkt_2",
			"pfkA - 1.0*fda - 1.0*fbp",
			"fda - 0.129*bmsynth - 1.0*gapA - 1.0*tal + tkt_1 + tkt_2 + tpiA",
			"gnd - 1.0*rpe - 1.0*rpi","zwf - 1.0*opcA","opcA - 1.0*gnd",
			"rpe - 1.0*tkt_1 - 1.0*tkt_2 + xylB",
			"rpi - 0.879*bmsynth - 1.0*tkt_1","tkt_1 - 1.0*tal",
			"tal - 0.268*bmsynth - 1.0*tkt_2","fda - 1.0*tpiA",
			"bmsynth - 1.0*CO2_t + gnd + icd + mez + odhA + odx + pckG + pdh - 1.0*ppc - 1.0*pyc",
			"pgm - 1.0*eno",
			"ATPase - 1.0*Xyl_t + ackA - 17.002*bmsynth - 1.0*pckG - 1.0*pfkA + pgk - 1.0*pyc + pyk + sucD - 1.0*xylB",
			"aldA + 3.111*bmsynth + gapA + ksh - 1.0*ldh - 1.0*ndh + odhA + pdh + xdh + xldh",
			"glcD - 16.429*bmsynth + gnd + icd + mez - 1.0*xr + zwf",
			"2.0*bc1_aa3 + 2.0*cyto_bd - 1.0*mqo_mdh - 1.0*ndh - 1.0*pqo - 1.0*sdhCAB_m",
			"mqo_mdh - 2.0*cyto_bd - 2.0*bc1_aa3 + ndh + pqo + sdhCAB_m",
			"NH3_t - 6.231*bmsynth","O2_t - 1.0*bc1_aa3 - 1.0*cyto_bd",
			"Xyl_t - 1.0*xdh - 1.0*xi - 1.0*xr","pgk - 1.295*bmsynth - 1.0*pgm",
			"gapA - 1.0*pgk","SO3_t - 0.233*bmsynth","gltA - 1.0*acnA",
			"acnA - 1.0*acnB",
			"4.0*ATPase - 12.0*bc1_aa3 - 4.0*cyto_bd + 2.0*sdhCAB_m",
			"kdy - 1.0*ksh","xdy - 1.0*kdy - 1.0*yagE","xls - 1.0*xdy",
			"xdh - 1.0*xls","GLC_t_PEP - 1.0*b_1 + dGLC_t",
			"Xyl_t - 1.0*b_2 + dXyl_t","O2_t - 1.0*b_3 + dO2_t",
			"xi + xldh - 1.0*xylB","yagE - 1.0*aldA","aldA - 1.0*glcD",
			"xr - 1.0*xldh","pta - 1.0*ackA","ackA - 1.0*AC_t + pqo",
			"ldh - 1.0*LAC_t","mez","odx","ppc","pqo" };//	"DAEO equality constraints";
		const char* inequalities[q_] = { "icd","odhA","sucD","gltA","pyk","pdh",//6
			"aceB","aceA","pyc","pckG","pfkA","zwf","opcA","gnd","fbp",//15
			"bmsynth","SO3_t","NH3_t","bc1_aa3","O2_t","ksh","Xyl_t",//22
			"GLC_t_PEP","SUCC_t","ndh","cyto_bd","ATPase","xdh","xls","xdy",//30
			"kdy","dGLC_t","dXyl_t","dO2_t","xi","xylB","yagE","aldA","glcD",//39
			"xr","xldh","AC_t","ldh","LAC_t",//44
			"upt_max - 1.0*Xyl_t - 1.0*GLC_t_PEP","xdh_ub - 1.0*xdh",
			"kdy_ub - 1.0*kdy","xi_ub - 1.0*xi","yagE_ub - 1.0*yagE",
			"xr_ub - 1.0*xr" };// "DAEO inequality constraints"
		const std::vector<double> QdiagLP;
		std::vector<int> info = std::vector<int>(2, 0);

		std::vector<double> Qdiag;
		Qdiag.resize(n_ + p_);
		for (int i = 0; i < n_ + p_; ++i) {
			if (i < p_) {
				Qdiag[i] = 1e-12;
			}
			else {
				Qdiag[i] = 1e-6;
			}
		}

		return parseDFBAmodel(params,
			//x,
			variables,
			objective,
			equalities,
			inequalities,
			p_,
			m_,
			n_,
			q_,
			Qdiag.data(),
			Qdiag.size(),
			info.data());
	};

	void * create_external_object2() {
		//std::cout << "------------ Creating external object for CG example ------------" << std::endl;
		//////// Metabolic network model
		static const int n_ = 66;// "number of DAEO variables";
		static const int p_ = 9;// "number of DAEO inputs";
		static const int m_ = 55;// "number of DAEO equalities";
		static const int q_ = 50;// "number of DAEO inequalities";

		const char* params[p_] = { "b_1","b_2","b_3","upt_max","xdh_ub",
			"kdy_ub","xi_ub","yagE_ub","xr_ub" };// "DAEO inputs";
		const char* variables[n_] = { "icd","odhA","sucD","sdhCAB_m","fumC",
			"mqo_mdh","gltA","pgi","pyk","pdh","aceB","aceA","odx","pyc","mez",
			"pckG","ppc","pfkA","fda","gapA","eno","zwf","opcA","gnd","rpe",
			"rpi","tkt_1","tal","tkt_2","tpiA","fbp","CO2_t","bmsynth","SO3_t",
			"NH3_t","bc1_aa3","O2_t","ksh","Xyl_t","GLC_t_PEP","pgm","pgk",
			"acnB","acnA","SUCC_t","ndh","cyto_bd","ATPase","xdh","xls","xdy",
			"kdy","dO2_t","xi","xylB","yagE","aldA","glcD",
			"xr","xldh","pta","ackA","pqo","AC_t","ldh","LAC_t" };//	"DAEO variables";
		const char* objective = "- bmsynth";//"DAEO objective function"
																// DAEO constraints with bounds
		const char* equalities[m_] = { "acnB - 1.0*aceA - 1.0*icd",
			"icd - 1.224*bmsynth + ksh - 1.0*odhA","odhA - 1.0*sucD",
			"sdhCAB_m - 1.0*fumC","aceB + fumC - 1.0*mez - 1.0*mqo_mdh",
			"mqo_mdh - 1.0*gltA - 1.68*bmsynth - 1.0*odx - 1.0*pckG + ppc + pyc",
			"aceA - 1.0*SUCC_t - 1.0*sdhCAB_m + sucD",
			"pdh - 3.177*bmsynth - 1.0*gltA - 1.0*aceB - 1.0*pta",
			"GLC_t_PEP - 2.604*bmsynth - 1.0*ldh + mez + odx - 1.0*pdh - 1.0*pqo - 1.0*pyc + pyk + yagE",
			"eno - 0.652*bmsynth - 1.0*GLC_t_PEP + pckG - 1.0*ppc - 1.0*pyk",
			"GLC_t_PEP - 0.205*bmsynth - 1.0*pgi - 1.0*zwf",
			"aceA - 1.0*aceB + glcD",
			"fbp - 0.308*bmsynth - 1.0*pfkA + pgi + tal + tkt_2",
			"pfkA - 1.0*fda - 1.0*fbp",
			"fda - 0.129*bmsynth - 1.0*gapA - 1.0*tal + tkt_1 + tkt_2 + tpiA",
			"gnd - 1.0*rpe - 1.0*rpi","zwf - 1.0*opcA","opcA - 1.0*gnd",
			"rpe - 1.0*tkt_1 - 1.0*tkt_2 + xylB",
			"rpi - 0.879*bmsynth - 1.0*tkt_1","tkt_1 - 1.0*tal",
			"tal - 0.268*bmsynth - 1.0*tkt_2","fda - 1.0*tpiA",
			"bmsynth - 1.0*CO2_t + gnd + icd + mez + odhA + odx + pckG + pdh - 1.0*ppc - 1.0*pyc",
			"pgm - 1.0*eno",
			"ATPase - 1.0*Xyl_t + ackA - 17.002*bmsynth - 1.0*pckG - 1.0*pfkA + pgk - 1.0*pyc + pyk + sucD - 1.0*xylB",
			"aldA + 3.111*bmsynth + gapA + ksh - 1.0*ldh - 1.0*ndh + odhA + pdh + xdh + xldh",
			"glcD - 16.429*bmsynth + gnd + icd + mez - 1.0*xr + zwf",
			"2.0*bc1_aa3 + 2.0*cyto_bd - 1.0*mqo_mdh - 1.0*ndh - 1.0*pqo - 1.0*sdhCAB_m",
			"mqo_mdh - 2.0*cyto_bd - 2.0*bc1_aa3 + ndh + pqo + sdhCAB_m",
			"NH3_t - 6.231*bmsynth","O2_t - 1.0*bc1_aa3 - 1.0*cyto_bd",
			"Xyl_t - 1.0*xdh - 1.0*xi - 1.0*xr","pgk - 1.295*bmsynth - 1.0*pgm",
			"gapA - 1.0*pgk","SO3_t - 0.233*bmsynth","gltA - 1.0*acnA",
			"acnA - 1.0*acnB",
			"4.0*ATPase - 12.0*bc1_aa3 - 4.0*cyto_bd + 2.0*sdhCAB_m",
			"kdy - 1.0*ksh","xdy - 1.0*kdy - 1.0*yagE","xls - 1.0*xdy",
			"xdh - 1.0*xls","O2_t - 1.0*b_3 + dO2_t",
			"xi + xldh - 1.0*xylB","yagE - 1.0*aldA","aldA - 1.0*glcD",
			"xr - 1.0*xldh","pta - 1.0*ackA","ackA - 1.0*AC_t + pqo",
			"ldh - 1.0*LAC_t","mez","odx","ppc","pqo" };//	"DAEO equality constraints";
		const char* inequalities[q_] = { "icd","odhA","sucD","gltA","pyk","pdh",//6
			"aceB","aceA","pyc","pckG","pfkA","zwf","opcA","gnd","fbp",//15
			"bmsynth","SO3_t","NH3_t","bc1_aa3","O2_t","ksh","Xyl_t",//22
			"GLC_t_PEP","SUCC_t","ndh","cyto_bd","ATPase","xdh","xls","xdy",//30
			"kdy","b_1 - 1.0*GLC_t_PEP","b_2 - 1.0*Xyl_t","dO2_t","xi","xylB","yagE","aldA","glcD",//39
			"xr","xldh","AC_t","ldh","LAC_t",//44
			"upt_max - 1.0*Xyl_t - 1.0*GLC_t_PEP","xdh_ub - 1.0*xdh",
			"kdy_ub - 1.0*kdy","xi_ub - 1.0*xi","yagE_ub - 1.0*yagE",
			"xr_ub - 1.0*xr" };// "DAEO inequality constraints"
		const std::vector<double> QdiagLP;
		std::vector<int> info = std::vector<int>(2, 0);

		std::vector<double> Qdiag;
		Qdiag.resize(n_ + p_);
		for (int i = 0; i < n_ + p_; ++i) {
			if (i < p_) {
				Qdiag[i] = 1e-12;
			}
			else {
				Qdiag[i] = 1e-6;
			}
		}

		return parseDFBAmodel(params,
			//x,
			variables,
			objective,
			equalities,
			inequalities,
			p_,
			m_,
			n_,
			q_,
			Qdiag.data(),
			(int)Qdiag.size(),
			info.data());
	};

	// inline functions to help match variables with their entry in variable vector
	// differential variables
	template <typename T> inline const T& X(const T* x) const {
		return x[i_X];
	}
	template <typename T> inline const T tfinal(const T* p) const {
		return p[1];
	}
	//template <typename T> inline const T& vmax_g_fun(const T* x, const T* y, const T* p) const {
	//	return vmax_g;
	//}

	template <typename T> inline const T& C_Glc(const T* y) const {
		return y[i_C_Glc];
	}
	template <typename T> inline const T C_Glc_0(const T* p) const {
		return (1 - p[0])*1000.0 / 6.0;
	}
	template <typename T> inline const T& C_Xyl(const T* y) const {
		return y[i_C_Xyl];
	}
	template <typename T> inline const T C_Xyl_0(const T* p) const {
		return p[0] * 1000.0 / 5.0;
	}
	template <typename T> inline const T& C_Suc(const T* x) const {
		return x[i_C_Suc];
	}
	template <typename T> inline const T& C_Lac(const T* x) const {
		return x[i_C_Lac];
	}
	template <typename T> inline const T& C_Ace(const T* x) const {
		return x[i_C_Ace];
	}
	template <typename T> inline const T& V_R(const T* x) const {
		return x[i_V_R];
	}
	template <typename T> inline const T& n_C_added(const T* x) const {
		return x[i_n_C_added];
	}
	// extracellular fluxes
	template <typename T> inline const T& bmsynth(const T* y) const {
		return y[41]; //daeoOut[33]
	}
	template <typename T> inline const T& GLC_t_PEP(const T* y) const {
		return y[48]; //daeoOut[40]
	}
	template <typename T> inline const T& Xyl_t(const T* y) const {
		return y[47]; //daeoOut[39]
	}
	template <typename T> inline const T& SUCC_t(const T* y) const {
		return y[53]; //  SUCC_t = daeoOut[45];
	}
	template <typename T> inline const T& LAC_t(const T* y) const {
		return y[76]; //  LAC_t = daeoOut[68];
	}
	template <typename T> inline const T& AC_t(const T* y) const {
		return y[74]; //  AC_t = daeoOut[66];
	}
};

template<typename Real> inline unsigned CorynebacteriumGlutamicumNdae<Real>::Eval_n_p() const { return N_P; }
template<typename Real> inline unsigned CorynebacteriumGlutamicumNdae<Real>::Eval_n_sigma() const { return N_S; }
template<typename Real> inline unsigned CorynebacteriumGlutamicumNdae<Real>::Eval_n_x() const { return N_X; }
template<typename Real> inline unsigned CorynebacteriumGlutamicumNdae<Real>::Eval_n_y() const { return N_Y; }
template<typename Real> inline unsigned CorynebacteriumGlutamicumNdae<Real>::Eval_n_Theta() const { return N_THETA; }
template<typename Real> inline Real CorynebacteriumGlutamicumNdae<Real>::Eval_t0() const { return t0; }

template<typename Real> void CorynebacteriumGlutamicumNdae<Real>::Eval_Theta(Real* Theta) const {
	//for (int i = 0; i < N_THETA; ++i) {
	//	Theta[i] = (double)(i + 1) * tf / (double)N_THETA;
	//	//std::cout << "i = " << i << ", Theta[i] = " << Theta[i] << std::endl;
	//}
	//Theta[0] = 0.0;
	Theta[0] = tf;
}

template<typename Real> std::vector<int> CorynebacteriumGlutamicumNdae<Real>::EvalInitialMode() const {
	std::vector<int> temp(N_S);
	for (int i = 0; i < temp.size(); ++i) { temp[i] = mode0[i]; }
	return temp;
}

template<typename Real> std::vector<int> CorynebacteriumGlutamicumNdae<Real>::EvalInitialActiveSet(Real time, Real *x, Real *y, Real *p) const {
	Real g[N_Y];
	int info[2];
	std::vector<int> initialMode(N_S, 0);
	std::vector<int> tempMode(q_, 0);
	std::vector<Real> y_DFBA_0(p_ + n_ + q_, 0.0);
	for (int i = 0; i < (p_ + n_ + q_); i++) {
		y_DFBA_0[i] = y[i];
	}
	y_DFBA_0[0] = (C_Glc_0(p)*vmax_g) / (C_Glc_0(p) + K_g);
	y_DFBA_0[1] = (C_Xyl_0(p)*vmax_x) / (C_Xyl_0(p) + K_x);
	y_DFBA_0[2] = vmax_o2;
	y_DFBA_0[3] = upt_max;
	y_DFBA_0[4] = xdh_ub;
	y_DFBA_0[5] = kdy_ub;
	y_DFBA_0[6] = xi_ub;
	y_DFBA_0[7] = yagE_ub;
	y_DFBA_0[8] = xr_ub;
	resetDaeoToolbox(DFBA,info);
	Eval_g_toolbox<Real>(g, time, x, y_DFBA_0.data(), p, tempMode);
	returnActiveSet(DFBA, tempMode.data(), info);
	for (int i = 0; i < q_; i++) {
		initialMode[i] = tempMode[i];
	}
	initialMode[i_Glc_depleted] = C_Glc_0(p) < eps;
	initialMode[i_Xyl_depleted] = C_Glc_0(p) < eps;
	return initialMode;
}

template<typename Real> std::vector<int> CorynebacteriumGlutamicumNdae<Real>::EvalRootDirection(const std::vector<int>& mode) const {
	std::vector<int> rootDirection(N_S, 0);
	for (int i = 0; i < N_S; ++i) {
		rootDirection[i] = -1;
	}
	return rootDirection;

}

template<typename Real> void CorynebacteriumGlutamicumNdae<Real>::EvalInitialGuess_y(Real *y) const {
	int n_y = Eval_n_y();
	// initial guess for alg variables
	for (int i = 0; i < n_y; ++i) {
		y[i] = y_0[i];
	}
}

template<typename Real> 
std::vector<int> CorynebacteriumGlutamicumNdae<Real>::EvalMode(Real time, Real *x, Real *y, Real *p, const std::vector<int>& oldMode) const {
	return EvalMode_impl(time, x, y, p, oldMode);
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Set_ActiveSet(const std::vector<int>& mode) const { 
	int info[2];
	std::vector<int> tempMode(q_, 0);
	for (int i = 0; i < q_; i++) { tempMode[i] = mode[i]; }
	setActiveSet(DFBA, tempMode.data(), info);
};

template<typename Real>
template<typename T>
std::vector<int> CorynebacteriumGlutamicumNdae<Real>::EvalMode_impl(T time, T *x, T *y, T *p, const std::vector<int>& oldMode) const {
	typedef ad::mode<T> AD_MODE;
	typedef typename AD_MODE::value_t AD_VALUE_TYPE;
	bool newActiveSetCalculated = false;

	std::vector<int> newMode(N_S);
	std::vector<int> newMode_DFBA(q_,0);
	std::vector<Real> sigma(N_S);
	Eval_sigma(sigma.data(), time, x, y, p, oldMode);

	std::vector<T> b(p_, 0.0);
	for (int i = 0; i < p_; i++) {
		b[i] = y[i];
	}
	
	for (int i = 0; i < q_; i++) {
		if (sigma[i] < 0) {
			newMode_DFBA= updateAndReturnActiveSet_DFBA<AD_VALUE_TYPE>(b, DFBA);
			newActiveSetCalculated = true;
			break;
		}
	}
	for (int i = 0; i < q_; i++) {
		if (newActiveSetCalculated) { newMode[i] = newMode_DFBA[i]; }
		else { newMode[i] = oldMode[i]; }
	}
	newMode[i_Glc_depleted] = oldMode[i_Glc_depleted];
	if (sigma[i_Glc_depleted]<0) {
		if (oldMode[i_Glc_depleted]) {
			std::cout << "There is new glucose in the medium" << std::endl;
			newMode[i_Glc_depleted] = 0;
		}
		else {
			std::cout << "Glucose is depleted at t=" << time << std::endl;
			newMode[i_Glc_depleted] = 1;
		}
	}
	newMode[i_Xyl_depleted] = oldMode[i_Xyl_depleted];
	if (sigma[i_Xyl_depleted]<0) {
		if (oldMode[i_Xyl_depleted]) {
			std::cout << "There is new xylose in the medium" << std::endl;
			newMode[i_Xyl_depleted] = 0;
		}
		else {
			std::cout << "Xylose is depleted at t=" << time << std::endl;
			newMode[i_Xyl_depleted] = 1;
		}
	}
	//int changed_activity = 0;
	//for (int i = 0; i < N_S; i++) {
	//	if (oldMode[i] != newMode[i]) {
	//		std::cout << "Inequality " << i << " changed activity from " << oldMode[i] << " to " << newMode[i] << std::endl;
	//		changed_activity += 1;
	//	}
	//}
	//std::cout << "A total of " << changed_activity << " inequalities changed their activities." << std::endl;
	//std::cout << "newMode[0] = " << newMode[0] << ", oldMode[0] = " << oldMode[0] << ", newMode[1] = " << newMode[1] << ", oldMode[1] = " << oldMode[1] << std::endl;
	return newMode;
}

template<typename Real>
template<typename T>
void CorynebacteriumGlutamicumNdae<Real>::Eval_f_impl(T *f, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const
{
	f[i_X] = p[1]*(bmsynth(y)*X(x) - F_in / V_R(x)*X(x));
	//if (mode[i_Glc_depleted]) {
	//	f[i_C_Glc] = 0;
	//}
	//else {
	//	f[i_C_Glc] = p[1]*(F_in / V_R(x)*(C_Glc_in - C_Glc(x)) - (GLC_t_PEP(y)*X(x)));
	//}
	//if (mode[i_Xyl_depleted]) {
	//	f[i_C_Xyl] = 0;
	//}
	//else {
	//	f[i_C_Xyl] = p[1]*(F_in / V_R(x)*(C_Xyl_in - C_Xyl(x)) - (Xyl_t(y)*X(x)));
	//}
	if (mode[i_Glc_depleted]) {
		f[i_dC_Glc] = 0;
	}
	else {
		f[i_dC_Glc] = p[1]*(F_in / V_R(x)*(C_Glc_in - C_Glc(y)) - (GLC_t_PEP(y)*X(x)));
	}
	if (mode[i_Xyl_depleted]) {
		f[i_dC_Xyl] = 0;
	}
	else {
		f[i_dC_Xyl] = p[1]*(F_in / V_R(x)*(C_Xyl_in - C_Xyl(y)) - (Xyl_t(y)*X(x)));
	}
		
	f[i_C_Suc] = p[1]*((SUCC_t(y)*X(x)) - F_in / V_R(x)*C_Suc(x));
	f[i_C_Lac] = p[1]*(LAC_t(y)*X(x)*V_R(x) - F_in / V_R(x)*C_Lac(x));
	f[i_C_Ace] = p[1]*(AC_t(y)*X(x)*V_R(x) - F_in / V_R(x)*C_Ace(x));
	f[i_V_R] = p[1]*(F_in);
	f[i_n_C_added] = p[1]* (F_in*(C_Glc_in * 6 + C_Xyl_in * 5));
	f[i_time] = p[1];
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_f(Real *f, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const
{
	Eval_f_impl(f, time, x, y, p, mode);
}


template<typename Real>
template<typename T>
void CorynebacteriumGlutamicumNdae<Real>::Eval_g_params(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const
{
	//for (int i = 0; i < N_Y; ++i) {
	//	g[i] = -2.0;
	//}
	if (mode[i_Glc_depleted]) {
		g[0] = y[0];
	}
	else {
		g[0] = y[0] - (C_Glc(y)*vmax_g) / (C_Glc(y) + K_g);
	}
	if (mode[i_Xyl_depleted]) {
		g[1] = y[1];
	}
	else {
		g[1] = y[1] - (C_Xyl(y)*vmax_x) / (C_Xyl(y) + K_x);
	}
	g[2] = y[2] - vmax_o2;
	g[3] = y[3] - upt_max;
	g[4] = y[4] - xdh_ub;
	g[5] = y[5] - kdy_ub;
	g[6] = y[6] - xi_ub;
	g[7] = y[7] - yagE_ub;
	g[8] = y[8] - xr_ub;
}

//template<typename Real>
//template<typename AD_TYPE>
//void CorynebacteriumGlutamicumNdae<Real>::updateActiveSet_DFBA(std::vector<AD_TYPE>& b, void* externalObject) const {
//	typedef ad::mode<AD_TYPE> AD_MODE;
//	typedef typename AD_MODE::value_t AD_VALUE_TYPE;
//	int info[2];
//
//	std::vector<AD_VALUE_TYPE> xv(b.size(), 0.0);
//	for (int i = 0; i < b.size(); i++) {
//		xv[i] = ad::value(b[i]);
//	}
//
//	updateBasicSet(externalObject, xv.data(), info);
//}

template<typename Real>
template<typename AD_TYPE>
std::vector<int> CorynebacteriumGlutamicumNdae<Real>::updateAndReturnActiveSet_DFBA(std::vector<AD_TYPE>& b, void* externalObject) const {
	typedef ad::mode<AD_TYPE> AD_MODE;
	typedef typename AD_MODE::value_t AD_VALUE_TYPE;
	int info[2];
	std::vector<int> activeSet(q_, 0);

	std::vector<AD_VALUE_TYPE> xv(b.size(), 0.0);
	for (int i = 0; i < b.size(); i++) {
		xv[i] = ad::value(b[i]);
	}

	updateBasicSet(externalObject, xv.data(), info);
	returnActiveSet(externalObject, activeSet.data(), info);
	int sum_active_ineq = 0;
	for (int i = 0; i < activeSet.size(); i++) {
		sum_active_ineq += activeSet[i];
	}
	//std::cout << "ss of freedom: " << n_ - m_ - sum_active_ineq << std::endl;
	return activeSet;
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::g_DFBA(std::vector<double>& b, std::vector<double>& v, std::vector<double>& sigma, void* externalObject, const std::vector<int>& mode) const {
	typedef ad::mode<double> AD_MODE;
	typedef typename AD_MODE::value_t AD_VALUE_TYPE;
	typedef typename AD_MODE::derivative_t AD_DERIVATIVE_TYPE;

	int info[2];

	//std::cout << "double implementation" << std::endl;
	//std::cout << " AD_TYPE CONST: " << std::is_const<AD_TYPE>::value << std::endl;
	//std::cout << "NAME: " << __FUNCSIG__ << std::endl;
	//std::vector<int> activeSet(q_, 0);
	//returnActiveSet(externalObject, activeSet.data(), info);

	std::vector<AD_VALUE_TYPE> xv(b.size(), 0.0);
	//std::vector<AD_VALUE_TYPE> xt(x.size(), 0.0);
	for (int i = 0; i < b.size(); i++) {
		xv[i] = ad::value(b[i]);
		//xt[i] = ad::derivative(x[i]);
	}
	vector<AD_VALUE_TYPE> yv(v.size());
	vector<AD_VALUE_TYPE> sigmav(sigma.size());
	//vector<AD_VALUE_TYPE> yt(y.size());
	//vector<AD_VALUE_TYPE> sigmat(sigma.size());

	solveActiveSet(externalObject, xv.data(), yv.data(), sigmav.data(), info);
	//derivative_tangent(externalObject, xt.data(), yt.data(), sigmat.data(), info);

	for (unsigned i = 0; i < v.size(); i++) {
		ad::value(v[i]) = yv[i];
		//ad::derivative(y[i]) = yt[i];
	}
	for (unsigned i = 0; i < sigma.size(); i++) {
		ad::value(sigma[i]) = sigmav[i];
		//ad::derivative(sigma[i]) = sigmat[i];
	}
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::g_DFBA(std::vector<Gt1s>& b, std::vector<Gt1s>& v, std::vector<Gt1s>& sigma, void* externalObject, const std::vector<int>& mode) const {
	typedef ad::mode<ad::gt1s<double>::type> AD_MODE;
	typedef typename AD_MODE::value_t AD_VALUE_TYPE;
	typedef typename AD_MODE::derivative_t AD_DERIVATIVE_TYPE;

	int info[2];

	//std::cout << "tangent implementation" << std::endl;
	//std::cout << " AD_TYPE CONST: " << std::is_const<AD_TYPE>::value << std::endl;
	//std::cout << "NAME: " << __FUNCSIG__ << std::endl;
	
	//std::vector<AD_VALUE_TYPE> xv(x.size(), 0.0);
	std::vector<AD_VALUE_TYPE> xt(b.size(), 0.0);
	for (int i = 0; i < b.size(); i++) {
		//xv[i] = ad::value(x[i]);
		xt[i] = ad::derivative(b[i]);
	}
	//vector<AD_VALUE_TYPE> yv(y.size());
	//vector<AD_VALUE_TYPE> sigmav(sigma.size());
	vector<AD_VALUE_TYPE> yt(v.size());
	vector<AD_VALUE_TYPE> sigmat(sigma.size());

	//solveActiveSet(externalObject, xv.data(), yv.data(), sigmav.data(), info);
	derivative_tangent(externalObject, xt.data(), yt.data(), sigmat.data(), info);

	for (unsigned i = 0; i < v.size(); i++) {
		//ad::value(y[i]) = yv[i];
		ad::derivative(v[i])=yt[i];
	}
	for (unsigned i = 0; i < sigma.size(); i++) {
		//ad::value(sigma[i]) = sigmav[i];
		ad::derivative(sigma[i]) = sigmat[i];
	}
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::g_DFBA(std::vector<Ga1s>& b, std::vector<Ga1s>& v, std::vector<Ga1s>& sigma, void* externalObject, const std::vector<int>& mode) const {
	typedef ad::mode<Ga1s> AD_MODE;
	//typedef typename AD_MODE::value_t AD_VALUE_TYPE;
	//typedef typename AD_MODE::external_adjoint_object_t AD_EAO_TYPE;
	//typedef typename AD_MODE::derivative_t AD_DERIVATIVE_TYPE;ad::ga1s<AD_A1_REAL>::global_tape
	//AD_EAO_TYPE *D = AD_MODE::global_tape->template create_callback_object<AD_EAO_TYPE>();

	//std::cout << "adjoint implementation" << std::endl;

	//std::cout << "adjoint implementation" << std::endl;
	//std::cout << " AD_TYPE CONST: " << std::is_const<AD_TYPE>::value << std::endl;
	//std::cout << "NAME: " << __FUNCSIG__ << std::endl;
	//for (int i = 0; i < x.size(); i++) { std::cout << "x_a[" << i << "] = " << x[i] << std::endl; }

	g_make_gap_DFBA(b, v, sigma, externalObject, mode);

	//for (int i = 0; i < y.size(); i++) { std::cout << "y_a[" << i << "] = " << y[i] << std::endl; }
	//int info[2];



	//std::vector<AD_VALUE_TYPE> xv(x.size(), 0.0);
	//std::vector<AD_VALUE_TYPE> xt(x.size(), 0.0);
	//for (int i = 0; i < x.size(); i++) {
	//	xv[i] = ad::value(x[i]);
	//	xt[i] = ad::derivative(x[i]);
	//}
	//vector<AD_VALUE_TYPE> yv(y.size());
	//vector<AD_VALUE_TYPE> sigmav(sigma.size());
	//vector<AD_VALUE_TYPE> yt(y.size());
	//vector<AD_VALUE_TYPE> sigmat(sigma.size());

	//solveActiveSet(externalObject, xv.data(), yv.data(), sigmav.data(), info);
	//derivative_tangent(externalObject, xt.data(), yt.data(), sigmat.data(), info);

	//for (unsigned i = 0; i < y.size(); i++) {
	//	ad::value(y[i]) = yv[i];
	//	ad::derivative(y[i]) = yt[i];
	//}
	//for (unsigned i = 0; i < sigma.size(); i++) {
	//	ad::value(sigma[i]) = sigmav[i];
	//	ad::derivative(sigma[i]) = sigmat[i];
	//}
}


template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::g_fill_gap_DFBA(ad::mode<Ga1s>::external_adjoint_object_t *D) const {//, void* externalObject) {
	typedef typename ad::mode<Ga1s>::value_t AD_VALUE_TYPE;


	//typedef typename AD_MODE::external_adjoint_object_t AD_EAO_TYPE;

	const int &n_p = D->template read_data<int>();
	const int &n_n = D->template read_data<int>();
	const int &n_q = D->template read_data<int>();
	const std::vector<int> mode_(D->template read_data<std::vector<int>>());
	//cout << "n after read: " << n_p << endl;
	//cout << "m after read: " << n_n << endl;
	//cout << "q after read: " << n_q << endl;

	

	vector<AD_VALUE_TYPE> ya1(n_n, 0.0);
	D->get_output_adjoint(ya1);
  for (int i = 0; i < ya1.size(); i++) {
		if (std::isnan(ya1[i])) {
			std::cout << "The adjoint ya1[" << i << "] is nan" << std::endl;
		}
	}
	vector<AD_VALUE_TYPE> sigma_a1(n_q, 0.0);
	D->get_output_adjoint(sigma_a1);
  for (int i = 0; i < sigma_a1.size(); i++) {
		//if ((sigma_a1[i] * sigma_a1[i]) > 1e-5) {
		//	std::cout << "sigma_a1[" << i << "] = " << sigma_a1[i] << std::endl;
		//}
		if (std::isnan(sigma_a1[i])) {
			std::cout << "The adjoint sigma_a1[" << i << "] is nan" << std::endl;
		}
	}

	void* externalObject = D->template read_data<void*>();
	int info[2];
	
	std::vector<int> tempMode(n_q, 0);
	//returnActiveSet(externalObject, tempMode.data(), info);
	//vector<AD_VALUE_TYPE> xa1_y(n_p);
	//vector<AD_VALUE_TYPE> xa1_sigma(n_p);
	//derivative_adjoint_x(externalObject, ya1.data(), xa1_y.data(), info);
	//derivative_adjoint_sigma(externalObject, sigma_a1.data(), xa1_sigma.data(), info);
	
	vector<AD_VALUE_TYPE> xa1(n_p);
	derivative_adjoint(externalObject, ya1.data(), sigma_a1.data(), xa1.data(), info);
	for (int i = 0; i < n_p; i++) {
		//std::cout << "xa1[" << i << "] = " << xa1[i] << ", xa1_y = " << xa1_y[i] << ", xa1_sigma = " << xa1_sigma[i] << ", squared dev = " << (xa1[i] - (xa1_y[i] + xa1_sigma[i]))*(xa1[i] - (xa1_y[i] + xa1_sigma[i]))<< std::endl;
		if (std::isnan(xa1[i])) {
			std::cout << "Number of registered inputs: " << D->get_number_of_registered_inputs() << std::endl;
			std::cout << "Number of registered outputs: " << D->get_number_of_registered_outputs() << std::endl;
			std::cout << "All adjoints read: " << D->all_adjoints_read() << std::endl;
			
			//for (int j = 0; j < ya1.size(); j++) { std::cout << "y_a[" << j << "] = " << ya1[j] << std::endl; }
			std::cout << "The adjoint xa[" << i << "] is nan" << std::endl;
		}
		//D->increment_input_adjoint(xa1_y[i] + xa1_sigma[i]); 
		D->increment_input_adjoint(xa1[i]);
	}
	if (!D->all_adjoints_written()) {
		std::cout << "Not all adjoints written: " << D->all_adjoints_written() << std::endl;
	}
	//std::cout << "All adjoints written: " << D->all_adjoints_written() << std::endl;
 }

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::g_make_gap_DFBA(std::vector<Ga1s>& b, std::vector<Ga1s>& v, std::vector<Ga1s>& sigma, void* externalObject, const std::vector<int>& mode) const {//, void* externalObject) {
	typedef ad::mode<Ga1s> AD_MODE;
	typedef typename AD_MODE::value_t AD_VALUE_TYPE;
	typedef typename AD_MODE::external_adjoint_object_t AD_EAO_TYPE;
	AD_EAO_TYPE *D = AD_MODE::global_tape->template create_callback_object<AD_EAO_TYPE>();
	//direkt am Anfang den this pointer speichern:
	D->write_data(this);


	size_t n_p = b.size();
	size_t n_n = v.size();
	size_t n_q = sigma.size();
	vector<AD_VALUE_TYPE> xv(n_p);
	for (size_t i = 0; i<n_p; i++) xv[i] = D->register_input(b[i]);
	D->write_data(n_p);
	D->write_data(n_n);
	D->write_data(n_q);
	D->write_data(mode);
	D->write_data(externalObject);
	

	vector<AD_VALUE_TYPE> yv(n_n);
	vector<AD_VALUE_TYPE> sigmav(n_q);

	//updateActiveSet_DFBA(xv, externalObject);
	g_DFBA(xv, yv, sigmav, externalObject, mode);

	for (size_t i = 0; i<n_n; i++) v[i] = D->register_output(yv[i]);
	for (size_t i = 0; i<n_q; i++) sigma[i] = D->register_output(sigmav[i]);
	
	//void(*temp_func)(ad::mode<Ga1s>::external_adjoint_object_t*);
	//temp_func = &CorynebacteriumGlutamicumNdae<Real>::g_fill_gap_DFBA;
	//temp_func(D);
	//temp_func = CorynebacteriumGlutamicumNdae<Real>::g_fill_gap_DFBA;
	//AD_MODE::global_tape->insert_callback(CorynebacteriumGlutamicumNdae<Real>::g_fill_gap_DFBA, D);
	auto wrapper_g_fill_gap_DFBA = [](AD_EAO_TYPE* Din) { CorynebacteriumGlutamicumNdae *thisptr = Din->read_data<CorynebacteriumGlutamicumNdae*>(); thisptr->g_fill_gap_DFBA(Din);  };
	AD_MODE::global_tape->insert_callback(wrapper_g_fill_gap_DFBA, D);

	//auto wrapper_g_fill_gap_DFBA = [this](AD_EAO_TYPE * Din) -> void {g_fill_gap_DFBA(Din); };
	//AD_MODE::global_tape->insert_callback(wrapper_g_fill_gap_DFBA, D);
	//AD_MODE::global_tape->insert_callback(g_fill_gap_DFBA, D);
	//D->check_tape(v);
	
}


template<typename Real>
template<typename T>
void CorynebacteriumGlutamicumNdae<Real>::Eval_g_toolbox(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const
{
	typedef ad::mode<T> AD_MODE;
	//typedef AD_MODE::type AD_TYPE;
	typedef typename AD_MODE::value_t AD_VALUE_TYPE;
	

	std::vector<T> b(p_, 0.0);
	for (int i = 0; i < p_; i++) {
		b[i] = y[i];
	}
	std::vector<T> v(n_, 0.0);
	std::vector<T> sigma(q_, 0.0);
	//int info[2];

	//std::cout << "NAME: " << __FUNCSIG__ << std::endl;

	//g_make_gap_DFBA<Real>(b, v, sigma, DFBA);
	//std::cout << "Calling g_DFBA with mode[4] = " << mode[4] << std::endl;
	g_DFBA(b, v, sigma, DFBA, mode);


	for (int i = 0; i < n_; i++) {
		g[i] = y[i + p_] - v[i];
	}
	for (int i = 0; i < q_; i++) {
		g[i+n_] = y[i + p_ + n_] - sigma[i];
	}
	//solveActiveSet(DFBA, b.data(), v.data(), sigma.data(), info);

	//typedef ad::mode<AD_TYPE> AD_MODE;
	//typedef typename AD_MODE::value_t AD_VALUE_TYPE;
	//typedef typename AD_MODE::external_adjoint_object_t AD_EAO_TYPE;

	//std::vector<double> daeoIn(p_);
	//for (int i = 0; i < p_; i++) {
	//	daeoIn[i] = (double)y[i];
	//}

	//std::vector<double> sol_AS(n_);
	//std::vector<double> sigma_AS(q_);
	//int status[2];
	//solveActiveSet(DFBA, daeoIn.data(), sol_AS.data(), sigma_AS.data(), status);
	//typedef ad::mode<AD_TYPE> AD_MODE;
	//typedef typename AD_MODE::value_t AD_VALUE_TYPE;
	//typedef typename AD_MODE::external_adjoint_object_t AD_EAO_TYPE;
	//AD_EAO_TYPE *D = AD_MODE::global_tape->template create_callback_object<AD_EAO_TYPE>();

	//size_t n = x.size();
	//vector<AD_VALUE_TYPE> xv(n);
	//for (size_t i = 0; i<n; i++) xv[i] = D->register_input(x[i]);
	//D->write_data(n);
	//AD_VALUE_TYPE yv;
	//g<AD_VALUE_TYPE>(xv, yv);
	//y = D->register_output(yv);
	//AD_MODE::global_tape->insert_callback(g_fill_gap<AD_MODE>, D);
}
template<typename Real>
template<typename T>
void CorynebacteriumGlutamicumNdae<Real>::Eval_g_impl(T *g, T& time, T *x, T *y, T *p, const std::vector<int>& mode) const
{
	T g_PARAMS[p_];
	Eval_g_params(g_PARAMS, time, x, y, p, mode);
	for (int i = 0; i < p_; i++) {
		g[i] = g_PARAMS[i];
	}
	T g_toolbox[n_ + q_];
	Eval_g_toolbox(g_toolbox, time, x, y, p, mode);
	for (int i = 0; i < (n_ + q_); i++) {
		g[i + p_] = g_toolbox[i];
	}
	g[i_STY] = y[i_STY] + x[i_X] / p[1];// (x[i_time] + 1.e-03);
	g[i_C_atoms] = y[i_C_atoms]- (5 * C_Xyl(y) + 6 * C_Glc(y));
	g[i_C_Glc] = y[i_C_Glc] - (C_Glc_0(p) + x[i_dC_Glc]);
	g[i_C_Xyl] = y[i_C_Xyl] - (C_Xyl_0(p) + x[i_dC_Xyl]);
	//if (time < 1e-4){
	//	std::cout << "C_Glc(t=" << time << ") = " << C_Glc(y) << ", dC_Glc = " << x[i_dC_Glc]  << std::endl;
	//	std::cout << "C_Xyl(t=" << time << ") = " << C_Xyl(y) << ", dC_Xyl = " << x[i_dC_Xyl] << std::endl;
	//}
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_g(Real *g, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const
{
	Eval_g_impl(g, time, x, y, p, mode);
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_a1_f(Real*f, Real *a1_f, Real& time, Real& a1_time,
	Real *x, Real *a1_x, Real *y, Real *a1_y,
	Real *p, Real* a1_p, const std::vector<int>& mode) const
{

	AD_A1_CREATE
		
		AD_A1_RESET

		AD_A1_DEC2(f, N_X)
		AD_A1_DEC1(time)
		AD_A1_DEC2(x, N_X)
		AD_A1_DEC2(y, N_Y)
		AD_A1_DEC2(p, N_P)

		Eval_f_impl(ad_f, ad_time, ad_x, ad_y, ad_p, mode);
	AD_A1_INT2(f, N_X)
		AD_A1_GET1(time)
		AD_A1_GET2(x, N_X)
		AD_A1_GET2(y, N_Y)
		AD_A1_GET2(p, N_P)

}




template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_a1_g(Real* g, Real *a1_g, Real& time, Real& a1_time,
	Real *x, Real *a1_x, Real *y, Real *a1_y,
	Real *p, Real* a1_p, const std::vector<int>& mode) const
{	
	
	//std::cout << "Call to Eval_a1_g: " << mode[4] << std::endl;
		//create tape
		AD_A1_CREATE
		//reset tape
		AD_A1_RESET
		auto test = ad::ga1s<AD_A1_REAL>::global_tape->get_position();

		AD_A1_DEC2(g, N_Y)
		AD_A1_DEC1(time)
		AD_A1_DEC2(x, N_X)
		AD_A1_DEC2(y, N_Y)
		AD_A1_DEC2(p, N_P)
		test= ad::ga1s<AD_A1_REAL>::global_tape->get_position();

		Eval_g_impl(ad_g, ad_time, ad_x, ad_y, ad_p, mode);
		AD_A1_INT2(g, N_Y)

		AD_A1_GET2(g, N_Y)
		AD_A1_GET1(time)
		AD_A1_GET2(x, N_X)
		AD_A1_GET2(y, N_Y)
		AD_A1_GET2(p, N_P)
}

template<typename Real>
template<typename T>
void CorynebacteriumGlutamicumNdae<Real>::Eval_sigma_impl(T* sigma, T& time,
	T* x, T *y, T* p, const std::vector<int>& mode) const
{
	double temp_tolEvent=0.0;
	for (int i = 0; i < q_; ++i) {
		temp_tolEvent = (1.+double(i) / double(q_-1))*tolEvent;
		sigma[i] = y[p_ + n_ + i] + temp_tolEvent;
	}
	if (mode[i_Glc_depleted]) { sigma[i_Glc_depleted] = -y[i_C_Glc] + eps; }
	else { sigma[i_Glc_depleted] = y[i_C_Glc] - eps; }
	if (mode[i_Xyl_depleted]) { sigma[i_Xyl_depleted] = -y[i_C_Xyl] + eps; }
	else { sigma[i_Xyl_depleted] = y[i_C_Xyl] - eps; }
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_sigma(Real* sigma, Real& time,
	Real* x, Real *y, Real* p, const std::vector<int>& mode) const
{
	Eval_sigma_impl(sigma, time, x, y, p, mode);
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_a1_sigma(Real* sigma, Real* a1_sigma, Real& time, Real& a1_time,
	Real *x, Real *a1_x, Real *y, Real *a1_y,
	Real *p, Real* a1_p, const std::vector<int>& mode) const
{
		//std::cout << "Call to Eval_a1_sigma: " << mode[4] << std::endl;
		AD_A1_CREATE
			AD_A1_RESET

			AD_A1_DEC2(sigma, N_S)
			AD_A1_DEC1(time)
			AD_A1_DEC2(x, N_X)
			AD_A1_DEC2(y, N_Y)
			AD_A1_DEC2(p, N_P)

		Eval_sigma_impl(ad_sigma, ad_time, ad_x, ad_y, ad_p, mode);
		AD_A1_INT2(sigma, N_S)

		AD_A1_GET2(sigma, N_S)
		AD_A1_GET1(time)
		AD_A1_GET2(x, N_X)
		AD_A1_GET2(y, N_Y)
		AD_A1_GET2(p, N_P)
}

template<typename Real>
template<typename T>
void CorynebacteriumGlutamicumNdae<Real>::Eval_psi_impl(T *psi, T& time, T* x, T *y,
	T* p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const
{
	for (int i = 0; i < N_X; ++i) {
		psi[i] = x[i];
	}
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_psi(Real *psi, Real& time, Real* x, Real *y,
	Real* p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const
{
	Eval_psi_impl(psi, time, x, y, p, newMode, oldMode);
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_a1_psi(Real*psi, Real *a1_psi, Real& time, Real& a1_time,
	Real *x, Real *a1_x, Real *y, Real *a1_y,
	Real *p, Real* a1_p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const
{
	AD_A1_CREATE
		AD_A1_RESET

		AD_A1_DEC2(psi, N_X)
		AD_A1_DEC1(time)
		AD_A1_DEC2(x, N_X)
		AD_A1_DEC2(y, N_Y)
		AD_A1_DEC2(p, N_P)

		Eval_psi_impl(ad_psi, ad_time, ad_x, ad_y, ad_p, newMode, oldMode);
	AD_A1_INT2(psi, N_X)

		AD_A1_GET1(time)
		AD_A1_GET2(x, N_X)
		AD_A1_GET2(y, N_Y)
		AD_A1_GET2(p, N_P)
}

template<typename Real>
template<typename T>
void CorynebacteriumGlutamicumNdae<Real>::Eval_psi0_impl(T *psi0, T* p) const
{
	//for (int i = 0; i < N_X; ++i) {
	//	psi0[i] = x_0[i];
	//}
	psi0[i_X] = x_0[i_X];
	psi0[i_dC_Glc] = 0.0;// (1 - p[0])*1000.0 / 6.0;
	psi0[i_dC_Xyl] = 0.0;// p[0] * 1000.0 / 5.0;
	psi0[i_C_Suc] = x_0[i_C_Suc];
	psi0[i_C_Lac] = x_0[i_C_Lac];
	psi0[i_C_Ace] = x_0[i_C_Ace];
	psi0[i_V_R] = x_0[i_V_R];
	psi0[i_n_C_added] = x_0[i_n_C_added];
	psi0[i_time] = x_0[i_time];
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_psi0(Real *psi0, Real* p) const
{
	Eval_psi0_impl(psi0, p);
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_a1_psi0(Real *psi0, Real *a1_psi0, Real* p, Real *a1_p) const
{
	AD_A1_CREATE
		AD_A1_RESET

		AD_A1_DEC2(psi0, N_X)
		AD_A1_DEC2(p, N_P)

		Eval_psi0_impl(ad_psi0, ad_p);
	AD_A1_INT2(psi0, N_X)

		AD_A1_GET2(p, N_P)
}


template<typename Real>
template<typename T>
void CorynebacteriumGlutamicumNdae<Real>::Eval_phi_impl(T& phi, T *Theta, T **X, T **Y, T *p) const
{
	if (objMode == ObjectiveMode::ComputeObjective) {
		phi = Y[N_THETA - 1][i_STY];
	}
	else if (objMode == ObjectiveMode::ComputeConstraint) {
		phi = Y[N_THETA - 1][i_C_atoms];
	}
	else {
		phi = Y[N_THETA - 1][i_STY];
	}
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_phi(Real& phi, Real *Theta, Real **X, Real **Y, Real *p) const
{
	Eval_phi_impl(phi, Theta, X, Y, p);
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_a1_phi(Real& phi, Real& a1_phi, Real *Theta, Real* a1_Theta,
	Real **X, Real ** a1_X,
	Real **Y, Real **a1_Y, Real *p, Real *a1_p) const
{
		AD_A1_CREATE
		AD_A1_RESET

		AD_A1_DEC1(phi)
		AD_A1_DEC2(Theta, N_THETA)
		AD_A1_DEC3(X, N_X, N_THETA)
		AD_A1_DEC3(Y, N_Y, N_THETA)
		AD_A1_DEC2(p, N_P)

		Eval_phi_impl(ad_phi, ad_Theta, ad_X, ad_Y, ad_p);
		AD_A1_INT1(phi)

		AD_A1_GET2(Theta, N_THETA)
		AD_A1_GET3(X, N_X, N_THETA)
		AD_A1_GET3(Y, N_Y, N_THETA)
		AD_A1_GET2(p, N_P)
}

inline std::vector<Gt1s> CreateGt1sVector(const double* values, const double* t1_values, const unsigned n)
{
	std::vector<Gt1s> gt1sVector(n);
	for (unsigned i = 0; i < n; ++i) {
		ad::value(gt1sVector[i]) = values[i];
		ad::derivative(gt1sVector[i]) = t1_values[i];
	}
	return gt1sVector;
}

inline std::vector<Gt1s> CreateGt1sVector(const unsigned n)
{
	std::vector<Gt1s> gt1sVector(n);
	for (unsigned i = 0; i < n; ++i) {
		ad::value(gt1sVector[i]) = 0.0;
		ad::derivative(gt1sVector[i]) = 0.0;
	}
	return gt1sVector;
}

inline void ExtractGt1sVector(double *vec, double *t1_vec, const std::vector<Gt1s>& ad_vec) {
	for (std::size_t i = 0; i < ad_vec.size(); ++i) {
		vec[i] = ad::value(ad_vec[i]);
		t1_vec[i] = ad::derivative(ad_vec[i]);
	}
}

inline Gt1s CreateGt1sScalar(const double value) {
	Gt1s scalar;
	ad::value(scalar) = value;
	ad::derivative(scalar) = 0.0;
	return scalar;
}

inline void ExtractGt1sScalar(double& scalar, double& t1_scalar, const Gt1s& ad_scalar) {
	scalar = ad::value(ad_scalar);
	t1_scalar = ad::derivative(ad_scalar);
}



template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_t1_f(Real *f, Real *t1_f, Real& time, Real& t1_time, Real *x, Real *t1_x,
	Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const
{

	auto ad_f = CreateGt1sVector(N_X);
	auto ad_time = CreateGt1sScalar(time);
	auto ad_x = CreateGt1sVector(x, t1_x, N_X);
	auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
	auto ad_p = CreateGt1sVector(p, t1_p, N_P);

	Eval_f_impl(ad_f.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), mode);

	ExtractGt1sVector(f, t1_f, ad_f);


}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_t1_g(Real *g, Real *t1_g, Real& time, Real& t1_time, Real *x, Real *t1_x,
	Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const
{
	auto ad_g = CreateGt1sVector(N_Y);
	auto ad_time = CreateGt1sScalar(time);
	auto ad_x = CreateGt1sVector(x, t1_x, N_X);
	auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
	auto ad_p = CreateGt1sVector(p, t1_p, N_P);

	Eval_g_impl(ad_g.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), mode);

	ExtractGt1sVector(g, t1_g, ad_g);
}


template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_t1_sigma(Real* sigma, Real* t1_sigma, Real& time, Real& t1_time, Real *x, Real *t1_x,
	Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const
{
	auto ad_sigma = CreateGt1sVector(N_S);
	auto ad_time = CreateGt1sScalar(time);
	auto ad_x = CreateGt1sVector(x, t1_x, N_X);
	auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
	auto ad_p = CreateGt1sVector(p, t1_p, N_P);

	Eval_sigma_impl(ad_sigma.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), mode);

	ExtractGt1sVector(sigma, t1_sigma, ad_sigma);
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_t1_psi(Real * psi, Real *t1_psi, Real& time, Real& t1_time, Real *x, Real *t1_x,
	Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& newMode,
	const std::vector<int>& oldMode) const
{
	auto ad_psi = CreateGt1sVector(N_X);
	auto ad_time = CreateGt1sScalar(time);
	auto ad_x = CreateGt1sVector(x, t1_x, N_X);
	auto ad_y = CreateGt1sVector(y, t1_y, N_Y);
	auto ad_p = CreateGt1sVector(p, t1_p, N_P);

	Eval_psi_impl(ad_psi.data(), ad_time, ad_x.data(), ad_y.data(), ad_p.data(), newMode, oldMode);

	ExtractGt1sVector(psi, t1_psi, ad_psi);
}


template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_t1_psi0(Real *psi0, Real *t1_psi0, Real *p, Real* t1_p) const
{
	auto ad_psi0 = CreateGt1sVector(N_X);
	auto ad_p = CreateGt1sVector(p, t1_p, N_P);

	Eval_psi0_impl(ad_psi0.data(), ad_p.data());

	ExtractGt1sVector(psi0, t1_psi0, ad_psi0);
}

template<typename Real>
void CorynebacteriumGlutamicumNdae<Real>::Eval_t1_phi(Real& phi, Real& t1_phi, Real *Theta, Real *t1_Theta, Real **X, Real ** t1_X,
	Real **Y, Real **t1_Y, Real *p, Real *t1_p) const
{
	std::vector<Gt1s*> ad_X(N_THETA), ad_Y(N_THETA);
	auto ad_X_data = CreateGt1sVector(N_THETA * N_X);
	auto ad_Y_data = CreateGt1sVector(N_THETA * N_Y);
	for (unsigned i = 0; i < N_THETA; ++i) {
		ad_X[i] = &ad_X_data[i*N_X];
		ad_Y[i] = &ad_Y_data[i*N_Y];
		for (unsigned j = 0; j < N_X; ++j) {
			ad::value(ad_X[i][j]) = X[i][j];
			ad::derivative(ad_X[i][j]) = t1_X[i][j];
		}
		for (unsigned j = 0; j < N_Y; ++j) {
			ad::value(ad_Y[i][j]) = Y[i][j];
			ad::derivative(ad_Y[i][j]) = t1_Y[i][j];
		}

	}
	auto ad_p = CreateGt1sVector(p, t1_p, N_P);
	auto ad_Theta = CreateGt1sVector(Theta, t1_Theta, N_THETA);
	Gt1s ad_phi;
	Eval_phi_impl(ad_phi, ad_Theta.data(), ad_X.data(), ad_Y.data(), ad_p.data());

	ExtractGt1sScalar(phi, t1_phi, ad_phi);

}


#endif // CorynebacteriumGlutamicumNdae_NDAE_HPP
