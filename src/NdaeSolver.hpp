#ifndef NDAE_SOLVER_HPP
#define NDAE_SOLVER_HPP

#include "Ndae.hpp"
#include "idas/idas.h"
#include <string>


/** Solver routines for NDAEs represented by Ndae<Real>. */
class NdaeSolver {
public:
  class Options;
  /** Return type of EvaluatePhi(Gradient).
   *  The follwowing two values are possible:
   *  Flag_Success: successful call of EvaluatePhi(Gradient)
   *  Flag_Failure: sum erroer occured in  EvaluatePhi(Gradient)
   */
  enum FlagReturn { Flag_Success = 0, Flag_Failure = 1 };

  /** \brief Compute objective functional \f$\Phi(\boldsymbol{p})\f$ for a
      parameter vector \f$\boldsymbol{p}\f$.
      \param[out] Phi objective function value
      \param[in]  p parameter vector p
      \param[in]  dae NDAE representation of type Ndae<realtype>
      \param[in]  options solver options
      \return     Flag_Success/Flag_Failure if computation succeded/failed */
  static FlagReturn EvaluatePhi(realtype& Phi, const realtype* p,  Ndae<realtype>& dae,
                   const Options& options=StandardOptions());

  /** \brief Compute objective functional Phi and its gradient for parameter vector p.
      \param[out] Phi objective function value
      \param[out] Phi_p gradient of the objective function
      \param[in]  p parameter vector p
      \param[in]  dae NDAE representation of type Ndae<realtype>
      \param[in]  options solver options
      \return     Flag_Success/Flag_Failure if computation succeded/failed */
  static FlagReturn EvaluatePhiGradient(realtype& Phi, realtype* Phi_p, const realtype *p,
                           Ndae<realtype>& dae,
                           const Options& options=StandardOptions());

  static FlagReturn EvaluatePhiGradientNew(realtype & Phi, realtype * Phi_p, realtype * x_f, realtype * y_f, std::vector<int>& mode_f, const realtype * p, Ndae<realtype>& dae, const Options & options);

  //static FlagReturn EvaluatePhiGradientNew(realtype & Phi, realtype * Phi_p, realtype * x_f, std::vector<int>& mode_f, const realtype * p, const Ndae<realtype>& dae, const Options & options);

 // static FlagReturn EvaluatePhiGradientNew(realtype & Phi, realtype * Phi_p, realtype * x_f, std::vector<int> mode_f, const realtype * p, const Ndae<realtype>& dae, const Options & options);

  //static FlagReturn EvaluatePhiGradientNew(realtype & Phi, realtype * Phi_p, realtype * x_f, const realtype * p, const Ndae<realtype>& dae, const Options& options = StandardOptions());


  /** \brief Standard options for numerical solver.
      \return standard options */
  static const Options& StandardOptions();


};

/** Solver options for EvaluatePhi(Gradient) of class NDAESolver. */
class NdaeSolver::Options{
public:
  /** Standard constructor. */
  Options();
  /** Set integration tolerance. */
  void SetTol(const realtype tol) { m_AbsTol = m_RelTol = tol; }
  /** Get absolute integration tolerance */
  const realtype GetAbsTol() const { return m_AbsTol; }

  void SetAbsTol(const realtype tol) {m_AbsTol = tol;}
  void SetRelTol(const realtype tol) {m_RelTol = tol;}


  /** Get absolute integration tolerance */
  const realtype GetRelTol() const { return m_RelTol; }

  realtype GetAbsTolAdjoint() const;
  void SetAbsTolAdjoint(const realtype &AbsTolAdjoint);

  realtype GetRelTolAdjoint() const;
  void SetRelTolAdjoint(const realtype &RelTolAdjoint);

  void SetTolAdjoint(const realtype tol) {m_AbsTolAdjoint = m_RelTolAdjoint = tol;}


  /** Set state event tolerance. */
  void SetStateEventTol(const realtype tol) { m_StateEventTol = tol; }
  /** Get state event tolerance */
  const realtype GetStateEventTol() const { return m_StateEventTol; }

  /** Set maximal discontinuity tolerance. */
  void SetMaxDiscontinuityTol(const realtype tol) { m_MaxDiscontinuityTol = tol; }
  /** Get maximal discontinuity tolerance */
  const realtype GetMaxDiscontinuityTol() const { return m_MaxDiscontinuityTol; }

  /** Set maximal step size for dense output. */
  void SetMaxStepDenseOutput(const realtype stepSize) { m_MaxStepDenseOutput = stepSize; }

  /** Set maximal step size for dense output. */
  const realtype GetMaxStepDenseOutput() const { return m_MaxStepDenseOutput; }

  /** Switch dense output option on or off. */
  void SetDoPlot(const bool doPlot) { m_DoPlot = doPlot; }

  /** Set file name for output of states */
  void SetFilenameStates(const std::string& filename) { m_FilenameStates = filename; }

  /** Geit file name for output of states */
  std::string GetFilenameStates() const { return  m_FilenameStates; }

  /** Check whether dense output option is activated. */
  const bool GetDoPlot() const { return m_DoPlot; }

  /** Set standard options. */
  void SetToStandard();
  /** Print options to screen. */
  void PrintOptions();

private:
  realtype m_AbsTol; ///< Absolute integration  tolerance.
  realtype m_RelTol; ///< Relative integration tolerance.
  realtype m_AbsTolAdjoint; ///<  Absolute integration  tolerance for adjoints.
  realtype m_RelTolAdjoint; ///<  Relative integration  tolerance for adjoints.
  realtype m_StateEventTol; ///< State event tolerance.
  realtype m_MaxDiscontinuityTol; ///< Maximal discontinuity tolerance
  realtype m_MaxStepDenseOutput; ///< Maximal step size for dense output.
  bool m_DoPlot;  ///< Controls whether we have dense output to a file or not.
  std::string m_FilenameStates;
};


#endif // NDAE_SOLVER_HPP

