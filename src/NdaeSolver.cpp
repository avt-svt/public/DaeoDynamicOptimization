#include <iostream>
#include "NdaeSolver.hpp"
#include "NdaeIdasAdapter.hpp"

void NdaeSolver::Options::SetToStandard() {
  m_RelTol = m_AbsTol = 1e-6;
  m_RelTolAdjoint = m_AbsTolAdjoint = 1e-5;
  m_StateEventTol = 1e-6;
  m_DoPlot = false;
  m_MaxStepDenseOutput = 0.001;
  m_FilenameStates = "x.txt";
}

NdaeSolver::Options::Options()
{
  SetToStandard();
}

void NdaeSolver::Options::PrintOptions()
{
  std::cout << "NDAESolverOptions are:" << std::endl;
  std::cout << "Absolute integration tolerance: " << m_AbsTol << std::endl;
  std::cout << "Relative integration tolerance: " << m_RelTol << std::endl;
  std::cout << "Plotting is " << (m_DoPlot?"on":"off") << std::endl;
}

realtype Options::GetAbsTolAdjoint() const
{
    return m_AbsTolAdjoint;
}

void Options::SetAbsTolAdjoint(const realtype &AbsTolAdjoint)
{
    m_AbsTolAdjoint = AbsTolAdjoint;
}

realtype Options::GetRelTolAdjoint() const
{
    return m_RelTolAdjoint;
}

void Options::SetRelTolAdjoint(const realtype &RelTolAdjoint)
{
    m_RelTolAdjoint = RelTolAdjoint;
}

const NdaeSolver::Options& NdaeSolver::StandardOptions()
{
    static Options standardOptions;
    return standardOptions;
}

NdaeSolver::FlagReturn NdaeSolver::EvaluatePhi(realtype &Phi, const realtype *p, Ndae<realtype> &dae,
                                               const Options &options)
{

  int flag = NdaeIdasAdapter::EvaluateObjective(Phi,p,dae,options);

  if(flag == 0)
    return  Flag_Success;
  else
    return Flag_Failure;

}


NdaeSolver::FlagReturn NdaeSolver::EvaluatePhiGradient(realtype &Phi, realtype *Phi_p, const realtype *p,
                                                       Ndae<realtype> &dae,  const Options &options)
{
  int flag = NdaeIdasAdapter::EvaluateObjAndGrad(Phi,Phi_p,p,dae,options);

  if(flag == 0)
    return  Flag_Success;
  else
    return Flag_Failure;;
}

//NdaeSolver::FlagReturn NdaeSolver::EvaluatePhiGradientNew(realtype &Phi, realtype *Phi_p, realtype *x_f, realtype *y_f, std::vector<int>& mode_f, const realtype *p,
//	Ndae<realtype> &dae, const Options &options)
//{
//	int flag = NdaeIdasAdapter::EvaluateObjAndGradNew(Phi, Phi_p, x_f, y_f, mode_f, p, dae, options);
//	if (flag == 0)
//		return  Flag_Success;
//	else
//		return Flag_Failure;;
//}
