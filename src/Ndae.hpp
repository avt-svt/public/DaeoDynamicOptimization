#ifndef NDAE_HPP
#define NDAE_HPP

#include <vector>

/** This abstract class has to be overloaded by the user to implement the NDAE (1.1) of the accompanying article. */
template <typename Real>
class Ndae {
private:
	int itheta=0;
public:
  virtual ~Ndae() {}

  /*! \brief Define dimension of parameter vector \f$\boldsymbol{p}\f$.
  \return \f$n_p\f$ */
  void set_itheta(const int value) { itheta=value; }
  int get_itheta() const { return itheta; }

   /*! \brief Define dimension of parameter vector \f$\boldsymbol{p}\f$.
       \return \f$n_p\f$ */
  virtual unsigned Eval_n_p() const { return 2; }

  /*! \brief Define dimension of number of switching functions \f$\boldsymbol{\sigma}\f$.
  \return \f$n_{\sigma}\f$ */
  virtual unsigned Eval_n_sigma() const = 0;

  /*! \brief Define dimension of differential variable vector \f$\boldsymbol{x}\f$.
      \return \f$n_x\f$ */
  virtual unsigned Eval_n_x() const = 0;

  /*! \brief Define dimension of algebraic variable vector \f$\boldsymbol{x}\f$.
      \return \f$n_y\f$ */
  virtual unsigned Eval_n_y() const = 0;

  /*! \brief Define cardinality of set of input times \f$\boldsymbol{\Theta}\f$.
      \return \f$n_{\Theta}\f$ */
  virtual unsigned Eval_n_Theta () const = 0;

  /*! \brief Define intial time \f$t^0\f$.
      \return \f$t^0\f$ */
  virtual Real Eval_t0() const = 0;

  /*! \brief Define  set of input times \f$\boldsymbol{\Theta}\f$.
      \param[out] Theta \f$\boldsymbol{\Theta}\f$. */
  virtual void Eval_Theta(Real *Theta) const = 0;

  /*! \brief Function to set the active set of the embedded optimization problem in DAEOs.
	\param[out] Theta \f$\boldsymbol{\Theta}\f$. */
  virtual void Set_ActiveSet(const std::vector<int>& mode) const { };

  /*! \brief Define new mode for given t,x,p,y when an event is detected.
  \return new mode.
  \param[in] time current time t.
  \param[in] x diffential variables x.
  \param[in] y algebraic variables x.
  \param[in] p parameter vector p.
  \param[in] oldMolde the preceding mode. */
  virtual std::vector<int> EvalMode(Real time, Real *x, Real *y, Real *p, const std::vector<int>& oldMode) const { return std::vector<int>(); };
  //virtual int EvalMode(Real time, Real *x, Real *y, Real *p, int oldMode) const = 0;

  /*! \brief Define direction of roots of the switching function.
  \param[in] mode current mode of stage k
  \return root direction, -1(+1) if switching function is negative(positive) slope */
  virtual std::vector<int> EvalRootDirection(const std::vector<int>& mode) const { return std::vector<int>(); };

  /*! \brief Define initial guess for algebraic variables at the initial time.
  \param[out] y initial guess for algebraic variables at the initial time */
  virtual void EvalInitialGuess_y(Real *y) const {}

  /*! \brief Define mode of initial stage.
  \return mode of initial stage */
  virtual std::vector<int> EvalInitialMode() const { return std::vector<int>(); };
  //virtual int EvalInitialMode() const = 0;

	/*! \brief Function to set the active set of the embedded optimization problem in DAEOs.
	\param[out] Theta \f$\boldsymbol{\Theta}\f$. */
  virtual std::vector<int> EvalInitialActiveSet(Real time, Real *x, Real *y, Real *p) const { return EvalInitialMode(); };
  
  /*! \brief Evaluate right hand side of differential equations \f$\boldsymbol{x}=\boldsymbol{g}^k(t,\boldsymbol{x},\boldsymbol{y},\boldsymbol{p})\f$  for the mode of stage \f$k\f$.
  \param[out] f right hand side of differential equations.
  \param[in] time current time t
  \param[in] x diffential variables x
  \param[in] y algebraic variables y
  \param[in] p parameter vector p
  \param[in] mode mode in stage k */
  virtual void Eval_f(Real *f, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const {}

  /*! \brief Evaluate right hand side of algebraic equations
   * \f$0=\boldsymbol{g}^k(t,\boldsymbol{x},\boldsymbol{y},\boldsymbol{p})\f$ for the mode of stage \f$k\f$.
  \param[out] g right hand side of algebraic equations.
  \param[in] time current time t
  \param[in] x diffential variables x
  \param[in] y algebraic variables y
  \param[in] p parameter vector p
  \param[in] mode mode in stage k */
  virtual void Eval_g(Real *g, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const {}

  /*! \brief Evaluate switching function
   * \f$\sigma^k(t,\boldsymbol{x},\boldsymbol{y},\boldsymbol{p})\f$ for the mode of stage \f$k\f$.
  \param[out] sigma value of switching function
  \param[in] time current time t
  \param[in] x diffential variables x
  \param[in] y algebraic variables y
  \param[in] p parameter vector p
  \param[in] mode mode in stage k */
  virtual void Eval_sigma(Real *sigma, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& mode) const = 0;

  /*! \brief Evaluate right hand side of transition condition
   * \f$\boldsymbol{x}^{k+1}=\boldsymbol{\psi}^k(t^k,\boldsymbol{x},\boldsymbol{y},\boldsymbol{p})\f$ at switching time \f$t^k\f$ for the mode of stage \f$k\f$ and the new mode of stage \f$k+1\f$.
  \param[out] psi right hand side of transition condition
  \param[in] time switching time t^k
  \param[in] x diffential variables x
  \param[in] y algebraic variables y
  \param[in] p parameter vector p
  \param[in] oldMode mode in stage k
  \param[in] newMode mode in stage k+1  */
  virtual void Eval_psi(Real *psi, Real& time, Real *x, Real *y, Real *p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const = 0;

  /*! \brief Evaluate right hand side of initial condition
   * \f$\boldsymbol{x}^{1}(t^0)=\boldsymbol{\psi}^k_0({p})\f$.
  \param[out] psi0 right hand side of initial condition
  \param[in] p parameter vector p */
  virtual void Eval_psi0(Real *psi0, Real *p) const = 0;

  /*! \brief Evaluate objective function \f$\phi(\boldsymbol{\Theta},\boldsymbol{X},\boldsymbol{Y},\boldsymbol{p})\f$
  \param[out] phi value of objective function
  \param[in] Theta set of evaluation times \f$\boldsymbol{\Theta}\f$
  \param[in] X set of diffential variables x evalutaed at \f$t\in\boldsymbol{\Theta}\f$
  \param[in] Y set of algebraic variables y evalutaed at \f$t\in\boldsymbol{\Theta}\f$
  \param[in] p parameter vector p  */
  virtual void Eval_phi(Real& phi, Real *Theta, Real **X, Real **Y, Real *p) const = 0;

  /*! \brief Adjoint of Eval_f() (usually generated by algorithmic differentiation). */
  virtual void Eval_a1_f(Real *f, Real *a1_f, Real& time, Real& a1_time, Real *x, Real *a1_x,
                         Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode) const = 0;

  /*! \brief Adjoint of Eval_g() (usually generated by algorithmic differentiation. */
  virtual void Eval_a1_g(Real *g, Real *a1_g, Real& time, Real& a1_time, Real *x, Real *a1_x,
                         Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode) const = 0;

  /*! \brief Adjoint of Eval_sigma() (usually generated by algorithmic differentiation). */
  virtual void Eval_a1_sigma(Real* sigma, Real* a1_sigma, Real& time, Real& a1_time, Real *x, Real *a1_x,
                             Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& mode) const = 0;

  /*! \brief Adjoint of Eval_psi() (usually generated by algorithmic differentiation). */
  virtual void Eval_a1_psi(Real * psi, Real *a1_psi, Real& time, Real& a1_time, Real *x, Real *a1_x,
                           Real *y, Real *a1_y, Real *p, Real *a1_p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const = 0;

  /*! \brief Adjoint of Eval_psi0() (usually generated by algorithmic differentiation). */
  virtual void Eval_a1_psi0(Real *psi0, Real *a1_psi0, Real *p, Real* a1_p) const = 0;

  /*! \brief Adjoint of Eval_phi() (usually generated by algorithmic differentiation). */
  virtual void Eval_a1_phi(Real& phi, Real& a1_phi, Real *Theta, Real *a1_Theta, Real **X, Real ** a1_X,
                           Real **Y, Real **a1_Y, Real *p, Real *a1_p) const = 0;


  /*! \brief Tangent-linear model of Eval_f() (usually generated by algorithmic differentiation). */
  virtual void Eval_t1_f(Real *f, Real *t1_f, Real& time, Real& t1_time, Real *x, Real *t1_x,
                            Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const = 0;

     /*! \brief Tangent-linear model Eval_g() (usually generated by algorithmic differentiation. */
     virtual void Eval_t1_g(Real *g, Real *t1_g, Real& time, Real& t1_time, Real *x, Real *t1_x,
                            Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const = 0;

     /*! \brief Tangent-linear model Eval_sigma() (usually generated by algorithmic differentiation). */
     virtual void Eval_t1_sigma(Real* sigma, Real* t1_sigma, Real& time, Real& t1_time, Real *x, Real *t1_x,
                                Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& mode) const = 0;

     /*! \brief Tangent-linear model Eval_psi() (usually generated by algorithmic differentiation). */
     virtual void Eval_t1_psi(Real * psi, Real *t1_psi, Real& time, Real& t1_time, Real *x, Real *t1_x,
                              Real *y, Real *t1_y, Real *p, Real *t1_p, const std::vector<int>& newMode, const std::vector<int>& oldMode) const = 0;

     /*! \brief Tangent-linear model Eval_psi0() (usually generated by algorithmic differentiation). */
     virtual void Eval_t1_psi0(Real *psi0, Real *t1_psi0, Real *p, Real* t1_p) const = 0;

     /*! \brief Tangent-linear model Eval_phi() (usually generated by algorithmic differentiation). */
     virtual void Eval_t1_phi(Real& phi, Real& t1_phi, Real *Theta, Real *t1_Theta, Real **X, Real ** t1_X,
                              Real **Y, Real **t1_Y, Real *p, Real *t1_p) const = 0;


};

#endif // NDAE_HPP

