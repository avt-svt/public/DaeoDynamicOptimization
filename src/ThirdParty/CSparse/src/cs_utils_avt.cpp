/** 
* @file cs_utils_avt.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Additional utility functions for cs_sparse library                   \n
* =====================================================================\n
* This file contains the following functions:                          \n
* =====================================================================\n
* @author Klaus Stockmann
* @date 21.10.2011
*/

#include <cassert>

#include <vector>

#include "cs.h"
#include "cs_utils_avt.hpp"


/**
  * @brief  Constructing a csparse-struct cs (in csc format) from row, column indices and values
  *
  * Conversion from the Coordinate-Data-Structure used by GpromsESO-functions
  * to the "compressed sparse column format" used by csparse (cs/dmperm). Details
  * to struct cs in sourcecode.
  * The function performs 3 tasks:
  * 1) conversion of STL vectors (in which matrix is stored in triplet format) to C arrays
  * 2) constructing cs matrix in triplet form from this C arrays
  * 3) calling cs_compress to convert cs matrix from triplet to compressed sparse column format
  *
  * @param iP row indices of the temp struct-component, which will be returned (A)
  * @param jP col indices of the temp struct-component
  * @param valueP value vector of the temp struct-component
  * @param mP number of rows of the temp struct-component
  * @param nP number of cols of the temp struct-component
  * @return   pointer to struct of type cs
  *
  */
cs *csutil_convert2cs(const std::vector<int> &iP,
                      const std::vector<int> &jP,
                      const std::vector<double> &valueP,
                      const int mP,
                      const int nP)
{
  // typedef struct cs_sparse    // matrix in compressed-column or triplet form
  //{
  //    int nzmax ;     // maximum number of entries
  //    int m ;         // number of rows 
  //    int n ;         // number of columns 
  //    int *p ;        // column pointers (size n+1) or col indices (size nzmax)
  //    int *i ;        // row indices, size nzmax
  //    double *x ;     // numerical values, size nzmax
  //    int nz ;        // # of entries in triplet matrix, -1 for compressed-col
  //} cs ;
  
  // initialize the cs object with the values of parameters
  const int triplet = 1;
  const int values = 1;
  const int size = int(valueP.size());
  cs *A = (cs*) cs_spalloc(mP, nP, size, values, triplet);
  
  A->nzmax = size;
  A->m = mP;
  A->n = nP;
  for (int i = 0; i<size; i++) {
    A->p[i] = jP[i];
    A->i[i] = iP[i];
    A->x[i] = valueP[i];
  }
  A->nz = size;
  
  cs *B;
  B = cs_compress(A);            // B = compressed-column form of A
  cs_spfree(A);                  // clear A 
  
  return B;
}


// Remark: what to do if only structure, but no values are available?
/**
* @brief get single value from csparse matrix (may be in CSC or in COO format)
*
* @param rowind row index 
* @param colind column index
* @param *mat pointer to csparse matrix
* @return value
*/
double csutil_getval(const int rowind, const int colind, const cs *mat)
{
   // Remark: indices are zero-based
   assert(rowind>=0);
   assert(colind>=0);
   assert(rowind<mat->m);
   assert(colind<mat->n);

   double zero = 0.0;

    if (CS_CSC(mat)) {                // compressed-sparse column
      int indStart, indEnd;
      indStart = mat->p[colind];
      indEnd = mat->p[colind+1]; // -1 ;
      if (indStart == indEnd) {
          return zero; // no non-zeros in this column 
      }
      else {
          for (int i=indStart; i<indEnd; i++) {
              if (mat->i[i] == rowind)  {
                  return mat->x[i];
              }
          }
          return zero;
      }
    }
    else if (CS_TRIPLET(mat)) { // triplet format; Remark: very expensive
        int i, found = 0;
        double val;

        for (i = 0; i < mat->nz; i++) {
            if (rowind == mat->i[i] && colind == mat->p[i]) {
                found = 1;
                val = mat->x[i];
                break;
            }
        }
        if (found == 0) return(zero);
        else            return(val);

    }
    else {
        assert(1==0);
        return (zero); // otherwise compiler complains about control path that does not return a value
    }
}

/**
* @brief set single value in csparse matrix (may be in CSC or in COO format)
*
* @param rowind row index 
* @param colind column index
* @param *mat pointer to csparse matrix
* @param value value to be set
* @date 12.10.2011
* @author Tjalf Hoffmann
*/
void csutil_setval(const int rowind, const int colind, const cs *mat, const double value)
{
  // Remark: indices are zero-based
  assert(rowind>=0);
  assert(colind>=0);
  assert(rowind<mat->m);
  assert(colind<mat->n);

  if (CS_CSC(mat)) {                // compressed-sparse column
    int indStart, indEnd;
    indStart = mat->p[colind];
    indEnd = mat->p[colind+1]; // -1 ;
    if (indStart == indEnd) {
        //throw exception
    }
    else {
        for (int i=indStart; i<indEnd; i++) {
            if (mat->i[i] == rowind)  {
                mat->x[i] = value;
                return;
            }
        }
        //throw exception
    }
  }
  else if (CS_TRIPLET(mat)) { // triplet format; Remark: very expensive
      int i, found = 0;

      for (i = 0; i < mat->nz; i++) {
          if (rowind == mat->i[i] && colind == mat->p[i]) {
              found = 1;
              mat->x[i] = value;
              break;
          }
      }
      if (found == 0){
        //throw exception
      }

  }
}



/**
* @brief checks if at (i,j) there is a structurally non-zero entry (may be in CSC or in COO format)
*
* @param rowind row index 
* @param colind column index
* @param *mat pointer to csparse matrix
* @return 1, if structurally non-zero, 0 if not
*/
int csutil_has_entry_at(const int rowind, const int colind, const cs *mat)
{
   // Remark: indices are zero-based
   assert(rowind>=0);
   assert(colind>=0);
   assert(rowind<mat->m);
   assert(colind<mat->n);

   const int TRUE = 1;
   const int FALSE = 0;

    if (CS_CSC(mat)) {                // compressed-sparse column
      int indStart, indEnd;
      indStart = mat->p[colind];
      indEnd = mat->p[colind+1]; // -1 ;
      if (indStart == indEnd) {
          return FALSE; // no non-zeros in this column 
      }
      else {
          for (int i=indStart; i<indEnd; i++) {
              if (mat->i[i] == rowind)  {
                  return TRUE;
              }
          }
          return FALSE;
      }
    }
    else if (CS_TRIPLET(mat)) { // triplet format; Remark: very expensive
        int i, found = 0;

        for (i = 0; i < mat->nz; i++) {
            if (rowind == mat->i[i] && colind == mat->p[i]) {
                found = 1;
                break;
            }
        }
        if (found == 0) return FALSE;
        else            return TRUE;

    }
    else {
        assert(1==0);
        return FALSE; // otherwise compiler complains about control path that does not return a value
    }
}


// return number of nonzeros of cs-matrix; 
// convenience function: you don't have to worry if it is in triplet or csc format

/**
* @brief return number of nonzeros of csparse matrix
*
* this is a convenience function: you don't have to worry if the matrix is
* in CSC or COO format
*
* @param *mat pointer to csparse matrix
* @return number of nonzeros
*/
int csutil_getNNZ(const cs *mat)
{
    if (CS_TRIPLET(mat)) return mat->nz;
    else if (CS_CSC(mat)) return mat->p[mat->n];
    else {
        assert(1==0);
        return 0; // otherwise compiler complains that control path element does not return a value
    }
}


/**
* @brief extract matrix slices from csparse matrix
*
* extract matrix slices from a csparse matrix. The slices are defined by vectors
* containing the row and column indices. Currently this works only, if matrix is 
* in CSC format
* 
* @param rowIndices indices of rows to be extracted
* @param colIndices indices of columns to be extracted
* @param *mat pointer to input csparse matrix
* @return extracted csparse matrix
*/
cs *csutil_extractmatslices_csc(const std::vector<int> rowIndices, const std::vector<int> colIndices, const cs *mat)
{
    assert(CS_CSC(mat)); // check if csc matrix
    
    cs *extractedMat;

    int nnz = csutil_getNNZ(mat);

    // allocate triplet matrix with as much memory as input matrix (will be reallocated before exit)
    const int values = 1, triplet = 1;
    extractedMat = cs_spalloc(mat->m, mat->n, nnz, values, triplet);

    // extract matrix slices and put them into output matrix (currently still in triplet format)
    unsigned m = unsigned(rowIndices.size());
    unsigned n = unsigned(colIndices.size());

    for (unsigned i=0; i<m; i++) {
        assert(rowIndices[i] >= 0);
        assert(rowIndices[i] < mat->m);
    }
    
    for (unsigned i=0; i<n; i++) {
        assert(colIndices[i] >= 0);
        assert(colIndices[i] < mat->n);
    }
    
    

    unsigned counter = 0;

    unsigned startInd, endInd;
    unsigned currColInd, currRowInd;

    // first, extract whole columns
    for (unsigned j=0; j<n; j++) {
         currColInd = colIndices[j];
         startInd = mat->p[currColInd];
         endInd = mat->p[currColInd+1];
         for (unsigned i=startInd; i<endInd; i++) {
             currRowInd = mat->i[i];
             extractedMat->i[counter] = currRowInd;
             extractedMat->p[counter] = j;
             extractedMat->x[counter] = csutil_getval(currRowInd, currColInd, mat);
             extractedMat->nz = counter+1;
             counter++;
         }
    }
    extractedMat->n = n; // a number of colIndices.size() columns have been extracted

    // convert to csc format
    extractedMat = cs_compress(extractedMat);


    // transpose 
    extractedMat = cs_transpose(extractedMat, values);

    // extract rows, which are now - as the matrix has been transposed - columns
    cs *extractedMatFinal;  // allocate new triplet matrix because populating a triplet matrix is easier
    extractedMatFinal = cs_spalloc(extractedMat->m, extractedMat->n, nnz, values, triplet);

    counter = 0; // reset counter
    for (unsigned j=0; j<m; j++) {
         currColInd = rowIndices[j];   
         startInd = extractedMat->p[currColInd];
         endInd = extractedMat->p[currColInd+1];
         for (unsigned i=startInd; i<endInd; i++) {
             currRowInd = extractedMat->i[i];
             extractedMatFinal->i[counter] = currRowInd;
             extractedMatFinal->p[counter] = j;
             extractedMatFinal->x[counter] = csutil_getval(currRowInd, currColInd, extractedMat);
             extractedMatFinal->nz = counter+1;
             counter++;
         }
    }

    extractedMatFinal->n = m; // a number of rowIndices.size() columns have been extracted from the transposed matrix


    // transpose back
    extractedMatFinal = cs_compress(extractedMatFinal); // before transposing, we need to transform the triplet to csc format
    extractedMatFinal = cs_transpose(extractedMatFinal, values);


    // reallocate output matrix exactly to required space
    cs_sprealloc(extractedMatFinal, extractedMatFinal->nz);

    return extractedMatFinal;
}



// convert matrix from csc to coordinate (i.e. triplet) format
// todo: handle input matrices without values (i.e.  structure only)

/**
* @brief convert matrix from CSC to COO format
*
* this is the reverse operation of the function cs_compress() included in 
* the CSparse package
* 
* @param *cscmat csparse matrix in CSC format
* @return csparse matrix in COO format
*/
cs *csutil_csc2coo(const cs *cscmat)
{
    if (!CS_CSC(cscmat)) return (NULL); 

    cs *coomat;

    const int m = cscmat->m;
    const int n = cscmat->n;
    const int nnz = csutil_getNNZ(cscmat);

    const int values = 1; // what if input has no values 
    const int triplet = 1; 
    coomat = cs_spalloc(m, n, nnz, values, triplet);

    // convert
    int counter = 0;
    int startInd, endInd;
    for (int j=0; j<n; j++) {
        startInd = cscmat->p[j];
        endInd = cscmat->p[j+1];
        for (int i=startInd; i<endInd; i++) {
            coomat->i[counter] = cscmat->i[i];
            coomat->p[counter] = j;
            coomat->x[counter] = cscmat->x[counter];
            coomat->nz = counter+1;
            counter++;
        }
    }
    assert(coomat->nz == nnz);
    return coomat;
}



// writes a cs matrix (either in triplet or csc format) to a file; output format is in triplet format with 
// an additional header in format: 
// m n nnz
// such that it can be read into Matlab by loadcoomat.m

/**
* @brief writes csparse matrix to file
*
* writes csparse matrix to file. Outpt format is
* 'nrows' 'ncols' 'nnz'
* 'rowind_i' 'colind_i' 'val_i'
* 
* Remark: this format can be read by Matlab function loadcoomat.m
* 
* @param *mat pointer to csparse matrix
* @param filename name of output file
* @param offset added to row and column indices to switch between zero and one base indexing
* @return 1 on success, 0 on failure
*/
int csutil_writemat(/*const*/ cs *mat, const char *filename, const int offset)
{
    FILE *fp;

    cs *coomat; // in case input has csc format, we need transition to triplet format
    
    const int m = mat->m;
    const int n = mat->n;
    const int nnz = csutil_getNNZ(mat);


    // if input matrix is in CSC format, then convert to triplet format
    if (CS_CSC(mat)) {
        coomat = csutil_csc2coo(mat);
    }
    else {
        coomat = mat; // so we can further below access the matrix as 'coomat'
    }
    
    
    /* if file cannot be opened, return '0'*/
    if ((fp = fopen(filename,"w")) == NULL ) return (0);

    fprintf(fp, "%d %d %d\n", m, n, nnz);
    for (int i=0; i<nnz; i++) {
        //if (val != NULL) fprintf(fp, "%d %d %23.13f\n", row[i] + offset, col[i] + offset, val[i]);
        //else             fprintf(fp, "%d %d 0.0\n", row[i] + offset, col[i] + offset);
        fprintf(fp, "%d %d %23.13f\n", coomat->i[i] + offset, coomat->p[i] + offset, coomat->x[i]);
    }

    fclose(fp);
    

    return 1;
}


/** @brief extract one column of a csparse matrix (in CSC format) into a std::vector<double> */
std::vector<double> csutil_extractcolumn_csc(const cs *mat, const unsigned colInd)
{
    assert(colInd >= 0);  // required, if index is of type unsigned?
    assert(colInd < unsigned(mat->n));
    assert(CS_CSC(mat));
    
    unsigned currRowInd;

    const unsigned startInd = mat->p[colInd];
    const unsigned endInd = mat->p[colInd+1];
    

    std::vector<double> colVals(mat->m, 0.0);
    
    for (unsigned i=startInd; i<endInd; i++) {
        currRowInd = mat->i[i];
        colVals[currRowInd] = csutil_getval(currRowInd, colInd, mat);
    }

    return colVals;
}

/** 
 * @brief keeps the matrix slices specified by row and column indices
 *
 *  function is similar to csutil_extractmatslices_csc(), but here the size of the matrix is preserved, 
 *  i.e. entries in all rows and columns not in rowIndices and colIndices are set to 0.0
 *
 * @param rowIndices indices of rows to be kept
 * @param colIndices indices of columns to be kept
 * @param *mat pointer to input csparse matrix
 * @return kept csparse matrix
 */
cs *csutil_keepmatslices_csc(const std::vector<int> rowIndices, const std::vector<int> colIndices, const cs *mat)
{
    assert(CS_CSC(mat)); // check if csc matrix
    
    cs *keptMat;

    int nRowIndices = rowIndices.size();
    int nColIndices = colIndices.size();
    
    // check that entries in index vectors don't exceed matrix size
    for (int i=0; i<nRowIndices; i++) {
        assert(rowIndices[i] >= 0);
        assert(rowIndices[i] < mat->m);
    }
    
    for (int i=0; i<nColIndices; i++) {
        assert(colIndices[i] >= 0);
        assert(colIndices[i] < mat->n);
    }
    
    int nnzIn = csutil_getNNZ(mat);
    int nnzOut = 0;

    // allocate triplet matrix with as much memory as input matrix (will be reallocated later)
    // this implies that we do not construct matrices larger than the input matrix, which might
    // be achieved by specifying row or column indices more than once (which is not checked)
    const int values = 1, triplet = 1;
    keptMat = cs_spalloc(mat->m, mat->n, nnzIn, values, triplet);
    
   
   int currRowInd, currColInd;
   
    for (int i=0; i<nRowIndices; i++) {
        currRowInd = rowIndices[i];
        for (int j=0; j<nColIndices; j++) {
            currColInd = colIndices[j];
            if (csutil_has_entry_at(currRowInd, currColInd, mat)) {
                keptMat->i[nnzOut] = currRowInd;
                keptMat->p[nnzOut] = currColInd;
                keptMat->x[nnzOut] = csutil_getval(currRowInd, currColInd, mat);
                nnzOut++;
            }
        }
    }
    keptMat->nz=nnzOut;
    int ok = cs_sprealloc (keptMat, nnzOut);
    assert(ok);
    
    // convert output matrix from triplet to csc format
    cs *outMat;
    outMat = cs_compress(keptMat);       // outMat = compressed-column form of keptMat
    cs_spfree(keptMat);                  // clear keptMat 
  
    return outMat;
}

/**
* @brief makes deep copy of CSparse matrix
* 
* @param *cscmat CSparse matrix
* @return deep copy of input csparse matrix
*/
cs *csutil_copy(const cs *mat)
{
  cs *matOut;

  if (mat == NULL) {
    return NULL;
  }
  

  assert(mat->m >= 0);
  assert(mat->n >= 0);
  assert(mat->nzmax >= 0);
  assert(mat->nz >= -1);


  int m, n, nzmax, values, nnz;
  m = mat->m;
  n = mat->n;
  nzmax = mat->nzmax;
  
  if (mat->x == NULL) {
    values = 0;
  }
  else {
    values = 1;
  }

  
  matOut = cs_spalloc(m, n, nzmax, values, CS_TRIPLET(mat));
  matOut->nzmax = mat->nzmax;
  matOut->m = mat->m;
  matOut->n = mat->n;
  matOut->nz = mat->nz;
  
  nnz = csutil_getNNZ(mat);
  for (int i= 0; i<nnz; i++) {
      matOut->i[i] = mat->i[i];
      matOut->x[i] = mat->x[i];
  }

  int sizep; 


  if (CS_TRIPLET(mat))  sizep = nnz;
  else if (CS_CSC(mat)) sizep = n+1;
  else                  assert(1==0);
  

  for (int i=0; i<sizep; i++) {
    matOut->p[i] = mat->p[i];
  }

  return matOut;
}



