/** 
* @file cs_utils_avt.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Header file for cs_utils_avt.cpp                                     \n
* =====================================================================\n
* @author Klaus Stockmann
* @date 21.10.2011
*/

#include "cs.h"


#ifndef CS_UTILS_AVT_HPP
#define CS_UTILS_AVT_HPP

cs *csutil_convert2cs(const std::vector<int> &iP,
                      const std::vector<int> &jP,
                      const std::vector<double> &valueP,
                      const int mP,
                      const int nP);

int csutil_has_entry_at(const int rowind, const int colind, const cs *mat);                      
double csutil_getval(const int rowind, const int colind, const cs *mat);
void csutil_setval(const int rowind, const int colind, const cs *mat, const double value);
int csutil_getNNZ(const cs *mat);
cs *csutil_extractmatslices_csc(const std::vector<int> rowIndices, const std::vector<int> colIndices, const cs *mat);
cs *csutil_keepmatslices_csc(const std::vector<int> rowIndices, const std::vector<int> colIndices, const cs *mat);
cs *csutil_csc2coo(const cs *cscmat);
int csutil_writemat(/*const*/ cs *mat, const char *filename, const int offset); 
std::vector<double> csutil_extractcolumn_csc(const cs *mat, const unsigned colInd);
cs *csutil_copy(const cs *csmat);

#endif                     
