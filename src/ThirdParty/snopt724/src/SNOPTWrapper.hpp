/**
* @file SNOPTWrapper.hpp
*
* ========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                       \n
* ========================================================================\n
* SNOPTWrapper - Part of DyOS                                             \n
* ========================================================================\n
* This file contains the class definitions of the SNOPTWrapper class      \n
* ========================================================================\n
* @author Klaus Stockmann, adapted by Ralf Hannemann-Tamas
* @date 15.06.2013
*/
#pragma once

#include<vector>
#include<map>


#include "SNOPTLoader.hpp"
#include "JadeNlp.hpp"


/**
* @class SNOPTWrapper
*
* @brief Class defining a GenericOptimizer
*
* Derived class of the class GenericOptimizer. With this class, the user is able to call the NLP
* optimizer SNOPT (Gill, Murray and Saunders)
*/
class SNOPTWrapper
{
protected:

  std::vector<char> m_charWorkspace;
  std::vector<int> m_intWorkspace;
  std::vector<double> m_realWorkspace;

  // Fortran file handles: <=0 : no output
  //                         6 : stdout
  //                       all other values: filename specified by user
  // handle of options file
  int m_specFileHandleSNOPT;

  // handles of output files
  int m_printFileHandleSNOPT;
  int m_summaryFileHandleSNOPT;

  SNOPTLoader snopt;

  static void objfun(int *mode, int *nnobj, double *x, double *fobj,
                     double *gobj, int *nstate, char *cu, int *lencu,
                     void *user, int *leniu, double *ru, int *lenru, int flencu);

  static void confun(int *mode, int *nncon, int *nnjac, int *nejac, double *x,
                     double *fcon, double *gcon, int *nstate,
                     char *cu, int *lencu, void *user, int *leniu,
                     double *ru, int *lenru,  int flencu);


  // utility functions
  void assembleJacobian(std::vector<double> &jacValsDummy,
                        std::vector<int> &jacRowIndices,
                        std::vector<int> &jacColPointer, JadeNlp* myNlp) const;

  void assembleBounds(std::vector<double> &lowerBounds,
                      std::vector<double> &upperBounds, JadeNlp* myNlp) const;

  void allocateSNOPTWorkspace(std::vector<char> &cw, std::vector<int> &iw, std::vector<double> &rw,
                              const int numVars, const int numConstraints) const;
  //void checkOptimizerOptions(const char* filename,
  //                           const std::map<std::string, std::string> &optimizerOptions);
  void writeOptimizerOptions(const char* filename,
                             const std::map<std::string, std::string> &optimizerOptions);

  void matchOptimizerOptions(std::map<std::string, std::string> &matchedOptimizerOptions,
                             const std::map<std::string, std::string> &optimizerOptions);
  void getOptimizerOptions(std::map<std::string, std::string> &matchedOptimizerOptions,
                                         const char* filename);
  enum NumberType{INTEGER, DBL, NO_NUMBER};
  pair<NumberType, double> number(const std::string& s);

public:
  SNOPTWrapper(const std::map<std::string, std::string>& optimizerOptions = std::map<std::string,std::string>());
  ~SNOPTWrapper();
  void solve(JadeNlp* myNlp);
  void setOutputFilename(std::string filename) { snopt.snOpenFile(m_printFileHandleSNOPT, filename); }

};
