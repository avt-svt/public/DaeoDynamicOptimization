#ifndef JADE_NLP_HPP__
#define JADE_NLP_HPP__

class JadeNlp {
public:
  virtual void EvalDimensions(int& numVariables, int& numConstraints)=0;
  virtual void EvalVariableBounds(double *lowerBounds, double* upperBounds, int numVariables)=0;
  virtual void EvalConstraintBounds(double *lowerBounds, double* upperBounds, int numConstraints)=0;
  virtual void EvalInitialGuess(double* x, int numVariables)=0;
  virtual void EvalObjectiveAndConstraints(int derivativeOrder, double* x, double& f, double* grad_f, 
    double* g, double** jac_g, int numVariables, int numConstraints)=0;
  virtual void Simulate(double *x, int numVariables) {}
  virtual void FinalizeSolution(int n, const double * x,
                                  const double* z_L, const double* z_U,
                                  int m, const double* g, const double* lambda,
                                  double obj_value){}
};

#endif
