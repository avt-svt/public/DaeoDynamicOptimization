/**
* @file SNOPTWrapper.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* SNOPTWrapper - Part of DyOS                                          \n
* =====================================================================\n
* This file contains the definitions of the member functions of        \n
* SNOPTWrapper according to the UML Diagramm METAESO_8                 \n
* =====================================================================\n
* @author Klaus Stockmann, modified by Ralf Hannemann-Tamas
* @date 12.12.2011
*/


#include "SNOPTWrapper.hpp"
#include "SNOPTLoader.hpp"


#include "Utils.hpp"
#include <algorithm>

#include <string>
#include <cassert>
#include <cmath>
#include <sstream>
#include <fstream>


// initialization of statics. Instead of doing it in every test suite we do it here for all together.

/**
* @brief constructor: initialize resources which are independent of current problem (e.g. open output files)
*
* @param optMD pointer to an OptimizerMetaData object used for the optimization
* @param genInt pointer to an integrator object used for the optimization
*/
SNOPTWrapper::SNOPTWrapper( const std::map<std::string, std::string> &optimizerOptions)
{

  const char* optimizerOptionsFile = "snopt.config";
  std::map<std::string, std::string> matchedOptimizerOptions;
//  if there is not already a snoptoptionsfile a new one is created
  if (!fileExists(optimizerOptionsFile)){
    //set standard options, key needs to be written in lower case
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("solution", "Yes"));
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("system information", "Yes"));
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("iterations limit", "10000"));
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("major optimality tolerance", "1.E-6"));
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("major feasibility tolerance", "1.E-6"));
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("function precision", "1.E-8"));
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("verify level", "-1"));
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("major iterations limit", "50"));
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("minor iterations limit", "50"));
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("jacobian", "sparse"));
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("derivative level", "3"));
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("linesearch tolerance", "0.5"));
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("major step limit", "2.0"));
    matchedOptimizerOptions.insert(make_pair<std::string, std::string>("minimize", ""));
	matchedOptimizerOptions.insert(make_pair<std::string, std::string>("minor feasibility tolerance", "1.e-6"));

    //compare optimizerOptions with standard options
    matchOptimizerOptions(matchedOptimizerOptions, optimizerOptions);
    //write matchedOptions into optimizerOptionsFile
    optimizerOptionsFile = "snopt2.config";
    writeOptimizerOptions(optimizerOptionsFile, matchedOptimizerOptions);
  }
  else{
    //if options were added in UserInput they need to be matched with existing options in file
    //if the map is empty the file snopt.config is directly used as specFileSNOPT
    if(optimizerOptions.empty()==false){
      //write options from file into map
      getOptimizerOptions(matchedOptimizerOptions, optimizerOptionsFile);
      //compare optimizerOptions with options from snopt.config file
      matchOptimizerOptions(matchedOptimizerOptions, optimizerOptions);
      //write matchedOptions into optimizerOptionsFile
      optimizerOptionsFile = "snopt2.config";
      writeOptimizerOptions(optimizerOptionsFile, matchedOptimizerOptions);
    }
  }

  std::string specFileSNOPT = optimizerOptionsFile;//previously: "input/snopt.config";
  m_specFileHandleSNOPT  = 4;      /* File handler for Specs file, ispecs */

  // specify output files
  const int stdoutFileHandle = 6;


  std::string printFileSNOPT = "snopt";
  printFileSNOPT += ".out";

  m_printFileHandleSNOPT = 9;      /* File handler for Output file, iprint  */

  std::string summaryFileSNOPT = ""; // -> will translate to screen output, what about no output -> input option to constructor
  m_summaryFileHandleSNOPT = stdoutFileHandle;

  // spec file
  // @todo check if file exists (using utility function isfile)
 if (m_specFileHandleSNOPT != stdoutFileHandle && m_specFileHandleSNOPT > 0) {
   if ( !fileExists(specFileSNOPT.c_str()) ) {
     std::cout << "snopt.config does not exist" << std::endl;
     exit(1);
   }
 }

  snopt.snOpenFile(m_specFileHandleSNOPT, specFileSNOPT);

  // print file
  if (m_printFileHandleSNOPT != stdoutFileHandle) { // what about no output (-1 or 0?)
    snopt.snOpenFile(m_printFileHandleSNOPT, printFileSNOPT);
  }

  // summary file
  if (m_summaryFileHandleSNOPT != stdoutFileHandle) { // what about no outpt (-1 or 0?)
    snopt.snOpenFile(m_summaryFileHandleSNOPT, summaryFileSNOPT);
  }




  // if (optInform > 0) { //
  //   assert(1==0); // @todo throw an exception instead
  // }


}

/**
* @brief destructor: free resources allocated in constructor (e.g. close output file)
*/
SNOPTWrapper::~SNOPTWrapper()
{}

/**
* @brief solve optimization problem
*
* @todo bis jetzt wird als exit status (in m_optOut.m_informFlag) immer "OK" zur�ckgegeben.
*       Dies sollte auf den richtigen Exitstatus (in inform) gemapped werden.
*/
void SNOPTWrapper::solve(JadeNlp* myNlp)
{
  // set static data object to current attributes - needed by callback functions



  int numVariables=0;
  int numConstraints=0;
  myNlp->EvalDimensions(numVariables,numConstraints);
  const int numNonLinConstraints = numConstraints;

  allocateSNOPTWorkspace(m_charWorkspace, m_intWorkspace, m_realWorkspace,
                         numVariables, numConstraints);

  // call sninit routine
  snopt.snInit(m_printFileHandleSNOPT, m_summaryFileHandleSNOPT,
               m_charWorkspace, m_intWorkspace, m_realWorkspace);

  int optInform = -1; // number of errors while reading options file

  snopt.snSpec(optInform, m_specFileHandleSNOPT,
               m_charWorkspace, m_intWorkspace, m_realWorkspace);

  snopt.snCloseFile(m_specFileHandleSNOPT);

  void* user=reinterpret_cast<void*>(myNlp);

  char probName[] = "--DyOS--"; // not really required(may be blank), only for output purposes

  // assemble Jacobian, in general case non-linear and linear; in new DyOS only non-linear
  std::vector<double> jacValsDummy;
  std::vector<int> jacRowIndices, jacColPointer;

  assembleJacobian(jacValsDummy, jacRowIndices, jacColPointer,myNlp);

  const int nnzA = jacValsDummy.size(); // =^ne


  // lower and upper bounds of variables AND slacks (x,s)
  std::vector<double> upperBoundsOnVarsAndSlacks(numVariables+numConstraints); // =^ bu
  std::vector<double> lowerBoundsOnVarsAndSlacks(numVariables+numConstraints); // =^ bl
  assembleBounds(lowerBoundsOnVarsAndSlacks, upperBoundsOnVarsAndSlacks,myNlp);





  std::vector<double>  lagrangeMultipliers(numConstraints,0); // =^pi


  int inform; // only output

  int numInfeas;  // =^ nInf, output
  double sumInfeas; // =^ sInf, output
  double objFunValAtSolution; // =^ Obj, output

  //initialize hs with 0 (cold start without basis file)
  const unsigned nm = numVariables + numConstraints;

  std::vector<double> optVarAndSlackValues(nm, 1);
  std::vector<double> optVarValues(numVariables, 1);
  std::vector<int> hs(nm, 0);
  myNlp->EvalInitialGuess(&optVarAndSlackValues[0],numVariables);


  std::vector<double> rc(nm, 0.0);

  assert(!jacValsDummy.empty());
  assert(!jacRowIndices.empty());
  assert(!jacColPointer.empty());
  assert(!lowerBoundsOnVarsAndSlacks.empty());
  assert(!upperBoundsOnVarsAndSlacks.empty());
  // these variables have to be either 1) hard-coded or
  //                                   2) retrieved from OptimizerSingleStageMetaData
  snopt.snopt(numConstraints,
              numVariables,
              nnzA,
              numNonLinConstraints,
              probName,
              &SNOPTWrapper::confun, // not required, function names do not change
              &SNOPTWrapper::objfun, // not required, function names do not change
              &jacValsDummy[0], &jacRowIndices[0], &jacColPointer[0],
              &lowerBoundsOnVarsAndSlacks[0], &upperBoundsOnVarsAndSlacks[0],
              &hs[0],
              &optVarAndSlackValues[0],
              &lagrangeMultipliers[0],
              &rc[0],
              inform,
              numInfeas,
              sumInfeas,
              objFunValAtSolution,
              m_charWorkspace,
              m_intWorkspace,
              m_realWorkspace,user);

  std::cout << "optimal solution" << std::endl;
 // for (int i=0; i<numVariables; i++) {
 //   std::cout << "x[" << i+1 << "] = " << optVarAndSlackValues[i] << std::endl;
 //   optVarAndSlackValues[i]=0.7;
 // }
  std::vector<double> z_L(numVariables, 0.0);
  std::vector<double> z_U(numVariables, 0.0);

  for (int i = 0; i < numVariables; i++) {
	  if (rc[i] >= 0) {
		  z_L[i] = rc[i];
		  z_U[i] = 0;
	  }
	  else {
		  z_U[i] = -rc[i];
		  z_L[i] = 0;
	  }
  }

  std::vector<double> lambda(numConstraints, 0.0);
  for (int i = 0; i < numConstraints; i++)
	  lambda[i] = -lagrangeMultipliers[i];

  myNlp->Simulate(&optVarAndSlackValues[0],numVariables);
  myNlp->FinalizeSolution(numVariables, &optVarAndSlackValues[0],
	  &z_L[0], &z_U[0], numConstraints,
	  &optVarAndSlackValues[numVariables],
	  &lambda[0], objFunValAtSolution);
}

/**
  * @brief static callback function used by the SNOPT library to set  the value
  * and derivatives of the objective function
  *
  * @param[in,out] mode  on input: indicates whether fobj, gobj or both must be
  *                                assigned during the present call of funobj (0<=mode<=2).\n
  *                                -if mode=2, assign fobj and known components of gobj\n
  *                                -if mode=1, assign the known components of gobj; fobj is ignored\n
  *                                -if mode=0, only fobj needs to be assigned; gobj is ignored\n
  *                      on exit:  if mode = -1: indicates, that fobj has not been computed (for any reason)
  *                                if mode < -1: tell to SNOPT to terminate optimization
  *
  * @param[in] nnobj number of variables involved in the objective function
  *                  (the first nnobj variables in vector x)
  * @param[in] x contains the nonlinear objective variables x
  * @param[out] fobj must contain computed value of the objective function, except for mode=1
  * @param[out] gobj must contain the known components of the gradient vector of the objective
  *                  function , except for mode=0
  * @param[in] nstate indicates the first and last calls to objfun\n
  *                   -if nstate=0, nothing special about the current call\n
  *                   -if nstate=1, SNOPT is calling the subroutine for the first time.\n
  *                   -if nstate>=2 SNOPT is calling the subroutine for the last time.\n
  *                    Note that confun will always be called before objfun, if there are any
  *                    nonlinear constraints.\n
  *                    At the last call nstate = 2+inform.
  * @param[in,out] cu character array of user data
  * @param[in] flencu length of the field cu (needed for certain fortran builds)
  * @param[in] lencu length of the field cu
  * @param[in,out] iu integer array of user data
  * @param[in] lencu length of the field iu
  * @param[in,out] ru double field of user data
  * @param[in] lenru length of field ru
  * @todo try to remove the additional parameter flencu which is not needed by all compilations
  * of SNOPT. Does not work with snopt if compiled with the intel compiler. This could be achieved
  * by declaring and defining of the callback functions in the SNOPTLoader module. In that case
  * this function could even use vectors as parameters.
  */
void SNOPTWrapper::objfun(int *mode, // input, output
                          int *nnobj,
                          double *x,
                          double *fobj, // output
                          double *gobj, // output
                          int *nstate,
                          char *cu, int *lencu, // character workspace; not used by DyOS -> into wrapper
                          void *user, int *leniu,              // integer workspace; not used by DyOS   -> into wrapper
                          double *ru, int *lenru,           // real workspace; not used by DyOS      -> into wrapper
                          int flencu)
{
  assert(*mode == 0 || *mode == 1 || *mode == 2);
  assert(*nstate >= 0);
  assert(*nnobj > 0);
  //std::cout << "Calling objfun with mode " << *mode << std::endl;
  int derivativeOrder=0;
  if ( (*mode == 2) || (*mode == 1) ){
    derivativeOrder=3;
  }



  JadeNlp *myNlp=reinterpret_cast<JadeNlp*>(user);
  int numVariables=0;
  int numConstraints=0;
  myNlp->EvalDimensions(numVariables,numConstraints);

  double f;
  std::vector<double> grad_f(numVariables);
  std::vector<double> g(numConstraints);
  vector<double*> jac_g(numConstraints);
  std::vector<double> storeJac(numVariables*numConstraints);
  for(int i=0; i<numConstraints; i++)
    jac_g[i]=&storeJac[numVariables*i];

  myNlp->EvalObjectiveAndConstraints(derivativeOrder,x,f,&grad_f[0],&g[0],&jac_g[0],numVariables,numConstraints);
  if( *mode==0 || *mode==2){
    *fobj=f;
  }

  // store the gradient at current point x into gObj
  if( (*mode==1) || (*mode==2) ){
    for(int i=0; i<numVariables; i++) {
      gobj[i]=grad_f[i];
    }
  }



}

/**
  *@brief static callback function used by the SNOPT library to set value vector and Jacobian
  *of the nonlinear constraints.
  *
  * @param[in] mode indicates whether fobj, gobj or both must be assigned during the present
  *            call of funobj (0<=mode<=2).\n
  *            -if mode=2, assign fcon and known components of gcon\n
  *            -if mode=1, assign the known components of gcon, fcon is ignored\n
  *            -if mode=0, only fcon needs to be assigned, gcon is ignored\n
  *            May be used to indicate that in this method the objective function
  *            is not set for any reason by setting mode to -1. If the solution of the
  *            current problem is to be terminated, set mode to any other negative value.
  * @param[in] nncon number if nonlinear constraints. These must be the first nnCon constraints
  *                  in the problem.
  * @param[in] nnjac number of nonlinear variables in the constraint matrix. These must be the
  *                  first nnJac variables in the problem
  * @param[in] nejac number of Jacobian entries in the nonlinear Jacobian matrix (nncon*nnjac)
  * @param[in] x contains the current values of the nonlinear variables in the constraint matrix.
  * @param[out] fcon contains the computed constraint vector , except for mode=1
  * @param[out] gcon contains computed Jacobian, except  for mode=0. These gradient elements
  *                  must be stored in gCon in exactly the same positions as implied by the
  *                 definitions of SNOPT arrays a, ha, ka. There is no internal check for consistency.
  * @param[in] nstate indicates the first and last calls to objfun\n
  *                   -if nstate=0, nothing special about the current call\n
  *                   -if nstate=1, SNOPT is calling the subroutine for the first time.\n
  *                   -if nstate>=2 SNOPT is calling the subroutine for the last time.\n
  *                    Note that confun will always be called before objfun, if there are any
  *                    nonlinear constraints.\n
  *                    At the last call nstate has generally the value of 2+inform.
  * @param[in,out] cu character array of user data
  * @param[in] flencu length of the field cu (needed for certain fortran builds
  * @param[in] lencu length of the field cu
  * @param[in,out] iu integer array of user data
  * @param[in] lencu length of the field iu
  * @param[in,out] ru double field of user data
  * @param[in] lenru length of field ru
  * @todo try to remove the additional parameter flencu which is needed not by all compilations
  * of SNOPT. Does not work with snopt if compiled with the intel compiler
  */
void SNOPTWrapper::confun(int *mode,
  int *nncon,
  int *nnjac,
  int *nejac,
  double *x,
  double *fcon,
  double *gcon,
  int *nstate,
  char *cu, int *lencu, // character workspace; not used by DyOS -> into wrapper
  void *user, int *leniu,              // integer workspace; not used by DyOS   -> into wrapper
  double *ru, int *lenru,            // real workspace; not used by DyOS      -> into wrapper
  int flencu)
{
  assert(*mode == 0 || *mode == 1 || *mode == 2);
  assert(*nstate >= 0);
  assert(*nnjac >= 0);
  //std::cout << "Calling confun with mode " << *mode << std::endl;
  int derivativeOrder=0;
  if ( (*mode == 2) || (*mode == 1) ){
    derivativeOrder=4;
  }

  JadeNlp *myNlp=reinterpret_cast<JadeNlp*>(user);
  int numVariables=0;
  int numConstraints=0;
  myNlp->EvalDimensions(numVariables,numConstraints);

  double f;
  std::vector<double> grad_f(numVariables);
  std::vector<double> g(numConstraints);
  vector<double*> jac_g(numConstraints);
  std::vector<double> storeJac(numVariables*numConstraints);
  for(int i=0; i<numConstraints; i++)
    jac_g[i]=&storeJac[numVariables*i];

  myNlp->EvalObjectiveAndConstraints(derivativeOrder,x,f,&grad_f[0],&g[0],&jac_g[0],numVariables,numConstraints);
  if( *mode==0 || *mode==2){
    for(int i=0; i<numConstraints; i++){
      fcon[i]=g[i];
    }
  }

  // store the gradient at current point x into gObj
  if( (*mode==1) || (*mode==2) ){
    int index=0;
    for(int j=0; j<numVariables; j++)  {
      for(int i=0; i<numConstraints; i++) {
        gcon[index]=jac_g[i][j];
        ++index;
      }
    }
  }
}



// utility functions

/*
* @brief assemble Jacobian matrix for SNOPT in CSC format
*
* assemble complete Jacobian matrix from linear and nonlinear parts; value
* vector is only a dummy but allocated to the correct size
*
* @param[out] jacValsDummy dummy ExtendableArray for Jacobian values
* @param[out] jacRowIndices ExtendableArray containing row indices of Jacobian
* @param[out] jacColPointer ExtendableArray containing column pointers
* @ remarks if optProbDim.nonLinJac was in csc format we could directly pass the
*   rowind, colptr and val vector to SNOPT
*/
void SNOPTWrapper::assembleJacobian(std::vector<double> &jacValsDummy,
                                    std::vector<int> &jacRowIndices,
                                    std::vector<int> &jacColPointer, JadeNlp* myNlp) const
{

  int numVariables;
  int numConstraints;
  myNlp->EvalDimensions(numVariables, numConstraints);
  int nnz = numVariables*numConstraints;
  jacValsDummy.resize(nnz, 0.0); // dummy array
  jacRowIndices.resize(nnz, 0);
  jacColPointer.resize(numVariables+1, 0);// two
  int index=0;
  for(int j=0; j<numVariables; j++) {
    for(int i=0; i<numConstraints; i++) {
      jacRowIndices[index]=i+1;
      jacColPointer[j]=numConstraints*j+1;
      ++index;
    }
  }
  jacColPointer[numVariables]=nnz+1;
}

/*
 *@brief assemble upper and lower bounds of variables and slacks
 *
 *@param[out] lowerBounds Array containing lower bounds for optimization variables and slack variables
 *@param[out] upperBounds Array containing upper bounds for optimization variables and slack variables
 */
void SNOPTWrapper::assembleBounds(std::vector<double> &lowerBoundsOnVarsAndSlacks,
                                  std::vector<double> &upperBoundsOnVarsAndSlacks, JadeNlp* myNlp) const
{
  int numVariables;
  int numConstraints;
  myNlp->EvalDimensions(numVariables, numConstraints);
  lowerBoundsOnVarsAndSlacks.resize(numVariables+ numConstraints);
  upperBoundsOnVarsAndSlacks.resize(numVariables+ numConstraints);
  myNlp->EvalVariableBounds(&lowerBoundsOnVarsAndSlacks[0],&upperBoundsOnVarsAndSlacks[0],numVariables);
  myNlp->EvalConstraintBounds(&lowerBoundsOnVarsAndSlacks[numVariables],&upperBoundsOnVarsAndSlacks[numVariables],numConstraints);
}

/*
 *@brief allocate workspace required by SNOPT
 *
 *@param[out] cw character workspace required by SNOPT
 *@param[out] iw integer workspace required by SNOPT
 *@param[out] rw real workspace required by SNOPT
 *@param[in] numVars number of optimization variables
 *@param[in] numConstraints number of constraints
 */
void SNOPTWrapper::allocateSNOPTWorkspace(      std::vector<char> &cw,
                                                std::vector<int> &iw,
                                                std::vector<double> &rw,
                                          const int numVars,
                                          const int numConstraints) const
{
  assert(numVars>0);

   /* numbers according to SNOPT manual */
  const int lencw = 50000; /* 550; */
  int leniw = 11000*(numConstraints + numVars);
  const int lenrw = max(22000*(numConstraints + numVars), 500);
  if (leniw <= 550) leniw = 550; /* Assure minimum allocation */

  //information vectors for SNOPT
  const int snoptStringLength = 8;
  cw.resize(lencw * snoptStringLength);
  rw.resize(lenrw, 0.0);
  iw.resize(leniw, 0);
}


/**
* @brief check if a string is an integer, a double or no number at all
*
* @param string to be checked
* @return enum for integer, double or no number, double d
*/
pair<SNOPTWrapper::NumberType, double> SNOPTWrapper::number(const std::string& s)
{
  double d;
  const char *str=s.c_str();//convert string to char* for function strtod
  char* end = 0 ;

  d = strtod( str, &end );
  if(*end ==0 ){
    if(fmod(d,1)==0){//integer
      return make_pair(INTEGER, d); //<NumberType, double>
    }
    return make_pair(DBL, d);//double //<NumberType, double>
  }
  else
    return make_pair<NumberType, double>(NO_NUMBER, 0);//string didn't represent a number
}
/**
* @brief matchinh two std::maps that contain optimizer options
*
* @param two maps
*/
void SNOPTWrapper::matchOptimizerOptions(std::map<std::string, std::string> &matchedOptimizerOptions,
                                         const std::map<std::string, std::string> &optimizerOptions)
{
  std::map<std::string,std::string>::const_iterator iter;
  std::string tempString;
  for(iter=optimizerOptions.begin(); iter!=optimizerOptions.end(); iter++){
    tempString=(*iter).first;
    trim(tempString);
    stringToLower(tempString);
    matchedOptimizerOptions[tempString]=(*iter).second;//change existing options or create new
  }
  if(matchedOptimizerOptions.find("maximize")!=matchedOptimizerOptions.end())
    matchedOptimizerOptions.erase("minimize");//if user chose option maximize, minimize is deleted from map
}
//this is a temporary function replacing checkOptimizerOption till checkOptimizerOptions includes all possible Options
void SNOPTWrapper::writeOptimizerOptions(const char* filename,
                                         const std::map<std::string, std::string> &optimizerOptions)
{
  //note: in this function not all possible options are checked but only the most common ones
  ofstream myfile;
  myfile.open(filename, ios_base::trunc);
  myfile << "Begin DyOS options\n";
  myfile << "* This file includes the optimizer options set in snopt.config\n";
  myfile << "* and options that might have been added in UserInput.\n";
  myfile << "* If there was no file snopt.config, standard options were added.\n";

  std::map<std::string,std::string>::const_iterator iter;
  for(iter=optimizerOptions.begin(); iter!=optimizerOptions.end(); iter++){
    myfile << "  "+(*iter).first+"\t"+(*iter).second+"\n";
  }
  myfile << "End options\n";
  myfile.close();
}
/**
* @brief writing options from given file snopt.config into map
*
* @param filename, map
*/
void SNOPTWrapper::getOptimizerOptions(std::map<std::string, std::string> &matchedOptimizerOptions,
                                       const char* filename)
{
  std::string line;
  std::vector<std::string> vec;
  std::vector<std::string> tempvec;
  std::string optionstring;
  ifstream infile(filename);
  while(!infile.eof()){//read whole file into vector of strings
    getline(infile, line);
    trim(line);  infile.open(filename);
    stringToLower(line);
    vec.push_back(line);
  }
  vec.erase(vec.end()-1);//last line is an empty line
  if(vec.size()<2)
    return;
  for(unsigned i=1; i<(vec.size()-1); i++){//ignore first and last line
      tempvec.clear();
      optionstring.clear();
      if(vec[i].find("*")==std::string::npos){//ignore comments
        std::istringstream iss(vec[i]);
        stringToLower(vec[i]);
        while(iss){//split string into words
          string sub;
          iss >> sub;
          tempvec.push_back(sub);
        }
        tempvec.erase(tempvec.end()-1);//last entry is space
        if(tempvec.size()==1)//only one word in line(for example "maximize")
          matchedOptimizerOptions.insert(make_pair(tempvec[0], "")); //<std::string, std::string>
        else{
          for(unsigned j=0; j<tempvec.size()-1; j++){
            optionstring.append(tempvec[j]);
            optionstring.append(" ");
          }
          trim(optionstring);//erase last space
          //last word in tempvec is second entry for map. The other words are the first entry.
          matchedOptimizerOptions.insert(make_pair(optionstring, tempvec[tempvec.size()-1])); //<std::string, std::string>
        }
    }
  }
  infile.close();
}
