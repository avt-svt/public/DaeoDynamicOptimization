#ifndef UTILS_HPP__
#define UTILS_HPP__

#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <fstream>
#include <string>
#include <memory>

inline bool fileExists (const char* name) {
    std::ifstream f(name);
    if (f.good()) {
        f.close();
        return true;
    } else {
        f.close();
        return false;
    }   
}


// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) { return !std::isspace(ch); }));
        return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) { return !std::isspace(ch); }).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}

static inline void stringToLower(std::string& data) {
  std::transform(data.begin(), data.end(), data.begin(), ::tolower);
}


#define SNOPT_WRAPPER_WEAK_PTR std::weak_ptr

#endif

