#set standard compiler flags for C-compilers in here
if(MSVC)
  set(CMAKE_C_FLAGS_DEBUG_INIT "/MD /Zi /Ob0 /Od /D BOOST_ALL_NO_LIB")
  set(CMAKE_C_FLAGS_MINSIZEREL_INIT     "/MD /O1 /Ob1 /D NDEBUG /D BOOST_ALL_NO_LIB")
  set(CMAKE_C_FLAGS_RELEASE_INIT        "/MD /O2 /Ob2 /D NDEBUG /D BOOST_ALL_NO_LIB")
  set(CMAKE_C_FLAGS_RELWITHDEBINFO_INIT "/MD /Zi /O2 /Ob1 /D NDEBUG /D BOOST_ALL_NO_LIB")
  SET(${CMAKE_PROJECT_NAME}_FOR_SUBPROJECTS_C_FLAGS "/W0" CACHE STRING "$C FLAGS for subprojects to turn off utility warnings")
endif(MSVC)

if(UNIX)
  set(CMAKE_C_FLAGS "${CMAKE_CXX_FLAGS} -DBOOST_TEST_DYN_LINK")
  set(CMAKE_C_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wno-long-long -pedantic")
endif(UNIX)