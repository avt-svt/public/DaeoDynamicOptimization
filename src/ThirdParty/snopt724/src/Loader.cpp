/**
* @file Loader.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Utilities                                                            \n
* =====================================================================\n
* define member functions declared in Loader.hpp                       \n
* =====================================================================\n
* @author Tjalf Hoffmann
* @date 18.7.2011
*/


#include "Loader.hpp"
#ifndef WIN32
#include <unistd.h>
#include <memory>
//#include <boost/filesystem.hpp>
//namespace fs = boost::filesystem;
#endif
#include <string>
#include <sstream>


using namespace utils;

/**
* standard-constructor - calls init (sets handle to NULL)
*/
Loader::Loader(){
  init();
}

/**
* constructor loading a given dll
*@param dllName_in name of the dll to be loaded
*/
Loader::Loader(const std::string &dllName_in){
  init();
  loadDLL(dllName_in);
}

/**
* copy-constructor - gets handle to the same dll as l
* @param l Loader to be copied
*/
Loader::Loader(const Loader &l){
  init();
  loadDLL(l.dllName);
}

/**
* destructor - unload the dll held by Loader
*/
Loader::~Loader(){
  unloadDLL();
}

/**
* unload current dll and load new dll
* @param dllName_in name of the dll to be loaded
*/
void Loader::loadDLL(const std::string &dllName_in){
  unloadDLL();
  dllName = dllName_in;
#ifdef WIN32
  handle = LoadLibrary(dllName.c_str());
  if (!handle)
    handle = LoadLibrary((dllName + std::string(".dll")).c_str());
#elif __APPLE__
  handle = dlopen(dllName.c_str(), RTLD_NOW);
  if (!handle)
    handle = dlopen((std::string("lib")+dllName+std::string(".dylib")).c_str(), RTLD_NOW);  
#else
   handle = dlopen(dllName.c_str(), RTLD_NOW);
  if (!handle)
    handle = dlopen((std::string("lib")+dllName+std::string(".so")).c_str(), RTLD_NOW);
  if(!handle) {
      std::unique_ptr<char, decltype(std::free) *>
      current_dir_name{ get_current_dir_name(), std::free };
      std::string fullDllName(current_dir_name.get());
      fullDllName += "/lib";
      fullDllName += dllName;
      fullDllName += ".so";
      handle = dlopen(fullDllName.c_str(), RTLD_NOW);
  }
#endif
  if(handle == 0){
    std::ostringstream error;
#ifdef WIN32
    error<<GetLastError();
#else
    error << dlerror();
#endif
    std::string message = "Unable to load " + dllName_in + ". Errorcode: " + error.str() + ". Please check for unsolved dependecies.";
    throw DllLoaderException(message);
  }
}

/**
* free handle of the current dll
*/
void Loader::unloadDLL(){
  if(handle){
#ifdef WIN32
    FreeLibrary(handle);
#else
    dlclose(handle);
#endif
  }
  init();
}

/**
* set all attributes to their start values
*/
void Loader::init(){
  handle = 0;
}

/**
* @return true, if handle is valid - false otherwise
*/
bool Loader::isLoaded(){
  return (handle != 0);
}

