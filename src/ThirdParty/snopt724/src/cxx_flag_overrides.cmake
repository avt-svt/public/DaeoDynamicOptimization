#set standard compiler flags for C++-compilers in here
if(MSVC)
  set(CMAKE_CXX_FLAGS_DEBUG_INIT "/MD /Zi /Ob0 /Od /D BOOST_ALL_NO_LIB")
  set(CMAKE_CXX_FLAGS_MINSIZEREL_INIT     "/MD /O1 /Ob1 /D NDEBUG /D BOOST_ALL_NO_LIB")
  set(CMAKE_CXX_FLAGS_RELEASE_INIT        "/MD /O2 /Ob2 /D NDEBUG /D BOOST_ALL_NO_LIB")
  set(CMAKE_CXX_FLAGS_RELWITHDEBINFO_INIT "/MD /Zi /O2 /Ob1 /D /W4 NDEBUG /D BOOST_ALL_NO_LIB")
  set(CMAKE_EXE_LINKER_FLAGS "/fixed:no")
  SET(${CMAKE_PROJECT_NAME}_FOR_SUBPROJECTS_CXX_FLAGS "/W0" CACHE STRING
    "$CXX FLAGS for subprojects to turn off utility warnings")
endif(MSVC)

if(UNIX)
  set(BUILD_SHARED_LIBS 1)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DBOOST_TEST_DYN_LINK")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wno-long-long -pedantic")
endif(UNIX)
