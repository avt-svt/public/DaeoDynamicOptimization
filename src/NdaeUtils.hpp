#ifndef NDAE_UTILS_HPP
#define NDAE_UTILS_HPP

#include <idas/idas.h>
#include <idas/idas_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_math.h>
#include <vector>
#include <algorithm>
#include <iostream>

namespace NdaeUtils {

template<typename T> void ArrayCopy(T* dest, const T* src, const unsigned n) {
  for(unsigned i =0; i<n; i++)
    dest[i] = src[i];
}

template<typename T> void ArrayCopy(T* dest, const T src, const unsigned n) {
  for(unsigned i =0; i<n; i++)
    dest[i] = src;
}


template<typename T> const T EuclidianNorm(const T* x, const unsigned n){
  T sum = 0.0;
  for(unsigned i = 0; i < n; i++) {
    sum += x[i] * x[i];
  }
  return sqrt(sum);
}

template<typename T> const T ScaledDeviation(const T *x1, const T *x2, const unsigned n){
  std::vector<T> delta(n);
  for(unsigned i = 0; i < n; i++) {
    delta[i] = x2[i] - x1[i];
  }
  const T norm1 = EuclidianNorm(x1,n);
  const T norm2 = EuclidianNorm(x2,n);
  const T denominator = std::min(norm1,norm2);
  const T numinator = EuclidianNorm(&delta[0],n);
  const T deviation =  numinator / denominator;
  return deviation;
}

int CheckFlag(void *flagvalue, const char *funcname, int opt);

}

template <typename T, unsigned N>
void PrintArray(const T (&array)[N])
{
    for (unsigned i = 0; i < N; ++i) {
        std::cout << array[i] << std::endl;
    }
}


#endif // NDAE_UTILS_HPP

