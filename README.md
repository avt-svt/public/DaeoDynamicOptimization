# DaeoDynamicOptimization

Here is the companion repository of the paper "Direct single shooting for dynamic optimization of differential-algebraic equation systems with optimization criteria embedded".

The repository contains the DaeoToolbox submodule as dependency. Hence, you need to use the following commands (on Windows, you might need to get Git Bash first).

    $ git clone https://git.rwth-aachen.de/avt-svt/public/DaeoDynamicOptimization.git <directory> && cd <directory>
    $ git submodule init
    $ git submodule update
	

The project uses CMake as build system. The top-level CMakeLists.txt can be found in the subfolder src/. Doublecheck CMAKE_INSTALL_PREFIX before running the install target.

To perform optimization runs, the user needs to have the source code of SNOPT Version 7.2.4 
(see https://ccom.ucsd.edu/~optimizers/solvers/snopt/). The source code of SNOPT should be
placed in the subfolder src/ThirdParty/snopt724/src/snopt724. The CMake option HAVE_SNOPT 
can set be set to ON. But even if the user does not have access to SNOPT, they can perform
simulations with the software. 
